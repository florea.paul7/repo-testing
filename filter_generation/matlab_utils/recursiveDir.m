
function dd = recursiveDir(path)
% add a second argument "level" if you want to keep track of how many
% levels deep it goes
% pause
% level = level +1
d = dir(path);
j = 1;
dd = {};
for i=3:1:size(d,1)
    if(isdir(horzcat(path,'\',d(i).name)))
        dd = [dd; recursiveDir(horzcat(path,'\',d(i).name))];
        j = size(dd,1)+1;
    else
        dd{j,1} = horzcat(path,'\',d(i).name);
        j = j+1;
    end
end

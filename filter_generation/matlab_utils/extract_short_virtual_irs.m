clear all;
close all

pkg load signal
warning("off", "all");

%set this value to 1 if is a 7.1 setup (has LR and RR speakers)
is_7p1 = 1;

out_samplerate = [44100 48000 96000 192000];
numtaps = [300 300 600 1200];
numblocks = [1 1 1 2];

max_samplerate_idx = 3;

path = './wav_filters/virtualizer_filter/short/';

strL = sprintf('%s%s', path, 'L_PD_short.wav');
strR = sprintf('%s%s', path, 'R_PD_short.wav');
strC = sprintf('%s%s', path, 'C_PD_short.wav');
strLR = sprintf('%s%s', path, 'LR_PD_short.wav');
strRR = sprintf('%s%s', path, 'RR_PD_short.wav');
if is_7p1
  strLS = sprintf('%s%s', path, 'LS_PD_short.wav');
  strRS = sprintf('%s%s', path, 'RS_PD_short.wav');
end

virt_irs = zeros(max_samplerate_idx, 7, 2, numtaps(max_samplerate_idx));

  [Lir,fs] = audioread( strL );
  Rir = audioread( strR );
  Cir = audioread( strC );
  LRir = audioread( strLR );
  RRir = audioread( strRR );
  if is_7p1
    [LSir,fs2] = audioread( strLS );
    RSir = audioread( strRS );
    if fs2 ~= fs
      error('New channel and old channel do not have the same sample rate. Oops');
    end
  end

for idx = 1:max_samplerate_idx

  LirRs = resample(Lir,out_samplerate(idx), fs );
  RirRs = resample(Rir,out_samplerate(idx), fs );
  CirRs = resample(Cir,out_samplerate(idx), fs );
  LRirRs = resample(LRir,out_samplerate(idx), fs );
  RRirRs = resample(RRir,out_samplerate(idx), fs );
  if is_7p1
    LSirRs = resample(LSir,out_samplerate(idx), fs );
    RSirRs = resample(RSir,out_samplerate(idx), fs );
  end  

  Llir = LirRs(:,1);
  Lrir = LirRs(:,2);
  Rlir = RirRs(:,1);
  Rrir = RirRs(:,2);
  
  LRlir = LRirRs(:,1);
  LRrir = LRirRs(:,2);
  RRlir = RRirRs(:,1);
  RRrir = RRirRs(:,2);
  
  Clir = CirRs(:,1);
  Crir = Clir;
  
  if is_7p1  
    LSlir = LSirRs(:,1);
    LSrir = LSirRs(:,2);
    RSlir = RSirRs(:,1);
    RSrir = RSirRs(:,2);
  end
 
  if length(Llir)- numtaps(idx) > 0
  
  for j=1:length(Llir)-numtaps(idx)
      Llsum(j) = sum(abs(Llir(j:j+numtaps(idx))));
      Rlsum(j) = sum(abs(Rlir(j:j+numtaps(idx))));
      Lrsum(j) = sum(abs(Lrir(j:j+numtaps(idx))));
      Rrsum(j) = sum(abs(Rrir(j:j+numtaps(idx))));
      LRlsum(j) = sum(abs(LRlir(j:j+numtaps(idx))));
      RRlsum(j) = sum(abs(RRlir(j:j+numtaps(idx))));
      LRrsum(j) = sum(abs(LRrir(j:j+numtaps(idx))));
      RRrsum(j) = sum(abs(RRrir(j:j+numtaps(idx))));
      Clsum(j) = sum(abs(Clir(j:j+numtaps(idx))));
      Crsum(j) = sum(abs(Crir(j:j+numtaps(idx))));
      if is_7p1
        LSlsum(j) = sum(abs(LSlir(j:j+numtaps(idx))));
        RSlsum(j) = sum(abs(RSlir(j:j+numtaps(idx))));
        LSrsum(j) = sum(abs(LSrir(j:j+numtaps(idx))));
        RSrsum(j) = sum(abs(RSrir(j:j+numtaps(idx))));
      endif
  end

  Llmax = max(Llsum);
  Rlmax = max(Rlsum);
  Lrmax = max(Lrsum);
  Rrmax = max(Rrsum);
  LRlmax = max(LRlsum);
  RRlmax = max(RRlsum);
  LRrmax = max(LRrsum);
  RRrmax = max(RRrsum);
  Clmax = max(Clsum);
  Crmax = max(Crsum);
  if is_7p1
    LSlmax = max(LSlsum);
    RSlmax = max(RSlsum);
    LSrmax = max(LSrsum);
    RSrmax = max(RSrsum);
  end
  
  Llsum = Llsum./Llmax;
  Rlsum = Rlsum./Rlmax;
  Lrsum = Lrsum./Lrmax;
  Rrsum = Rrsum./Rrmax;
  LRlsum = LRlsum./LRlmax;
  RRlsum = RRlsum./RRlmax;
  LRrsum = LRrsum./LRrmax;
  RRrsum = RRrsum./RRrmax;
  Clsum = Llsum./Clmax;
  Crsum = Rlsum./Crmax;
  tsum = Llsum + Rlsum + Lrsum + Rrsum + LRlsum + RRlsum + LRrsum + RRrsum + Clsum + Crsum;
  if is_7p1
    LSlsum = LSlsum./LSlmax;
    RSlsum = RSlsum./RSlmax;
    LSrsum = LSrsum./LSrmax;
    RSrsum = RSrsum./RSrmax;
    tsum = tsum + LSlsum + RSlsum + LSrsum + RSrsum;
  end
  
  [tmax,imax] = max(tsum);
  disp(sprintf('The optimal %d tap truncation begins at index %g', numtaps(idx), imax ));
  
  else
  imax = 1;
  endif
  if length(Llir) < numtaps(idx)
    usetaps = length(Llir);
  else
    usetaps = numtaps(idx);
  endif
  
  virt_short_irs(idx,1,1,1:usetaps) = Llir(imax:imax+usetaps-1);
  virt_short_irs(idx,1,2,1:usetaps) = Lrir(imax:imax+usetaps-1);
  virt_short_irs(idx,2,1,1:usetaps) = Rlir(imax:imax+usetaps-1);
  virt_short_irs(idx,2,2,1:usetaps) = Rrir(imax:imax+usetaps-1);
  virt_short_irs(idx,3,1,1:usetaps) = Clir(imax:imax+usetaps-1);
  virt_short_irs(idx,3,2,1:usetaps) = Crir(imax:imax+usetaps-1);
  virt_short_irs(idx,4,1,1:usetaps) = LRlir(imax:imax+usetaps-1);
  virt_short_irs(idx,4,2,1:usetaps) = LRrir(imax:imax+usetaps-1);
  virt_short_irs(idx,5,1,1:usetaps) = RRlir(imax:imax+usetaps-1);
  virt_short_irs(idx,5,2,1:usetaps) = RRrir(imax:imax+usetaps-1);
  if is_7p1
    virt_short_irs(idx,6,1,1:usetaps) = LSlir(imax:imax+usetaps-1);
    virt_short_irs(idx,6,2,1:usetaps) = LSrir(imax:imax+usetaps-1);
    virt_short_irs(idx,7,1,1:usetaps) = RSlir(imax:imax+usetaps-1);
    virt_short_irs(idx,7,2,1:usetaps) = RSrir(imax:imax+usetaps-1);
  end
end

 %%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%
if 0
  graphics_toolkit("fltk");
  function plot_this(function1, function2, name)
    plot (function1, 'b', function2, 'r', [-0.2,0.2])
    title (name);
    xlim ([80,140]);
    [left_peaks,L_idx] = findpeaks(function1, "DoubleSided");
    [right_peaks,R_idx] = findpeaks(function2, "DoubleSided");
    disp(name)
    disp(abs(L_idx(1) - R_idx(1)))
  endfunction  
  
  subplot(421)
  plot_this(Clir, Crir, 'Center');
  
  subplot(423)
  plot_this(Llir, Rlir, 'Left')
  
  subplot(424)
  plot_this(Lrir, Rrir, 'Right')

  subplot(425)
  plot_this(LRlir, RRlir, 'Left Rear')
  
  subplot(426)
  plot_this(LRrir, RRrir, 'Right Rear')

  if is_7p1
    subplot(427)
    plot_this(LSlir, RSlir, 'Left Surround')
    
    subplot(428)
    plot_this(LSrir, RSrir, 'Right Surround')

 end
 

 end 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
%save irs virt_irs
outname = sprintf('BACCH_virt_short_coeffs.h');

writefloatarray(virt_short_irs,outname,'VIRT_SHORT',numtaps,numblocks,0);

printf('\n');




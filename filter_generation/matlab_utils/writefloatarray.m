function out = writefloatarray(varval,outfile,prefix,irLengths,irNumBlocks,span) 
%demovars
%James Mentz
%Part 1. A single variable that is an 4 dimension array
%dim 1 = samplerate
%dim 2 = ir for which speaker in virtual 7.1 system
%dim 3 = toLeft or toRight output channel
%dim 4 = ir data
%Part 2. Export the list as a C header file along with defines that help 
%C code to use the data


% use the following flag to develop header files for coding
% this will create truncated files that will still compile properly
% but WILL NOT WORK
% this is to help Visual Studio avoid locking up while repeatedly parsing
% extremely large header files when looking up defined values for autocomplete
isForCoding = false;

if isForCoding == true
  irLengths = [64 64 64 64];
end

%newvarlist(); %created a new list of variables to become an H file

%prefix: populate #defines with module specific definitions of array

%irLengths: array of ir lengths, used by this module to initialize with fewer
%values than actual array dimensions which is ok by the C compiler
% should look like the following:
%irLengths = [8192 8192 16384 32768];
irNames = {'44' '48' '96' '192'};

%S = load(varval);
%var_names = fieldnames(S);

##if length(var_names) > 1
##  %not for this function...
##  return;
##end

printf('\n\nGenerating %s\n', prefix);

varname=inputname(1); %var_names{1}; %get variable name
%varval=infile; %S.(var_names{1}); %get variable value

% first dimension of array is the sample rate of the IRs
% the length is the number of sample rates included per irNames

% calculate number of samples for progress indicator
dims = length(size(varval));
if dims == 5
  numIRs = size(varval,3);
  numChannelIrs = size(varval,4);
  numOrientIrs = size(varval,2);
elseif dims == 4
  numIRs = size(varval,2);
  numChannelIrs = size(varval,3);
  numOrientIrs = 1;
elseif dims == 3
  numIRs = 1;
  numChannelIrs = size(varval,2);
  numOrientIrs = 1;
endif

numFreqs = size(varval,1);
numValues = 0;
for idx=1:numFreqs
  numValues = numValues + numIRs * numOrientIrs * numChannelIrs * irLengths(idx);
end

maxsamplerateidx = size(varval,1);
%numIRs = size(varval,2);
%numDiscreteIRs = numIRs * size(varval,3);
%numValues = 0;
%for idx=1:maxsamplerateidx
%  numValues = numValues + (numDiscreteIRs * irLengths(idx));
%end
fivePercentValues = floor(numValues/100) - 5;
percentValues = 0;

str = sprintf('./header_files/BACCH_%s_constants.h',prefix);
hdr1 = fopen(str,'wb');

%write single inclusion lines, defines, enum
str=sprintf('#ifndef __BACCH_%s_CONSTS_INCLUDE__\n', prefix);
fwrite(hdr1, str, 'char');
str=sprintf('#define __BACCH_%s_CONSTS_INCLUDE__\n\n', prefix);
fwrite(hdr1, str, 'char');
str=sprintf('#define %s_NUM_IRS       %d\n\n', prefix, numIRs);
fwrite(hdr1, str, 'char');
if span > 0
str=sprintf('#define %s_SPAN          %.2ff\n\n', prefix, span);
fwrite(hdr1, str, 'char');
end
str=sprintf('static int %s_IR_LENGTHS[%d] = { %d', prefix, maxsamplerateidx, irLengths(1));
fwrite(hdr1, str, 'char');

for rateidx=2:maxsamplerateidx
  str=sprintf(', %d', irLengths(rateidx)); 
  fwrite(hdr1,str,'char');
end    
str=sprintf(' };\n\n');
fwrite(hdr1,str,'char');

str=sprintf('static int %s_IR_NUM_BLOCKS[%d] = { %d', prefix, maxsamplerateidx, irNumBlocks(1));
fwrite(hdr1, str, 'char');

for rateidx=2:maxsamplerateidx
  str=sprintf(', %d', irNumBlocks(rateidx)); 
  fwrite(hdr1,str,'char');
end    
str=sprintf(' };\n\n');
fwrite(hdr1,str,'char');

str=sprintf('#endif //__BACCH_%s_CONSTS_INCLUDE__\n', prefix);
fwrite(hdr1, str, 'char');

fclose(hdr1);

filename=sprintf('./header_files/BACCH_%s_coeffs.h', prefix);
%Open file for overwrite, 
fid = fopen(filename ,'wb'); 

% we'll put 8 values per line to minimize rows in the file, this used to count
cnt = 0;

% we'll put a progress indicator every so often, this cnts
PIcnt = 0;
 
str=sprintf('#ifndef __BACCH_%s_COEFFS_INCLUDE__\n', prefix);
fwrite(fid, str, 'char');
str=sprintf('#define __BACCH_%s_COEFFS_INCLUDE__\n\n', prefix);
fwrite(fid, str, 'char');

% case for m x n x o real arrays - validate input
if (length(size(varval))==3) && isreal(varval)
       %assume its a float
		fwrite(fid, sprintf('static const float %s[%d][%d][%d] = {\n',varname,size(varval,1),size(varval,2),size(varval,3)), 'char');
		for m=1:size(varval,1)
			fwrite(fid, '{', 'char');
			for n=1:size(varval,2)
				fwrite(fid, '{', 'char');
        for o=1:irLengths(m)-1
          fwrite(fid, sprintf(' %10.10ff,', varval(m,n,o)), 'char');
          cnt = cnt + 1;
          if cnt == 8
            fwrite(fid, sprintf('\n', 'char'));
            cnt = 0;
          end
        end
				fwrite(fid, sprintf('%10.10ff }', varval(m,n,o)), 'char');
        cnt = 0;
				if n == size(varval,2)
					fwrite(fid, '}', 'char');
				else
					fwrite(fid, sprintf(',\n'), 'char');
				end
			end
			if m == size(varval,1) %no comma on the last set
        fwrite(fid, sprintf('\n};\n\n'), 'char');
      else
        fwrite(fid, sprintf(',\n'), 'char');
			end
   end
   
   % close the single inclusion block
   str=sprintf('#endif //__BACCH_%s_COEFFS_INCLUDE__\n', prefix);
   fwrite(fid, str, 'char');

   % close the file, finished
   fclose(fid);
	% case for m x n x o x p real arrays - validate input
elseif (length(size(varval))==4) && isreal(varval)
        %assume its a float
		fwrite(fid, sprintf('static const float %s[%d][%d][%d][%d] = {\n',varname,size(varval,1),size(varval,2),size(varval,3), size(varval,4)), 'char');
		for m=1:size(varval,1)
			fwrite(fid, '{', 'char');
			for n=1:size(varval,2)
				fwrite(fid, '{', 'char');
				for o=1:size(varval,3)
          fwrite(fid, '{', 'char');
          for p=1:irLengths(m)-1
            fwrite(fid, sprintf(' %10.10ff,', varval(m,n,o,p)), 'char');
            PIcnt = PIcnt + 1;
            if  PIcnt == fivePercentValues-1
              PIcnt = 0;
              percentValues = percentValues + 1;
              printf('\b\b\b\b\b\b\b\b\b\b\b\b\b');
              printf('%02d%% complete', percentValues); 
            end
            cnt = cnt + 1;
            if cnt == 8
              fwrite(fid, sprintf('\n', 'char'));
              cnt = 0;
            end
          end
				  fwrite(fid, sprintf('%10.10ff }', varval(m,n,o,p)), 'char');
          cnt = 0;
          if o == size(varval,3)
            fwrite(fid, '}', 'char');
          else
					  fwrite(fid, sprintf(',\n'), 'char');
          end
				end
				if n == size(varval,2)
					fwrite(fid, '}', 'char');
				else
					fwrite(fid, sprintf(',\n'), 'char');
				end
			end
			if m == size(varval,1) %no comma on the last set
        fwrite(fid, sprintf('\n};\n\n'), 'char');
      else
        fwrite(fid, sprintf(',\n'), 'char');
			end
   end

   % close the single inclusion block
   str=sprintf('#endif //__BACCH_%s_COEFFS_INCLUDE__\n', prefix);
   fwrite(fid, str, 'char');

   % close the file, finished
   fclose(fid);

% case for m x n x o x p x q real arrays - validate input
elseif (length(size(varval))==5) && isreal(varval)
        %assume its a float
		fwrite(fid, sprintf('static const float %s[%d][%d][%d][%d][%d] = {\n',varname,size(varval,1),size(varval,2),size(varval,3),size(varval,4), size(varval,5)), 'char');
		for m=1:size(varval,1)
			fwrite(fid, '{', 'char');
			for n=1:size(varval,2)
				fwrite(fid, '{', 'char');
				for o=1:size(varval,3)
          fwrite(fid, '{', 'char');
          for p=1:size(varval,4)
            fwrite(fid, '{', 'char');
            for q=1:irLengths(m)-1
              fwrite(fid, sprintf(' %10.10ff,', varval(m,n,o,p,q)), 'char');
              PIcnt = PIcnt + 1;
              if  PIcnt == fivePercentValues-1
                PIcnt = 0;
                percentValues = percentValues + 1;
                printf('\b\b\b\b\b\b\b\b\b\b\b\b\b');
                printf('%02d%% complete', percentValues); 
              end
              cnt = cnt + 1;
              if cnt == 8
                fwrite(fid, sprintf('\n', 'char'));
                cnt = 0;
              end
            end
            fwrite(fid, sprintf('%10.10ff }', varval(m,n,o,p)), 'char');
            cnt = 0;
            if p == size(varval,4)
              fwrite(fid, '}', 'char');
            else
              fwrite(fid, sprintf(',\n'), 'char');
            end
          end
          if o == size(varval,3)
            fwrite(fid, '}', 'char');
          else
            fwrite(fid, sprintf(',\n'), 'char');
          end
				end
				if n == size(varval,2)
					fwrite(fid, '}', 'char');
				else
					fwrite(fid, sprintf(',\n'), 'char');
				end
			end
			if m == size(varval,1) %no comma on the last set
        fwrite(fid, sprintf('\n};\n\n'), 'char');
      else
        fwrite(fid, sprintf(',\n'), 'char');
			end
   end


   % close the single inclusion block
   str=sprintf('#endif //__BACCH_%s_COEFFS_INCLUDE__\n', prefix);
   fwrite(fid, str, 'char');

   % close the file, finished
   fclose(fid);
   
   
   printf('\n\n');
end
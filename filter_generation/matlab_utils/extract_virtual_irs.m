clear all;
close all

pkg load signal
warning("off", "all");

%set this value to 1 if is a 7.1 setup (has LR and RR speakers)
is_7p1 = 1;

out_samplerate = [44100 48000 96000 192000];
numtaps = [8192 8192 16384 32768];
numblocks = [8 8 16 32];

max_samplerate_idx = 3;


%% WE WILL ALWAYS USE the 7.1 FILTERS
if ~is_7p1
  strC = '.\wav_filters\virtualizer_filter\long\C-C\Room1-C-CL.wav';
  strL = '.\BACCH-VHP_filters_for_Room1\wav_version\L-R\Room1-L-RL.wav';
  strR = '.\BACCH-VHP_filters_for_Room1\wav_version\L-R\Room1-L-RR.wav';
  strLs = '.\BACCH-VHP_filters_for_Room1\wav_version\Ls-Rs\Room1-Ls-RsL.wav';
  strRs = '.\BACCH-VHP_filters_for_Room1\wav_version\Ls-Rs\Room1-Ls-RsR.wav';
endif

if is_7p1
  strC = '.\wav_filters\virtualizer_filter\long\C-C\7p1C-CL.wav';
  strL = '.\wav_filters\virtualizer_filter\long\L-R\7p1L-RL.wav';
  strR = '.\wav_filters\virtualizer_filter\long\L-R\7p1L-RR.wav';
  strLs = '.\wav_filters\virtualizer_filter\long\Ls-Rs\7p1Ls-RsL.wav';
  strRs = '.\wav_filters\virtualizer_filter\long\Ls-Rs\7p1Ls-RsR.wav';
  strLr = '.\wav_filters\virtualizer_filter\long\Lrs-Rrs\7p1Lrs-RrsL.wav';
  strRr = '.\wav_filters\virtualizer_filter\long\Lrs-Rrs\7p1Lrs-RrsR.wav';
end

virt_irs = zeros(max_samplerate_idx, 7, 2, numtaps(max_samplerate_idx));

  [Lir,fs] = audioread( strL );
  Rir = audioread( strR );
  Lsir = audioread( strLs );
  Rsir = audioread( strRs );
  Cir = audioread( strC );

  if is_7p1
    [LRir,fs2] = audioread( strLr );
    RRir = audioread( strRr );
    if fs2 ~= fs
      error('New channel and old channel do not have the same sample rate. Oops');
    end
  end

for idx = 1:max_samplerate_idx

  LirRs = resample(Lir,out_samplerate(idx), fs );
  RirRs = resample(Rir,out_samplerate(idx), fs );
  LsirRs = resample(Lsir,out_samplerate(idx), fs );
  RsirRs = resample(Rsir,out_samplerate(idx), fs );
  CirRs = resample(Cir,out_samplerate(idx), fs );
  if is_7p1
    LRirRs = resample(LRir,out_samplerate(idx), fs );
    RRirRs = resample(RRir,out_samplerate(idx), fs );
  end  

  Llir = LirRs(:,1);
  Lrir = LirRs(:,2);
  Rlir = RirRs(:,1);
  Rrir = RirRs(:,2);
  
  Lslir = LsirRs(:,1);
  Lsrir = LsirRs(:,2);
  Rslir = RsirRs(:,1);
  Rsrir = RsirRs(:,2);
  
  Clir = CirRs(:,1);
  Crir = CirRs(:,2);
  
  if is_7p1  
    LRlir = LRirRs(:,1);
    LRrir = LRirRs(:,2);
    RRlir = RRirRs(:,1);
    RRrir = RRirRs(:,2);
  end
 
  if length(Llir)- numtaps(idx) > 0
  
  for j=1:length(Llir)-numtaps(idx)
      Llsum(j) = sum(abs(Llir(j:j+numtaps(idx))));
      Rlsum(j) = sum(abs(Rlir(j:j+numtaps(idx))));
      Lrsum(j) = sum(abs(Lrir(j:j+numtaps(idx))));
      Rrsum(j) = sum(abs(Rrir(j:j+numtaps(idx))));
      Lslsum(j) = sum(abs(Lslir(j:j+numtaps(idx))));
      Rslsum(j) = sum(abs(Rslir(j:j+numtaps(idx))));
      Lsrsum(j) = sum(abs(Lsrir(j:j+numtaps(idx))));
      Rsrsum(j) = sum(abs(Rsrir(j:j+numtaps(idx))));
      Clsum(j) = sum(abs(Clir(j:j+numtaps(idx))));
      Crsum(j) = sum(abs(Crir(j:j+numtaps(idx))));
      if is_7p1
        LRlsum(j) = sum(abs(LRlir(j:j+numtaps(idx))));
        RRlsum(j) = sum(abs(RRlir(j:j+numtaps(idx))));
        LRrsum(j) = sum(abs(LRrir(j:j+numtaps(idx))));
        RRrsum(j) = sum(abs(RRrir(j:j+numtaps(idx))));
      endif
  end

  Llmax = max(Llsum);
  Rlmax = max(Rlsum);
  Lrmax = max(Lrsum);
  Rrmax = max(Rrsum);
  Lslmax = max(Lslsum);
  Rslmax = max(Rslsum);
  Lsrmax = max(Lsrsum);
  Rsrmax = max(Rsrsum);
  Clmax = max(Clsum);
  Crmax = max(Crsum);
  if is_7p1
    LRlmax = max(LRlsum);
    RRlmax = max(RRlsum);
    LRrmax = max(LRrsum);
    RRrmax = max(RRrsum);
  end
  
  Llsum = Llsum./Llmax;
  Rlsum = Rlsum./Rlmax;
  Lrsum = Lrsum./Lrmax;
  Rrsum = Rrsum./Rrmax;
  Lslsum = Lslsum./Lslmax;
  Rslsum = Rslsum./Rslmax;
  Lsrsum = Lsrsum./Lsrmax;
  Rsrsum = Rsrsum./Rsrmax;
  Clsum = Llsum./Clmax;
  Crsum = Rlsum./Crmax;
  tsum = Llsum + Rlsum + Lrsum + Rrsum + Lslsum + Rslsum + Lsrsum + Rsrsum + Clsum + Crsum;
  if is_7p1
    LRlsum = LRlsum./LRlmax;
    RRlsum = RRlsum./RRlmax;
    LRrsum = LRrsum./LRrmax;
    RRrsum = RRrsum./RRrmax;
    tsum = tsum + LRlsum + RRlsum + LRrsum + RRrsum;
  end
  
  [tmax,imax] = max(tsum);
  disp(sprintf('The optimal %d tap truncation begins at index %g', numtaps(idx), imax ));
  
  else
  imax = 1;
  endif
  if length(Llir) < numtaps(idx)
    usetaps = length(Llir);
  else
    usetaps = numtaps(idx);
  endif
  
  virt_irs(idx,1,1,1:usetaps) = Llir(imax:imax+usetaps-1);
  virt_irs(idx,1,2,1:usetaps) = Lrir(imax:imax+usetaps-1);
  virt_irs(idx,2,1,1:usetaps) = Rlir(imax:imax+usetaps-1);
  virt_irs(idx,2,2,1:usetaps) = Rrir(imax:imax+usetaps-1);
  virt_irs(idx,3,1,1:usetaps) = Clir(imax:imax+usetaps-1);
  virt_irs(idx,3,2,1:usetaps) = Crir(imax:imax+usetaps-1);
  virt_irs(idx,4,1,1:usetaps) = Lslir(imax:imax+usetaps-1);
  virt_irs(idx,4,2,1:usetaps) = Lsrir(imax:imax+usetaps-1);
  virt_irs(idx,5,1,1:usetaps) = Rslir(imax:imax+usetaps-1);
  virt_irs(idx,5,2,1:usetaps) = Rsrir(imax:imax+usetaps-1);
  if is_7p1
    virt_irs(idx,6,1,1:usetaps) = LRlir(imax:imax+usetaps-1);
    virt_irs(idx,6,2,1:usetaps) = LRrir(imax:imax+usetaps-1);
    virt_irs(idx,7,1,1:usetaps) = RRlir(imax:imax+usetaps-1);
    virt_irs(idx,7,2,1:usetaps) = RRrir(imax:imax+usetaps-1);
  end
end

 %%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%
if 0
  graphics_toolkit("fltk");
  function plot_this(function1, function2, name)
    plot (function1, 'b', function2, 'r', [-0.2,0.2])
    title (name);
    xlim ([80,140]);
    [left_peaks,L_idx] = findpeaks(function1, "DoubleSided");
    [right_peaks,R_idx] = findpeaks(function2, "DoubleSided");
    disp(name)
    disp(abs(L_idx(1) - R_idx(1)))
  endfunction  
  
  subplot(421)
  plot_this(Clir, Crir, 'Center');
  
  subplot(423)
  plot_this(Llir, Rlir, 'Left')
  
  subplot(424)
  plot_this(Lrir, Rrir, 'Right')

  subplot(425)
  plot_this(Lslir, Rslir, 'Left Surround')
  
  subplot(426)
  plot_this(Lsrir, Rsrir, 'Right Surround')

  if is_7p1
    subplot(427)
    plot_this(LRlir, RRlir, 'Left Rear')
    
    subplot(428)
    plot_this(LRrir, RRrir, 'Right Rear')

 end
 

 end 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
%save irs virt_irs
outname = sprintf('BACCH_virt_coeffs.h');

writefloatarray(virt_irs,outname,'VIRT',numtaps,numblocks,0);





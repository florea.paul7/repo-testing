clear all; clc; 
close all

pkg load signal

addpath('./matlab_utils')

%numtaps = [1024 1024 2048];

numtaps = [2048, 2048, 4096; 2048, 2048, 4096; 4096, 4096, 8192; 300 300 600];
block_size = 2048;

numblocks = numtaps/block_size;

filename = ['./wav_filters/xtcshorts/chop005_HpB-LS50Sub65Hz', %./wav_filters/hp_xtc_filter/BACCHfbin3'; ...
            './wav_filters/sp_filter/BACCHfbin1' ]; %SamsungGalaxyTabS_BACCHFilter'; './SamsungGalaxyTabS4_BACCHFilter']
prefixes = ['HP_XTC'; 'GALAXY_TABS'];

%sampleRates = [44100, 48000, 96000];
sampleRates = [44100, 48000, 96000];

filtIdx = 1;

for fidx=1:2

    strL = sprintf('%sL.wav',strtok(filename(fidx,:)));
    strR = sprintf('%sR.wav',strtok(filename(fidx,:)));
    disp( sprintf('\nopening %s', strL ));
    [Lir,fs] = audioread( strL );
    Rir = audioread( strR );
    disp( sprintf('%s is at %g Hz', strL, fs ) );
    
    irs = zeros(size(sampleRates,2),1,4,4096);
    
    for idx=1:size(sampleRates,2)

      if sampleRates(idx) == fs
        imax = 1;
        Llir = Lir(:,1);
        Lrir = Lir(:,2);
        Rlir = Rir(:,1);
        Rrir = Rir(:,2);
      else
        Llir = resample( Lir(:,1), sampleRates(idx), fs );
        Lrir = resample( Lir(:,2), sampleRates(idx), fs );
        Rlir = resample( Rir(:,1), sampleRates(idx), fs );
        Rrir = resample( Rir(:,2), sampleRates(idx), fs );
        
        if size(Llir,1) > numtaps(idx)
          
          lengthtaps = numtaps(idx);
        
          Llsum = zeros(1,length(Llir)-numtaps(idx));
          Lrsum = zeros(1,length(Llir)-numtaps(idx));
          Rlsum = zeros(1,length(Llir)-numtaps(idx));
          Rrsum = zeros(1,length(Llir)-numtaps(idx));


          for j=1:length(Llir)-numtaps(idx)
              Llsum(j) = sum(abs(Llir(j:j+numtaps(idx))));
              Lrsum(j) = sum(abs(Lrir(j:j+numtaps(idx))));
              Rlsum(j) = sum(abs(Rlir(j:j+numtaps(idx))));
              Rrsum(j) = sum(abs(Rrir(j:j+numtaps(idx))));
          end


          Lmax = max(Llsum);
          Llsum = Llsum./Lmax;
          Lmax = max(Rlsum);
          Rlsum = Rlsum./Lmax;
          Lmax = max(Lrsum);
          Lrsum = Lrsum./Lmax;
          Lmax = max(Rrsum);
          Rrsum = Rrsum./Lmax;

          tsum = Llsum + Rlsum + Lrsum + Rrsum;

          [tmax,imax] = max(tsum);
          disp(sprintf('The optimal %d tap truncation begins at index %g', numtaps(idx), imax ));

          figure(idx);
          plot(Llsum,'g');
          hold on;
          plot(Rlsum,'r');
          plot(Lrsum,'c');
          plot(Rrsum,'m');
          hold off;
          
        else
          imax = 1
          lengthtaps = size(Llir,1);
        endif
      endif
      irs(idx,filtIdx,1,1:lengthtaps) = Llir(imax:imax+lengthtaps-1);
      irs(idx,filtIdx,2,1:lengthtaps) = Lrir(imax:imax+lengthtaps-1);
      irs(idx,filtIdx,3,1:lengthtaps) = Rlir(imax:imax+lengthtaps-1);
      irs(idx,filtIdx,4,1:lengthtaps) = Rrir(imax:imax+lengthtaps-1);

    end
    
    %save hrirs irs
 	
    %outFolder = sprintf("%s%d/hrirs.h", parentFolder, binNum );
    %writeheader('hrirs', outFolder);
    if fidx == 2
      ts_irs = irs;
      writefloatarray(ts_irs, 'null', strtok(prefixes(fidx,:)), numtaps, numblocks, 1);
    end
    if fidx == 3
      sp_irs = irs;
      writefloatarray(sp_irs, 'null', strtok(prefixes(fidx,:)), numtaps, numblocks, 1);
    end
    if fidx == 1
      hp_xtc_irs = irs;
      writefloatarray(hp_xtc_irs, 'null', strtok(prefixes(fidx,:)), numtaps, numblocks, 1);
    end
end  

numtaps_HP = [4096, 4096, 8192];

numblocks_HP = numtaps_HP/block_size;

filename_HP = ['./wav_filters/shorts/cut108_HpX-LS50Sub65Hz_EQd_']; %hp_extern_filter/jssBRIRfx21bin3']; 
prefixes_HP = ['HP'];

sampleRates = [44100, 48000, 96000];

filtIdx = 1;

for fidx=1:1

    strL = sprintf('%sL.wav',strtok(filename_HP(fidx,:)));
    strR = sprintf('%sR.wav',strtok(filename_HP(fidx,:)));
    disp( sprintf('opening %s', strL ));
    [Lir,fs] = audioread( strL );
    Rir = audioread( strR );
    disp( sprintf('%s is at %g Hz', strL, fs ) );
    
    hp_irs = zeros(size(sampleRates,2),1,4,numtaps_HP(3));
    
    for idx=1:size(sampleRates,2)

      if sampleRates(idx) == fs
        imax = 1;
        Llir = Lir(:,1);
        Lrir = Lir(:,2);
        Rlir = Rir(:,1);
        Rrir = Rir(:,2);
      else
        Llir = resample( Lir(:,1), sampleRates(idx), fs );
        Lrir = resample( Lir(:,2), sampleRates(idx), fs );
        Rlir = resample( Rir(:,1), sampleRates(idx), fs );
        Rrir = resample( Rir(:,2), sampleRates(idx), fs );
        
        if size(Llir,1) > numtaps_HP(idx)
          
          lengthtaps = numtaps_HP(idx);
        
          Llsum = zeros(1,length(Llir)-numtaps_HP(idx));
          Lrsum = zeros(1,length(Llir)-numtaps_HP(idx));
          Rlsum = zeros(1,length(Llir)-numtaps_HP(idx));
          Rrsum = zeros(1,length(Llir)-numtaps_HP(idx));


          for j=1:length(Llir)-numtaps_HP(idx)
              Llsum(j) = sum(abs(Llir(j:j+numtaps_HP(idx))));
              Lrsum(j) = sum(abs(Lrir(j:j+numtaps_HP(idx))));
              Rlsum(j) = sum(abs(Rlir(j:j+numtaps_HP(idx))));
              Rrsum(j) = sum(abs(Rrir(j:j+numtaps_HP(idx))));
          end


          Lmax = max(Llsum);
          Llsum = Llsum./Lmax;
          Lmax = max(Rlsum);
          Rlsum = Rlsum./Lmax;
          Lmax = max(Lrsum);
          Lrsum = Lrsum./Lmax;
          Lmax = max(Rrsum);
          Rrsum = Rrsum./Lmax;

          tsum = Llsum + Rlsum + Lrsum + Rrsum;

          [tmax,imax] = max(tsum);
          disp(sprintf('The optimal %d tap truncation begins at index %g', numtaps_HP(idx), imax ));

          figure(idx);
          plot(Llsum,'g');
          hold on;
          plot(Rlsum,'r');
          plot(Lrsum,'c');
          plot(Rrsum,'m');
          hold off;
          
        else
          imax = 1
          lengthtaps = size(Llir,1);
        endif
      endif
      hp_irs(idx,filtIdx,1,1:lengthtaps) = Llir(imax:imax+lengthtaps-1);
      hp_irs(idx,filtIdx,2,1:lengthtaps) = Lrir(imax:imax+lengthtaps-1);
      hp_irs(idx,filtIdx,3,1:lengthtaps) = Rlir(imax:imax+lengthtaps-1);
      hp_irs(idx,filtIdx,4,1:lengthtaps) = Rrir(imax:imax+lengthtaps-1);

    end
    
    %save hrirs irs
 	
    %outFolder = sprintf("%s%d/hrirs.h", parentFolder, binNum );
    %writeheader('hrirs', outFolder);
    writefloatarray(hp_irs, 'null', strtok(prefixes_HP(fidx,:)), numtaps_HP, numblocks_HP, 1);
end  

extract_virtual_irs

extract_short_virtual_irs

return




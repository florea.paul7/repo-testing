#ifndef __BACCH_HP_CONSTS_INCLUDE__
#define __BACCH_HP_CONSTS_INCLUDE__

#define HP_NUM_IRS       1

#define HP_SPAN          1.00f

static int HP_IR_LENGTHS[3] = { 4096, 4096, 8192 };

static int HP_IR_NUM_BLOCKS[3] = { 2, 2, 4 };

#endif //__BACCH_HP_CONSTS_INCLUDE__

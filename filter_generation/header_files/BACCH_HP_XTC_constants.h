#ifndef __BACCH_HP_XTC_CONSTS_INCLUDE__
#define __BACCH_HP_XTC_CONSTS_INCLUDE__

#define HP_XTC_NUM_IRS       1

#define HP_XTC_SPAN          1.00f

static int HP_XTC_IR_LENGTHS[3] = { 2048, 2048, 4096 };

static int HP_XTC_IR_NUM_BLOCKS[3] = { 1, 1, 2 };

#endif //__BACCH_HP_XTC_CONSTS_INCLUDE__

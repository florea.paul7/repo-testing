package com.bitcauldron;

import java.nio.ByteBuffer;

public class SpeakerEngine {
	
	private static final int		SPKR_NUM_FILTERS = 8;
	
	private boolean					mSpkrEnabled = true;
	private boolean					mEqEnabled = false;
	
	private boolean					spkrNeedsUpdate = false;
	private boolean 				eqNeedsUpdate = false;
	
	private static int				mFilterIndex = 0;
	private static int				mNumChannels = 0;
	private static byte[]			mCurrentEq = new byte[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	private static int				mSampleRate = 44100;
	
	private static SpeakerEngine 			mSpkr;	
	
	public SpeakerEngine( int numChannels ) {
		mNumChannels = numChannels;
		createEngine( true ); 
		updateEqualizer( mCurrentEq );
		config(numChannels, mFilterIndex, mSampleRate);
	}
	
	public static SpeakerEngine getInstance( int numChannels ) {
		if ( mSpkr == null ) {
			mSpkr = new SpeakerEngine( numChannels );
		}		
		return mSpkr;
	}
	
	public void destroy( ) {
		destroyEngine();
	}
        
    public boolean process( ByteBuffer in_buf, ByteBuffer out_buf, int size ) {
    	if ( eqNeedsUpdate ) {
    		eqNeedsUpdate = false;
    		configEq( mCurrentEq );
    	}
    	if ( spkrNeedsUpdate ) {
    		spkrNeedsUpdate = false;
    		config( mNumChannels, mFilterIndex, mSampleRate );
    	}
    	return( process( in_buf, out_buf, size, mSpkrEnabled, mEqEnabled ));
    }	
    
    public void setSpeaker( int filterIndex ) {
    	if ( filterIndex >= 0 && filterIndex < SPKR_NUM_FILTERS ) {
    		mFilterIndex = filterIndex;
    		spkrNeedsUpdate = true;
    	}
    }
    public void setSampleRate( int rate ) {
    	if ( rate == 44100 || rate == 48000 ) {
    		mSampleRate = rate;
    		spkrNeedsUpdate = true;
    	}
    }
    
    public boolean updateEqualizer( byte[] neweq ) {
    	if ( neweq.length == mCurrentEq.length ) {
			mCurrentEq = neweq.clone();
			eqNeedsUpdate = true;
    		return true;
    	}
    	return false;
    }
    
    public void resetSpeaker() {
    	reset();
    }
	
	public void enableEqualizer( boolean val ) { mEqEnabled = val; }
	
    /** Native methods, implemented in jni folder */
    private static native boolean 	createEngine( boolean arm_neon_present );
    private static native void 		destroyEngine();
       
    private static native boolean	config( int numChannels, int filterIndex, int samplerate );
    private static native boolean 	configEq( byte[] eq );
    private static native boolean	process( ByteBuffer input, ByteBuffer output, int size, boolean spkrEnabled, boolean eqEnabled );
    private static native boolean   reset();
    
    /** Load jni .so on initialization */
    static {
         System.loadLibrary("speaker");
    }
}
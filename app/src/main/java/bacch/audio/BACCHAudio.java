package bacch.audio;

import java.nio.ByteBuffer;

public class BACCHAudio {

    public static final int         MODE_SPEAKER    = 0x00000001;
    public static final int         MODE_HEADPHONE  = 0x00000010;
    public static final int         MODE_UBACCH     = 0x00000100;
    public static final int         MODE_LCREX      = 0x00001000;
    public static final int         MODE_BYPASS     = 0x10000000;
	
	private boolean					mSPEnabled = true;
	private boolean                 mHPEnabled = false;
	private boolean					mEQEnabled = false;

    private static boolean			mNeedsUpdate = false;
    private static boolean          mVirtNeedsUpdate = false;
    private static boolean			mUBACCHNeedsUpdate = false;

	private static int				mSampleRate = 48000;
	private static int              mNumChannelsIn = 2;
	private static int				mOrientation = 0;
	private static boolean          mVirtIsDolby = false;
	private static boolean          mIsHP = false;
	private static boolean          mBACCHIsEnabled = true;

	// LCRex values
	private static float            mWidth = 0.5f;
	private static float            mGain = 4.5f;

    // UBACCH Values
    private static float            mSP_to_SP = 20;
    private static float            mListener_to_SP = 30;
    private static boolean          mIsMetric = true;

	
	private static BACCHAudio mBA = null;

	public BACCHAudio(int sampleRate, int numChannelsIn, int orientation ) {
		mSampleRate = sampleRate;
		mOrientation = orientation;
		mNumChannelsIn = numChannelsIn;

		createEngine( sampleRate, numChannelsIn, orientation);

		//grab the defaults from the engine
	}
	
	public static BACCHAudio getInstance(int sampleRate, int numChannels, int orientation ) {

        int newSR = sampleRate;
        if ( sampleRate == 44100 || sampleRate == 48000 ) {
            newSR = sampleRate;
        } else {
            newSR = 44100;
        }

	    if ( null == mBA ) {

			mBA = new BACCHAudio(newSR, numChannels, orientation);
		} else {
	        mSampleRate = newSR;
	        mOrientation = orientation;
	        mNumChannelsIn = numChannels;

		    if (( newSR != mSampleRate ) || ( orientation != mOrientation) || ( numChannels != mNumChannelsIn)) {
		        mBA.setSampleRate(newSR);
		        mSampleRate = newSR;
		        mOrientation = orientation;
		        mNumChannelsIn = numChannels;
		        mNeedsUpdate = true;
            }
        }
		// instance already running, should verify that settings have changed????
		return mBA;
	}
	
	public void destroy( ) {
		destroyEngine();
	}



    public void resetBA() {
        reset();
    }

    public void setUBACCH( float sp2sp, float list2sp, boolean isMetric) {
        // need to add lower limit validation..
        mSP_to_SP = sp2sp;
        mListener_to_SP = list2sp;
        mIsMetric = isMetric;
        mUBACCHNeedsUpdate = true;
    }
    // 0 for DTS, 1 for Dolby
    public void set7_1mode(int mode) {
        mVirtIsDolby = (mode == 0) ? false : true;
        mVirtNeedsUpdate = true;
    }

    /* valid configurations:
     *  speaker = MODE_SPEAKER
     *  speaker + LCRex = MODE_SPEAKER | MODE_LCREX
     *  headphone = MODE_HEADPHONE
     *  headphone + LCRex = MODE_HEADPHONE | MODE_LCREX
     *  UBACCH = MODE_UBACCH
     */
    public boolean setMode(int mode, boolean enabled) {
        boolean bReturn = false;
        if (null == mBA) return false;
        switch (mode) {
            case MODE_SPEAKER | MODE_LCREX:
                if (mBACCHIsEnabled) {
                    enableSP(enabled, true, false);
                    enableLCex(enabled, true, false);
                }
                mIsHP = false;
                break;
            case MODE_HEADPHONE | MODE_LCREX:
                if (mBACCHIsEnabled) {
                    enableHP(enabled, true, false);
                    enableLCex(enabled, true, false);
                }
                mIsHP = true;
                break;
            case MODE_SPEAKER:
                if (mBACCHIsEnabled) {
                    enableSP(enabled, true, false);
                }
                mIsHP = false;
                break;
            case MODE_HEADPHONE:
                if (mBACCHIsEnabled) {
                    enableHP(enabled, true, false);
                }
                mIsHP = true;
                break;
            case MODE_UBACCH:
                if (mBACCHIsEnabled) {
                    enableUBACCH(enabled, true, false);
                }
                break;
            case MODE_BYPASS:

            default:
                return false;
        }
        return true;
    }

    public void setSampleRate( int rate ) {
        if ( rate == 44100 || rate == 48000 ) {
            mSampleRate = rate;
            mNeedsUpdate = true;
        }
    }

    public void enableEqualizer( boolean val ) { mEQEnabled = val; }

    public void enableBACCH(boolean val) {
        mBACCHIsEnabled = val;
        enable(val, mIsHP, false);
    }
        
    public int processBA(ByteBuffer in_buf, ByteBuffer out_buf, int numFrames, int numChannels ) {
	    if ( mBA == null)
	        return 0;

    	if ( mNeedsUpdate ) {
    		mNeedsUpdate = false;
    		configBA( mSampleRate, mNumChannelsIn, mOrientation );
    	}
    	if (mUBACCHNeedsUpdate ) {
    	    mUBACCHNeedsUpdate = false;
    	    configUBACCH(mSP_to_SP, mListener_to_SP, mIsMetric);
        }
        if (mVirtNeedsUpdate) {
            mVirtNeedsUpdate = false;
            configVirt(mVirtIsDolby);
        }
    	return( process( in_buf, out_buf, numFrames, numChannels ));
    }

    public int resampleBA(ByteBuffer in, ByteBuffer out, int numFrames, int numChannels, int srIn) {
        return resample( in, out, numFrames, numChannels, srIn, mSampleRate);
    }

    /** Native methods, implemented in bacch_audio_BACCHAudio */
    private static native boolean 	createEngine( int sampleRate, int channelsIn, int orientation );

    private static native void 		destroyEngine();
       
    private static native void	    configBA( int sampleRate, int channelsIn, int orientation );

    private static native void 		configUBACCH( float distance_speaker_to_speaker, float distance_listener_to_speaker, boolean isMetricUnits);

    private static native void      configVirt(boolean isDolby);

    private static native void      reset();

    private static native boolean   enableHP( boolean enabled, boolean isUser, boolean immediate);

    private static native boolean   enableSP( boolean enabled, boolean isUser, boolean immediate);

    private static native boolean   enableLCex( boolean enabled, boolean isUser, boolean immediate);

    private static native boolean   enableUBACCH( boolean enabled, boolean isUser, boolean immediate);

    private static native int	    process(ByteBuffer in_buf, ByteBuffer out_buf, int numFrames, int numChannels );

    private static native void      enable(boolean enableBACCH, boolean isHP, boolean immediate);

    private static native int       resample(ByteBuffer in, ByteBuffer out, int numFrames, int numChannels, int srIn, int srOut);
    
    /** Load jni .so on initialization */
    static {
         System.loadLibrary("bacchaudio");
    }
}

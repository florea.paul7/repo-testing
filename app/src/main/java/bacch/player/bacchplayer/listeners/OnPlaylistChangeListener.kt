package bacch.player.bacchplayer.listeners

import bacch.player.bacchplayer.model.Song


/**
 * Created by Ruslan Ivakhnenko on 07.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface OnPlaylistChangeListener {

    fun OnPlaylistChanged(playlist : List<Song>)
}
package bacch.player.bacchplayer.listeners

import bacch.player.bacchplayer.model.Video

interface OnVideoItemClickListener {

    fun onVideoClick(video : Video)
}
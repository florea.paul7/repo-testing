package bacch.player.bacchplayer.listeners


/**
 * Created by Ruslan Ivakhnenko on 05.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface OnPlayerClickListener {

    fun onPlay()

    fun onPause()

    fun onStop()

    fun onForward()

    fun onBackward()

    fun onSeek(percent : Int)

    fun onShuffle()
}
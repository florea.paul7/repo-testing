package bacch.player.bacchplayer.listeners

import bacch.player.bacchplayer.model.Song


/**
 * Created by Ruslan Ivakhnenko on 23.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface OnMediaItemClickListener {

    fun onClick(song : Song)
}
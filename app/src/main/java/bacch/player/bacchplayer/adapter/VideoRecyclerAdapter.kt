package bacch.player.bacchplayer.adapter


import android.animation.PropertyValuesHolder
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.databinding.ItemAudioBinding
import bacch.player.bacchplayer.databinding.ItemVideoBinding
import bacch.player.bacchplayer.fragments.SongFragment.OnListFragmentInteractionListener
import bacch.player.bacchplayer.listeners.OnVideoItemClickListener
import bacch.player.bacchplayer.model.Video

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 *
 */
class VideoRecyclerAdapter(
    private var mValues: List<Video>,
    private val mListener: OnVideoItemClickListener?
) : RecyclerView.Adapter<VideoRecyclerAdapter.ViewHolder>() {

    var currentSong : Video? = null
    set(value)  {
        val prevIndex = mValues?.indexOf(field)
        field = value
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_video, parent, false) as ItemVideoBinding
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val video = mValues[position]
        val binding = DataBindingUtil.bind<ItemAudioBinding>(holder.itemView) as ItemVideoBinding
        binding.cardView.apply {
            if (video == currentSong) {
                val color = context.resources.getColor(R.color.colorAccentPressed)
                setCardBackgroundColor(color)
            }
        }

        holder.itemView.setOnClickListener {
            val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f, 0f)
            val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f, 0f)
            val alpha = PropertyValuesHolder.ofFloat(View.ALPHA, 1f, 0f)
//            ObjectAnimator.ofPropertyValuesHolder(binding.imageViewCover, scaleX, scaleY, alpha).apply {
//                interpolator = OvershootInterpolator()
//                duration = 300
//            }.start()
//            ObjectAnimator.ofFloat(binding.imageViewCover, View.ALPHA, 1f, 0f).apply {
//                duration = 500
//            }.start()
            mListener?.onVideoClick(video)
        }
        binding.video = video
    }

    override fun getItemCount(): Int = mValues.size

    fun setData(songs : List<Video>){
        mValues = songs
        notifyDataSetChanged()
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView)

}

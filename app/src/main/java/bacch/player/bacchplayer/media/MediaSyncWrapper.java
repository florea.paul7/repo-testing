package bacch.player.bacchplayer.media;

import android.content.Context;
import android.media.*;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import bacch.audio.BACCHAudio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * MediaSync API: local video/audio playback.
 */
public class MediaSyncWrapper {
    private static final String LOG_TAG = "MediaSyncWrapper";

    private final long NO_TIMESTAMP = -1;
    private final float FLOAT_PLAYBACK_RATE_TOLERANCE = .02f;
    private final long TIME_MEASUREMENT_TOLERANCE_US = 20000;
    private final int APPLICATION_AUDIO_PERIOD_MS = 200;
    private final int TEST_MAX_SPEED = 1;
    private Context mContext;
    private MediaSync mMediaSync = null;
    private SurfaceHolder mSurfaceHolder = null;
    private Surface mSurface = null;
    private AudioTrack mAudioTrack = null;

    private Decoder mDecoderVideo = null;
    private Decoder mDecoderAudio = null;
    private boolean mHasAudio = false;
    private boolean mHasVideo = false;
    private boolean mEosAudio = false;
    private boolean mEosVideo = false;
    private boolean mIsPaused = false;
    private boolean mIsHeadset  = false;
    private boolean mIsBACCHEnabled  = true;
    private final Object mConditionEos = new Object();
    private final Object mConditionEosAudio = new Object();

    private final AtomicBoolean inSeek = new AtomicBoolean();
    private final AtomicBoolean inStart = new AtomicBoolean();

    public MediaSyncWrapper(PlaybackEventListener listener, SurfaceHolder surfaceHolder) {
        mSurfaceHolder = surfaceHolder;
        if ( mSurfaceHolder != null) {
            mSurface = surfaceHolder.getSurface();
        }
        mMediaSync = new MediaSync();
        mPBEL = listener;
        mDecoderVideo = new Decoder(this, mMediaSync, false);
        mDecoderAudio = new Decoder(this, mMediaSync, true);
    }

    public PlaybackEventListener mPBEL;

    public interface PlaybackEventListener {
        void onTimeChanged( long time );
        void eos();
    }

    public void tearDown() throws Exception {
        if (mIsPaused) {
            resume();
        }
        if (mMediaSync != null) {
            onEosAll(mDecoderAudio, mDecoderVideo);
            //onEos(mDecoderAudio);
            //onEos(mDecoderVideo);
            mMediaSync.flush();
            mMediaSync.release();
            mMediaSync = null;
        }
        if (mDecoderAudio != null) {
            mDecoderAudio.release();
            mDecoderAudio = null;
        }
        if (mDecoderVideo != null) {
            mDecoderVideo.release();
            mDecoderVideo = null;
        }
        if (mSurface != null) {
            mSurface.release();
            mSurface = null;
        }
    }

    public void pause() {
        mIsPaused = true;
        mMediaSync.setPlaybackParams(new PlaybackParams().setSpeed(0.0f));
    }

    public void resume() {
        mIsPaused = false;
        mMediaSync.setPlaybackParams(new PlaybackParams().setSpeed(1.0f));
    }

    public void setBACCHMode(int mode, boolean enable) {
        if (mDecoderAudio != null) {
            mDecoderAudio.setBACCHMode(mode, enable);
        }
    }

    public void setRIRMode(int mode) {
        mDecoderAudio.setRIRMode(mode);
    }

    public void setBACCHEnable(boolean enable) {
        mIsBACCHEnabled = enable;
        mDecoderAudio.setBACCHEnable(enable);
    }

    private boolean reachedEos_l() {
        return ((!mHasVideo || mEosVideo) && (!mHasAudio || mEosAudio));
    }

    public void onEosAll(Decoder aDecoder, Decoder vDecoder) {
        if (aDecoder == mDecoderAudio) {
            mEosAudio = true;
            synchronized (mConditionEosAudio) {
                mConditionEosAudio.notifyAll();
            }
        }

        if (vDecoder == mDecoderVideo) {
            mEosVideo = true;
            synchronized(mConditionEos) {
                if (reachedEos_l()) {
                    mConditionEos.notifyAll();
                }
            }
        }
    }

    public void onEos(Decoder decoder) {
        synchronized(mConditionEosAudio) {
            if (decoder == mDecoderAudio) {
                mEosAudio = true;
                mConditionEosAudio.notify();
                if (mDecoderVideo != null) {
                    synchronized (mConditionEos) {
                        mConditionEos.notifyAll();
                    }
                }
            }
        }

        synchronized(mConditionEos) {
            if (decoder == mDecoderVideo) {
                mEosVideo = true;
            }
            if (reachedEos_l()) {
                mConditionEos.notify();
            }
        }
    }

    /**
     * Tests playing back audio and video successfully.
     */
    public void playAudioAndVideo(Uri fileUri, boolean isHeadset, boolean isBACCHEnabled) throws InterruptedException {
        playAV(fileUri, 5*60*1000 /* lastBufferTimestampMs */,
               true /* audio */, true /* video */, isHeadset, isBACCHEnabled); //10*60*1000 /* timeOutMs */);
    }

    public void playAudio(Uri fileUri, boolean isHeadset, boolean isBACCHEnabled) throws InterruptedException {
        playAV(fileUri, 5*60*1000, true, false,  isHeadset, isBACCHEnabled); //10*60*1000);
    }

    private void playAV(
            final Uri inputResourceId,
            final long lastBufferTimestampMs,
            final boolean audio,
            final boolean video,
            boolean isHeadset,
            boolean isBACCHEnabled ) throws InterruptedException {
        playAV(inputResourceId, lastBufferTimestampMs, audio, video, isHeadset, isBACCHEnabled, 1.0f);
    }

    private void playAV(
            final Uri inputResourceId,
            final long lastBufferTimestampMs,
            final boolean audio,
            final boolean video,
            final boolean isHeadset,
            final boolean isBACCHEnabled,
            final float playbackRate) throws InterruptedException {
        final AtomicBoolean completed = new AtomicBoolean();
        Thread decodingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                completed.set(runPlayAV(inputResourceId, lastBufferTimestampMs * 1000,
                        audio, video, isHeadset, isBACCHEnabled, playbackRate));
            }
        });
        decodingThread.start();
        decodingThread.join(); //timeOutMs);
        if (!completed.get()) {
            throw new RuntimeException("timed out decoding to end-of-stream");
        }
    }

    public void seekTo(long time) {
        if (!inSeek.get() ) {
            inSeek.set(true);
            if (mHasVideo) {
                time = mDecoderVideo.seekTo(time);
            }
            if (mHasAudio) {
                mDecoderAudio.seekTo(time);
            }
            inSeek.set(false);
        }
    }

    private boolean runPlayAV(
            Uri inputResourceId,
            long lastBufferTimestampUs,
            boolean audio,
            boolean video,
            boolean isHeadset,
            boolean isBACCHEnabled,
            float playbackRate) {
        // allow 250ms for playback to get to stable state.
        final int PLAYBACK_RAMP_UP_TIME_MS = 250;

        final Object conditionFirstAudioBuffer = new Object();
        mIsPaused = false;
        mIsHeadset = isHeadset;
        mIsBACCHEnabled = isBACCHEnabled;

        if (video) {
            mMediaSync.setSurface(mSurfaceHolder.getSurface());
            mSurface = mMediaSync.createInputSurface();

            if (mDecoderVideo.setup(
                    inputResourceId, mSurface, mIsHeadset, mIsBACCHEnabled, lastBufferTimestampUs) == false) {
                return true;
            }
            mHasVideo = true;
        }

        if (audio) {
            if (mDecoderAudio.setup(inputResourceId, null, mIsHeadset, mIsBACCHEnabled, lastBufferTimestampUs) == false) {
                return true;
            }

            // get audio track.
            mAudioTrack = mDecoderAudio.getAudioTrack();

            mMediaSync.setAudioTrack(mAudioTrack);

            mMediaSync.setCallback(new MediaSync.Callback() {
                @Override
                public void onAudioBufferConsumed(
                        MediaSync sync, ByteBuffer byteBuffer, int bufferIndex) {
                    Decoder decoderAudio = mDecoderAudio;
                    if (decoderAudio != null) {
                        decoderAudio.releaseOutputBuffer(bufferIndex, NO_TIMESTAMP);
                    }
                    synchronized (conditionFirstAudioBuffer) {
                        conditionFirstAudioBuffer.notify();
                    }
                }
            }, null);

            mHasAudio = true;
        }

        SyncParams sync = new SyncParams().allowDefaults();
        mMediaSync.setSyncParams(sync);
        sync = mMediaSync.getSyncParams();

        mMediaSync.setPlaybackParams(new PlaybackParams().setSpeed(playbackRate));

        synchronized (conditionFirstAudioBuffer) {
            if (video) {
                mDecoderVideo.start();
            }
            if (audio) {
                mDecoderAudio.start();

                // wait for the first audio output buffer returned by media sync.
                try {
                    conditionFirstAudioBuffer.wait();
                } catch (InterruptedException e) {
                    Log.i(LOG_TAG, "worker thread is interrupted.");
                    return true;
                }
            }
        }

        if (audio) {
            try {
                Thread.sleep(PLAYBACK_RAMP_UP_TIME_MS);
            } catch (InterruptedException e) {
                Log.i(LOG_TAG, "worker thread is interrupted during sleeping.");
                return true;
            }

            MediaTimestamp mediaTimestamp = mMediaSync.getTimestamp();
            if (mediaTimestamp == null) {
                Log.d(LOG_TAG, "No timestamp available for starting");
                return true;
            }
            long checkStartTimeRealUs = System.nanoTime() / 1000;
            long checkStartTimeMediaUs = mediaTimestamp.getAnchorMediaTimeUs();

            synchronized (mConditionEosAudio) {
                if (!mEosAudio) {
                    try {
                        mConditionEosAudio.wait();
                    } catch (InterruptedException e) {
                        Log.i(LOG_TAG, "worker thread is interrupted when waiting for audio EOS.");
                        return true;
                    }
                }
            }
            if (mMediaSync != null) {
                mediaTimestamp = mMediaSync.getTimestamp();
            }
            if (mediaTimestamp == null) {
                Log.d(LOG_TAG, "No timestamp available for ending");
                return true;
            }
            long playTimeUs = System.nanoTime() / 1000 - checkStartTimeRealUs;
            long mediaDurationUs = mediaTimestamp.getAnchorMediaTimeUs() - checkStartTimeMediaUs;
            if (!(Math.abs(mediaDurationUs - playTimeUs * playbackRate) <=
                    (mediaDurationUs * (sync.getTolerance() + FLOAT_PLAYBACK_RATE_TOLERANCE)
                            + TIME_MEASUREMENT_TOLERANCE_US))){
                // sync.getTolerance() is MediaSync's tolerance of the playback rate, whereas
                // FLOAT_PLAYBACK_RATE_TOLERANCE is our test's tolerance.
                // We need to add both to get an upperbound for allowable error.
                Log.d(LOG_TAG, "Mediasync may have error in playback rate " + playbackRate
                        + ", play time is " + playTimeUs + " vs expected " + mediaDurationUs);
                return true;
            }
        }

        boolean completed = false;
        synchronized (mConditionEos) {
            if (!reachedEos_l()) {
                try {
                    mConditionEos.wait();
                } catch (InterruptedException e) {
                }
            }
            completed = reachedEos_l();
        }
        return completed;
    }

    private class Decoder extends MediaCodec.Callback {
        private MediaSyncWrapper mMSW = null;
        private MediaSync mMediaSync = null;
        private boolean mIsAudio = false;
        private long mLastBufferTimestampUs = 0;

        private Surface mSurface = null;

        private AudioTrack mAudioTrack = null;

        private final Object mConditionCallback = new Object();
        private MediaExtractor mExtractor = null;
        private MediaCodec mDecoder = null;

        private final Object mAudioBufferLock = new Object();
        private List<AudioBuffer> mAudioBuffers = new LinkedList<AudioBuffer>();

        private ByteBuffer bbOutBuf;
        private ByteBuffer bbIn1;
        private ByteBuffer holdBuf;
        private ByteBuffer resampleBuf;
        private ByteBuffer resampleOutBuf;
        private int inHoldBuf = 0;

        private List<ByteBuffer> outBufs = new LinkedList<ByteBuffer>();

        private BACCHAudio mBA;
        private Boolean mBAEnabled = true;
        private Boolean decoderIsHeadSet = false;
        private int  numChannels = 2;
        private int  mFPS = 0;

        private int mSampleRate = 44100;
        private int mMediaSampleRate = 0;
        private boolean mResample = false;

        private int  stopPosition = 0;
        private int  stopCount = 0;

        // accessed only on callback thread.
        private boolean mEos = false;
        private boolean mSignaledEos = false;

        private class AudioBuffer {
            public ByteBuffer mByteBuffer;
            public int mBufferIndex;

            public AudioBuffer(ByteBuffer byteBuffer, int bufferIndex) {
                mByteBuffer = byteBuffer;
                mBufferIndex = bufferIndex;
            }
        }

        private HandlerThread mHandlerThread;
        private Handler mHandler;

        Decoder(MediaSyncWrapper test, MediaSync sync, boolean isAudio) {
            mMSW = test;
            mMediaSync = sync;
            mIsAudio = isAudio;
        }

        public boolean setup(Uri inputResourceId, Surface surface, boolean headSetPluggedIN, boolean isBACCHEnabled, long lastBufferTimestampUs) {
            if (!mIsAudio) {
                mSurface = surface;
                // handle video callback in a separate thread as releaseOutputBuffer is blocking
                mHandlerThread = new HandlerThread("SyncViewVidDec");
                mHandlerThread.start();
                mHandler = new Handler(mHandlerThread.getLooper());
            }
            mBAEnabled = isBACCHEnabled;
            mLastBufferTimestampUs = lastBufferTimestampUs;
            decoderIsHeadSet = headSetPluggedIN;
            try {
                // get extrator.
                String type = mIsAudio ? "audio/" : "video/";
                mExtractor = MediaUtils.createMediaExtractorForMimeType(
                        mContext, inputResourceId, type);

                // get decoder.
                MediaFormat mediaFormat =
                    mExtractor.getTrackFormat(mExtractor.getSampleTrackIndex());
                String mimeType = mediaFormat.getString(MediaFormat.KEY_MIME);
                if (!MediaUtils.hasDecoder(mimeType)) {
                    Log.i(LOG_TAG, "No decoder found for mimeType= " + mimeType);
                    return false;
                }
                mDecoder = MediaCodec.createDecoderByType(mimeType);
                mDecoder.configure(mediaFormat, mSurface, null, 0);
                mDecoder.setCallback(this, mHandler);

                if (!mIsAudio) {
                    mDecoder.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT); //  VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING | VIDEO_SCALING_MODE_SCALE_TO_FIT
                    mFPS = mediaFormat.getInteger(MediaFormat.KEY_FRAME_RATE);
                }
                else {
                    int sampleRate = mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                    mMediaSampleRate = sampleRate;
                    switch (mMediaSampleRate) {
                        case 44100:
                        case 48000:
                            mSampleRate = mMediaSampleRate;
                            mResample = false;
                            break;
                        case 88200:
                        case 176400:
                            mSampleRate = 44100;
                            mResample = true;
                            break;
                        case 96000:
                        case 192000:
                            mSampleRate = 48000;
                            mResample = true;
                            break;
                        default:
                            mSampleRate = mMediaSampleRate;
                    }
                    numChannels = mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                    mBA = BACCHAudio.getInstance(mSampleRate, numChannels, 0);
                    if (decoderIsHeadSet) {
                        mBA.setMode(BACCHAudio.MODE_HEADPHONE | BACCHAudio.MODE_LCREX, true);
                    } else {
                        mBA.setMode(BACCHAudio.MODE_SPEAKER | BACCHAudio.MODE_LCREX, true);
                    }
                    Log.i("MediaSync", "Configured Engine with Headset: " + decoderIsHeadSet);

                    mBA.enableBACCH(mBAEnabled);
                }


                return true;
            } catch (IOException e) {
                throw new RuntimeException("error reading input resource", e);
            }
        }

        public void setBACCHEnable(boolean enabled) {
            mBAEnabled = enabled;
            mBA.enableBACCH(enabled);
        }

        public void setBACCHMode(int mode, boolean enable) {

            mBA.setMode(mode, enable);
            Log.i("MediaSync", "New Engine Mode: " + mode + " is enabled: " + enable);
        }

        public void setRIRMode(int mode) {
            mBA.set7_1mode(mode);
        }

        public long seekTo(long time) {
            long actual = 0;

            mExtractor.seekTo(time, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
            actual = mExtractor.getSampleTime();
            //mEos = false;
            if (!mIsAudio) {
                mDecoder.flush();
                mDecoder.start();
//                stopCount = 0;
//                stopPosition = (int) ((time - mExtractor.getSampleTime()) * (mFPS / 1000000));
            }
            return actual;
        }

        public void start() {
            if (mDecoder != null) {
                mDecoder.start();
            }
        }

        public void release() {
            synchronized (mConditionCallback) {
                if (mDecoder != null) {
                    try {
                        mDecoder.stop();
                    } catch (IllegalStateException e) {
                    }
                    mDecoder.release();
                    mDecoder = null;
                }
                if (mExtractor != null) {
                    mExtractor.release();
                    mExtractor = null;
                }

                if (mAudioTrack != null) {
                    mAudioTrack.release();
                    mAudioTrack = null;
                }
            }
        }

        public int getBytesPerSample(int audioFormat)
        {
            switch (audioFormat) {
                case AudioFormat.ENCODING_PCM_8BIT:
                    return 1;
                case AudioFormat.ENCODING_PCM_16BIT:
                case AudioFormat.ENCODING_IEC61937:
                case AudioFormat.ENCODING_DEFAULT:
                    return 2;
                case AudioFormat.ENCODING_PCM_FLOAT:
                    return 4;
                case AudioFormat.ENCODING_INVALID:
                default:
                    throw new IllegalArgumentException("Bad audio format " + audioFormat);
            }
        }

        public AudioTrack getAudioTrack() {
            if (!mIsAudio) {
                throw new RuntimeException("can not create audio track for video");
            }

            if (mExtractor == null) {
                throw new RuntimeException("extrator is null");
            }

            if (mAudioTrack == null) {
                MediaFormat mediaFormat =
                    mExtractor.getTrackFormat(mExtractor.getSampleTrackIndex());
                int sampleRateInHz = mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                numChannels = mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                int channelConfig = (numChannels == 1) ?
                        AudioFormat.CHANNEL_OUT_MONO : AudioFormat.CHANNEL_OUT_STEREO;
                int actualChannelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                switch (numChannels) {
                    case 2:
                        actualChannelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                        break;
                    case 6:
                        actualChannelConfig = AudioFormat.CHANNEL_OUT_5POINT1;
                        break;
                    case 8:
                        actualChannelConfig = AudioFormat.CHANNEL_OUT_7POINT1_SURROUND;
                        break;
                    default:
                        actualChannelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                }
                int audioFormat = AudioFormat.ENCODING_PCM_16BIT;

                mMediaSampleRate = sampleRateInHz;
                switch (mMediaSampleRate) {
                    case 44100:
                    case 48000:
                        mSampleRate = mMediaSampleRate;
                        mResample = false;
                        break;
                    case 88200:
                    case 176400:
                        mSampleRate = 44100;
                        mResample = true;
                        break;
                    case 96000:
                    case 192000:
                        mSampleRate = 48000;
                        mResample = true;
                        break;
                    default:
                        mSampleRate = mMediaSampleRate;
                }
                int minBufferSizeInBytes = AudioTrack.getMinBufferSize(
                        mSampleRate,
                        actualChannelConfig,
                        audioFormat);
                final int frameCount = APPLICATION_AUDIO_PERIOD_MS * mSampleRate / 1000;
                final int frameSizeInBytes = Integer.bitCount(actualChannelConfig)
                        * getBytesPerSample(audioFormat);
                // ensure we consider application requirements for writing audio data
                minBufferSizeInBytes = TEST_MAX_SPEED /* speed influences buffer size , default is 2*/
                        * Math.max(minBufferSizeInBytes, frameCount * frameSizeInBytes);
                mAudioTrack = new AudioTrack(
                        AudioManager.STREAM_MUSIC,
                        mSampleRate,
                        channelConfig,
                        audioFormat,
                        minBufferSizeInBytes,
                        AudioTrack.MODE_STREAM);

                bbOutBuf = ByteBuffer.allocateDirect(minBufferSizeInBytes * 4);
                bbOutBuf.order(ByteOrder.nativeOrder());
                bbOutBuf.clear();

                bbIn1 = ByteBuffer.allocateDirect(minBufferSizeInBytes * (4 / 2) * numChannels);
                bbIn1.order(ByteOrder.nativeOrder());
                bbIn1.clear();

                holdBuf = ByteBuffer.allocateDirect(minBufferSizeInBytes*(4 / 2) * numChannels * 2);
                holdBuf.order(ByteOrder.nativeOrder());
                holdBuf.clear();
                holdBuf.limit(0);

                resampleBuf = ByteBuffer.allocateDirect(minBufferSizeInBytes * (4 / 2) * numChannels);
                resampleBuf.order(ByteOrder.nativeOrder());
                resampleBuf.clear();
                resampleBuf.limit(0);

                resampleOutBuf = ByteBuffer.allocateDirect(minBufferSizeInBytes * 4);
                resampleOutBuf.order(ByteOrder.nativeOrder());
                resampleOutBuf.clear();
                resampleOutBuf.limit(0);

                for (int i=0;i<8;i++) {
                    ByteBuffer one = ByteBuffer.allocateDirect(minBufferSizeInBytes * 4);
                    one.order(ByteOrder.nativeOrder());
                    one.clear();
                    outBufs.add(one);
                }
            }

            return mAudioTrack;
        }

        public void releaseOutputBuffer(int bufferIndex, long renderTimestampNs) {
            synchronized (mConditionCallback) {
                if (mDecoder != null) {
                    if (renderTimestampNs == NO_TIMESTAMP) {
                        mDecoder.releaseOutputBuffer(bufferIndex, false /* render */);
                    } else {
                        mDecoder.releaseOutputBuffer(bufferIndex, renderTimestampNs);
                    }
                }
            }
        }

        @Override
        public void onError(MediaCodec codec, MediaCodec.CodecException e) {
        }

        @Override
        public void onInputBufferAvailable(MediaCodec codec, int index) {
            synchronized (mConditionCallback) {
                if (mExtractor == null || mExtractor.getSampleTrackIndex() == -1
                        || mSignaledEos || mDecoder != codec) {
                    return;
                }

                ByteBuffer buffer = codec.getInputBuffer(index);
                int size = mExtractor.readSampleData(buffer, 0);
                long timestampUs = mExtractor.getSampleTime();
                mExtractor.advance();
                mSignaledEos = mExtractor.getSampleTrackIndex() == -1;
                        //|| timestampUs >= mLastBufferTimestampUs;
                if (mSignaledEos) {
                    Log.i("MSW", "End of Stream");
                }
                codec.queueInputBuffer(
                        index,
                        0,
                        size,
                        timestampUs,
                        mSignaledEos ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);
            }
        }

        @Override
        public void onOutputBufferAvailable(
                MediaCodec codec, int index, MediaCodec.BufferInfo info) {
            synchronized (mConditionCallback) {
                if (mEos || mDecoder != codec) {
                    return;
                }

                mEos = (info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;

                if ( mEos ) {
                    Log.i("MSW", "End of Stream");
                }

                if (info.size > 0) {
                    if (mIsAudio) {
                        mMSW.mPBEL.onTimeChanged(info.presentationTimeUs / (1000));
                        ByteBuffer outputByteBuffer = codec.getOutputBuffer(index);

                        int inputBytes = info.size;
                        int outputBytes = inputBytes/numChannels * 2;  // adjust for multitrack in
                        int processBytes  = 4096 * numChannels;

                        if (numChannels == 1) {
                            mMediaSync.queueAudio(
                                    outputByteBuffer,
                                    index,
                                    info.presentationTimeUs);
                        }
                        if (index >= outBufs.size()) {
                            Integer bufSize = outBufs.get(0).capacity();
                            Integer numShort = index - outBufs.size();
                            Integer i = 0;
                            for (i = 0; i <= numShort; i++) {
                                ByteBuffer one = ByteBuffer.allocateDirect(bufSize);
                                one.order(ByteOrder.nativeOrder());
                                one.clear();
                                outBufs.add(one);
                            }
                        }
                        ByteBuffer myOutBuffer = outBufs.get(index);
                        myOutBuffer.clear();

                        if (mResample) {
                            resampleBuf.clear();
                            resampleBuf.put(outputByteBuffer);
                            resampleOutBuf.clear();

                            int numFrames = inputBytes / 2 / numChannels;
                            int numBytes = mBA.resampleBA(resampleBuf, resampleOutBuf, numFrames, numChannels, mMediaSampleRate);
                            resampleBuf.clear();
                            resampleOutBuf.position(numBytes);
                            resampleOutBuf.flip();
                            bbIn1.put(resampleOutBuf);
                            resampleOutBuf.clear();
                            outputBytes = numBytes / numChannels * 2;
                        } else {
                            bbIn1.put(outputByteBuffer);
                        }

                        int size = bbIn1.position();
                        int consumed = 0;
                        if (size > processBytes) {
                            consumed = mBA.processBA(bbIn1, bbOutBuf, size, numChannels);
                            // copy processed data to output
                            bbOutBuf.position(0);
                            bbOutBuf.limit(consumed / numChannels * 2);
                            holdBuf.position(inHoldBuf);
                            holdBuf.limit(holdBuf.capacity());
                            holdBuf.put(bbOutBuf);
                            inHoldBuf += (consumed / numChannels * 2);
                            bbOutBuf.clear();
                        }
                        if (inHoldBuf >= 0) {
                            holdBuf.position(0);
                            int zeros = outputBytes - inHoldBuf;
                            if (zeros > 0) {
                                Byte b = 0;
                                for (int i = 0; i < zeros; i++) {
                                    myOutBuffer.put(b);
                                }
                                holdBuf.limit(inHoldBuf);
                                myOutBuffer.put(holdBuf);
                                inHoldBuf = 0;
                                holdBuf.clear();
                            } else {
                                holdBuf.limit(outputBytes);
                                myOutBuffer.put(holdBuf);
                                holdBuf.limit(inHoldBuf);
                                holdBuf.compact();
                                inHoldBuf -= outputBytes;
                            }
                            myOutBuffer.flip();
                        } else {

                            holdBuf.position(0);
                            holdBuf.position(inHoldBuf);
                            myOutBuffer.put(holdBuf);
                            holdBuf.clear();
                            inHoldBuf = 0;

                            myOutBuffer.flip();
                        }

                        // check if any left in bbIn1
                        if (consumed < size) {
                            bbIn1.position(consumed);
                            bbIn1.limit(size);
                            bbIn1.compact();
                        } else {
                            bbIn1.clear();
                        }

                        mMediaSync.queueAudio(
                                myOutBuffer,
                                index,
                                info.presentationTimeUs);
                    }
                    else {
                        if ( stopPosition - stopCount < 2) {
                            codec.releaseOutputBuffer(index, true); //info.presentationTimeUs * 1000);
                        } else {
                            codec.releaseOutputBuffer(index, false);
                        }
                        stopCount ++;
                    }
                } else {
                    codec.releaseOutputBuffer(index, false);
                }
            }

            if (mEos) {
                Log.i("---", "EOS");
                mMSW.onEos(this);
                mPBEL.eos();
            }
        }

        @Override
        public void onOutputFormatChanged(MediaCodec codec, MediaFormat format) {
        }
    }
}

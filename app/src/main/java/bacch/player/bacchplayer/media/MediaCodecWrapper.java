/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bacch.player.bacchplayer.media;

import bacch.player.bacchplayer.utils.MediaBuffer;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Surface;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Simplifies the MediaCodec interface by wrapping around the buffer processing operations.
 */
public class MediaCodecWrapper {

    // Handler to use for {@code OutputSampleListener} and {code OutputFormatChangedListener}
    // callbacks
    private Handler mHandler;


    // Callback when media output format changes.
    public interface OutputFormatChangedListener {
        void outputFormatChanged(MediaCodecWrapper sender, MediaFormat newFormat);
    }

    private OutputFormatChangedListener mAudioOutputFormatChangedListener = null;
    private OutputFormatChangedListener mVideoOutputFormatChangedListener = null;

    /**
     * Callback for decodes frames. Observers can register a listener for optional stream
     * of decoded data
     */
    public interface AudioOutputSampleListener {
        void outputSample(MediaCodecWrapper sender, MediaCodec.BufferInfo info, ByteBuffer buffer);
    }

    /**
     * The {@link MediaCodec} that is managed by this class.
     */
    private MediaCodec mAudioDecoder;
    private MediaCodec mVideoDecoder;

    private MediaFormat mAudioFormat;

    private MediaExtractor mExtractor;

    private int             mVideoIndex;
    private int             mAudioIndex;

    // References to the internal buffers managed by the codec. The codec
    // refers to these buffers by index, never by reference so it's up to us
    // to keep track of which buffer is which.
    private ByteBuffer[] mAudioInputBuffers;
    private ByteBuffer[] mAudioOutputBuffers;
    
    private ByteBuffer[] mVideoInputBuffers;
    private ByteBuffer[] mVideoOutputBuffers;

    // Indices of the input buffers that are currently available for writing. We'll
    // consume these in the order they were dequeued from the codec.
    private Queue<Integer> mAvailableAudioInputBuffers;
    private Queue<Integer> mAvailableVideoInputBuffers;
    
    // Indices of the output buffers that currently hold valid data, in the order
    // they were produced by the codec.
    private Queue<Integer> mAvailableAudioOutputBuffers;
    private Queue<Integer> mAvailableVideoOutputBuffers;

    // Information about each output buffer, by index. Each entry in this array
    // is valid if and only if its index is currently contained in mAvailableAudioOutputBuffers.
    private MediaCodec.BufferInfo[] mAudioOutputBufferInfo;
    private MediaCodec.BufferInfo[] mVideoOutputBufferInfo;

    // An (optional) stream that will receive decoded data.
    private AudioOutputSampleListener mOutputSampleListener;

    public boolean isValid = false;

    public MediaCodecWrapper( String path, Surface surface ) throws IOException {

        mAudioIndex = -1;
        mVideoIndex = -1;

        mExtractor = new MediaExtractor();

        mExtractor.setDataSource(path);

        int nTracks = mExtractor.getTrackCount();

        AtomicInteger i = new AtomicInteger();

        for (i.set(0); i.get() <nTracks; i.getAndIncrement()) {
            mExtractor.unselectTrack(i.get());
        }

        for (i.set(0); i.get() <nTracks; i.getAndIncrement()) {
            MediaFormat trackFormat = mExtractor.getTrackFormat(i.get());

            // look for audio track
            if (mAudioDecoder == null) {
                if (trackFormat.containsKey(MediaFormat.KEY_SAMPLE_RATE)) {

                    if (trackFormat.containsKey("encoder-delay") && trackFormat.getInteger("encoder-delay") > 5000) {
                        trackFormat.setInteger("encoder-delay", 0);
                    }

                    final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);

                    mAudioDecoder = MediaCodec.createDecoderByType(mimeType);
                    if (mAudioDecoder != null) {
                        mAudioIndex = i.get();
                        mAudioDecoder.configure(trackFormat, null, null, 0);
                        mAudioFormat = mExtractor.getTrackFormat(i.get());
                        mExtractor.selectTrack(i.get());
                    }
                }
            }

            // look for video track
            if (mVideoDecoder == null ) {
                final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);

                // Check to see if this is actually a video mime type. If it is, then create
                // a codec that can decode this mime type.
                if (mimeType.contains("video/")) {
                    mVideoDecoder = MediaCodec.createDecoderByType(mimeType);
                    if (mVideoDecoder != null) {
                        mVideoIndex = i.get();
                        mVideoDecoder.configure(trackFormat, surface, null, 0);
                        mVideoDecoder.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT); //  VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                        mExtractor.selectTrack(i.get());
                    }
                }
            }
        }

        if ( mAudioDecoder != null ) {
            isValid = true;
            mAudioDecoder.start();
            mAudioInputBuffers = mAudioDecoder.getInputBuffers();
            mAudioOutputBuffers = mAudioDecoder.getOutputBuffers();
            mAudioOutputBufferInfo = new MediaCodec.BufferInfo[mAudioOutputBuffers.length];
            mAvailableAudioInputBuffers = new ArrayDeque<Integer>(mAudioInputBuffers.length);
            mAvailableAudioOutputBuffers = new ArrayDeque<Integer>(mAudioOutputBuffers.length);
        }

        if (mVideoDecoder != null) {
            isValid = true;
            mVideoDecoder.start();
            mVideoInputBuffers = mVideoDecoder.getInputBuffers();
            mVideoOutputBuffers = mVideoDecoder.getOutputBuffers();
            mVideoOutputBufferInfo = new MediaCodec.BufferInfo[mVideoOutputBuffers.length];
            mAvailableVideoInputBuffers = new ArrayDeque<Integer>(mVideoOutputBuffers.length);
            mAvailableVideoOutputBuffers = new ArrayDeque<Integer>(mVideoInputBuffers.length);
        }
    }

//    private MediaCodecWrapper(int audioIndex, int videoIndex, MediaCodec audioCodec, MediaFormat audioFormat, MediaCodec videoCodec, MediaExtractor extractor) {
//        mExtractor = extractor;
//
//        mAudioIndex = audioIndex;
//        mVideoIndex = videoIndex;
//
//        mAudioDecoder = audioCodec;
//        mAudioDecoder.start();
//        mAudioInputBuffers = audioCodec.getInputBuffers();
//        mAudioOutputBuffers = audioCodec.getOutputBuffers();
//        mAudioOutputBufferInfo = new MediaCodec.BufferInfo[mAudioOutputBuffers.length];
//        mAvailableAudioInputBuffers = new ArrayDeque<Integer>(mAudioOutputBuffers.length);
//        mAvailableAudioOutputBuffers = new ArrayDeque<Integer>(mAudioInputBuffers.length);
//
//        mAudioFormat = audioFormat;
//
//        if (videoCodec != null) {
//            mVideoDecoder = videoCodec;
//            mVideoDecoder.start();
//            mVideoInputBuffers = videoCodec.getInputBuffers();
//            mVideoOutputBuffers = videoCodec.getOutputBuffers();
//            mVideoOutputBufferInfo = new MediaCodec.BufferInfo[mVideoOutputBuffers.length];
//            mAvailableVideoInputBuffers = new ArrayDeque<Integer>(mVideoOutputBuffers.length);
//            mAvailableVideoOutputBuffers = new ArrayDeque<Integer>(mVideoInputBuffers.length);
//        }
//    }

    public int getAudioSampleRate() {
        return mAudioFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
    }

    public int getAudioChannels() {
        return mAudioFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
    }

    public boolean hasVideo() { return (mVideoIndex >= 0);}

    /**
     * Releases resources and ends the encoding/decoding session.
     */
    public void stopAndRelease() {
    	if ( mAudioDecoder != null ) {
	        mAudioDecoder.stop();
	        mAudioDecoder.release();
	        mAudioDecoder = null;
    	}
        if ( mVideoDecoder != null ) {
            mVideoDecoder.stop();
            mVideoDecoder.release();
            mVideoDecoder = null;
        }
        if ( mExtractor != null ) {
            mExtractor.advance();
            mExtractor.release();
            mExtractor = null;
        }
        mHandler = null;
    }

    /**
     * Getter for the registered {@link OutputFormatChangedListener}
     */
    public OutputFormatChangedListener getAudioOutputFormatChangedListener() {
        return mAudioOutputFormatChangedListener;
    }
    public OutputFormatChangedListener getVideoOutputFormatChangedListener() {
        return mVideoOutputFormatChangedListener;
    }

    /**
     *
     * @param outputFormatChangedListener the listener for callback.
     * @param handler message handler for posting the callback.
     */
    public void setAudioOutputFormatChangedListener(final OutputFormatChangedListener
            outputFormatChangedListener, Handler handler) {
        mAudioOutputFormatChangedListener = outputFormatChangedListener;

        // Making sure we don't block ourselves due to a bad implementation of the callback by
        // using a handler provided by client.
        Looper looper;
        mHandler = handler;
        if (outputFormatChangedListener != null && mHandler == null) {
            if ((looper = Looper.myLooper()) != null) {
                mHandler = new Handler();
            } else {
                throw new IllegalArgumentException(
                        "Looper doesn't exist in the calling thread");
            }
        }
    }
    
    public void setVideoOutputFormatChangedListener(final OutputFormatChangedListener
                                                            outputFormatChangedListener, Handler handler) {
        mVideoOutputFormatChangedListener = outputFormatChangedListener;

        // Making sure we don't block ourselves due to a bad implementation of the callback by
        // using a handler provided by client.
        Looper looper;
        mHandler = handler;
        if (outputFormatChangedListener != null && mHandler == null) {
            if ((looper = Looper.myLooper()) != null) {
                mHandler = new Handler();
            } else {
                throw new IllegalArgumentException(
                        "Looper doesn't exist in the calling thread");
            }
        }
    }

    // only required for audio
    public void registerOutputListener(AudioOutputSampleListener listener) {
    	mOutputSampleListener = listener;
    }

//    public static MediaCodecWrapper fromPath( String path, Surface surface ) throws IOException  {
//        MediaCodecWrapper result = null;
//        MediaCodec videoCodec = null;
//        MediaCodec audioCodec = null;
//        MediaFormat audioFormat = null;
//
//        int audioIndex = -1;
//        int videoIndex = -1;
//
//        if (path == null) return null;
//
//        MediaExtractor extractor = new MediaExtractor();
//
//        extractor.setDataSource(path);
//
//        int nTracks = extractor.getTrackCount();
//
//        AtomicInteger i = new AtomicInteger();
//
//        for (i.set(0); i.get() <nTracks; i.getAndIncrement()) {
//            extractor.unselectTrack(i.get());
//        }
//
//
//        for (i.set(0); i.get() <nTracks; i.getAndIncrement()) {
//            MediaFormat trackFormat = extractor.getTrackFormat(i.get());
//
//            // look for audio track
//            if (audioCodec == null) {
//                if (trackFormat.containsKey(MediaFormat.KEY_SAMPLE_RATE)) {
//
//                    if (trackFormat.containsKey("encoder-delay") && trackFormat.getInteger("encoder-delay") > 5000) {
//                        trackFormat.setInteger("encoder-delay", 0);
//                    }
//
//                    final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);
//
//                    audioCodec = MediaCodec.createDecoderByType(mimeType);
//                    if (audioCodec != null) {
//                        audioIndex = i.get();
//                        audioCodec.configure(trackFormat, null, null, 0);
//                        audioFormat = extractor.getTrackFormat(i.get());
//                    }
//                }
//            }
//
//            // look for video track
//            if (videoCodec == null ) {
//                final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);
//
//                // Check to see if this is actually a video mime type. If it is, then create
//                // a codec that can decode this mime type.
//                if (mimeType.contains("video/")) {
//                    videoCodec = MediaCodec.createDecoderByType(mimeType);
//                    if (videoCodec != null) {
//                        videoIndex = i.get();
//                        videoCodec.configure(trackFormat, surface, null, 0);
//                        videoCodec.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//                    }
//                }
//            }
//        }
//
//        return new MediaCodecWrapper(audioIndex, videoIndex, audioCodec, audioFormat, videoCodec, extractor);
//
//    }

//    /**
//     * Constructs the {@link MediaCodecWrapper} wrapper object around the video codec.
//     * The codec is created using the encapsulated information in the
//     * {@link MediaFormat} object.
//     *
//     * @param trackFormat The format of the media object to be decoded.
//     * @param surface Surface to render the decoded frames.
//     * @return
//     */
//    public static MediaCodecWrapper fromVideoFormat(final MediaFormat trackFormat,
//            Surface surface) throws IOException {
//        MediaCodecWrapper result = null;
//        MediaCodec videoCodec = null;
//
//
//        final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);
//
//        // Check to see if this is actually a video mime type. If it is, then create
//        // a codec that can decode this mime type.
//        if (mimeType.contains("video/")) {
//            videoCodec = MediaCodec.createDecoderByType(mimeType);
//            videoCodec.configure(trackFormat, surface, null,  0);
//            videoCodec.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
//        }
//
//        // If codec creation was successful, then create a wrapper object around the
//        // newly created codec.
//        if (videoCodec != null) {
//            result = new MediaCodecWrapper(videoCodec);
//        }
//
//        return result;
//    }
//    public static MediaCodecWrapper fromAudioFormat(final MediaFormat trackFormat)
//    	throws IOException {
//        MediaCodecWrapper result = null;
//        MediaCodec audioCodec = null;
//
//        if (trackFormat.containsKey(MediaFormat.KEY_SAMPLE_RATE)) {
//
//            if (trackFormat.containsKey("encoder-delay") && trackFormat.getInteger("encoder-delay") > 5000) {
//                trackFormat.setInteger("encoder-delay", 0);
//            }
//
//	        final String mimeType = trackFormat.getString(MediaFormat.KEY_MIME);
//
//	        audioCodec = MediaCodec.createDecoderByType(mimeType);
//
//	        // If codec creation was successful, then create a wrapper object around the
//	        // newly created codec.
//	        if (audioCodec != null) {
//	        	audioCodec.configure(trackFormat, null, null,  0);
//	            result = new MediaCodecWrapper(audioCodec);
//	        }
//        }
//
//        return result;
//    }


//    public boolean writeSample(final ByteBuffer input,
//            final MediaCodec.CryptoInfo crypto,
//            final long presentationTimeUs,
//            final int flags) throws MediaCodec.CryptoException, WriteException {
//        boolean result = false;
//        int size = input.remaining();
//
//        // check if we have dequed input buffers available from the codec
//        if (size > 0 &&  !mAvailableInputBuffers.isEmpty()) {
//            int index = mAvailableInputBuffers.remove();
//            ByteBuffer buffer = mAudioInputBuffers[index];
//
//            // we can't write our sample to a lesser capacity input buffer.
//            if (size > buffer.capacity()) {
//                throw new MediaCodecWrapper.WriteException(String.format(
//                        "Insufficient capacity in MediaCodec buffer: "
//                            + "tried to write %d, buffer capacity is %d.",
//                        input.remaining(),
//                        buffer.capacity()));
//            }
//
//            buffer.clear();
//            buffer.put(input);
//
//            // Submit the buffer to the codec for decoding. The presentationTimeUs
//            // indicates the position (play time) for the current sample.
//            if (crypto == null) {
//                mDecoder.queueInputBuffer(index, 0, size, presentationTimeUs, flags);
//            } else {
//                mDecoder.queueSecureInputBuffer(index, 0, crypto, presentationTimeUs, flags);
//            }
//            result = true;
//        }
//        return result;
//    }

    public void seekTo(long time) {
        if ( mExtractor != null ) {
            mExtractor.seekTo(time, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
        }
    }


    static MediaCodec.CryptoInfo cryptoInfo= new MediaCodec.CryptoInfo();

    public ByteBuffer getAudioInputBuffer() {
        ByteBuffer bReturn = null;


        if (mAudioDecoder == null) return bReturn;

        if (!mAvailableAudioInputBuffers.isEmpty()) {
            int index = mAvailableAudioInputBuffers.remove();
            bReturn = mAudioInputBuffers[index];
        }
        return bReturn;
    }

    public ByteBuffer getVideoInputBuffer() {
        ByteBuffer bReturn = null;

        if (mVideoDecoder == null) return bReturn;

        if (!mAvailableVideoInputBuffers.isEmpty()) {
            int index = mAvailableVideoInputBuffers.remove();
            bReturn = mVideoInputBuffers[index];
        }
        return bReturn;
    }

    public boolean returnAudioInputBuffer( MediaBuffer aBuf ) {

        if (mAudioDecoder == null) return false;

		boolean result = false;
		int size = aBuf.buf.remaining();
		int index = -1;

		for (int i=0;i<mAudioInputBuffers.length;i++){
			if ( aBuf.buf == mAudioInputBuffers[i]) {
				index = i;
				break;
			}
		}
		if ( index == -1 ) return false;
        // Submit the buffer to the codec for decoding. The presentationTimeUs
        // indicates the position (play time) for the current sample.
        if ( size> 0 ) {
        	if (!aBuf.isSecure) {
	            mAudioDecoder.queueInputBuffer(index, 0, size, aBuf.time, aBuf.flags); //presentationTimeUs, flags);
	        } else {
	            //extractor.getSampleCryptoInfo(cryptoInfo);
	            //mDecoder.queueSecureInputBuffer(index, 0, cryptoInfo, presentationTimeUs, flags);
	        }
	        result = true;
	    }
        return result;
    }

    public boolean returnVideoInputBuffer( MediaBuffer vBuf ) {

        if (mVideoDecoder == null) return false;

        boolean result = false;
        int size = vBuf.buf.remaining();
        int index = -1;

        for (int i=0;i<mVideoInputBuffers.length;i++){
            if ( vBuf.buf == mVideoInputBuffers[i]) {
                index = i;
                break;
            }
        }
        if ( index == -1 ) return false;
        // Submit the buffer to the codec for decoding. The presentationTimeUs
        // indicates the position (play time) for the current sample.
        if ( size> 0 ) {
            if (!vBuf.isSecure) {
                mVideoDecoder.queueInputBuffer(index, 0, size, vBuf.time, vBuf.flags);
            } else {
                //extractor.getSampleCryptoInfo(cryptoInfo);
                //mDecoder.queueSecureInputBuffer(index, 0, cryptoInfo, presentationTimeUs, flags);
            }
            result = true;
        }
        return result;
    }

    /**
     * Performs a peek() operation in the queue to extract media info for the buffer ready to be
     * released i.e. the head element of the queue.
     *
     * @param out_bufferInfo An output var to hold the buffer info.
     *
     * @return True, if the peek was successful.
     */
    public boolean peekAudioSample(MediaCodec.BufferInfo out_bufferInfo) {
        // dequeue available buffers and synchronize our data structures with the codec.

        if (mAudioDecoder == null) return false;

        updateAudio();
        boolean result = false;
        if (!mAvailableAudioOutputBuffers.isEmpty()) {
            int index = mAvailableAudioOutputBuffers.peek();
            MediaCodec.BufferInfo info = mAudioOutputBufferInfo[index];
            // metadata of the sample
            out_bufferInfo.set(
                    info.offset,
                    info.size,
                    info.presentationTimeUs,
                    info.flags);
            result = true;
        }
        return result;
    }

    public boolean peekVideoSample(MediaCodec.BufferInfo out_bufferInfo) {
        // dequeue available buffers and synchronize our data structures with the codec.
        if (mVideoDecoder == null) return false;

        updateVideo();
        boolean result = false;
        if (!mAvailableVideoOutputBuffers.isEmpty()) {
            int index = mAvailableVideoOutputBuffers.peek();
            MediaCodec.BufferInfo info = mVideoOutputBufferInfo[index];
            // metadata of the sample
            out_bufferInfo.set(
                    info.offset,
                    info.size,
                    info.presentationTimeUs,
                    info.flags);
            result = true;
        }
        return result;
    }

    /**
     * Processes, releases and optionally renders the output buffer available at the head of the
     * queue. All observers are notified with a callback. See {@link
    // * OutputSampleListener#outputSample(MediaCodecWrapper, MediaCodec.BufferInfo,
     * ByteBuffer)}
     *
     * @param render True, if the buffer is to be rendered on the {@link Surface} configured
     *
     */
    public void popAudioSample(boolean render) {
        // dequeue available buffers and synchronize our data structures with the codec.

        if (mAudioDecoder == null) return;

        updateAudio();
        if (!mAvailableAudioOutputBuffers.isEmpty()) {
            int index = mAvailableAudioOutputBuffers.remove();

            if (render && mOutputSampleListener != null) {
                ByteBuffer buffer = mAudioOutputBuffers[index];
                MediaCodec.BufferInfo info;
                info = mAudioOutputBufferInfo[index];
                mOutputSampleListener.outputSample(this, info, buffer);
            }

            // releases the buffer back to the codec
            try {
                mAudioDecoder.releaseOutputBuffer(index, render);
            } catch (IllegalStateException | NullPointerException ex){
                ex.printStackTrace();
            }
        }
    }

    public void popVideoSample(boolean render) {
        // dequeue available buffers and synchronize our data structures with the codec.

        if (mVideoDecoder == null) return;

        updateVideo();
        if (!mAvailableVideoOutputBuffers.isEmpty()) {
            int index = mAvailableVideoOutputBuffers.remove();

//            if (render && mOutputSampleListener != null) {
//                ByteBuffer buffer = mVideoOutputBuffers[index];
//                MediaCodec.BufferInfo info = mAudioOutputBufferInfo[index];
//                mOutputSampleListener.outputSample(this, info, buffer);
//            }

            // releases the buffer back to the codec
            try {
                mVideoDecoder.releaseOutputBuffer(index, render);
            } catch (IllegalStateException | NullPointerException ex){
                ex.printStackTrace();
            }
        }
    }

    public int readAudioSampleData( MediaBuffer buffer ) { //ByteBuffer buf, int offset, long time, int flags) {
        int cnt = 0;

        if (mAudioDecoder == null) return -1;

        if (mExtractor.getSampleTrackIndex() == mAudioIndex) {
            cnt = mExtractor.readSampleData(buffer.buf, 0);

            if (cnt > 0) {
                buffer.buf.position(0);
                buffer.buf.limit(cnt);
                buffer.time = mExtractor.getSampleTime();
                buffer.flags = mExtractor.getSampleFlags();
            }
            if ( mExtractor.advance() == false && cnt <= 0) {
                cnt = -1;
            }
        }
        else if (mVideoIndex == -1) {
            if ( mExtractor.advance() == false ) {
                cnt = -1;
            };
        }
        return cnt;
    }

    public int readVideoSampleData( MediaBuffer vBuf ) { //ByteBuffer buf, int offset, long time, int flags) {
        int cnt = -1;

        if (mVideoDecoder == null) return -1;

        int index = mExtractor.getSampleTrackIndex();

        if (index == mVideoIndex) {
            cnt = mExtractor.readSampleData(vBuf.buf, vBuf.offset);

            if (cnt > 0) {
                vBuf.buf.position(0);
                vBuf.buf.limit(cnt);
                vBuf.time = mExtractor.getSampleTime();
                vBuf.flags = mExtractor.getSampleFlags();
            }

            if ( mExtractor.advance() == false && cnt <= 0) {
                cnt = -1;
            }
        }
        else if (index != mAudioIndex) {
            mExtractor.advance();
        }
        return cnt;
    }

    /**
     * Synchronize this object's state with the internal state of the wrapped
     * MediaCodec.
     */
    private void updateAudio() {

        int index;

        // Get valid input buffers from the codec to fill later in the same order they were
        // made available by the codec.
        while ((index = mAudioDecoder.dequeueInputBuffer(0)) != MediaCodec.INFO_TRY_AGAIN_LATER) {
            mAvailableAudioInputBuffers.add(index);
        }
        
        // Likewise with output buffers. If the output buffers have changed, start using the
        // new set of output buffers. If the output format has changed, notify listeners.
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        while ((index = mAudioDecoder.dequeueOutputBuffer(info, 0)) !=  MediaCodec.INFO_TRY_AGAIN_LATER) {
            switch (index) {
                case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                    mAudioOutputBuffers = mAudioDecoder.getOutputBuffers();
                    mAudioOutputBufferInfo = new MediaCodec.BufferInfo[mAudioOutputBuffers.length];
                    mAvailableAudioOutputBuffers.clear();
                    break;
                case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                    if (mAudioOutputFormatChangedListener != null) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mAudioOutputFormatChangedListener
                                        .outputFormatChanged(MediaCodecWrapper.this,
                                                mAudioDecoder.getOutputFormat());

                            }
                        });
                    }
                    break;
                default:
                    // Making sure the index is valid before adding to output buffers. We've already
                    // handled INFO_TRY_AGAIN_LATER, INFO_OUTPUT_FORMAT_CHANGED &
                    // INFO_OUTPUT_BUFFERS_CHANGED i.e all the other possible return codes but
                    // asserting index value anyways for future-proofing the code.
                    if(index >= 0) {
                        mAudioOutputBufferInfo[index] = info;
                        mAvailableAudioOutputBuffers.add(index);
                    } else {
                        throw new IllegalStateException("Unknown status from dequeueOutputBuffer");
                    }
                    break;
            }
        }
    }

    private void updateVideo() {

        int index;

        if (mVideoDecoder == null) return;

        // Get valid input buffers from the codec to fill later in the same order they were
        // made available by the codec.
        while ((index = mVideoDecoder.dequeueInputBuffer(0)) != MediaCodec.INFO_TRY_AGAIN_LATER) {
            mAvailableVideoInputBuffers.add(index);
        }
        
        // Likewise with output buffers. If the output buffers have changed, start using the
        // new set of output buffers. If the output format has changed, notify listeners.
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        while ((index = mVideoDecoder.dequeueOutputBuffer(info, 0)) !=  MediaCodec.INFO_TRY_AGAIN_LATER) {
            switch (index) {
                case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                    mVideoOutputBuffers = mVideoDecoder.getOutputBuffers();
                    mVideoOutputBufferInfo = new MediaCodec.BufferInfo[mVideoOutputBuffers.length];
                    mAvailableVideoOutputBuffers.clear();
                    break;
                case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                    if (mVideoOutputFormatChangedListener != null) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mVideoOutputFormatChangedListener
                                        .outputFormatChanged(MediaCodecWrapper.this,
                                                mVideoDecoder.getOutputFormat());

                            }
                        });
                    }
                    break;
                default:
                    // Making sure the index is valid before adding to output buffers. We've already
                    // handled INFO_TRY_AGAIN_LATER, INFO_OUTPUT_FORMAT_CHANGED &
                    // INFO_OUTPUT_BUFFERS_CHANGED i.e all the other possible return codes but
                    // asserting index value anyways for future-proofing the code.
                    if (index >= 0) {
                        mVideoOutputBufferInfo[index] = info;
                        mAvailableVideoOutputBuffers.add(index);
                    } else {
                        throw new IllegalStateException("Unknown status from dequeueVideoOutputBuffer");
                    }
                    break;
            }
        }
    }
    
    public class WriteException extends Throwable {
        private WriteException(final String detailMessage) {
            super(detailMessage);
        }
    }
}

package bacch.player.bacchplayer.viewmodel

import android.arch.lifecycle.MutableLiveData
import bacch.player.bacchplayer.model.Song


/**
 * Created by Ruslan Ivakhnenko on 07.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class PlayerMediatorImpl : PlayerMediator {
    private var song: Song? = null

    private var currentTime: MutableLiveData<Long> = MutableLiveData()

    override fun actionPlay() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPlay() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun actionForward() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onForward() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTimeChanged(time: Long) {
        currentTime.postValue(time)
    }
}
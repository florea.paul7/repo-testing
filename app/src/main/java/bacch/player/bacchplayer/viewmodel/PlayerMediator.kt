package bacch.player.bacchplayer.viewmodel


/**
 * Created by Ruslan Ivakhnenko on 07.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface PlayerMediator {

    fun actionPlay()

    fun onPlay()

    fun actionForward()

    fun onForward()

    fun onTimeChanged(time : Long)


}
package bacch.player.bacchplayer.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.net.Uri
import android.view.SurfaceHolder
import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.model.Video
import bacch.player.bacchplayer.player.BacchPlayer
import bacch.player.bacchplayer.player.BackwardMediaListener
import bacch.player.bacchplayer.repository.MediaRepository


/**
 * Created by Ruslan Ivakhnenko on 14.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class PlayerViewModel(application: Application) : AndroidViewModel(application) {

    var buttonShuffleAction  = MutableLiveData<List<Song>>()

    var buttonLockAction  = MutableLiveData<Boolean>()

    var playingSong = MutableLiveData<Song>()

    var playerMediator : PlayerMediator = PlayerMediatorImpl()
        private set

    private val repository = MediaRepository(application)

    var bacchModeEnabled : MutableLiveData<Boolean> = MutableLiveData()

    private val player : BacchPlayer = BacchPlayer(this, application)

    var currentPlayingSong : Song? = null
        private set

    fun getSongs(uri : Uri) : LiveData<List<Song>> = repository.getSongs(uri)

    fun getVideos() : LiveData<List<Video>> = repository.getVideos()

    fun playSong(song : Song, listener : BackwardMediaListener?) {
        stopPlayer()
        player.play(song.data, listener)
        currentPlayingSong = song
    }

    fun playVideo(video : Video, listener : BackwardMediaListener?, surfaceHolder : SurfaceHolder?){
        stopPlayer()
        player.play(video.imageUri, listener, surfaceHolder)
//        currentPlayingSong = song
    }

    fun isPlayingPlayer() = player.isPlaying()

    fun isStopPlayer() = player.isStop()

    fun pausePlayer() = player.pause()

    fun stopPlayer() = player.stop()

    fun seekPlayer(newTime : Long) = player.seek(newTime)

    override fun onCleared() {
        super.onCleared()
        player.stopAndDestroy()
    }
}
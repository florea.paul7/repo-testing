package bacch.player.bacchplayer

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity : AppCompatActivity() {

    private lateinit var viewModel : PlayerViewModel

    private var songList : List<Song>? = null

    private var songIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)

        setContentView(R.layout.activity_player)

        imageViewPlay.setOnClickListener {

            if (viewModel.isStopPlayer()){
                playSong(songIndex)
                (it as ImageView).setImageResource(android.R.drawable.ic_media_pause)
            } else {
                if (viewModel.isPlayingPlayer()){
                    (it as ImageView).setImageResource(android.R.drawable.ic_media_play)
                } else {
                    (it as ImageView).setImageResource(android.R.drawable.ic_media_pause)
                }
                viewModel.pausePlayer()
            }

        }

        imageViewNext.setOnClickListener {
            songIndex +=1
            playSong(songIndex)
        }

        imageViewPrev.setOnClickListener {
            songIndex -=1
            playSong(songIndex)
        }

//        viewModel.currentTime.observe(this, Observer {
//            val totalTime = viewModel.totalTime.value!!
//            if (it!! < totalTime){
//                val percent = (it * 100 / totalTime).toInt()
//                progressBar.progress = percent
//            }
//            textViewStartTime.text =  convertLongToTime(it)
//        })

//        viewModel.playerTimer.observe(this, Observer {
//            val time = convertLongToTime(it?.currentTime ?: 0)
//            textViewStartTime.text = time
//        })

        viewModel.getSongs(Uri.EMPTY).observe(this,  Observer {
            songList = it ?: null
        })

    }

    private fun convertLongToTime(time : Long) : String {
        val min = time/60000
        val sec = (time % 60000)/1000
        return String.format("%02d:%02d", min, sec)
    }

    private fun playSong(index : Int){
        val song = songList?.get(index)
        if (song != null) {
            viewModel.playSong(song, null)
            textViewTotalTime.text = convertLongToTime(song.duration)
        }
    }
}

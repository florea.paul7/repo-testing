package bacch.player.bacchplayer.services

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.IBinder
import android.util.Log

class MediaPlayerService : Service() {

    private lateinit var broadcastReceiver : HeadsetBroadcastReceiver

    override fun onCreate() {
        super.onCreate()
        broadcastReceiver = HeadsetBroadcastReceiver()
        val intentFilter = IntentFilter(AudioManager.ACTION_HEADSET_PLUG)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }


    class HeadsetBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.i("---", intent?.getIntExtra("state", 0).toString())
        }

    }
}

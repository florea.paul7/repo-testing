package bacch.player.bacchplayer.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.Switch;
import bacch.player.bacchplayer.R;


/**
 * Created by Ruslan Ivakhnenko on 09.01.2019.
 * <p>
 * e-mail: ruslan1910@gmail.com
 */
public class BacchModeSwitch extends Switch {

    private SharedPreferences preferences;

    public BacchModeSwitch(Context context) {
        super(context);
    }

    public BacchModeSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise(context);
    }

    public BacchModeSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialise(context);
    }

    public BacchModeSwitch(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialise(context);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        initialise(getContext());
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setOnCheckedChangeListener(null);
    }

    private void initialise(Context context){
        if (preferences == null) {
            final Context tcontext = context;
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            Resources resources = getResources();
            boolean checked = preferences.getBoolean(resources.getString(R.string.pref_setting_bacch_mode), resources.getBoolean(R.bool.default_bacch_mode));
            setChecked(checked);
            setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    preferences.edit().putBoolean(tcontext.getResources().getString(R.string.pref_setting_bacch_mode), isChecked).apply();
                }
            });
        }
    }
}

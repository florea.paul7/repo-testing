package bacch.player.bacchplayer.view

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.res.ColorStateList
import android.databinding.DataBindingUtil
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.FrameLayout
import android.widget.ImageView
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.databinding.ContentBottomPlayerAllBinding
import bacch.player.bacchplayer.model.RepeatState
import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.player.BackwardMediaListener
import bacch.player.bacchplayer.utils.BindingUtil
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import java.util.*

class PlayerView(context : Context, attrs : AttributeSet)
    : FrameLayout(context, attrs), BackwardMediaListener {

    private lateinit var activity : FragmentActivity

    private var isAllowRenderProgressBar = true

    private var repeatState : RepeatState = RepeatState.REPEAT_OFF

    private var mBinding : ContentBottomPlayerAllBinding =
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.content_bottom_player_all, this, true)

    private val viewModel : PlayerViewModel = ViewModelProviders.of(context as FragmentActivity).get(PlayerViewModel::class.java)

    private var songList : List<Song> = listOf()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        activity = context as FragmentActivity
        mBinding.actionPlay.setOnClickListener {
            if (viewModel.isStopPlayer()){
                mBinding.song?.let { s -> play(s) }
            } else {
                if (viewModel.isPlayingPlayer()){
                    (it as ImageView).setImageResource(R.drawable.play)
                } else {
                    (it as ImageView).setImageResource(R.drawable.pause)
                }
                viewModel.pausePlayer()
            }

        }

        //Forward button click
        mBinding.actionForward.setOnClickListener {
            it.isClickable = false
            it.isEnabled = false
            val index = generateForwardIndex()
            if (index != -1) {
                play(songList[index])
            }
            Handler().postDelayed(r, 750)
        }

        //Backward button click
        mBinding.actionBackward.setOnClickListener {
            it.isClickable = false
            it.isEnabled = false
            val index = generateBackwardIndex()
            if (index != -1){
                play(songList[index])
            }
            Handler().postDelayed(r, 750)
        }

        mBinding.actionShuffle.setOnClickListener {
            Collections.shuffle(songList)
            viewModel.buttonShuffleAction.postValue(songList)
        }

        //Progressbar click
        mBinding.progressBar.setOnTouchListener { v, event ->
            Log.i("---", "Touch position x = ${event.x} and y = ${event.y} and width = ${v.width}")
            val duration = getSongDuration()
            if (event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_MOVE){
                isAllowRenderProgressBar = false
                mBinding.progressBar.progress = event.x.times(100).div(v.width).toInt()
            }
            if (event.action == MotionEvent.ACTION_UP) {

                isAllowRenderProgressBar = true
                val currentPercentPosition = event.x.div(v.width)
                viewModel.seekPlayer(duration.times(currentPercentPosition).times(1000).toLong())
            }
            if (event.action == MotionEvent.ACTION_CANCEL){
                isAllowRenderProgressBar = true
            }

            return@setOnTouchListener true
        }

//        viewModel.playerTimer.observe(activity, Observer {
//            val totalDuration = getSongDuration()
//            val currentTime = it?.currentTime ?: 0
//            if (currentTime < totalDuration){
//                val percent = currentTime.times(100).div(totalDuration).toInt()
//                setProgress(percent, true)
//            }
//            BindingUtil.convertToTime(mBinding.textViewTime, it?.currentTime ?: 0)
//        })
        mBinding.actionRepeat.setOnClickListener {
            repeatState = repeatState.changeRepeat()
            when (repeatState){
                RepeatState.REPEAT_OFF -> {
                    mBinding.actionRepeat.setImageResource(R.drawable.ic_repeat)
                    mBinding.actionRepeat.backgroundTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.colorAccent))
                }
                RepeatState.REPEAT_ONE -> {
                    mBinding.actionRepeat.setImageResource(R.drawable.ic_repeat_one_black_24dp)
                    mBinding.actionRepeat.backgroundTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.colorAccentPressed))
                }

                RepeatState.REPEAT_PLAYLIST -> {
                    mBinding.actionRepeat.setImageResource(R.drawable.ic_repeat)
                    mBinding.actionRepeat.backgroundTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.colorAccentPressed))
                }

            }
        }
    }

    val r = Runnable {
        mBinding.actionForward.isEnabled = true
        mBinding.actionForward.isClickable = true
        mBinding.actionBackward.isEnabled = true
        mBinding.actionBackward.isClickable = true
    }


    fun play(song : Song){
        mBinding.song = song
//        if (viewModel.currentPlayingSong?.equals(song) == false) {
//            viewModel.stopPlayer()
//        }
        viewModel.playSong(song, this)
        mBinding.actionPlay.setImageResource(R.drawable.pause)
    }

    fun setSongsList(songs : List<Song>){
        songList = songs
    }

    private fun getSongDuration() : Long = mBinding.song?.duration ?: 0

    private fun setProgress(progress : Int, isAnimate : Boolean){
        if (isAllowRenderProgressBar) {
            mBinding.progressBar.incrementProgressBy(progress.minus(mBinding.progressBar.progress))
        }
    }

    override fun onTimeChanged(time: Long) {
        val totalDuration = getSongDuration()
//        val currentTime = time
        if (time < totalDuration){
            val percent = time.times(100).div(totalDuration).toInt()
            setProgress(percent, true)
        }
        post { BindingUtil.convertToTime(mBinding.textViewTime, time) }

    }

    override fun onFinished() {
        val index = generateForwardIndex()
        if (index != -1) {
            play(songList[index])
        } else {
            stop()
        }

    }

    private fun generateForwardIndex() : Int {
        var index = songList.indexOf(mBinding.song)
        var newIndex = index.inc()

        if (repeatState == RepeatState.REPEAT_OFF && newIndex >= songList.size){
            newIndex = -1
        } else if (repeatState == RepeatState.REPEAT_ONE){
            newIndex = index
        } else if (repeatState == RepeatState.REPEAT_PLAYLIST && index == songList.size - 1){
            newIndex = 0
        }
        return newIndex
    }

    private fun generateBackwardIndex() : Int {
        var index = songList.indexOf(mBinding.song)
        var newIndex = index.dec()

        if (repeatState == RepeatState.REPEAT_OFF && newIndex < 0){
            newIndex = -1
        } else if (repeatState == RepeatState.REPEAT_ONE){
            newIndex = index
        } else if (repeatState == RepeatState.REPEAT_PLAYLIST && newIndex < 0){
            newIndex = songList.size - 1
        }
        return newIndex
    }

    fun stop(){
        viewModel.stopPlayer()
        mBinding.actionPlay.setImageResource(R.drawable.play)
    }
}
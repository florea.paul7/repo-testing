package bacch.player.bacchplayer.model


/**
 * Created by Ruslan Ivakhnenko on 15.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class Song(var id :     Int,
           var title :  String,
           var artist : String,
           var album :  String,
           var duration : Long,
           var data  :  String,
           var imageId : Int,
           var imageUri : String){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Song

        if (id != other.id) return false
        if (title != other.title) return false
        if (artist != other.artist) return false
        if (album != other.album) return false
        if (duration != other.duration) return false
        if (data != other.data) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + title.hashCode()
        result = 31 * result + artist.hashCode()
        result = 31 * result + album.hashCode()
        result = 31 * result + duration.hashCode()
        result = 31 * result + data.hashCode()
        return result
    }
}
package bacch.player.bacchplayer.model

enum class RepeatState {

    REPEAT_OFF, REPEAT_ONE, REPEAT_PLAYLIST;

    fun changeRepeat(): RepeatState {
        var newOriginal = ordinal.inc()
        if (newOriginal >= values().size) {
            newOriginal = 0
        }
        return when (newOriginal) {
            REPEAT_OFF.ordinal -> REPEAT_OFF
            REPEAT_ONE.ordinal -> REPEAT_ONE
            else               -> REPEAT_PLAYLIST
        }
    }
}
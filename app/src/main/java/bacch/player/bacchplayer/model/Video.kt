package bacch.player.bacchplayer.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Ruslan Ivakhnenko on 15.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
@Parcelize
class Video(var id :     Int,
            var fileName :  String,
            var duration : Long,
            var width :  Int,
            var height : Int,
            var imageUri : String) : Parcelable{

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Video

        if (id != other.id) return false
        if (fileName != other.fileName) return false
        if (duration != other.duration) return false
        if (width != other.width) return false
        if (height != other.height) return false
        if (imageUri != other.imageUri) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + fileName.hashCode()
        result = 31 * result + duration.hashCode()
        result = 31 * result + width.hashCode()
        result = 31 * result + height.hashCode()
        result = 31 * result + imageUri.hashCode()
        return result
    }
}
package bacch.player.bacchplayer.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.databinding.FragmentBottomPlayerBinding


/**
 * Created by Ruslan Ivakhnenko on 24.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class BottomPlayerFragment : BottomSheetDialogFragment() {

    private lateinit var mBinding : FragmentBottomPlayerBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate<FragmentBottomPlayerBinding>(inflater, R.layout.fragment_bottom_player, container, false)
        return mBinding.root
    }
}
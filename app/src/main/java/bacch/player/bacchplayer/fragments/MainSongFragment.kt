package bacch.player.bacchplayer.fragments

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bacch.player.bacchplayer.R
import kotlinx.android.synthetic.main.fragment_songs.*


/**
 * Created by Ruslan Ivakhnenko on 21.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class MainSongFragment : Fragment() {

    private var mSectionsPagerAdapter: SongSectionsPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_songs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mSectionsPagerAdapter = SongSectionsPagerAdapter(requireFragmentManager())

        viewPager.adapter = mSectionsPagerAdapter

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
    }

    override fun onStop() {
        super.onStop()
//        viewPager.adapter = null
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SongSectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): SongFragment {

            val uri = when (position){
                0 -> android.provider.MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI
                1 -> android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI
                2 -> android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                3 -> android.provider.MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI
                else -> android.provider.MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI
            }
            val args = Bundle()
            args.putParcelable(SongFragment.EXTRA_KEY_SOURCE, uri)
            val songFragment = SongFragment()
            songFragment.arguments = args
            return songFragment
        }

        override fun getCount(): Int {
            return 5
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position){
                0 -> "Artists"
                1 -> "Albums"
                2 -> "Tracks"
                3 -> "Genres"
                else -> "Playlists"
            }
        }
    }
}
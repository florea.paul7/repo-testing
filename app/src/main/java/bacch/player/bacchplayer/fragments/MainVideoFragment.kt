package bacch.player.bacchplayer.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bacch.player.bacchplayer.adapter.VideoRecyclerAdapter
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.listeners.OnVideoItemClickListener
import bacch.player.bacchplayer.model.Video
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import kotlinx.android.synthetic.main.fragment_videos.*


/**
 * Created by Ruslan Ivakhnenko on 21.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class MainVideoFragment : Fragment(), OnVideoItemClickListener {

    private lateinit var viewModel : PlayerViewModel

    private var videoClickListener : OnVideoItemClickListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(PlayerViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_videos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getVideos().observe(this, Observer {
            val adapter = VideoRecyclerAdapter(it ?: emptyList(), this)
            recyclerViewVideo.adapter = adapter
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnVideoItemClickListener){
            videoClickListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        videoClickListener = null
    }

    override fun onVideoClick(video: Video) {
        videoClickListener?.onVideoClick(video)
    }
}
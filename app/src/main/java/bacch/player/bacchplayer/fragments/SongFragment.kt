package bacch.player.bacchplayer.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bacch.player.bacchplayer.adapter.SongRecyclerAdapter
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.listeners.OnMediaItemClickListener
import bacch.player.bacchplayer.listeners.OnPlaylistChangeListener

import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import kotlinx.android.synthetic.main.fragment_item_list.*


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 */
class SongFragment : Fragment(), OnMediaItemClickListener {

    companion object {
        const val EXTRA_KEY_SOURCE = "bacch.MEDIA_SOURCE"
    }
    private var columnCount = 1

    private lateinit var viewModel : PlayerViewModel

    private var adapter : SongRecyclerAdapter? = null

    private var mediaClickListener : OnMediaItemClickListener? = null

    private lateinit var sourceUri : Uri

    private var songList = ArrayList<View>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sourceUri = arguments?.getParcelable<Uri>(EXTRA_KEY_SOURCE) ?: Uri.EMPTY

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(PlayerViewModel::class.java)
        }

        viewModel.buttonShuffleAction.observe( this, Observer {
            Log.i("-----", "Notify shuffle")
            adapter?.setData(it ?: listOf())
        }
        );

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)


        //recyclerView = activity?.findViewById<RecyclerView>(R.id.recyclerView)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getSongs(sourceUri).observe(this, Observer {
            (activity as OnPlaylistChangeListener).OnPlaylistChanged(it ?: listOf())
            adapter = SongRecyclerAdapter(it ?: listOf(), this);
            recyclerView.adapter = adapter
//            recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        })
        val itemCount = adapter?.itemCount ?: 0
        for (i in 0 until itemCount ) {
            val view = recyclerView.getLayoutManager()!!.findViewByPosition(i) ?: null
            if (view != null) {
                songList.add(view)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMediaItemClickListener){
            mediaClickListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        mediaClickListener = null
    }

    override fun onClick(song: Song) {

        adapter?.enableList(false)
        //setViewAndChildrenEnabled(recyclerView, false)

        mediaClickListener?.onClick(song)

        Handler().postDelayed(r, 750)

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Song){
            Log.i("OLFIL", "We have been interacted")
        }
    }

    private fun setViewAndChildrenEnabled(view: View, enabled: Boolean) {
        view.isEnabled = enabled
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val child = view.getChildAt(i)
                setViewAndChildrenEnabled(child, enabled)
            }
        }
    }

    val r = Runnable {
        adapter?.enableList(true)
    }
}

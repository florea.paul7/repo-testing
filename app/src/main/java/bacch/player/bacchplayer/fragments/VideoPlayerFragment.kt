package bacch.player.bacchplayer.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.ImageView
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.databinding.FragmentVideoPlayerBinding
import bacch.player.bacchplayer.model.Video
import bacch.player.bacchplayer.player.BackwardMediaListener
import bacch.player.bacchplayer.utils.BindingUtil
import bacch.player.bacchplayer.viewmodel.PlayerViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class VideoPlayerFragment : Fragment(), BackwardMediaListener, SurfaceHolder.Callback {

    companion object {
        val EXTRA_VIDEO = "bacch.EXTRA_VIDEO"
        val EXTRA_VIDEO_LIST = "bacch.EXTRA_VIDEO_LIST"
    }

    private lateinit var mBinding: FragmentVideoPlayerBinding

    private var isRepeat = false

    private lateinit var video : Video

    private var videoList : List<Video> = listOf()

    private lateinit var viewModel : PlayerViewModel

    private lateinit var mSurfaceHolder: SurfaceHolder

    private var pausedSeek: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(PlayerViewModel::class.java)
        video = arguments?.getParcelable(EXTRA_VIDEO) ?: throw NullPointerException("You need to put video file to play")
        viewModel.getVideos().observe(this, Observer {
            videoList = it ?: listOf()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_player, container, false )
        val surfaceView = mBinding.root.findViewById<SurfaceView>(R.id.surfaceView)
        mSurfaceHolder = surfaceView.holder
        mSurfaceHolder.addCallback(this)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.video = video
        mBinding.actionPlay.setOnClickListener {
            if (viewModel.isStopPlayer()){
//                mBinding.video?.let { s -> play(s) }
            } else {
                if (viewModel.isPlayingPlayer()){
                    (it as ImageView).setImageResource(R.drawable.play)
                } else {
                    (it as ImageView).setImageResource(R.drawable.pause)
                }
                viewModel.pausePlayer()
            }
        }

        mBinding.actionForward.setOnClickListener {
            val index = generateForwardIndex()
            if (index != -1) {
                play(videoList[index])
            }
        }

        //Backward button click
        mBinding.actionBackward.setOnClickListener {
            var index = videoList.indexOf(mBinding.video ?: null)
            if (index  > 0){
                index = index.dec()
                play(videoList[index])
            } else if (isRepeat && index == 0){
                index = videoList.size - 1
                play(videoList[index])
            }
        }

        mBinding.actionShuffle.setOnClickListener {
            //Collections.shuffle(videoList)
//            viewModel.buttonShuffleAction.postValue(videoList)

            var activity = activity
            activity?.onBackPressed()
        }

        mBinding.progressBar.setOnTouchListener { v, event ->
            Log.i("---", "Touch position x = ${event.x} and y = ${event.y} and width = ${v.width}")

            if (viewModel.isPlayingPlayer()) {
                if (event.action == MotionEvent.ACTION_UP) {
                    mBinding.progressBar.isEnabled = false
                    mBinding.progressBar.isClickable = false
                    val duration = getSongDuration()
                    val currentPercentPosition = event.x.div(v.width)
                    viewModel.seekPlayer(duration.times(currentPercentPosition).times(1000).toLong())

                    Handler().postDelayed(r, 2000)
                }
//                else {
//                    val percent = event.x.div(v.width)
//                    pausedSeek = percent.toInt()
//                    mBinding.progressBar.progress = (percent*100).toInt()
//                }
            }
            return@setOnTouchListener true
        }
    }

    val r = Runnable {
        mBinding.progressBar.isEnabled = true
        mBinding.progressBar.isClickable = true
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        this.mSurfaceHolder = holder
        this.mSurfaceHolder.setKeepScreenOn(true)
        play(video)
    }

    override fun surfaceChanged(
        holder: SurfaceHolder, format: Int, width: Int,
        height: Int
    ) {

    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        viewModel.stopPlayer()
    }

    fun play(video : Video){
        mBinding.video = video
        viewModel.playVideo(video, this, mSurfaceHolder)
        mBinding.actionPlay.setImageResource(R.drawable.pause)
    }

    override fun onStop() {
        super.onStop()
        viewModel.stopPlayer()
    }

    override fun onTimeChanged(time: Long) {
        val totalDuration = getSongDuration()
        if (time < totalDuration){
            val percent = time.times(100).div(totalDuration).toInt()
            mBinding.progressBar.incrementProgressBy(percent.minus(mBinding.progressBar.progress))
        }

        mBinding.textViewCurrentTime.post { BindingUtil.convertToTime(mBinding.textViewCurrentTime, time) }

//        if (pausedSeek > 0) {
//            Handler().postDelayed(seekOnPlay, 500)
//        }
    }

//    val seekOnPlay = Runnable {
//        val duration = getSongDuration()
//        val currentPercentPosition = pausedSeek
//        viewModel.seekPlayer(duration.times(currentPercentPosition).times(1000).toLong())
//        pausedSeek = 0
//    }


    override fun onFinished() {
        //val index = generateForwardIndex()
        //if (index != -1){
            //play(videoList[index])
        //}
    }

    private fun getSongDuration() : Long = mBinding.video?.duration ?: 0

    private fun generateForwardIndex() : Int {
        var index = videoList.indexOf(mBinding.video)
        if (index > -1 && index != videoList.size - 1){
            index = index.inc()
        } else if (isRepeat && index == videoList.size - 1){
            index = 0
        }
        return index
    }
}

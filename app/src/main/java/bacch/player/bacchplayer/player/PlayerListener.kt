package bacch.player.bacchplayer.player


/**
 * Created by Ruslan Ivakhnenko on 18.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface PlayerListener {

    fun onPause()

    fun onStop()

    fun isPlaying() : Boolean

    fun isStop() : Boolean

    fun onTotalTime(totalTime : Long)

    fun onTimeChanged(currentTime : Long)

    fun setCurrentTime(newTime : Long)

    fun onBacchModeChanged(mode : Boolean)
}
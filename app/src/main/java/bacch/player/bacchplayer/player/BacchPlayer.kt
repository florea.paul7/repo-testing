package bacch.player.bacchplayer.player

import android.arch.lifecycle.Observer
import android.content.Context
import android.view.SurfaceHolder
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future


/**
 * Created by Ruslan Ivakhnenko on 17.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class BacchPlayer(private val viewModel : PlayerViewModel, private val context: Context) {

    private val commandExecutor : ExecutorService = Executors.newSingleThreadExecutor()

    private var playerListener: Future<PlayerListener>? = null

    private val bacchModeObserver  = Observer<Boolean> {
        playerListener?.get()?.onBacchModeChanged(it ?: true)
    }
    init {
        viewModel.bacchModeEnabled.observeForever(bacchModeObserver)
    }

    fun play(path : String, backwardListener : BackwardMediaListener?, surfaceHolder : SurfaceHolder? = null){

        if (playerListener == null) {
            playerListener = commandExecutor.submit(PlayCommand(path, backwardListener, surfaceHolder, context))
            playerListener?.get()?.onBacchModeChanged(viewModel.bacchModeEnabled.value ?: true)
        }
    }

    fun stop(){
        playerListener?.get()?.onStop()
        playerListener = null
    }


    fun pause(){
        playerListener?.get()?.onPause()
    }

    fun isPlaying() : Boolean = playerListener?.get()?.isPlaying() ?: false

    fun isStop() : Boolean = (playerListener == null || playerListener?.get()?.isStop() ?: true)

    fun seek(newPosition : Long) {
        playerListener?.get()?.setCurrentTime(newPosition)
    }

    fun stopAndDestroy(){
        playerListener?.get()?.onStop()
        viewModel.bacchModeEnabled.removeObserver(bacchModeObserver)
    }
}
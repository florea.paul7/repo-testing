package bacch.player.bacchplayer.player

interface BackwardMediaListener {

    fun onTimeChanged(time : Long)

    fun onFinished()
}
package bacch.player.bacchplayer.player

import android.content.Context
import android.content.SharedPreferences
import android.view.SurfaceHolder
import android.net.Uri
import android.os.AsyncTask
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import bacch.audio.BACCHAudio
import bacch.player.bacchplayer.SongsActivity
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.media.MediaSyncWrapper


/**
 * Created by Ruslan Ivakhnenko on 17.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class PlayCommand(val path : String, val backwardListener : BackwardMediaListener?, private val surfaceHolder: SurfaceHolder? = null, val context: Context) :
    BacchPlayerCommand<PlayerListener>,
    PlayerListener,
    MediaSyncWrapper.PlaybackEventListener,
    SharedPreferences.OnSharedPreferenceChangeListener {

    private var mMediaSync: MediaSyncWrapper? = null

    private var preferences: SharedPreferences

    private var isPaused : Boolean = false

    private var isStopped : Boolean = false

    private var mBAEnabled : Boolean = true

    private var isHeadSet : Boolean = false

    private var hasVideo : Boolean = false

    private var mHandler =  Handler()

    init {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
        preferences.registerOnSharedPreferenceChangeListener(this)
        mBAEnabled = preferences.getBoolean(context.resources.getString(R.string.pref_setting_bacch_mode), true)
    }

    override fun call(): PlayerListener {
        return doCommand()
    }

    override fun doCommand() : PlayerListener {

        if (surfaceHolder != null) {
            hasVideo = true
        } else {
            hasVideo = false
        }

        mBAEnabled = preferences.getBoolean(context.resources.getString(R.string.pref_setting_bacch_mode), true)
        doAsync().execute()

        return this
    }

    private fun initializePlayer() {
        mMediaSync = MediaSyncWrapper(this, surfaceHolder)

        isHeadSet = preferences.getBoolean(SongsActivity.EXTRA_HEADSET_CONNECTED, false)
        mBAEnabled = preferences.getBoolean(context.resources.getString(R.string.pref_setting_bacch_mode), true)

        if (hasVideo) {
            mMediaSync?.playAudioAndVideo(Uri.parse(path), isHeadSet, mBAEnabled)
        } else {
            mMediaSync?.playAudio(Uri.parse(path), isHeadSet, mBAEnabled)
        }


    }

    val setHeadset = Runnable {

        var mode = BACCHAudio.MODE_SPEAKER or BACCHAudio.MODE_LCREX
        var includeRIR = 1
        if (isHeadSet) {
            mode = BACCHAudio.MODE_HEADPHONE or BACCHAudio.MODE_LCREX
            includeRIR = 0
        }
        Log.i("PC", "initialize HEADSET - Player setting HP: " + isHeadSet + " long RIR: " + (includeRIR==1));
        mMediaSync?.setBACCHMode(mode, true)
        mMediaSync?.setRIRMode(includeRIR)

    }

    inner class doAsync: AsyncTask<Void, Void, Boolean>()  {
        override fun doInBackground(vararg params: Void?): Boolean {
            initializePlayer()
            return true
        }
    }

    override fun onPause() {
        isPaused = !isPaused
        if (isPaused) {
            mMediaSync?.pause()
        } else {
            mMediaSync?.resume()
        }
    }

    override fun onStop() {
        isStopped = true

        mMediaSync?.tearDown()

        onTimeChanged(0)

        preferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun isPlaying(): Boolean = !isPaused

    override fun isStop(): Boolean = isStopped

    override fun onBacchModeChanged(mode: Boolean) {
        mBAEnabled = mode
        //mBA.bypassBACCH(!mBAEnabled)
    }

    override fun onTotalTime(totalTime: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTimeChanged(currentTime: Long) {
        backwardListener?.onTimeChanged(currentTime)
    }

    override fun eos() {
        backwardListener?.onFinished()
    }

    override fun setCurrentTime(newTime: Long) {
        mMediaSync?.seekTo(newTime)
    }

    override fun onSharedPreferenceChanged(prefs:SharedPreferences, key:String) {
        if (key.equals(SongsActivity.EXTRA_HEADSET_CONNECTED)) {
            val isConnected = preferences.getBoolean(SongsActivity.EXTRA_HEADSET_CONNECTED, false)
            Log.i("PC", "HEADSET_PREFERENCE isConnected: " + isConnected);
            if (isHeadSet != isConnected) {
                isHeadSet = isConnected
                var mode = BACCHAudio.MODE_SPEAKER or BACCHAudio.MODE_LCREX
                var includeRIR = 1
                if (isHeadSet) {
                    mode = BACCHAudio.MODE_HEADPHONE or BACCHAudio.MODE_LCREX
                    includeRIR = 0
                }
                Log.i("PC", "HEADSET_PREFERENCE isConnected: " + isConnected + " long RIR: " + includeRIR );
                mMediaSync?.setBACCHMode(mode, true)
                mMediaSync?.setRIRMode(includeRIR)
            }
        }
        else if (key.equals(context.resources.getString(R.string.pref_setting_bacch_mode))) {
            val isEnabled = preferences.getBoolean(context.resources.getString(R.string.pref_setting_bacch_mode), true)
            mMediaSync?.setBACCHEnable(isEnabled)
            mBAEnabled = isEnabled;
        }
    }
}
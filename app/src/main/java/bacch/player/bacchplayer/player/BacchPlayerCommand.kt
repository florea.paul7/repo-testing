package bacch.player.bacchplayer.player

import java.util.concurrent.Callable


/**
 * Created by Ruslan Ivakhnenko on 17.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface BacchPlayerCommand<T> : Callable<T> {

    fun doCommand() : T;
}
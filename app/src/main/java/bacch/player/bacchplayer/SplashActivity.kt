package bacch.player.bacchplayer

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import bacch.player.bacchplayer.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_splash)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1001)
        } else {
            Handler().postDelayed({
                if (!isFinishing){
                    startCatalogueActivity()
                }
            }, 2000)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1001 && grantResults.contains(PackageManager.PERMISSION_GRANTED)){
            startSongActivity()
        } else {
            Toast.makeText(this, "We need granted permission for read storage", Toast.LENGTH_LONG).show()
        }
    }

    private fun startSongActivity(){
        startActivity(Intent(baseContext, SongsActivity::class.java))
    }

    private fun startCatalogueActivity(){
        startActivity(Intent(baseContext, CatalogueActivity::class.java))
    }
}

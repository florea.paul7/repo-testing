package bacch.player.bacchplayer.behaviors;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import bacch.player.bacchplayer.view.PlayerView;

/**
 * Created by Ruslan Ivakhnenko on 26.12.2018.
 * <p>
 * e-mail: ruslan1910@gmail.com
 */
public class ScrollBehavior extends CoordinatorLayout.Behavior<ImageView> {

    @Override
    public boolean layoutDependsOn(@NonNull CoordinatorLayout parent, @NonNull ImageView child, @NonNull View dependency) {
        return dependency instanceof PlayerView;
    }

    @Override
    public boolean onDependentViewChanged(@NonNull CoordinatorLayout parent, @NonNull ImageView child, @NonNull View dependency) {
        ViewGroup.LayoutParams layoutParams = child.getLayoutParams();
        layoutParams.width +=100;
        layoutParams.height += 100;
        child.setLayoutParams(layoutParams);
        return true;
    }
}

package bacch.player.bacchplayer


/**
 * Created by Ruslan Ivakhnenko on 13.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
enum class SourceType {

    SOURCE_MOVIE, SOURCE_MUSIC
}
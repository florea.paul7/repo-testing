package bacch.player.bacchplayer

import android.arch.lifecycle.ViewModelProviders
import android.bluetooth.BluetoothHeadset
import android.content.*
import android.media.AudioManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ToggleButton
import bacch.player.bacchplayer.R
import bacch.player.bacchplayer.fragments.MainSongFragment
import bacch.player.bacchplayer.fragments.MainVideoFragment
import bacch.player.bacchplayer.fragments.VideoPlayerFragment
import bacch.player.bacchplayer.listeners.OnMediaItemClickListener
import bacch.player.bacchplayer.listeners.OnPlaylistChangeListener
import bacch.player.bacchplayer.listeners.OnVideoItemClickListener
import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.model.Video
import bacch.player.bacchplayer.view.PlayerView
import bacch.player.bacchplayer.viewmodel.PlayerViewModel
import kotlinx.android.synthetic.main.activity_songs.*


class SongsActivity : AppCompatActivity(),
    OnMediaItemClickListener,
    OnVideoItemClickListener,
    OnPlaylistChangeListener,
    SharedPreferences.OnSharedPreferenceChangeListener{

    companion object {
        const val EXTRA_SOURCE_TYPE = "bacch.EXTRA_SOURCE_TYPE"
        const val EXTRA_HEADSET_CONNECTED = "bacch.EXTRA_HEADSET_CONNECTED"
    }

    private lateinit var bottomSheetBehavior : BottomSheetBehavior<PlayerView>

    private lateinit var preferences: SharedPreferences

    private lateinit var viewModel : PlayerViewModel

    private lateinit var sourceType : SourceType

    private var selectedItemMenu : Int = R.id.menuItemMusic

    private lateinit var broadcastReceiver : HeadsetBroadcastReceiver

    private var is3DEnabled = true

    private var isWiredHeadset = false

    private var isAudioPlaying = false

    override fun onBackPressed() {
        playerView.visibility = View.GONE
        playerView.stop()
        if (!isAudioPlaying) {
            super.onBackPressed()
        }
        isAudioPlaying = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_songs)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)
        sourceType = SourceType.valueOf(intent.getStringExtra(EXTRA_SOURCE_TYPE) ?: SourceType.SOURCE_MUSIC.name)

        val audioMgr: AudioManager? = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        isWiredHeadset = audioMgr?.isWiredHeadsetOn() ?: false
        if (isWiredHeadset) {
            Log.i("SA", "Headset is plugged in at SongActivity Creation");
        } else {
            Log.i("SA", "Headset is UNPLUGGED at SongActivity Creation");
        }

        preferences.edit().putBoolean(EXTRA_HEADSET_CONNECTED, isWiredHeadset).apply()

        // set 3D on at app start
        //is3DEnabled = preferences.getBoolean("bacch_mode", true)

        preferences.edit().putBoolean("bacch_mode", is3DEnabled).apply()

        toggle_BACCH_MODE.setOnClickListener{ v -> onBacchClick(v) }
        Change_BACCH_MODE.setOnClickListener {
            is3DEnabled = !is3DEnabled
            if (is3DEnabled){
                textView3.text = "3D On"
                textView3.setTextColor(getColor(R.color.colorAccent))
                preferences.edit().putBoolean("bacch_mode", true).apply()
                (it as ImageButton).imageTintList = null
            } else {
                textView3.text = "3D Off"
                textView3.setTextColor(getColor(android.R.color.white))
                preferences.edit().putBoolean("bacch_mode", false).apply()
                (it as ImageButton).imageTintList = getColorStateList(android.R.color.white)
            }
        }

        navigationView.setNavigationItemSelectedListener(object: NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(p0: MenuItem): Boolean {

                if (p0.isChecked){
                    return false
                }
                return when (p0.itemId) {
                    R.id.menuItemMusic -> {
                        playerView.visibility = View.GONE
                        playerView.stop()
                        isAudioPlaying = false
                        drawerLayout.closeDrawers()
                        showFragment(MainSongFragment(), false)
                        true
                    }
                    R.id.menuItemVideo -> {
//                        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                        playerView.visibility = View.GONE
                        playerView.stop()
                        isAudioPlaying = false
                        drawerLayout.closeDrawers()
                        showFragment(MainVideoFragment(), false)
                        true
                    }
                    else -> false
                }
            }

        })

        when (sourceType) {
            SourceType.SOURCE_MUSIC -> {
                selectedItemMenu = R.id.menuItemMusic
                showFragment(MainSongFragment(), false)
            }
            SourceType.SOURCE_MOVIE -> {
                playerView.visibility = View.GONE
                selectedItemMenu = R.id.menuItemVideo
                isAudioPlaying = false
                showFragment(MainVideoFragment(), false)
            }
        }
        navigationView.setCheckedItem(selectedItemMenu)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }

//        bottomSheetBehavior = BottomSheetBehavior.from(playerView)
//
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

    }

    override fun onStop() {
        super.onStop()
        isAudioPlaying = false
        preferences.unregisterOnSharedPreferenceChangeListener(this)
        unregisterReceiver(broadcastReceiver)
    }

    override fun onStart() {
        super.onStart()
        preferences.registerOnSharedPreferenceChangeListener(this)
        broadcastReceiver = HeadsetBroadcastReceiver()

        val intentFilter = IntentFilter(AudioManager.ACTION_HEADSET_PLUG)
        intentFilter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED)
        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return when (id){
            android.R.id.home -> {
                drawerLayout.openDrawer(Gravity.START)
                true
            }
            R.id.action_settings -> true
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun onBacchClick( v: View ) {
        val bacch  = v as ToggleButton
        val bacchState = bacch.isChecked()

        preferences.edit().putBoolean("bacch_mode", bacchState).apply()
    }

    override fun onClick(song: Song) {
//        val bottomSheetBehavior = BottomSheetBehavior.from(playerView)
        playerView.play(song)

        playerView.visibility = View.VISIBLE

        isAudioPlaying = true

//        bottomSheetBehavior.peekHeight = resources.getDimensionPixelSize(R.dimen.player_peek_height)
//        var w  = imageViewSongCover.layoutParams.width
//        var h  = imageViewSongCover.layoutParams.height
//        bottomSheetBehavior.setBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback(){
//            override fun onSlide(p0: View, p1: Float) {
//                if (bottomSheetBehavior.state.equals(BottomSheetBehavior.STATE_DRAGGING)){
////                    val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, imageViewSongCover.scaleX, 1.plus(p1).toFloat())
////                    val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, imageViewSongCover.scaleY, 1.plus(p1).toFloat())
////                    ObjectAnimator.ofPropertyValuesHolder(imageViewSongCover, scaleX, scaleY).apply {
////                        duration = 100
////                    }.start()
//
//                    //TODO change image cover size
////                    val layoutParams = imageViewSongCover.layoutParams
////                    val newWidth = w.plus(w.times(p1).toInt())
////                    val newHeight = h.plus(h.times(p1).toInt())
////                    layoutParams.width = if (newWidth.compareTo(w) == -1) w else newWidth
////                    layoutParams.height = if (newHeight.compareTo(h) == -1) h else newHeight
////                    imageViewSongCover.layoutParams = layoutParams
//                }
//
//                Log.i("onSlide", p0.javaClass.simpleName.plus( " $p1 ${p0.height}"))
//            }
//
//            override fun onStateChanged(p0: View, p1: Int) {
//                val state : String
//                when (p1){
//                    BottomSheetBehavior.STATE_SETTLING -> state = "Settling"
//                    BottomSheetBehavior.STATE_HALF_EXPANDED -> state = "Half Expanded"
//                    BottomSheetBehavior.STATE_COLLAPSED -> state = "Collapsed"
//                    BottomSheetBehavior.STATE_DRAGGING -> state = "Dragging"
//                    BottomSheetBehavior.STATE_EXPANDED -> state = "Expanded"
//                    else -> state = "Hidden"
//                }
//                Log.i("onStateChanged", p0.javaClass.simpleName.plus( " $state").plus( " ${p0.height}"))
//            }
//
//        })
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onVideoClick(video: Video) {
        val fragment = VideoPlayerFragment().apply {
            val args = Bundle()
            args.putParcelable(VideoPlayerFragment.EXTRA_VIDEO, video)
            args.putParcelable(VideoPlayerFragment.EXTRA_VIDEO_LIST, null)
            arguments = args
        }
        isAudioPlaying = false
        showFragment(fragment, true)
    }

    override fun OnPlaylistChanged(playlist: List<Song>) {
        playerView.setSongsList(playlist)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        val bacchMode = resources.getBoolean(R.bool.default_bacch_mode)
        val bacchModeKey = resources.getString(R.string.pref_setting_bacch_mode)
        if (bacchModeKey == key){
            var bacchModeRead = sharedPreferences?.getBoolean(resources.getString(R.string.pref_setting_bacch_mode), bacchMode) ?: bacchMode
            viewModel.bacchModeEnabled.postValue(bacchModeRead)
            toggle_BACCH_MODE.setChecked(bacchModeRead)
        }
        else if (EXTRA_HEADSET_CONNECTED == key){
            val isConnected = sharedPreferences?.getBoolean(key, false) ?: false
            if (isConnected){
                viewModel.bacchModeEnabled.postValue(true)
            } else {
                viewModel.bacchModeEnabled.postValue(sharedPreferences?.getBoolean(bacchModeKey, bacchMode) ?: bacchMode)
            }
        }
    }


    private class HeadsetBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            var isConnected = false
            if (intent?.action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)){
                val state = intent?.getIntExtra(BluetoothHeadset.EXTRA_STATE, 0)
                isConnected = (state == 2)
            } else if (intent?.action.equals(AudioManager.ACTION_HEADSET_PLUG)) {
                val state = intent?.getIntExtra("state", 0)
                isConnected = state == 1
            }

            Log.i("SongsActivity", "Received broadcast Headset isConnected: " + isConnected )
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            preferences.edit().putBoolean(EXTRA_HEADSET_CONNECTED, isConnected).apply()
        }

    }

    private fun showFragment(fragment : Fragment, isAddToBack : Boolean){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mediaContent, fragment)
        if (isAddToBack){
            transaction.addToBackStack(fragment.toString())
        }
        transaction.commit()
    }
}

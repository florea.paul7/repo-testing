package bacch.player.bacchplayer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import bacch.player.bacchplayer.R
import kotlinx.android.synthetic.main.activity_catalogue.*

class CatalogueActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catalogue)
        setSupportActionBar(toolbar)

        cardViewMovies.setOnClickListener {
            startSongActivity(SourceType.SOURCE_MOVIE)
        }

        cardViewMusic.setOnClickListener {
            startSongActivity(SourceType.SOURCE_MUSIC)
        }

    }

    fun startSongActivity(sourceType : SourceType){
        val intent = Intent(this, SongsActivity::class.java).apply {
            putExtra(SongsActivity.EXTRA_SOURCE_TYPE, sourceType.name)
            startActivity(this)
        }
    }
}

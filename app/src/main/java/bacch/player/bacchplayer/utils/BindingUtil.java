package bacch.player.bacchplayer.utils;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;
import bacch.player.bacchplayer.R;
import bacch.player.bacchplayer.model.Song;
import bacch.player.bacchplayer.model.Video;

/**
 * Created by Ruslan Ivakhnenko on 23.12.2018.
 * <p>
 * e-mail: ruslan1910@gmail.com
 */
public class BindingUtil {

    @BindingAdapter("app:loadImageThumb")
    public static void loadImageThumb(ImageView imageView, Song song){
        if (song != null) {
            MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
            metadataRetriever.setDataSource(song.getData());
            byte[] embeddedPicture = metadataRetriever.getEmbeddedPicture();
            if (embeddedPicture != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(embeddedPicture, 0, embeddedPicture.length, options);
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                int outWidth = options.outWidth;
                int outHeight = options.outHeight;
                int sampleSize = 1;
                if (width != 0 && height != 0) {
                    if (outWidth > width || outHeight > height) {
                        if (outWidth > outHeight) {
                            sampleSize = outWidth / width;
                        } else {
                            sampleSize = outHeight / height;
                        }
                    }
                }
                options.inJustDecodeBounds = false;
                options.inSampleSize = sampleSize;
                Bitmap bitmap = BitmapFactory.decodeByteArray(embeddedPicture, 0, embeddedPicture.length, options);
                imageView.setImageBitmap(bitmap);
                metadataRetriever.release();
            } else {
                imageView.setImageResource(R.mipmap.ic_bacch_cc_foreground);
            }
        }
    }

    @BindingAdapter("app:loadVideoThumb")
    public static void loadVideoThumb(final ImageView imageView, final Video video){
        if (video != null) {
            imageView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(video.getImageUri(), MediaStore.Video.Thumbnails.MINI_KIND);
                    if (thumbnail != null){
                        imageView.setImageBitmap(thumbnail);
                    } else {
                        imageView.setImageResource(R.mipmap.ic_bacch_cc_foreground);
                    }
                }
            }, 100);

        }
    }

    @BindingAdapter("app:mediaTime")
    public static void convertToTime(TextView textView, long time){
        int min = (int) (time/60000);
        int sec = (int) ((time % 60000)/1000);
        textView.setText(String.format("%02d:%02d", min, sec));
    }
}

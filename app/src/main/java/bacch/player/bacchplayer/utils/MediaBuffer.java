package bacch.player.bacchplayer.utils;

import java.nio.ByteBuffer;

public class MediaBuffer {
    public ByteBuffer   buf;
    public int          offset;
    public long         time;
    public int          flags;
    public boolean      isSecure;
}

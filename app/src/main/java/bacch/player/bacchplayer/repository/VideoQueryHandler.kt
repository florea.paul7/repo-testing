package bacch.player.bacchplayer.repository

import android.arch.lifecycle.MutableLiveData
import android.content.AsyncQueryHandler
import android.content.ContentResolver
import android.database.Cursor
import android.provider.MediaStore
import bacch.player.bacchplayer.model.Video


/**
 * Created by Ruslan Ivakhnenko on 21.01.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class VideoQueryHandler(cr: ContentResolver?) : AsyncQueryHandler(cr) {

    override fun onQueryComplete(token: Int, cookie: Any?, cursor: Cursor?) {
        if (cursor?.count?.compareTo(0) == 1) {
            val liveData = cookie as MutableLiveData<List<Video>>
            cursor.moveToFirst()
            val videos = arrayListOf<Video>()
            do {
                val id = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media._ID))
                val fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME))
                val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION))
                val width = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media.WIDTH))
                val height = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media.HEIGHT))
                val imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA))
                val song = Video(id, fileName, duration, width, height, imagePath)
                videos.add(song)
            } while (cursor.moveToNext())
            liveData.postValue(videos)
        }
    }
}
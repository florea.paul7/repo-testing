package bacch.player.bacchplayer.repository

import android.arch.lifecycle.MutableLiveData
import android.content.AsyncQueryHandler
import android.content.ContentResolver
import android.database.Cursor
import android.provider.MediaStore
import bacch.player.bacchplayer.model.Song


/**
 * Created by Ruslan Ivakhnenko on 15.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class SongQueryHandler(cr: ContentResolver?) : AsyncQueryHandler(cr) {

    override fun onQueryComplete(token: Int, cookie: Any?, cursor: Cursor?) {
        if (cursor?.count?.compareTo(0) == 1) {
            val liveData = cookie as MutableLiveData<List<Song>>
            cursor.moveToFirst()
            val songs = arrayListOf<Song>()
            do {
                val id = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID))
                val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                val data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
//                val imageId = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID))
                val imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA))
                val song = Song(id, title, artist, album, duration, data, 0, imagePath)
                songs.add(song)
            } while (cursor.moveToNext())
            liveData.postValue(songs)
        }
    }
}
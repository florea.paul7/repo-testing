package bacch.player.bacchplayer.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.media.*
import android.net.Uri
import android.provider.MediaStore
import bacch.player.bacchplayer.model.Song
import bacch.player.bacchplayer.model.Video


/**
 * Created by Ruslan Ivakhnenko on 15.12.2018.
 *
 * e-mail: ruslan1910@gmail.com
 */
class MediaRepository(private val context : Context) {

    private val songsuri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI

    private val videosUuri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
    private val albumsuri = android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI
    private val music_selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
    private val album_sort = MediaStore.Audio.Albums.ALBUM + " ASC"

    private var mediaExtractor : MediaExtractor

    private val mBuf_Size : Int

    init {
        mediaExtractor = MediaExtractor()
        mBuf_Size = AudioTrack.getMinBufferSize(
            44100,
            AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT
        )
    }

    private val track_proj = arrayOf(
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.ALBUM,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.DATA,
        MediaStore.Images.Thumbnails.DATA
    )
    private val album_proj = arrayOf(
        MediaStore.Audio.Albums._ID,
        MediaStore.Audio.Albums.ALBUM,
        MediaStore.Audio.Albums.ALBUM_ART,
        MediaStore.Audio.Albums.ALBUM_KEY
    )

    fun getSongs(uri : Uri) : LiveData<List<Song>> {
        val handler = SongQueryHandler(context.contentResolver)
        val liveData = MutableLiveData<List<Song>>()
        handler.startQuery(0, liveData, songsuri, track_proj, music_selection, null, null)
//        handler.startQuery(0, liveData, uri, null, null, null, null)
        return liveData
    }

    fun getVideos() : LiveData<List<Video>> {
        val handler = VideoQueryHandler(context.contentResolver)
        val liveData = MutableLiveData<List<Video>>()
        handler.startQuery(0, liveData, videosUuri, null, null, null, null)
        return liveData
    }
/*
    fun startPlay(path : String){
        mediaExtractor.setDataSource(path)
        val trackFormat = mediaExtractor.getTrackFormat(0)
        mediaExtractor.selectTrack(0)
        val sampleRate = trackFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE)
        var mAudioCodecWrapper = bacch.player.media.MediaCodecWrapper.fromAudioFormat(trackFormat)
        val mAudioTrack = AudioTrack(
            AudioManager.STREAM_MUSIC,
            sampleRate, AudioFormat.CHANNEL_OUT_STEREO,
            AudioFormat.ENCODING_PCM_16BIT,
            mBuf_Size * 4, AudioTrack.MODE_STREAM
        )

        val channels = trackFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT)
        val mSpeaker = SpeakerEngine.getInstance(channels)

        val bbOut_Buf = ByteBuffer.allocateDirect(mBuf_Size * 8)
        bbOut_Buf.order(ByteOrder.nativeOrder())
        bbOut_Buf.rewind()

        val bbIn1 = ByteBuffer.allocateDirect(mBuf_Size * 8)
        bbIn1.order(ByteOrder.nativeOrder())
        bbIn1.rewind()

        var mInHeldBuf = 0
        mAudioCodecWrapper.registerOutputListener { sender, info, buffer ->
            if (mAudioTrack != null) {
                val bufRemaining: Int
                val copy_len = info.size
                val position = 0
                var written = 0
                val bufReadPosition = 0
                var bufPosition = 0
                var size = 2048
                var finalLimit = 0
                val idx = 0

                if (copy_len > mBuf_Size * 8) {
                    buffer.position(copy_len - mBuf_Size * 2)
                    buffer.limit(copy_len)
                    size = mBuf_Size * 8 / 2048
                    finalLimit = mBuf_Size * 2
                } else {
                    bbIn1.position(mInHeldBuf)
                    size = (copy_len + mInHeldBuf) / 2048 //2048;
                    finalLimit = copy_len + mInHeldBuf
                }

                bbIn1.put(buffer)
                //buffer.limit(copy_len);
                bbIn1.rewind()


                //size *= 2048;

                bbIn1.limit(size * 2048)

                mSpeaker.process(bbIn1, bbOut_Buf, size)
//                if (mSpeakerEnabled) {
//                } else {
//                    bbOut_Buf.put(bbIn1)
//                }
                bbOut_Buf.rewind()

                size *= 2048
                val holdsize = size
                while (size > 0) {
                    written = mAudioTrack.write(
                        bbOut_Buf,
                        size,
                        AudioTrack.WRITE_BLOCKING
                    ) //buf, bufPosition, size);  //buffer, info.size, AudioTrack.WRITE_BLOCKING);
                    bufPosition += written
                    size = size - written
                }

                bbIn1.limit(finalLimit)
                bbIn1.position(holdsize)
                mInHeldBuf = finalLimit - holdsize
                bbIn1.compact()
                bbOut_Buf.clear()

            }
            buffer.clear()
        }

        mAudioTrack.playbackRate = sampleRate
        mAudioTrack.setVolume(1f)

        mAudioTrack.play()


        var isEos = false

        var mPaused = false

        var isEof = false

        var startTime = SystemClock.uptimeMillis()

        while (!isEos) {
            if (!mPaused) {

                isEos = mediaExtractor.getSampleFlags() and MediaCodec
                    .BUFFER_FLAG_END_OF_STREAM == MediaCodec.BUFFER_FLAG_END_OF_STREAM

                if (!isEos && true) {
                    // check if any ready for display
                    var ready = false

                    val currentTime = (SystemClock.uptimeMillis() - startTime) * 1000
//                    if (mAudioIndex >= 0 && currentTime > 200000) {
//                        out_bufferInfo = MediaCodec.BufferInfo()
//                        ready = mAudioCodecWrapper.peekSample(out_bufferInfo)
//
//                        if (ready && out_bufferInfo.size > 0 && out_bufferInfo.presentationTimeUs < currentTime) {
//                            // Pop the sample off the queue and send it to {@link Surface}
//                            mAudioCodecWrapper.popSample(true)
//                        }
//                    }

                    val out_bufferInfo = MediaCodec.BufferInfo()
                    ready = mAudioCodecWrapper.peekSample(out_bufferInfo)

                    if (ready && out_bufferInfo.size > 0 && out_bufferInfo.presentationTimeUs < currentTime) {
                        // Pop the sample off the queue and send it to {@link Surface}
                        mAudioCodecWrapper.popSample(true)
                    }

                    // read a frame and push to the codec (will fill until the codec input buffers are full

                    val index = mediaExtractor.getSampleTrackIndex()
                    val mABuf = mAudioCodecWrapper.inputBuffer

                    if (mABuf == null) { // no available buffers - brief pause and continue
                        try {
                            Thread.sleep(5)
                        } catch (e: InterruptedException) {
                        }

                    } else if (!isEof) {

                        var cnt = 0

                        try {
                            //while ( cnt < mABuf.remaining() )
                            cnt = mediaExtractor.readSampleData(mABuf, 0)
                            mABuf.position(0)
                            mABuf.limit(cnt)
                        } catch (e: IllegalArgumentException) {
                            e.printStackTrace()
                        }

                        if (cnt > 0) {
                            val sampleTime = mediaExtractor.getSampleTime()
                            val sampleFlags = mediaExtractor.getSampleFlags()

                            val result = mAudioCodecWrapper.returnInputBuffer(mABuf, false, sampleTime, sampleFlags)

                            if (result) {
                                mediaExtractor.advance()
                                //Log.d("FullscreenActivity", "Audio Advancing the extractor");
                            }
                        } else {
                            // videoExtractor is done - error or eof
                            isEof = true
                        }
                    }
                }
            } else {
                var delta = System.currentTimeMillis()
                try {
                    Thread.sleep(25)
                } catch (e: InterruptedException) {
                }

                delta = System.currentTimeMillis() - delta
                startTime += delta
            }
        }

    }
*/
}
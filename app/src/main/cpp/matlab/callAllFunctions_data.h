/*
 * File: callAllFunctions_data.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef CALLALLFUNCTIONS_DATA_H
#define CALLALLFUNCTIONS_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Variable Declarations */
extern creal32_T minimag;

#endif

/*
 * File trailer for callAllFunctions_data.h
 *
 * [EOF]
 */

/*
 * File: step.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef STEP_H
#define STEP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void InitializeConditions(dsp_Delay_1 *obj);
extern void Outputs(const dsp_IFFT_5 *obj, const creal32_T U0_data[], const int
                    U0_size[2], float Y0_data[], int Y0_size[2]);
extern void b_InitializeConditions(dsp_Delay_0 *obj);

#endif

/*
 * File trailer for step.h
 *
 * [EOF]
 */

/*
 * File: filter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef FILTER_H
#define FILTER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_filter(float b[3], float a[3], const float x_data[], const int
                     x_size[2], const float zi[4], float y_data[], int y_size[2],
                     float zf[4]);
extern void filter(float b[3], float a[3], const float x[4096], const float zi[4],
                   float y[4096], float zf[4]);

#endif

/*
 * File trailer for filter.h
 *
 * [EOF]
 */

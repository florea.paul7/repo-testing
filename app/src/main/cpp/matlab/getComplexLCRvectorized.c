/*
 * File: getComplexLCRvectorized.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include <string.h>
#include "callAllFunctions.h"
#include "getComplexLCRvectorized.h"
#include "callAllFunctions_data.h"

/* Type Definitions */
#ifndef typedef_emxArray_creal32_T_2048
#define typedef_emxArray_creal32_T_2048

typedef struct {
  creal32_T data[2048];
  int size[1];
} emxArray_creal32_T_2048;

#endif                                 /*typedef_emxArray_creal32_T_2048*/

#ifndef struct_emxArray_real32_T_1x2048
#define struct_emxArray_real32_T_1x2048

struct emxArray_real32_T_1x2048
{
  float data[2048];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_1x2048*/

#ifndef typedef_emxArray_real32_T_1x2048
#define typedef_emxArray_real32_T_1x2048

typedef struct emxArray_real32_T_1x2048 emxArray_real32_T_1x2048;

#endif                                 /*typedef_emxArray_real32_T_1x2048*/

/* Variable Definitions */
static emxArray_creal32_T_2048 u;
static emxArray_creal32_T_2048 v;
static emxArray_real32_T_1x2048 d;
static emxArray_creal32_T_2048 kappa;
static bool b_runBefore_not_empty;
static int oldlenvec;

/* Function Declarations */
static float rt_hypotf_snf(float u0, float u1);

/* Function Definitions */

/*
 * Arguments    : float u0
 *                float u1
 * Return Type  : float
 */
static float rt_hypotf_snf(float u0, float u1)
{
  float y;
  float a;
  float b;
  a = fabsf(u0);
  b = fabsf(u1);
  if (a < b) {
    a /= b;
    y = b * sqrtf(a * a + 1.0F);
  } else if (a > b) {
    b /= a;
    y = a * sqrtf(b * b + 1.0F);
  } else if (rtIsNaNF(b)) {
    y = b;
  } else {
    y = a * 1.41421354F;
  }

  return y;
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void b_runBefore_not_empty_init(void)
{
  b_runBefore_not_empty = false;
}

/*
 * Determines alpha, beta, gamma one sample at a time
 *    alpha - Complex Left Signal
 *    beta  - Complex Right Signal
 *    width - Adjustable width parameter controlling width of extraction
 *          - width is hard coded to ZERO in this version of the function
 *    Handles everything as singles
 *    Double Maps with Parameter based off alpha and beta
 * Arguments    : const creal32_T alpha_data[]
 *                const int alpha_size[1]
 *                const creal32_T beta_data[]
 *                const int beta_size[1]
 *                creal32_T alphaHat_data[]
 *                int alphaHat_size[1]
 *                creal32_T betaHat_data[]
 *                int betaHat_size[1]
 *                creal32_T gammaHat_data[]
 *                int gammaHat_size[1]
 * Return Type  : void
 */
void getComplexLCRvectorized(const creal32_T alpha_data[], const int alpha_size
  [1], const creal32_T beta_data[], const int beta_size[1], creal32_T
  alphaHat_data[], int alphaHat_size[1], creal32_T betaHat_data[], int
  betaHat_size[1], creal32_T gammaHat_data[], int gammaHat_size[1])
{
  int lenvec;
  int stride;
  int r1;
  creal32_T x_tmp_data[2048];
  float y_data[2048];
  int n;
  float m2[2];
  float r;
  float t;
  float y[4];
  float coeff_idx_0;
  float absxk;

  /* INITIALIZE */
  if (!b_runBefore_not_empty) {
    b_runBefore_not_empty = true;
    lenvec = alpha_size[0];
    oldlenvec = alpha_size[0];
    u.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      u.data[stride].re = 1.0F;
      u.data[stride].im = 0.0F;
    }

    v.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      v.data[stride].re = 1.0F;
      v.data[stride].im = 0.0F;
    }

    d.size[0] = 1;
    d.size[1] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      d.data[stride] = 1.0F;
    }

    kappa.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      kappa.data[stride].re = 1.0F;
      kappa.data[stride].im = 0.0F;
    }

    /*          alphaHat = complex(ones(lenvec,1,'single')); */
    /*          betaHat = complex(ones(lenvec,1,'single')); */
    /*          gammaHat = complex(ones(lenvec,1,'single')); */
  }

  /* RESET */
  lenvec = alpha_size[0];
  if (alpha_size[0] != oldlenvec) {
    /*         lenvec=int32(length(alpha)); */
    oldlenvec = alpha_size[0];
    u.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      u.data[stride].re = 1.0F;
      u.data[stride].im = 0.0F;
    }

    v.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      v.data[stride].re = 1.0F;
      v.data[stride].im = 0.0F;
    }

    d.size[0] = 1;
    d.size[1] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      d.data[stride] = 1.0F;
    }

    kappa.size[0] = alpha_size[0];
    for (stride = 0; stride < lenvec; stride++) {
      kappa.data[stride].re = 1.0F;
      kappa.data[stride].im = 0.0F;
    }

    /*          alphaHat = complex(ones(lenvec,1,'single')); */
    /*          betaHat = complex(ones(lenvec,1,'single')); */
    /*          gammaHat = complex(ones(lenvec,1,'single')); */
  }

  /* Initialize */
  /*           alphaHat = complex(ones(lenvec,1,'single')); */
  /*           betaHat = complex(ones(lenvec,1,'single')); */
  /*           gammaHat = complex(ones(lenvec,1,'single')); */
  /* COMPUTE */
  /* Define U and V */
  r1 = beta_size[0];
  for (stride = 0; stride < r1; stride++) {
    x_tmp_data[stride].re = beta_data[stride].re - alpha_data[stride].re;
    x_tmp_data[stride].im = beta_data[stride].im - alpha_data[stride].im;
  }

  r1 = (short)beta_size[0];
  if (0 <= r1 - 1) {
    memset(&y_data[0], 0, (unsigned int)(r1 * (int)sizeof(float)));
  }

  stride = beta_size[0];
  for (r1 = 0; r1 < stride; r1++) {
    y_data[r1] = rt_hypotf_snf(x_tmp_data[r1].re, x_tmp_data[r1].im);
  }

  u.size[0] = beta_size[0];
  r1 = beta_size[0];
  for (stride = 0; stride < r1; stride++) {
    if (x_tmp_data[stride].im == 0.0F) {
      u.data[stride].re = x_tmp_data[stride].re / y_data[stride];
      u.data[stride].im = 0.0F;
    } else if (x_tmp_data[stride].re == 0.0F) {
      u.data[stride].re = 0.0F;
      u.data[stride].im = x_tmp_data[stride].im / y_data[stride];
    } else {
      u.data[stride].re = x_tmp_data[stride].re / y_data[stride];
      u.data[stride].im = x_tmp_data[stride].im / y_data[stride];
    }
  }

  v.size[0] = u.size[0];
  r1 = u.size[0];
  for (stride = 0; stride < r1; stride++) {
    v.data[stride].re = u.data[stride].re * 0.0F - u.data[stride].im;
    v.data[stride].im = u.data[stride].re + u.data[stride].im * 0.0F;
  }

  for (n = 0; n < lenvec; n++) {
    /* Form Matrices */
    m2[0] = alpha_data[n].re;
    m2[1] = alpha_data[n].im;

    /* Solve for Inverse */
    if (fabsf(-u.data[n].im) > fabsf(-u.data[n].re)) {
      r = -u.data[n].re / -u.data[n].im;
      t = 1.0F / (r * v.data[n].im - v.data[n].re);
      y[0] = v.data[n].im / -u.data[n].im * t;
      y[1] = -t;
      y[2] = -v.data[n].re / -u.data[n].im * t;
      y[3] = r * t;
    } else {
      r = -u.data[n].im / -u.data[n].re;
      t = 1.0F / (v.data[n].im - r * v.data[n].re);
      y[0] = v.data[n].im / -u.data[n].re * t;
      y[1] = -r * t;
      y[2] = -v.data[n].re / -u.data[n].re * t;
      y[3] = t;
    }

    if (fabsf(y[1]) > fabsf(y[0])) {
      r1 = 1;
      stride = 0;
    } else {
      r1 = 0;
      stride = 1;
    }

    r = y[stride] / y[r1];
    coeff_idx_0 = y[2 + r1];
    r = (m2[stride] - m2[r1] * r) / (y[2 + stride] - r * coeff_idx_0);
    coeff_idx_0 = (m2[r1] - r * coeff_idx_0) / y[r1];

    /* Get Coefficients */
    d.data[n] = r;

    /* Check for Cases */
    if (coeff_idx_0 < 0.0F) {
      if ((alpha_data[n].re == 0.0F) && (alpha_data[n].im == 0.0F)) {
        v.data[n] = minimag;
        d.data[n] = rt_hypotf_snf(alpha_data[n].re, alpha_data[n].im);
      } else {
        r = rt_hypotf_snf(alpha_data[n].re, alpha_data[n].im);
        if (alpha_data[n].im == 0.0F) {
          v.data[n].re = alpha_data[n].re / r;
          v.data[n].im = 0.0F;
        } else if (alpha_data[n].re == 0.0F) {
          v.data[n].re = 0.0F;
          v.data[n].im = alpha_data[n].im / r;
        } else {
          v.data[n].re = alpha_data[n].re / r;
          v.data[n].im = alpha_data[n].im / r;
        }

        d.data[n] = r;
      }
    }

    if (coeff_idx_0 > rt_hypotf_snf(beta_data[n].re - alpha_data[n].re,
         beta_data[n].im - alpha_data[n].im)) {
      if ((beta_data[n].re == 0.0F) && (beta_data[n].im == 0.0F)) {
        v.data[n] = minimag;
        r = 0.0F;
        coeff_idx_0 = 1.29246971E-26F;
        stride = beta_size[0];
        for (r1 = 0; r1 < stride; r1++) {
          absxk = fabsf(beta_data[r1].re);
          if (absxk > coeff_idx_0) {
            t = coeff_idx_0 / absxk;
            r = 1.0F + r * t * t;
            coeff_idx_0 = absxk;
          } else {
            t = absxk / coeff_idx_0;
            r += t * t;
          }

          absxk = fabsf(beta_data[r1].im);
          if (absxk > coeff_idx_0) {
            t = coeff_idx_0 / absxk;
            r = 1.0F + r * t * t;
            coeff_idx_0 = absxk;
          } else {
            t = absxk / coeff_idx_0;
            r += t * t;
          }
        }

        d.data[n] = coeff_idx_0 * sqrtf(r);
      } else {
        r = rt_hypotf_snf(beta_data[n].re, beta_data[n].im);
        if (beta_data[n].im == 0.0F) {
          v.data[n].re = beta_data[n].re / r;
          v.data[n].im = 0.0F;
        } else if (beta_data[n].re == 0.0F) {
          v.data[n].re = 0.0F;
          v.data[n].im = beta_data[n].im / r;
        } else {
          v.data[n].re = beta_data[n].re / r;
          v.data[n].im = beta_data[n].im / r;
        }

        d.data[n] = r;
      }
    }
  }

  /* Remap Width Parameter */
  /* w = widthDoubleMap(width, alpha, beta); %width==0 so w=1 */
  kappa.size[0] = d.size[1];
  r1 = d.size[1];
  for (stride = 0; stride < r1; stride++) {
    kappa.data[stride].re = d.data[stride] * v.data[stride].re;
    kappa.data[stride].im = d.data[stride] * v.data[stride].im;
  }

  /* kappa = w * d * v; %w=1 for this case; */
  alphaHat_size[0] = alpha_size[0];
  r1 = alpha_size[0];
  for (stride = 0; stride < r1; stride++) {
    alphaHat_data[stride].re = alpha_data[stride].re - kappa.data[stride].re;
    alphaHat_data[stride].im = alpha_data[stride].im - kappa.data[stride].im;
  }

  /* Left */
  betaHat_size[0] = beta_size[0];
  r1 = beta_size[0];
  for (stride = 0; stride < r1; stride++) {
    betaHat_data[stride].re = beta_data[stride].re - kappa.data[stride].re;
    betaHat_data[stride].im = beta_data[stride].im - kappa.data[stride].im;
  }

  /* Right */
  gammaHat_size[0] = kappa.size[0];
  r1 = kappa.size[0];
  for (stride = 0; stride < r1; stride++) {
    gammaHat_data[stride].re = 1.41421354F * kappa.data[stride].re;
    gammaHat_data[stride].im = 1.41421354F * kappa.data[stride].im;
  }

  /* Center */
  /* Now that we've vectorized everything we can, go back and check the */
  /* special cases */
  for (n = 0; n < lenvec; n++) {
    /* Check Zeros */
    if ((alpha_data[n].re == 0.0F) && (alpha_data[n].im == 0.0F) &&
        ((beta_data[n].re == 0.0F) && (beta_data[n].im == 0.0F))) {
      alphaHat_data[n] = minimag;

      /* Left */
      betaHat_data[n] = minimag;

      /* Right */
      gammaHat_data[n] = minimag;

      /* Center */
    } else if ((alpha_data[n].re == beta_data[n].re) && (alpha_data[n].im ==
                beta_data[n].im)) {
      alphaHat_data[n] = minimag;
      betaHat_data[n] = minimag;
      gammaHat_data[n].re = 1.41421354F * alpha_data[n].re + minimag.re;
      gammaHat_data[n].im = 1.41421354F * alpha_data[n].im + minimag.im;
    } else {
      /* keep things complex */
      /*          if imag(alpha) == 0 */
      /*             alpha=alpha+minimag;  */
      /*          end */
      /*          if imag(beta) == 0 */
      /*             beta=beta+minimag;  */
      /*          end */
    }
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void getComplexLCRvectorized_init(void)
{
  kappa.size[0] = 0;
  d.size[1] = 0;
  v.size[0] = 0;
  u.size[0] = 0;
}

/*
 * File trailer for getComplexLCRvectorized.c
 *
 * [EOF]
 */

/*
 * File: mod.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "mod.h"

/* Function Definitions */

/*
 * Arguments    : float x
 * Return Type  : float
 */
float b_mod(float x)
{
  float r;
  if ((!rtIsInfF(x)) && (!rtIsNaNF(x))) {
    if (x == 0.0F) {
      r = 0.0F;
    } else {
      r = fmodf(x, 2.0F);
      if (r == 0.0F) {
        r = 0.0F;
      } else {
        if (x < 0.0F) {
          r += 2.0F;
        }
      }
    }
  } else {
    r = rtNaNF;
  }

  return r;
}

/*
 * File trailer for mod.c
 *
 * [EOF]
 */

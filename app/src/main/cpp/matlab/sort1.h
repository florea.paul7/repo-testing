/*
 * File: sort1.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef SORT1_H
#define SORT1_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void sort(double x[3], int idx[3]);

#endif

/*
 * File trailer for sort1.h
 *
 * [EOF]
 */

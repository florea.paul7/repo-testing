/*
 * File: hpCompress.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

#ifndef HPCOMPRESS_H
#define HPCOMPRESS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void hpCompress(const float yin[4096], float preGain0, int fs, float
  yout_data[], int yout_size[2]);
extern void hpCompress_init(void);
extern void runbefore_not_empty_init(void);

#ifdef __cplusplus
};
#endif

#endif

/*
 * File trailer for hpCompress.h
 *
 * [EOF]
 */

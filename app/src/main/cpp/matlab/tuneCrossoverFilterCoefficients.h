/*
 * File: tuneCrossoverFilterCoefficients.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef TUNECROSSOVERFILTERCOEFFICIENTS_H
#define TUNECROSSOVERFILTERCOEFFICIENTS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void tuneCrossoverFilterCoefficients(float freq, float order, float Fs,
  float BL[12], float AL[12], float BH[12], float AH[12], float *phaseMult);

#endif

/*
 * File trailer for tuneCrossoverFilterCoefficients.h
 *
 * [EOF]
 */

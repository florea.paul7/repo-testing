/*
 * File: spkCompress.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef SPKCOMPRESS_H
#define SPKCOMPRESS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void e_runbefore_not_empty_init(void);
extern void spkCompress(const float yin[4096], int fs, float yout_data[], int
  yout_size[2]);
extern void spkCompress_init(void);

#ifdef __cplusplus
};
#endif

#endif



/*
 * File trailer for spkCompress.h
 *
 * [EOF]
 */

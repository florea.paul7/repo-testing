/*
 * File: filter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "filter.h"

/* Function Definitions */

/*
 * Arguments    : float b[3]
 *                float a[3]
 *                const float x_data[]
 *                const int x_size[2]
 *                const float zi[4]
 *                float y_data[]
 *                int y_size[2]
 *                float zf[4]
 * Return Type  : void
 */
void b_filter(float b[3], float a[3], const float x_data[], const int x_size[2],
              const float zi[4], float y_data[], int y_size[2], float zf[4])
{
  int nx;
  int lb;
  int i10;
  int i11;
  int k;
  int jp;
  int a_tmp;
  int u0;
  int naxpy_tmp;
  int j;
  int y_data_tmp;
  float as;
  if ((!rtIsInfF(a[0])) && (!rtIsNaNF(a[0])) && (!(a[0] == 0.0F)) && (a[0] !=
       1.0F)) {
    b[0] /= a[0];
    b[1] /= a[0];
    b[2] /= a[0];
    a[1] /= a[0];
    a[2] /= a[0];
    a[0] = 1.0F;
  }

  y_size[0] = (short)x_size[0];
  y_size[1] = 2;
  nx = x_size[0];
  zf[0] = 0.0F;
  zf[1] = 0.0F;
  zf[2] = 0.0F;
  zf[3] = 0.0F;
  y_data[0] = zi[0];
  y_data[1] = zi[1];
  if (3 <= x_size[0]) {
    memset(&y_data[2], 0, (unsigned int)((x_size[0] + -2) * (int)sizeof(float)));
  }

  y_data[x_size[0]] = zi[2];
  y_data[x_size[0] + 1] = zi[3];
  if (3 <= x_size[0]) {
    memset(&y_data[x_size[0] + 2], 0, (unsigned int)((((x_size[0] + x_size[0]) -
              x_size[0]) + -2) * (int)sizeof(float)));
  }

  lb = x_size[0] - 2;
  i10 = x_size[0] - 1;
  i11 = x_size[0] - 1;
  for (k = 0; k < nx; k++) {
    jp = k + 1;
    a_tmp = nx - k;
    if (a_tmp < 3) {
      naxpy_tmp = a_tmp;
    } else {
      naxpy_tmp = 3;
    }

    for (j = 0; j < naxpy_tmp; j++) {
      y_data_tmp = (jp + j) - 1;
      y_data[y_data_tmp] += x_data[jp - 1] * b[j];
    }

    u0 = a_tmp - 1;
    if (u0 >= 2) {
      u0 = 2;
    }

    as = -y_data[jp - 1];
    for (j = 0; j < u0; j++) {
      y_data_tmp = jp + j;
      y_data[y_data_tmp] += as * a[j + 1];
    }
  }

  for (k = lb; k <= i10; k++) {
    u0 = nx - k;
    naxpy_tmp = 2 - u0;
    for (j = 0; j <= naxpy_tmp; j++) {
      zf[j] += x_data[k] * b[u0 + j];
    }
  }

  for (k = lb; k <= i11; k++) {
    u0 = nx - k;
    naxpy_tmp = 2 - u0;
    for (j = 0; j <= naxpy_tmp; j++) {
      zf[j] += -y_data[k] * a[u0 + j];
    }
  }

  for (k = 0; k < nx; k++) {
    jp = (nx + k) + 1;
    u0 = nx - k;
    if (u0 >= 3) {
      u0 = 3;
    }

    for (j = 0; j < u0; j++) {
      y_data_tmp = (jp + j) - 1;
      y_data[y_data_tmp] += x_data[jp - 1] * b[j];
    }

    u0 = (nx - k) - 1;
    if (u0 >= 2) {
      u0 = 2;
    }

    as = -y_data[jp - 1];
    for (j = 0; j < u0; j++) {
      y_data_tmp = jp + j;
      y_data[y_data_tmp] += as * a[j + 1];
    }
  }

  for (k = lb; k <= i10; k++) {
    u0 = nx - k;
    a_tmp = 2 - (nx - k);
    as = x_data[nx + k];
    for (j = 0; j <= a_tmp; j++) {
      zf[2 + j] += as * b[u0 + j];
    }
  }

  for (k = lb; k <= i11; k++) {
    i10 = nx - k;
    u0 = 2 - (nx - k);
    as = -y_data[nx + k];
    for (j = 0; j <= u0; j++) {
      zf[2 + j] += as * a[i10 + j];
    }
  }
}

/*
 * Arguments    : float b[3]
 *                float a[3]
 *                const float x[4096]
 *                const float zi[4]
 *                float y[4096]
 *                float zf[4]
 * Return Type  : void
 */
void filter(float b[3], float a[3], const float x[4096], const float zi[4],
            float y[4096], float zf[4])
{
  int c;
  int offset;
  int offsetzf;
  int jp;
  int j;
  int naxpy;
  float as;
  int y_tmp;
  if ((!rtIsInfF(a[0])) && (!rtIsNaNF(a[0])) && (!(a[0] == 0.0F)) && (a[0] !=
       1.0F)) {
    b[0] /= a[0];
    b[1] /= a[0];
    b[2] /= a[0];
    a[1] /= a[0];
    a[2] /= a[0];
    a[0] = 1.0F;
  }

  zf[0] = 0.0F;
  zf[1] = 0.0F;
  zf[2] = 0.0F;
  zf[3] = 0.0F;
  y[0] = zi[0];
  y[1] = zi[1];
  memset(&y[2], 0, 2046U * sizeof(float));
  y[2048] = zi[2];
  y[2049] = zi[3];
  memset(&y[2050], 0, 2046U * sizeof(float));
  for (c = 0; c < 2; c++) {
    offset = (c << 11) + 2046;
    for (offsetzf = 0; offsetzf < 2048; offsetzf++) {
      jp = (offset + offsetzf) - 2046;
      if (2048 - offsetzf < 3) {
        naxpy = 2047 - offsetzf;
      } else {
        naxpy = 2;
      }

      for (j = 0; j <= naxpy; j++) {
        y_tmp = jp + j;
        y[y_tmp] += x[jp] * b[j];
      }

      if (2047 - offsetzf < 2) {
        naxpy = 2046 - offsetzf;
      } else {
        naxpy = 1;
      }

      as = -y[jp];
      for (j = 0; j <= naxpy; j++) {
        y_tmp = (jp + j) + 1;
        y[y_tmp] += as * a[1 + j];
      }
    }

    offsetzf = (c << 1) - 1;
    for (j = 0; j < 1; j++) {
      zf[offsetzf + 1] += x[offset] * b[2];
    }

    as = x[offset + 1];
    for (j = 0; j < 2; j++) {
      jp = (offsetzf + j) + 1;
      zf[jp] += as * b[j + 1];
    }

    for (j = 0; j < 1; j++) {
      zf[offsetzf + 1] += -y[offset] * a[2];
    }

    as = -y[offset + 1];
    for (j = 0; j < 2; j++) {
      jp = (offsetzf + j) + 1;
      zf[jp] += as * a[j + 1];
    }
  }
}

/*
 * File trailer for filter.c
 *
 * [EOF]
 */

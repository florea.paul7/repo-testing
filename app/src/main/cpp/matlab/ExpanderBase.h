/*
 * File: ExpanderBase.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef EXPANDERBASE_H
#define EXPANDERBASE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void ExpanderBase_detectLevel(expander *obj, const float x_data[], float
  yout_data[], int yout_size[1]);

#endif

/*
 * File trailer for ExpanderBase.h
 *
 * [EOF]
 */

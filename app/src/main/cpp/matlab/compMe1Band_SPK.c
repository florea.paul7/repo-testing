/*
 * File: compMe1Band_SPK.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "compMe1Band_SPK.h"
#include "power.h"
#include "SystemCore.h"
#include "abs.h"
#include "compressor.h"
#include "expander.h"
#include "crossoverFilter.h"

/* Type Definitions */
#ifndef struct_emxArray_real32_T_353x2
#define struct_emxArray_real32_T_353x2

struct emxArray_real32_T_353x2
{
  float data[706];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_353x2*/

#ifndef typedef_emxArray_real32_T_353x2
#define typedef_emxArray_real32_T_353x2

typedef struct emxArray_real32_T_353x2 emxArray_real32_T_353x2;

#endif                                 /*typedef_emxArray_real32_T_353x2*/

#ifndef struct_emxArray_real32_T_385x2
#define struct_emxArray_real32_T_385x2

struct emxArray_real32_T_385x2
{
  float data[770];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_385x2*/

#ifndef typedef_emxArray_real32_T_385x2
#define typedef_emxArray_real32_T_385x2

typedef struct emxArray_real32_T_385x2 emxArray_real32_T_385x2;

#endif                                 /*typedef_emxArray_real32_T_385x2*/

#ifndef struct_emxArray_real32_T_769x2
#define struct_emxArray_real32_T_769x2

struct emxArray_real32_T_769x2
{
  float data[1538];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_769x2*/

#ifndef typedef_emxArray_real32_T_769x2
#define typedef_emxArray_real32_T_769x2

typedef struct emxArray_real32_T_769x2 emxArray_real32_T_769x2;

#endif                                 /*typedef_emxArray_real32_T_769x2*/

/* Variable Definitions */
static bool f_runbefore_not_empty;
static int e_oldfs;
static crossoverFilter AxOver44;
static crossoverFilter AxOver48;
static crossoverFilter AxOver96;
static crossoverFilter BxOver44;
static crossoverFilter BxOver48;
static crossoverFilter BxOver96;
static emxArray_real32_T_2048 c_g1;
static emxArray_real32_T_2048 c_g1mag;
static emxArray_real32_T_2048x2 d_y1;
static emxArray_real32_T_2048x2 Ay1Padded;
static emxArray_real32_T_2048x2 By1Padded;
static expander b_e1_44;
static expander b_e1_48;
static expander b_e1_96;
static compressor c_c1_44;
static compressor c_c1_48;
static compressor c_c1_96;
static compressor b_c2_44;
static compressor b_c2_48;
static compressor b_c2_96;
static emxArray_real32_T_109x2 Ay1Pad44;
static emxArray_real32_T_119x2 Ay1Pad48;
static emxArray_real32_T_238x2 Ay1Pad96;
static emxArray_real32_T_353x2 By1Pad44;
static emxArray_real32_T_385x2 By1Pad48;
static emxArray_real32_T_769x2 By1Pad96;

/* Function Definitions */

/*
 * Speaker Compressor - Expander + Dual Stage Compressor (w/ Lookahead)
 *    FINAL: 190131-Samsung Tab S4
 *    ~ Nicholas Millias
 *
 *    yin     - input signal
 *    preGain - boost before compressor
 *    fs      - samples per second
 *
 *    1. Define Compressor Properties
 *        T:  Compression Threshold [dB]
 *        R:  Compression Ratio
 *        AT: Attack Time [seconds]
 *        RT: Release Time [seconds]
 *        PG: Post Gain [dB]
 *        knee: Range of db soft knee is applied default:6 [dB]
 *    2. Define Expander Properties
 *    3. Crossover Frequency sets LF cut off point
 *
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * Arguments    : const float yin[4096]
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void compMe1Band_SPK(const float yin[4096], int fs, float yout_data[], int
                     yout_size[2])
{
  float unusedU6[4096];
  float e_y1[4096];
  int tmp_size[1];
  float tmp_data[2048];
  float varargin_1_data[2049];
  int varargin_1_size[1];
  int b_tmp_size[1];
  float varargin_2_data[2049];
  short csz_idx_0;
  int maxval_size[1];
  int nx;
  int k;
  float maxval_data[2049];
  int b_varargin_1_size[1];
  int c_tmp_size[1];
  short b_tmp_data[2048];
  float f2;
  int d_tmp_size[1];
  int c_varargin_1_size[1];
  int unusedU9_size[2];
  int e_tmp_size[1];
  int f_tmp_size[1];
  int d_varargin_1_size[1];

  /*  First Time Initialization */
  if (!f_runbefore_not_empty) {
    e_oldfs = fs;
    f_runbefore_not_empty = true;

    /* lookahead time in seconds */
    /* lookahead time in seconds */
    /* Comp1 Parameters */
    /* dB */
    /* Compression Ratio */
    /* 0.0001; %Attack time in s */
    /* 0.100; %Release Time in s */
    /* v1(20) .  %13%14.9 - RC4 %Post Compression Make-Up Gain */
    /* Comp2 Parameters */
    /* dBxs */
    /* Compression Ratio */
    /* Attack time in s */
    /* Release Time in s */
    /* Post Compression Make-Up Gain */
    /* Expander Parameters  */
    /* Threshold */
    /* Compression Ratio */
    /* .001 %Attack time in s */
    /* Release Time in s */
    /* Crossover Parameters */
    /* TODO: 24db induces glitches on sin wave */
    /* Crossover */
    /* *Note stacked crossovers create linkwitz riley filter at xoverFreq */
    crossoverFilter_crossoverFilter(&AxOver44);
    b_crossoverFilter_crossoverFilt(&AxOver48);
    c_crossoverFilter_crossoverFilt(&AxOver96);
    crossoverFilter_crossoverFilter(&BxOver44);
    b_crossoverFilter_crossoverFilt(&BxOver48);
    c_crossoverFilter_crossoverFilt(&BxOver96);

    /* Expander (First Stage) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    d_expander_expander(&b_e1_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    e_expander_expander(&b_e1_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    f_expander_expander(&b_e1_96);

    /* limit output to -0.1 dB ); */
    /* Compressors (First Stage) */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    p_compressor_compressor(&c_c1_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    q_compressor_compressor(&c_c1_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    r_compressor_compressor(&c_c1_96);

    /* limit output to -0.1 dB ); */
    /* Compressors (Second Stage) */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    s_compressor_compressor(&b_c2_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    t_compressor_compressor(&b_c2_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    u_compressor_compressor(&b_c2_96);

    /* limit output to -0.1 dB ); */
    /* Declare Persistent Vars */
  }

  /*  Reset */
  if (fs != e_oldfs) {
    /* Declare Persistent Vars */
    Ay1Pad44.size[0] = 109;
    Ay1Pad44.size[1] = 2;
    memset(&Ay1Pad44.data[0], 0, 218U * sizeof(float));
    Ay1Pad48.size[0] = 119;
    Ay1Pad48.size[1] = 2;
    memset(&Ay1Pad48.data[0], 0, 238U * sizeof(float));
    Ay1Pad96.size[0] = 238;
    Ay1Pad96.size[1] = 2;
    memset(&Ay1Pad96.data[0], 0, 476U * sizeof(float));
    By1Pad44.size[0] = 353;
    By1Pad44.size[1] = 2;
    memset(&By1Pad44.data[0], 0, 706U * sizeof(float));
    By1Pad48.size[0] = 385;
    By1Pad48.size[1] = 2;
    memset(&By1Pad48.data[0], 0, 770U * sizeof(float));
    By1Pad96.size[0] = 769;
    By1Pad96.size[1] = 2;
    memset(&By1Pad96.data[0], 0, 1538U * sizeof(float));
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    Ay1Padded.size[0] = 2048;
    Ay1Padded.size[1] = 2;
    By1Padded.size[0] = 2048;
    By1Padded.size[1] = 2;
    memset(&d_y1.data[0], 0, sizeof(float) << 12);
    memset(&Ay1Padded.data[0], 0, sizeof(float) << 12);
    memset(&By1Padded.data[0], 0, sizeof(float) << 12);
    c_g1.size[0] = 2048;
    c_g1mag.size[0] = 2048;
    memset(&c_g1.data[0], 0, sizeof(float) << 11);
    memset(&c_g1mag.data[0], 0, sizeof(float) << 11);
  }

  /*  Process */
  /*  Compression Stage - 1 */
  if (fs < 46000) {
    /* SR = 44100 */
    /* Crossover */
    SystemCore_parenReference(&AxOver44, yin, unusedU6, e_y1);
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    memcpy(&d_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    b_SystemCore_parenReference(&b_e1_44, maxval_data, maxval_size, tmp_data,
      tmp_size, c_g1.data, c_g1.size);

    /* Construct the expanded signal */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    /* Get Gains     */
    c_tmp_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      f2 = d_y1.data[nx] * c_g1mag.data[nx];
      d_y1.data[nx] = f2;
      d_y1.data[nx + 2048] *= c_g1mag.data[nx];
      tmp_data[nx] = f2;
    }

    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c_c1_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    Ay1Padded.size[0] = 2048;
    Ay1Padded.size[1] = 2;
    memcpy(&Ay1Padded.data[0], &Ay1Pad44.data[0], 109U * sizeof(float));
    memcpy(&Ay1Padded.data[2048], &Ay1Pad44.data[109], 109U * sizeof(float));
    memcpy(&Ay1Padded.data[109], &d_y1.data[0], 1939U * sizeof(float));
    memcpy(&Ay1Padded.data[2157], &d_y1.data[2048], 1939U * sizeof(float));

    /* construct the output signal */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      Ay1Padded.data[nx] *= c_g1mag.data[nx];
      Ay1Padded.data[nx + 2048] *= c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    Ay1Pad44.size[0] = 109;
    Ay1Pad44.size[1] = 2;
    memcpy(&Ay1Pad44.data[0], &d_y1.data[1939], 109U * sizeof(float));
    memcpy(&Ay1Pad44.data[109], &d_y1.data[3987], 109U * sizeof(float));
  } else if (fs < 64000) {
    /* SR = 48000 */
    /* Crossover */
    SystemCore_parenReference(&AxOver48, yin, unusedU6, e_y1);
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    memcpy(&d_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    b_SystemCore_parenReference(&b_e1_48, maxval_data, maxval_size, tmp_data,
      tmp_size, c_g1.data, c_g1.size);

    /* construct the expanded signal */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    /* Get Gains    */
    c_tmp_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      f2 = d_y1.data[nx] * c_g1mag.data[nx];
      d_y1.data[nx] = f2;
      d_y1.data[nx + 2048] *= c_g1mag.data[nx];
      tmp_data[nx] = f2;
    }

    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c_c1_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    Ay1Padded.size[0] = 2048;
    Ay1Padded.size[1] = 2;
    memcpy(&Ay1Padded.data[0], &Ay1Pad48.data[0], 119U * sizeof(float));
    memcpy(&Ay1Padded.data[2048], &Ay1Pad48.data[119], 119U * sizeof(float));
    memcpy(&Ay1Padded.data[119], &d_y1.data[0], 1929U * sizeof(float));
    memcpy(&Ay1Padded.data[2167], &d_y1.data[2048], 1929U * sizeof(float));

    /* Construct Output */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      Ay1Padded.data[nx] *= c_g1mag.data[nx];
      Ay1Padded.data[nx + 2048] *= c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    Ay1Pad48.size[0] = 119;
    Ay1Pad48.size[1] = 2;
    memcpy(&Ay1Pad48.data[0], &d_y1.data[1929], 119U * sizeof(float));
    memcpy(&Ay1Pad48.data[119], &d_y1.data[3977], 119U * sizeof(float));
  } else {
    /* SR = 96000  */
    /* Crossover */
    SystemCore_parenReference(&AxOver96, yin, unusedU6, e_y1);
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    memcpy(&d_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    b_SystemCore_parenReference(&b_e1_96, maxval_data, maxval_size, tmp_data,
      tmp_size, c_g1.data, c_g1.size);

    /* construct the expanded signal */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      d_y1.data[nx] = yin[nx] * c_g1mag.data[nx];
      b_tmp_data[nx] = (short)nx;
    }

    for (nx = 0; nx < 2048; nx++) {
      d_y1.data[b_tmp_data[nx] + 2048] = yin[2048 + nx] * c_g1mag.data[nx];
    }

    c_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c_c1_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    Ay1Padded.size[0] = 2048;
    Ay1Padded.size[1] = 2;
    memcpy(&Ay1Padded.data[0], &Ay1Pad96.data[0], 238U * sizeof(float));
    memcpy(&Ay1Padded.data[2048], &Ay1Pad96.data[238], 238U * sizeof(float));
    memcpy(&Ay1Padded.data[238], &d_y1.data[0], 1810U * sizeof(float));
    memcpy(&Ay1Padded.data[2286], &d_y1.data[2048], 1810U * sizeof(float));

    /* Construct Output */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    memcpy(&tmp_data[0], &Ay1Padded.data[0], sizeof(float) << 11);
    for (nx = 0; nx < 2048; nx++) {
      Ay1Padded.data[b_tmp_data[nx]] = tmp_data[nx] * c_g1mag.data[nx];
    }

    memcpy(&tmp_data[0], &Ay1Padded.data[2048], sizeof(float) << 11);
    for (nx = 0; nx < 2048; nx++) {
      Ay1Padded.data[b_tmp_data[nx] + 2048] = tmp_data[nx] * c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    Ay1Pad96.size[0] = 238;
    Ay1Pad96.size[1] = 2;
    memcpy(&Ay1Pad96.data[0], &d_y1.data[1810], 238U * sizeof(float));
    memcpy(&Ay1Pad96.data[238], &d_y1.data[3858], 238U * sizeof(float));
  }

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Compression Stage - 2] */
  if (fs < 46000) {
    /* SR = 44100 */
    /* Crossover */
    f_SystemCore_parenReference(&BxOver44, Ay1Padded.data, unusedU6,
      unusedU9_size, d_y1.data, d_y1.size);

    /* Get Gains     */
    e_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, e_tmp_size, varargin_1_data, varargin_1_size);
    f_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c2_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    By1Padded.size[0] = 2048;
    By1Padded.size[1] = 2;
    memcpy(&By1Padded.data[0], &By1Pad44.data[0], 353U * sizeof(float));
    memcpy(&By1Padded.data[2048], &By1Pad44.data[353], 353U * sizeof(float));
    memcpy(&By1Padded.data[353], &d_y1.data[0], 1695U * sizeof(float));
    memcpy(&By1Padded.data[2401], &d_y1.data[2048], 1695U * sizeof(float));

    /* construct the output signal */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    d_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, d_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      By1Padded.data[nx] *= c_g1mag.data[nx];
      By1Padded.data[nx + 2048] *= c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    By1Pad44.size[0] = 353;
    By1Pad44.size[1] = 2;
    memcpy(&By1Pad44.data[0], &d_y1.data[1695], 353U * sizeof(float));
    memcpy(&By1Pad44.data[353], &d_y1.data[3743], 353U * sizeof(float));
  } else if (fs < 64000) {
    /* SR = 48000 */
    /* Crossover */
    SystemCore_parenReference(&BxOver48, yin, unusedU6, e_y1);
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    memcpy(&d_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Get Gains    */
    e_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, e_tmp_size, varargin_1_data, varargin_1_size);
    f_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c2_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    By1Padded.size[0] = 2048;
    By1Padded.size[1] = 2;
    memcpy(&By1Padded.data[0], &By1Pad48.data[0], 385U * sizeof(float));
    memcpy(&By1Padded.data[2048], &By1Pad48.data[385], 385U * sizeof(float));
    memcpy(&By1Padded.data[385], &d_y1.data[0], 1663U * sizeof(float));
    memcpy(&By1Padded.data[2433], &d_y1.data[2048], 1663U * sizeof(float));

    /* Construct Output */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    d_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, d_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      By1Padded.data[nx] *= c_g1mag.data[nx];
      By1Padded.data[nx + 2048] *= c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    By1Pad48.size[0] = 385;
    By1Pad48.size[1] = 2;
    memcpy(&By1Pad48.data[0], &d_y1.data[1663], 385U * sizeof(float));
    memcpy(&By1Pad48.data[385], &d_y1.data[3711], 385U * sizeof(float));
  } else {
    /* SR = 96000  */
    /* Crossover */
    SystemCore_parenReference(&BxOver96, yin, unusedU6, e_y1);
    d_y1.size[0] = 2048;
    d_y1.size[1] = 2;
    memcpy(&d_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Get Gains */
    e_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, e_tmp_size, varargin_1_data, varargin_1_size);
    f_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &d_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c2_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    c_g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&c_g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] *
              (int)sizeof(float)));
    }

    /* Delay Input */
    By1Padded.size[0] = 2048;
    By1Padded.size[1] = 2;
    memcpy(&By1Padded.data[0], &By1Pad96.data[0], 769U * sizeof(float));
    memcpy(&By1Padded.data[2048], &By1Pad96.data[769], 769U * sizeof(float));
    memcpy(&By1Padded.data[769], &d_y1.data[0], 1279U * sizeof(float));
    memcpy(&By1Padded.data[2817], &d_y1.data[2048], 1279U * sizeof(float));

    /* Construct Output */
    memcpy(&varargin_1_data[0], &c_g1.data[0], sizeof(float) << 11);
    d_varargin_1_size[0] = 2048;
    for (nx = 0; nx < 2048; nx++) {
      maxval_data[nx] = varargin_1_data[nx] / 20.0F;
    }

    b_power(maxval_data, d_varargin_1_size, varargin_1_data, varargin_1_size);
    c_g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&c_g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (nx = 0; nx < 2048; nx++) {
      By1Padded.data[nx] *= c_g1mag.data[nx];
      By1Padded.data[nx + 2048] *= c_g1mag.data[nx];
    }

    /* Fill Pads for Next Time */
    By1Pad96.size[0] = 769;
    By1Pad96.size[1] = 2;
    memcpy(&By1Pad96.data[0], &d_y1.data[1279], 769U * sizeof(float));
    memcpy(&By1Pad96.data[769], &d_y1.data[3327], 769U * sizeof(float));
  }

  /* Output */
  yout_size[0] = 2048;
  yout_size[1] = 2;
  memcpy(&yout_data[0], &By1Padded.data[0], sizeof(float) << 12);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe1Band_SPK_free(void)
{
  crossoverFilter *obj;
  obj = &AxOver44;
  if (!AxOver44.matlabCodegenIsDeleted) {
    AxOver44.matlabCodegenIsDeleted = true;
    if (AxOver44.isInitialized == 1) {
      AxOver44.isInitialized = 2;
      if (AxOver44.isSetupComplete) {
        AxOver44.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &AxOver48;
  if (!AxOver48.matlabCodegenIsDeleted) {
    AxOver48.matlabCodegenIsDeleted = true;
    if (AxOver48.isInitialized == 1) {
      AxOver48.isInitialized = 2;
      if (AxOver48.isSetupComplete) {
        AxOver48.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &AxOver96;
  if (!AxOver96.matlabCodegenIsDeleted) {
    AxOver96.matlabCodegenIsDeleted = true;
    if (AxOver96.isInitialized == 1) {
      AxOver96.isInitialized = 2;
      if (AxOver96.isSetupComplete) {
        AxOver96.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &BxOver44;
  if (!BxOver44.matlabCodegenIsDeleted) {
    BxOver44.matlabCodegenIsDeleted = true;
    if (BxOver44.isInitialized == 1) {
      BxOver44.isInitialized = 2;
      if (BxOver44.isSetupComplete) {
        BxOver44.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &BxOver48;
  if (!BxOver48.matlabCodegenIsDeleted) {
    BxOver48.matlabCodegenIsDeleted = true;
    if (BxOver48.isInitialized == 1) {
      BxOver48.isInitialized = 2;
      if (BxOver48.isSetupComplete) {
        BxOver48.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &BxOver96;
  if (!BxOver96.matlabCodegenIsDeleted) {
    BxOver96.matlabCodegenIsDeleted = true;
    if (BxOver96.isInitialized == 1) {
      BxOver96.isInitialized = 2;
      if (BxOver96.isSetupComplete) {
        BxOver96.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  if (!b_e1_44.matlabCodegenIsDeleted) {
    b_e1_44.matlabCodegenIsDeleted = true;
    if (b_e1_44.isInitialized == 1) {
      b_e1_44.isInitialized = 2;
      if (b_e1_44.isSetupComplete) {
        b_e1_44.pNumChannels = -1.0;
      }
    }
  }

  if (!b_e1_48.matlabCodegenIsDeleted) {
    b_e1_48.matlabCodegenIsDeleted = true;
    if (b_e1_48.isInitialized == 1) {
      b_e1_48.isInitialized = 2;
      if (b_e1_48.isSetupComplete) {
        b_e1_48.pNumChannels = -1.0;
      }
    }
  }

  if (!b_e1_96.matlabCodegenIsDeleted) {
    b_e1_96.matlabCodegenIsDeleted = true;
    if (b_e1_96.isInitialized == 1) {
      b_e1_96.isInitialized = 2;
      if (b_e1_96.isSetupComplete) {
        b_e1_96.pNumChannels = -1.0;
      }
    }
  }

  if (!c_c1_44.matlabCodegenIsDeleted) {
    c_c1_44.matlabCodegenIsDeleted = true;
    if (c_c1_44.isInitialized == 1) {
      c_c1_44.isInitialized = 2;
      if (c_c1_44.isSetupComplete) {
        c_c1_44.pNumChannels = -1.0;
      }
    }
  }

  if (!c_c1_48.matlabCodegenIsDeleted) {
    c_c1_48.matlabCodegenIsDeleted = true;
    if (c_c1_48.isInitialized == 1) {
      c_c1_48.isInitialized = 2;
      if (c_c1_48.isSetupComplete) {
        c_c1_48.pNumChannels = -1.0;
      }
    }
  }

  if (!c_c1_96.matlabCodegenIsDeleted) {
    c_c1_96.matlabCodegenIsDeleted = true;
    if (c_c1_96.isInitialized == 1) {
      c_c1_96.isInitialized = 2;
      if (c_c1_96.isSetupComplete) {
        c_c1_96.pNumChannels = -1.0;
      }
    }
  }

  if (!b_c2_44.matlabCodegenIsDeleted) {
    b_c2_44.matlabCodegenIsDeleted = true;
    if (b_c2_44.isInitialized == 1) {
      b_c2_44.isInitialized = 2;
      if (b_c2_44.isSetupComplete) {
        b_c2_44.pNumChannels = -1.0;
      }
    }
  }

  if (!b_c2_48.matlabCodegenIsDeleted) {
    b_c2_48.matlabCodegenIsDeleted = true;
    if (b_c2_48.isInitialized == 1) {
      b_c2_48.isInitialized = 2;
      if (b_c2_48.isSetupComplete) {
        b_c2_48.pNumChannels = -1.0;
      }
    }
  }

  if (!b_c2_96.matlabCodegenIsDeleted) {
    b_c2_96.matlabCodegenIsDeleted = true;
    if (b_c2_96.isInitialized == 1) {
      b_c2_96.isInitialized = 2;
      if (b_c2_96.isSetupComplete) {
        b_c2_96.pNumChannels = -1.0;
      }
    }
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe1Band_SPK_init(void)
{
  By1Pad96.size[1] = 0;
  By1Pad48.size[1] = 0;
  By1Pad44.size[1] = 0;
  Ay1Pad96.size[1] = 0;
  Ay1Pad48.size[1] = 0;
  Ay1Pad44.size[1] = 0;
  By1Padded.size[1] = 0;
  Ay1Padded.size[1] = 0;
  d_y1.size[1] = 0;
  c_g1mag.size[0] = 0;
  c_g1.size[0] = 0;
  b_c2_96.matlabCodegenIsDeleted = true;
  b_c2_48.matlabCodegenIsDeleted = true;
  b_c2_44.matlabCodegenIsDeleted = true;
  c_c1_96.matlabCodegenIsDeleted = true;
  c_c1_48.matlabCodegenIsDeleted = true;
  c_c1_44.matlabCodegenIsDeleted = true;
  b_e1_96.matlabCodegenIsDeleted = true;
  b_e1_48.matlabCodegenIsDeleted = true;
  b_e1_44.matlabCodegenIsDeleted = true;
  BxOver96.matlabCodegenIsDeleted = true;
  BxOver48.matlabCodegenIsDeleted = true;
  BxOver44.matlabCodegenIsDeleted = true;
  AxOver96.matlabCodegenIsDeleted = true;
  AxOver48.matlabCodegenIsDeleted = true;
  AxOver44.matlabCodegenIsDeleted = true;
  Ay1Pad44.size[0] = 109;
  Ay1Pad44.size[1] = 2;
  memset(&Ay1Pad44.data[0], 0, 218U * sizeof(float));
  Ay1Pad48.size[0] = 119;
  Ay1Pad48.size[1] = 2;
  memset(&Ay1Pad48.data[0], 0, 238U * sizeof(float));
  Ay1Pad96.size[0] = 238;
  Ay1Pad96.size[1] = 2;
  memset(&Ay1Pad96.data[0], 0, 476U * sizeof(float));
  By1Pad44.size[0] = 353;
  By1Pad44.size[1] = 2;
  memset(&By1Pad44.data[0], 0, 706U * sizeof(float));
  By1Pad48.size[0] = 385;
  By1Pad48.size[1] = 2;
  memset(&By1Pad48.data[0], 0, 770U * sizeof(float));
  By1Pad96.size[0] = 769;
  By1Pad96.size[1] = 2;
  memset(&By1Pad96.data[0], 0, 1538U * sizeof(float));
  d_y1.size[0] = 2048;
  d_y1.size[1] = 2;
  Ay1Padded.size[0] = 2048;
  Ay1Padded.size[1] = 2;
  By1Padded.size[0] = 2048;
  By1Padded.size[1] = 2;
  memset(&d_y1.data[0], 0, sizeof(float) << 12);
  memset(&Ay1Padded.data[0], 0, sizeof(float) << 12);
  memset(&By1Padded.data[0], 0, sizeof(float) << 12);
  c_g1.size[0] = 2048;
  c_g1mag.size[0] = 2048;
  memset(&c_g1.data[0], 0, sizeof(float) << 11);
  memset(&c_g1mag.data[0], 0, sizeof(float) << 11);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void f_runbefore_not_empty_init(void)
{
  f_runbefore_not_empty = false;
}

/*
 * File trailer for compMe1Band_SPK.c
 *
 * [EOF]
 */

/*
 * File: mltwinMod.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "mltwinMod.h"

/* Function Definitions */

/*
 * MODIFIED FOR MatlabCoder MLTWIN Symmetric Modulated Lapped Transform window
 *     MLTWIN(N)
 *
 *     Ref:
 *     Lapped Transforms for Efficient Transform/Subband Coding
 *     Henrique S. Malvar
 *     IEEE Transactions on Acoustics, Speech, and Signal Processing
 *     Vol. 38, No. 6, June 1990
 *     pgs. 969-978
 * Arguments    : double outWin[4096]
 * Return Type  : void
 */
void b_mltwinMod(double outWin[4096])
{
  int k;
  double ex;
  double d1;

  /*   Joe Henning - Jan 2014 */
  for (k = 0; k < 4096; k++) {
    outWin[k] = sin(((double)k + 0.5) * 3.1415926535897931 / 4096.0);
  }

  /*  normalize */
  ex = outWin[0];
  for (k = 0; k < 4095; k++) {
    d1 = outWin[k + 1];
    if (ex < d1) {
      ex = d1;
    }
  }

  for (k = 0; k < 4096; k++) {
    outWin[k] /= ex;
  }
}

/*
 * MODIFIED FOR MatlabCoder MLTWIN Symmetric Modulated Lapped Transform window
 *     MLTWIN(N)
 *
 *     Ref:
 *     Lapped Transforms for Efficient Transform/Subband Coding
 *     Henrique S. Malvar
 *     IEEE Transactions on Acoustics, Speech, and Signal Processing
 *     Vol. 38, No. 6, June 1990
 *     pgs. 969-978
 * Arguments    : double outWin[2048]
 * Return Type  : void
 */
void mltwinMod(double outWin[2048])
{
  int k;
  double ex;
  double d0;

  /*   Joe Henning - Jan 2014 */
  for (k = 0; k < 2048; k++) {
    outWin[k] = sin(((double)k + 0.5) * 3.1415926535897931 / 2048.0);
  }

  /*  normalize */
  ex = outWin[0];
  for (k = 0; k < 2047; k++) {
    d0 = outWin[k + 1];
    if (ex < d0) {
      ex = d0;
    }
  }

  for (k = 0; k < 2048; k++) {
    outWin[k] /= ex;
  }
}

/*
 * File trailer for mltwinMod.c
 *
 * [EOF]
 */

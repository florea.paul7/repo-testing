MY_PATH:=$(call my-dir)

include $(CLEAR_VARS)

LOCAL_PATH := $(MY_PATH)

define walk
  $(wildcard $(1)) $(foreach e, $(wildcard $(1)/*), $(call walk, $(e)))
endef

MAT_WALK := $(call walk, $(LOCAL_PATH))

MAT_FILES += $(filter %.c, $(MAT_WALK))

LOCAL_CFLAGS	+= -O1
LOCAL_CFLAGS	+= -fvisibility=hidden
LOCAL_CFLAGS    += -D__ARM_NEON

MATP_FILES    := $(MAT_FILES:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Ne10/inc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Ne10/modules/dsp

LOCAL_MODULE    := mat_static
LOCAL_SRC_FILES := $(MATP_FILES)

LOCAL_ARM_NEON	:= true
LOCAL_ARM_MODE := arm

include $(BUILD_STATIC_LIBRARY)



/*
 * File: compMe4Band_HP.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "compMe4Band_HP.h"
#include "power.h"
#include "SystemCore.h"
#include "abs.h"
#include "compressor.h"
#include "crossoverFilter.h"

/* Type Definitions */
#ifndef struct_emxArray_real32_T_2049
#define struct_emxArray_real32_T_2049

struct emxArray_real32_T_2049
{
  float data[2049];
  int size[1];
};

#endif                                 /*struct_emxArray_real32_T_2049*/

#ifndef typedef_emxArray_real32_T_2049
#define typedef_emxArray_real32_T_2049

typedef struct emxArray_real32_T_2049 emxArray_real32_T_2049;

#endif                                 /*typedef_emxArray_real32_T_2049*/

/* Variable Definitions */
static bool c_runbefore_not_empty;
static int c_oldfs;
static crossoverFilter_1 b_xOver44;
static crossoverFilter_1 b_xOver48;
static crossoverFilter_1 b_xOver96;
static compressor b_c1_44;
static compressor b_c1_48;
static compressor b_c1_96;
static compressor c2_44;
static compressor c2_48;
static compressor c2_96;
static compressor c3_44;
static compressor c3_48;
static compressor c3_96;
static compressor c4_44;
static compressor c4_48;
static compressor c4_96;
static emxArray_real32_T_2049 b_g1;
static emxArray_real32_T_2049 b_g1mag;
static emxArray_real32_T_2049 g2;
static emxArray_real32_T_2049 g2mag;
static emxArray_real32_T_2049 g3;
static emxArray_real32_T_2049 g3mag;
static emxArray_real32_T_2049 g4;
static emxArray_real32_T_2049 g4mag;
static emxArray_real32_T_2049x2 c_y1;
static emxArray_real32_T_2049x2 y2;
static emxArray_real32_T_2049x2 y3;
static emxArray_real32_T_2049x2 y4;
static emxArray_real32_T_2048x2 b_y1Padded;
static emxArray_real32_T_2048x2 y2Padded;
static emxArray_real32_T_2048x2 y3Padded;
static emxArray_real32_T_2048x2 y4Padded;

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void c_runbefore_not_empty_init(void)
{
  c_runbefore_not_empty = false;
}

/*
 * Headphone Compressor - 4 Band Compressor
 *    FINAL: 190131-Samsung Tab S4
 *    ~ Nicholas Millias
 *
 *    yin     - Input signal
 *    preGain - Boost before compressor
 *    fs      - Samples per second
 *
 *    1. Define crossover frequency
 *    2. Define compressor properties
 *        T: Compression Threshold [dB]
 *        R: Compression Ratio
 *        AT: Attack Time [seconds]
 *        RT: Release Time [seconds]
 *        PG: Post Gain [dB]
 *        knee: Range of db soft knee is applied default:6 [dB]
 *
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * Arguments    : const float yin_data[]
 *                const int yin_size[2]
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void compMe4Band_HP(const float yin_data[], const int yin_size[2], int fs, float
                    yout_data[], int yout_size[2])
{
  float y1_data[4098];
  int y1_size[2];
  float y2_data[4098];
  int y2_size[2];
  float y3_data[4098];
  int y3_size[2];
  float y4_data[4098];
  int y4_size[2];
  int nx;
  short csz_idx_0;
  short hoistedGlobal_size_idx_0;
  int tmp_size[1];
  float maxval_data[2049];
  float varargin_1_data[2049];
  int varargin_1_size[1];
  int b_tmp_size[1];
  int k;
  float varargin_2_data[2049];
  int maxval_size[1];
  int c_tmp_size[1];
  int d_tmp_size[1];
  int e_tmp_size[1];
  int f_tmp_size[1];
  int g_tmp_size[1];
  int h_tmp_size[1];
  short tmp_data[2048];

  /*  First Time Initialization */
  if (!c_runbefore_not_empty) {
    c_oldfs = fs;
    c_runbefore_not_empty = true;

    /* Crossover Parameters */
    /* TODO: Should be 24 but glitches */
    /* Comp Parameters */
    /* Range of DB for Knee */
    /* Tuning */
    /* T1  = -7.0;   %dB */
    /* dB */
    /* Compression Ratio */
    /* Attack time ins */
    /* Release Time in s */
    /* Post Compression Make-Up Gain  */
    /* PG1 = 4.0;    %Post Compression Make-Up Gain  */
    /* T2  = -7.0;   %dB */
    /* dB */
    /* Compression Ratio */
    /* Attack time in s */
    /* Release Time in s */
    /* PG2 = 4.0;    %Post Compression Make-Up Gain */
    /* Post Compression Make-Up Gain */
    /* T3  = -6.0;   %dB */
    /* dB */
    /* Compression Ratio */
    /* Attack time in s */
    /* Release Time in s */
    /* PG3 = 6.5;    %Post Compression Make-Up Gain */
    /* Post Compression Make-Up Gain */
    /* T4  = -8.0;   %dB */
    /* dB */
    /* Compression Ratio */
    /* Attack time in s */
    /* Release Time in s */
    /* PG4 = 7.0;    %Post Compression Make-Up Gain */
    /* Post Compression Make-Up Gain */
    /* Crossover */
    d_crossoverFilter_crossoverFilt(&b_xOver44);
    e_crossoverFilter_crossoverFilt(&b_xOver48);
    f_crossoverFilter_crossoverFilt(&b_xOver96);

    /* Compressors (First Band) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    d_compressor_compressor(&b_c1_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    e_compressor_compressor(&b_c1_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    f_compressor_compressor(&b_c1_96);

    /* limit output to -0.1 dB ); */
    /* Compressors ( Second Band ) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    g_compressor_compressor(&c2_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    h_compressor_compressor(&c2_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    i_compressor_compressor(&c2_96);

    /* limit output to -0.1 dB ); */
    /* Compressors ( Third Band ) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    j_compressor_compressor(&c3_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    k_compressor_compressor(&c3_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    l_compressor_compressor(&c3_96);

    /* limit output to -0.1 dB ); */
    /* Compressors ( Fourth Band ) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    m_compressor_compressor(&c4_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    n_compressor_compressor(&c4_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    o_compressor_compressor(&c4_96);

    /* limit output to -0.1 dB ); */
    /* Declare persistent vars used Stage 1 in block processing */
  }

  /*  Reset */
  if (fs != c_oldfs) {
    /* Is this Necessary to zero out buffers or are they simply overwritten? */
    c_y1.size[0] = 2048;
    c_y1.size[1] = 1;
    y2.size[0] = 2048;
    y2.size[1] = 1;
    y3.size[0] = 2048;
    y3.size[1] = 1;
    y4.size[0] = 2048;
    y4.size[1] = 1;
    memset(&c_y1.data[0], 0, sizeof(float) << 11);
    memset(&y2.data[0], 0, sizeof(float) << 11);
    memset(&y3.data[0], 0, sizeof(float) << 11);
    memset(&y4.data[0], 0, sizeof(float) << 11);
    b_y1Padded.size[0] = 2048;
    b_y1Padded.size[1] = 2;
    y2Padded.size[0] = 2048;
    y2Padded.size[1] = 2;
    y3Padded.size[0] = 2048;
    y3Padded.size[1] = 2;
    y4Padded.size[0] = 2048;
    y4Padded.size[1] = 2;
    memset(&b_y1Padded.data[0], 0, sizeof(float) << 12);
    memset(&y2Padded.data[0], 0, sizeof(float) << 12);
    memset(&y3Padded.data[0], 0, sizeof(float) << 12);
    memset(&y4Padded.data[0], 0, sizeof(float) << 12);
    b_g1.size[0] = 2048;
    b_g1mag.size[0] = 2048;
    g2.size[0] = 2048;
    g2mag.size[0] = 2048;
    g3.size[0] = 2048;
    g3mag.size[0] = 2048;
    g4.size[0] = 2048;
    g4mag.size[0] = 2048;
    memset(&b_g1.data[0], 0, sizeof(float) << 11);
    memset(&b_g1mag.data[0], 0, sizeof(float) << 11);
    memset(&g2.data[0], 0, sizeof(float) << 11);
    memset(&g2mag.data[0], 0, sizeof(float) << 11);
    memset(&g3.data[0], 0, sizeof(float) << 11);
    memset(&g3mag.data[0], 0, sizeof(float) << 11);
    memset(&g4.data[0], 0, sizeof(float) << 11);
    memset(&g4mag.data[0], 0, sizeof(float) << 11);
  }

  /*  Process */
  /* Pre-Gain */
  /* Crossover + Compression */
  if (fs < 46000) {
    /* SR = 44100    */
    d_SystemCore_parenReference(&b_xOver44, yin_data, yin_size, y1_data, y1_size,
      y2_data, y2_size, y3_data, y3_size, y4_data, y4_size);
    c_y1.size[0] = y1_size[0];
    c_y1.size[1] = 2;
    nx = y1_size[0] * y1_size[1];
    if (0 <= nx - 1) {
      memcpy(&c_y1.data[0], &y1_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y2.size[0] = y2_size[0];
    y2.size[1] = 2;
    nx = y2_size[0] * y2_size[1];
    if (0 <= nx - 1) {
      memcpy(&y2.data[0], &y2_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y3.size[0] = y3_size[0];
    y3.size[1] = 2;
    nx = y3_size[0] * y3_size[1];
    if (0 <= nx - 1) {
      memcpy(&y3.data[0], &y3_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y4.size[0] = y4_size[0];
    y4.size[1] = 2;
    nx = y4_size[0] * y4_size[1];
    if (0 <= nx - 1) {
      memcpy(&y4.data[0], &y4_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    /* Band 1 */
    csz_idx_0 = (short)c_y1.size[0];
    hoistedGlobal_size_idx_0 = (short)c_y1.size[0];
    tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &c_y1.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    b_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    b_abs(maxval_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c1_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, b_g1.data, b_g1.size);

    /* Band 2 */
    csz_idx_0 = (short)y2.size[0];
    hoistedGlobal_size_idx_0 = (short)y2.size[0];
    c_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y2.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, c_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    d_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y2.data[k + y2.size[0]];
    }

    b_abs(maxval_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c2_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g2.data, g2.size);

    /* Band 3 */
    csz_idx_0 = (short)y3.size[0];
    hoistedGlobal_size_idx_0 = (short)y3.size[0];
    e_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y3.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, e_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    f_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y3.data[k + y3.size[0]];
    }

    b_abs(maxval_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c3_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g3.data, g3.size);

    /* Band 4 */
    csz_idx_0 = (short)y4.size[0];
    hoistedGlobal_size_idx_0 = (short)y4.size[0];
    g_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y4.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, g_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    h_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y4.data[k + y4.size[0]];
    }

    b_abs(maxval_data, h_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c4_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g4.data, g4.size);

    /* Construct Output Signal */
    varargin_1_size[0] = b_g1.size[0];
    nx = b_g1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = b_g1.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, b_g1mag.data, b_g1mag.size);
    if (0 <= c_y1.size[0] - 1) {
      memcpy(&varargin_1_data[0], &c_y1.data[0], (unsigned int)(c_y1.size[0] *
              (int)sizeof(float)));
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    if (0 <= c_y1.size[0] - 1) {
      memcpy(&b_y1Padded.data[0], &varargin_1_data[0], (unsigned int)(c_y1.size
              [0] * (int)sizeof(float)));
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    if (0 <= c_y1.size[0] - 1) {
      memcpy(&b_y1Padded.data[2048], &varargin_1_data[0], (unsigned int)
             (c_y1.size[0] * (int)sizeof(float)));
    }

    varargin_1_size[0] = g2.size[0];
    nx = g2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g2.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g2mag.data, g2mag.size);
    if (0 <= y2.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y2.data[0], (unsigned int)(y2.size[0] * (int)
              sizeof(float)));
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    if (0 <= y2.size[0] - 1) {
      memcpy(&y2Padded.data[0], &varargin_1_data[0], (unsigned int)(y2.size[0] *
              (int)sizeof(float)));
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y2.data[k + y2.size[0]];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    if (0 <= y2.size[0] - 1) {
      memcpy(&y2Padded.data[2048], &varargin_1_data[0], (unsigned int)(y2.size[0]
              * (int)sizeof(float)));
    }

    varargin_1_size[0] = g3.size[0];
    nx = g3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g3.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g3mag.data, g3mag.size);
    if (0 <= y3.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y3.data[0], (unsigned int)(y3.size[0] * (int)
              sizeof(float)));
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    if (0 <= y3.size[0] - 1) {
      memcpy(&y3Padded.data[0], &varargin_1_data[0], (unsigned int)(y3.size[0] *
              (int)sizeof(float)));
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y3.data[k + y3.size[0]];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    if (0 <= y3.size[0] - 1) {
      memcpy(&y3Padded.data[2048], &varargin_1_data[0], (unsigned int)(y3.size[0]
              * (int)sizeof(float)));
    }

    varargin_1_size[0] = g4.size[0];
    nx = g4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g4.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g4mag.data, g4mag.size);
    if (0 <= y4.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y4.data[0], (unsigned int)(y4.size[0] * (int)
              sizeof(float)));
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    if (0 <= y4.size[0] - 1) {
      memcpy(&y4Padded.data[0], &varargin_1_data[0], (unsigned int)(y4.size[0] *
              (int)sizeof(float)));
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y4.data[k + y4.size[0]];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    if (0 <= y4.size[0] - 1) {
      memcpy(&y4Padded.data[2048], &varargin_1_data[0], (unsigned int)(y4.size[0]
              * (int)sizeof(float)));
    }
  } else if (fs < 64000) {
    /* SR = 48000    */
    d_SystemCore_parenReference(&b_xOver48, yin_data, yin_size, y1_data, y1_size,
      y2_data, y2_size, y3_data, y3_size, y4_data, y4_size);
    c_y1.size[0] = y1_size[0];
    c_y1.size[1] = 2;
    nx = y1_size[0] * y1_size[1];
    if (0 <= nx - 1) {
      memcpy(&c_y1.data[0], &y1_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y2.size[0] = y2_size[0];
    y2.size[1] = 2;
    nx = y2_size[0] * y2_size[1];
    if (0 <= nx - 1) {
      memcpy(&y2.data[0], &y2_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y3.size[0] = y3_size[0];
    y3.size[1] = 2;
    nx = y3_size[0] * y3_size[1];
    if (0 <= nx - 1) {
      memcpy(&y3.data[0], &y3_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y4.size[0] = y4_size[0];
    y4.size[1] = 2;
    nx = y4_size[0] * y4_size[1];
    if (0 <= nx - 1) {
      memcpy(&y4.data[0], &y4_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    /* Band 1 */
    csz_idx_0 = (short)c_y1.size[0];
    hoistedGlobal_size_idx_0 = (short)c_y1.size[0];
    tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &c_y1.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    b_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    b_abs(maxval_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c1_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, b_g1.data, b_g1.size);

    /* Band 2 */
    csz_idx_0 = (short)y2.size[0];
    hoistedGlobal_size_idx_0 = (short)y2.size[0];
    c_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y2.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, c_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    d_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y2.data[k + y2.size[0]];
    }

    b_abs(maxval_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c2_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g2.data, g2.size);

    /* Band 3 */
    csz_idx_0 = (short)y3.size[0];
    hoistedGlobal_size_idx_0 = (short)y3.size[0];
    e_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y3.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, e_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    f_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y3.data[k + y3.size[0]];
    }

    b_abs(maxval_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c3_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g3.data, g3.size);

    /* Band 4 */
    csz_idx_0 = (short)y4.size[0];
    hoistedGlobal_size_idx_0 = (short)y4.size[0];
    g_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y4.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, g_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    h_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y4.data[k + y4.size[0]];
    }

    b_abs(maxval_data, h_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c4_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g4.data, g4.size);

    /* Construct Output Signal */
    varargin_1_size[0] = b_g1.size[0];
    nx = b_g1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = b_g1.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, b_g1mag.data, b_g1mag.size);
    if (0 <= c_y1.size[0] - 1) {
      memcpy(&varargin_1_data[0], &c_y1.data[0], (unsigned int)(c_y1.size[0] *
              (int)sizeof(float)));
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    if (0 <= c_y1.size[0] - 1) {
      memcpy(&b_y1Padded.data[0], &varargin_1_data[0], (unsigned int)(c_y1.size
              [0] * (int)sizeof(float)));
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    if (0 <= c_y1.size[0] - 1) {
      memcpy(&b_y1Padded.data[2048], &varargin_1_data[0], (unsigned int)
             (c_y1.size[0] * (int)sizeof(float)));
    }

    varargin_1_size[0] = g2.size[0];
    nx = g2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g2.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g2mag.data, g2mag.size);
    if (0 <= y2.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y2.data[0], (unsigned int)(y2.size[0] * (int)
              sizeof(float)));
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    if (0 <= y2.size[0] - 1) {
      memcpy(&y2Padded.data[0], &varargin_1_data[0], (unsigned int)(y2.size[0] *
              (int)sizeof(float)));
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y2.data[k + y2.size[0]];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    if (0 <= y2.size[0] - 1) {
      memcpy(&y2Padded.data[2048], &varargin_1_data[0], (unsigned int)(y2.size[0]
              * (int)sizeof(float)));
    }

    varargin_1_size[0] = g3.size[0];
    nx = g3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g3.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g3mag.data, g3mag.size);
    if (0 <= y3.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y3.data[0], (unsigned int)(y3.size[0] * (int)
              sizeof(float)));
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    if (0 <= y3.size[0] - 1) {
      memcpy(&y3Padded.data[0], &varargin_1_data[0], (unsigned int)(y3.size[0] *
              (int)sizeof(float)));
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y3.data[k + y3.size[0]];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    if (0 <= y3.size[0] - 1) {
      memcpy(&y3Padded.data[2048], &varargin_1_data[0], (unsigned int)(y3.size[0]
              * (int)sizeof(float)));
    }

    varargin_1_size[0] = g4.size[0];
    nx = g4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g4.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g4mag.data, g4mag.size);
    if (0 <= y4.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y4.data[0], (unsigned int)(y4.size[0] * (int)
              sizeof(float)));
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    if (0 <= y4.size[0] - 1) {
      memcpy(&y4Padded.data[0], &varargin_1_data[0], (unsigned int)(y4.size[0] *
              (int)sizeof(float)));
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y4.data[k + y4.size[0]];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    if (0 <= y4.size[0] - 1) {
      memcpy(&y4Padded.data[2048], &varargin_1_data[0], (unsigned int)(y4.size[0]
              * (int)sizeof(float)));
    }
  } else {
    /* SR = 96000    */
    d_SystemCore_parenReference(&b_xOver96, yin_data, yin_size, y1_data, y1_size,
      y2_data, y2_size, y3_data, y3_size, y4_data, y4_size);
    c_y1.size[0] = y1_size[0];
    c_y1.size[1] = 2;
    nx = y1_size[0] * y1_size[1];
    if (0 <= nx - 1) {
      memcpy(&c_y1.data[0], &y1_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y2.size[0] = y2_size[0];
    y2.size[1] = 2;
    nx = y2_size[0] * y2_size[1];
    if (0 <= nx - 1) {
      memcpy(&y2.data[0], &y2_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y3.size[0] = y3_size[0];
    y3.size[1] = 2;
    nx = y3_size[0] * y3_size[1];
    if (0 <= nx - 1) {
      memcpy(&y3.data[0], &y3_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    y4.size[0] = y4_size[0];
    y4.size[1] = 2;
    nx = y4_size[0] * y4_size[1];
    if (0 <= nx - 1) {
      memcpy(&y4.data[0], &y4_data[0], (unsigned int)(nx * (int)sizeof(float)));
    }

    /* Band 1 */
    csz_idx_0 = (short)c_y1.size[0];
    hoistedGlobal_size_idx_0 = (short)c_y1.size[0];
    tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &c_y1.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    b_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    b_abs(maxval_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&b_c1_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, b_g1.data, b_g1.size);

    /* Band 2 */
    csz_idx_0 = (short)y2.size[0];
    hoistedGlobal_size_idx_0 = (short)y2.size[0];
    c_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y2.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, c_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    d_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y2.data[k + y2.size[0]];
    }

    b_abs(maxval_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c2_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g2.data, g2.size);

    /* Band 3 */
    csz_idx_0 = (short)y3.size[0];
    hoistedGlobal_size_idx_0 = (short)y3.size[0];
    e_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y3.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, e_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    f_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y3.data[k + y3.size[0]];
    }

    b_abs(maxval_data, f_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c3_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g3.data, g3.size);

    /* Band 4 */
    csz_idx_0 = (short)y4.size[0];
    hoistedGlobal_size_idx_0 = (short)y4.size[0];
    g_tmp_size[0] = csz_idx_0;
    if (0 <= csz_idx_0 - 1) {
      memcpy(&maxval_data[0], &y4.data[0], (unsigned int)(csz_idx_0 * (int)
              sizeof(float)));
    }

    b_abs(maxval_data, g_tmp_size, varargin_1_data, varargin_1_size);
    nx = hoistedGlobal_size_idx_0;
    h_tmp_size[0] = hoistedGlobal_size_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = y4.data[k + y4.size[0]];
    }

    b_abs(maxval_data, h_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      maxval_data[k] = fmaxf(varargin_1_data[k], varargin_2_data[k]);
    }

    c_SystemCore_parenReference(&c4_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, g4.data, g4.size);

    /* Construct Output Signal */
    varargin_1_size[0] = b_g1.size[0];
    nx = b_g1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = b_g1.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, b_g1mag.data, b_g1mag.size);
    if (0 <= c_y1.size[0] - 1) {
      memcpy(&varargin_1_data[0], &c_y1.data[0], (unsigned int)(c_y1.size[0] *
              (int)sizeof(float)));
    }

    for (k = 0; k < 2048; k++) {
      tmp_data[k] = (short)k;
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      b_y1Padded.data[tmp_data[k]] = varargin_1_data[k];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = c_y1.data[k + c_y1.size[0]];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= b_g1mag.data[k];
    }

    nx = c_y1.size[0];
    for (k = 0; k < nx; k++) {
      b_y1Padded.data[tmp_data[k] + 2048] = varargin_1_data[k];
    }

    varargin_1_size[0] = g2.size[0];
    nx = g2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g2.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g2mag.data, g2mag.size);
    if (0 <= y2.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y2.data[0], (unsigned int)(y2.size[0] * (int)
              sizeof(float)));
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      y2Padded.data[tmp_data[k]] = varargin_1_data[k];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y2.data[k + y2.size[0]];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g2mag.data[k];
    }

    nx = y2.size[0];
    for (k = 0; k < nx; k++) {
      y2Padded.data[tmp_data[k] + 2048] = varargin_1_data[k];
    }

    varargin_1_size[0] = g3.size[0];
    nx = g3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g3.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g3mag.data, g3mag.size);
    if (0 <= y3.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y3.data[0], (unsigned int)(y3.size[0] * (int)
              sizeof(float)));
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      y3Padded.data[tmp_data[k]] = varargin_1_data[k];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y3.data[k + y3.size[0]];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g3mag.data[k];
    }

    nx = y3.size[0];
    for (k = 0; k < nx; k++) {
      y3Padded.data[tmp_data[k] + 2048] = varargin_1_data[k];
    }

    varargin_1_size[0] = g4.size[0];
    nx = g4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = g4.data[k] / 20.0F;
    }

    b_power(varargin_1_data, varargin_1_size, g4mag.data, g4mag.size);
    if (0 <= y4.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y4.data[0], (unsigned int)(y4.size[0] * (int)
              sizeof(float)));
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      y4Padded.data[tmp_data[k]] = varargin_1_data[k];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] = y4.data[k + y4.size[0]];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      varargin_1_data[k] *= g4mag.data[k];
    }

    nx = y4.size[0];
    for (k = 0; k < nx; k++) {
      y4Padded.data[tmp_data[k] + 2048] = varargin_1_data[k];
    }
  }

  /* Output */
  yout_size[0] = 2048;
  yout_size[1] = 2;
  for (k = 0; k < 4096; k++) {
    yout_data[k] = ((b_y1Padded.data[k] + y2Padded.data[k]) + y3Padded.data[k])
      + y4Padded.data[k];
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe4Band_HP_free(void)
{
  crossoverFilter_1 *obj;
  obj = &b_xOver44;
  if (!b_xOver44.matlabCodegenIsDeleted) {
    b_xOver44.matlabCodegenIsDeleted = true;
    if (b_xOver44.isInitialized == 1) {
      b_xOver44.isInitialized = 2;
      if (b_xOver44.isSetupComplete) {
        b_xOver44.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &b_xOver96;
  if (!b_xOver96.matlabCodegenIsDeleted) {
    b_xOver96.matlabCodegenIsDeleted = true;
    if (b_xOver96.isInitialized == 1) {
      b_xOver96.isInitialized = 2;
      if (b_xOver96.isSetupComplete) {
        b_xOver96.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &b_xOver48;
  if (!b_xOver48.matlabCodegenIsDeleted) {
    b_xOver48.matlabCodegenIsDeleted = true;
    if (b_xOver48.isInitialized == 1) {
      b_xOver48.isInitialized = 2;
      if (b_xOver48.isSetupComplete) {
        b_xOver48.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  if (!b_c1_44.matlabCodegenIsDeleted) {
    b_c1_44.matlabCodegenIsDeleted = true;
    if (b_c1_44.isInitialized == 1) {
      b_c1_44.isInitialized = 2;
      if (b_c1_44.isSetupComplete) {
        b_c1_44.pNumChannels = -1.0;
      }
    }
  }

  if (!b_c1_48.matlabCodegenIsDeleted) {
    b_c1_48.matlabCodegenIsDeleted = true;
    if (b_c1_48.isInitialized == 1) {
      b_c1_48.isInitialized = 2;
      if (b_c1_48.isSetupComplete) {
        b_c1_48.pNumChannels = -1.0;
      }
    }
  }

  if (!b_c1_96.matlabCodegenIsDeleted) {
    b_c1_96.matlabCodegenIsDeleted = true;
    if (b_c1_96.isInitialized == 1) {
      b_c1_96.isInitialized = 2;
      if (b_c1_96.isSetupComplete) {
        b_c1_96.pNumChannels = -1.0;
      }
    }
  }

  if (!c2_44.matlabCodegenIsDeleted) {
    c2_44.matlabCodegenIsDeleted = true;
    if (c2_44.isInitialized == 1) {
      c2_44.isInitialized = 2;
      if (c2_44.isSetupComplete) {
        c2_44.pNumChannels = -1.0;
      }
    }
  }

  if (!c2_48.matlabCodegenIsDeleted) {
    c2_48.matlabCodegenIsDeleted = true;
    if (c2_48.isInitialized == 1) {
      c2_48.isInitialized = 2;
      if (c2_48.isSetupComplete) {
        c2_48.pNumChannels = -1.0;
      }
    }
  }

  if (!c2_96.matlabCodegenIsDeleted) {
    c2_96.matlabCodegenIsDeleted = true;
    if (c2_96.isInitialized == 1) {
      c2_96.isInitialized = 2;
      if (c2_96.isSetupComplete) {
        c2_96.pNumChannels = -1.0;
      }
    }
  }

  if (!c3_44.matlabCodegenIsDeleted) {
    c3_44.matlabCodegenIsDeleted = true;
    if (c3_44.isInitialized == 1) {
      c3_44.isInitialized = 2;
      if (c3_44.isSetupComplete) {
        c3_44.pNumChannels = -1.0;
      }
    }
  }

  if (!c3_48.matlabCodegenIsDeleted) {
    c3_48.matlabCodegenIsDeleted = true;
    if (c3_48.isInitialized == 1) {
      c3_48.isInitialized = 2;
      if (c3_48.isSetupComplete) {
        c3_48.pNumChannels = -1.0;
      }
    }
  }

  if (!c3_96.matlabCodegenIsDeleted) {
    c3_96.matlabCodegenIsDeleted = true;
    if (c3_96.isInitialized == 1) {
      c3_96.isInitialized = 2;
      if (c3_96.isSetupComplete) {
        c3_96.pNumChannels = -1.0;
      }
    }
  }

  if (!c4_44.matlabCodegenIsDeleted) {
    c4_44.matlabCodegenIsDeleted = true;
    if (c4_44.isInitialized == 1) {
      c4_44.isInitialized = 2;
      if (c4_44.isSetupComplete) {
        c4_44.pNumChannels = -1.0;
      }
    }
  }

  if (!c4_48.matlabCodegenIsDeleted) {
    c4_48.matlabCodegenIsDeleted = true;
    if (c4_48.isInitialized == 1) {
      c4_48.isInitialized = 2;
      if (c4_48.isSetupComplete) {
        c4_48.pNumChannels = -1.0;
      }
    }
  }

  if (!c4_96.matlabCodegenIsDeleted) {
    c4_96.matlabCodegenIsDeleted = true;
    if (c4_96.isInitialized == 1) {
      c4_96.isInitialized = 2;
      if (c4_96.isSetupComplete) {
        c4_96.pNumChannels = -1.0;
      }
    }
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe4Band_HP_init(void)
{
  y4Padded.size[1] = 0;
  y3Padded.size[1] = 0;
  y2Padded.size[1] = 0;
  b_y1Padded.size[1] = 0;
  y4.size[1] = 0;
  y3.size[1] = 0;
  y2.size[1] = 0;
  c_y1.size[1] = 0;
  g4mag.size[0] = 0;
  g4.size[0] = 0;
  g3mag.size[0] = 0;
  g3.size[0] = 0;
  g2mag.size[0] = 0;
  g2.size[0] = 0;
  b_g1mag.size[0] = 0;
  b_g1.size[0] = 0;
  c4_96.matlabCodegenIsDeleted = true;
  c4_48.matlabCodegenIsDeleted = true;
  c4_44.matlabCodegenIsDeleted = true;
  c3_96.matlabCodegenIsDeleted = true;
  c3_48.matlabCodegenIsDeleted = true;
  c3_44.matlabCodegenIsDeleted = true;
  c2_96.matlabCodegenIsDeleted = true;
  c2_48.matlabCodegenIsDeleted = true;
  c2_44.matlabCodegenIsDeleted = true;
  b_c1_96.matlabCodegenIsDeleted = true;
  b_c1_48.matlabCodegenIsDeleted = true;
  b_c1_44.matlabCodegenIsDeleted = true;
  b_xOver48.matlabCodegenIsDeleted = true;
  b_xOver96.matlabCodegenIsDeleted = true;
  b_xOver44.matlabCodegenIsDeleted = true;
  c_y1.size[0] = 2048;
  c_y1.size[1] = 1;
  y2.size[0] = 2048;
  y2.size[1] = 1;
  y3.size[0] = 2048;
  y3.size[1] = 1;
  y4.size[0] = 2048;
  y4.size[1] = 1;
  memset(&c_y1.data[0], 0, sizeof(float) << 11);
  memset(&y2.data[0], 0, sizeof(float) << 11);
  memset(&y3.data[0], 0, sizeof(float) << 11);
  memset(&y4.data[0], 0, sizeof(float) << 11);
  b_y1Padded.size[0] = 2048;
  b_y1Padded.size[1] = 2;
  y2Padded.size[0] = 2048;
  y2Padded.size[1] = 2;
  y3Padded.size[0] = 2048;
  y3Padded.size[1] = 2;
  y4Padded.size[0] = 2048;
  y4Padded.size[1] = 2;
  memset(&b_y1Padded.data[0], 0, sizeof(float) << 12);
  memset(&y2Padded.data[0], 0, sizeof(float) << 12);
  memset(&y3Padded.data[0], 0, sizeof(float) << 12);
  memset(&y4Padded.data[0], 0, sizeof(float) << 12);
  b_g1.size[0] = 2048;
  b_g1mag.size[0] = 2048;
  g2.size[0] = 2048;
  g2mag.size[0] = 2048;
  g3.size[0] = 2048;
  g3mag.size[0] = 2048;
  g4.size[0] = 2048;
  g4mag.size[0] = 2048;
  memset(&b_g1.data[0], 0, sizeof(float) << 11);
  memset(&b_g1mag.data[0], 0, sizeof(float) << 11);
  memset(&g2.data[0], 0, sizeof(float) << 11);
  memset(&g2mag.data[0], 0, sizeof(float) << 11);
  memset(&g3.data[0], 0, sizeof(float) << 11);
  memset(&g3mag.data[0], 0, sizeof(float) << 11);
  memset(&g4.data[0], 0, sizeof(float) << 11);
  memset(&g4mag.data[0], 0, sizeof(float) << 11);
}

/*
 * File trailer for compMe4Band_HP.c
 *
 * [EOF]
 */

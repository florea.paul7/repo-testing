/*
 * File: SystemCore.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <string.h>
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "SystemCore.h"
#include "CompressorBase.h"
#include "TwoBandCrossoverFilter.h"
#include "tuneCrossoverFilterCoefficients.h"
#include "crossoverFilter.h"
#include "expander.h"
#include "FourBandCrossoverFilter.h"
#include "sort1.h"

/* Function Declarations */
static void SystemCore_checkTunableProps(crossoverFilter *obj);
static void SystemCore_setup(c_audio_internal_FourBandCrosso *obj);
static void SystemCore_setupAndReset(crossoverFilter *obj);
static void c_SystemCore_checkTunableProps(expander *obj);
static bool c_SystemCore_detectInputSizeCha(compressor *obj, const int
  varargin_1_size[1]);
static void c_SystemCore_setupAndReset(expander *obj);
static void d_SystemCore_checkTunableProps(compressor *obj);
static bool d_SystemCore_detectInputSizeCha(crossoverFilter_1 *obj, const int
  varargin_1_size[2]);
static void d_SystemCore_setupAndReset(compressor *obj, const int
  varargin_1_size[1]);
static void e_SystemCore_checkTunableProps(crossoverFilter_1 *obj);
static void e_SystemCore_setupAndReset(crossoverFilter_1 *obj, const int
  varargin_1_size[2]);
static void f_SystemCore_checkTunableProps(c_audio_internal_FourBandCrosso *obj);

/* Function Definitions */

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : void
 */
static void SystemCore_checkTunableProps(crossoverFilter *obj)
{
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    c_crossoverFilter_processTunedP(obj);
  }
}

/*
 * Arguments    : c_audio_internal_FourBandCrosso *obj
 * Return Type  : void
 */
static void SystemCore_setup(c_audio_internal_FourBandCrosso *obj)
{
  int jtilecol;
  int ibtile;
  signed char b[12];
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  for (jtilecol = 0; jtilecol < 4; jtilecol++) {
    ibtile = jtilecol * 3;
    b[ibtile] = 1;
    b[ibtile + 1] = 0;
    b[ibtile + 2] = 0;
  }

  for (jtilecol = 0; jtilecol < 12; jtilecol++) {
    obj->pCR1_B_LP[jtilecol] = b[jtilecol];
    obj->pCR1_A_LP[jtilecol] = b[jtilecol];
    obj->pCR1_B_HP[jtilecol] = b[jtilecol];
    obj->pCR1_A_HP[jtilecol] = b[jtilecol];
    obj->pCR2_B_LP[jtilecol] = b[jtilecol];
    obj->pCR2_A_LP[jtilecol] = b[jtilecol];
    obj->pCR2_B_HP[jtilecol] = b[jtilecol];
    obj->pCR2_A_HP[jtilecol] = b[jtilecol];
    obj->pCR3_B_LP[jtilecol] = b[jtilecol];
    obj->pCR3_A_LP[jtilecol] = b[jtilecol];
    obj->pCR3_B_HP[jtilecol] = b[jtilecol];
    obj->pCR3_A_HP[jtilecol] = b[jtilecol];
  }

  obj->pPhaseMult[0] = 0.0F;
  obj->pPhaseMult[1] = 0.0F;
  obj->pPhaseMult[2] = 0.0F;
  c_FourBandCrossoverFilter_tuneC(obj);
  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
}

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : void
 */
static void SystemCore_setupAndReset(crossoverFilter *obj)
{
  float F0;
  float FsByTwo;
  double value;
  bool flag;
  float AH1[12];
  int i1;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  obj->pNumChannels = 2.0;
  obj->pCrossoverFrequencies = (float)obj->pFreq[0];
  F0 = obj->pCrossoverFrequencies;
  FsByTwo = (float)obj->pSampleRateDialog / 2.0F;
  obj->pCrossoverFrequencies = F0;
  if (F0 >= FsByTwo) {
    obj->pCrossoverFrequencies = FsByTwo - 1.0F;
  }

  if (F0 == 0.0F) {
    obj->pCrossoverFrequencies = 1.0F;
  }

  obj->pOrders = (float)obj->pSlopes[0] / 6.0F;
  value = obj->pSampleRateDialog;
  obj->pFilter.isInitialized = 0;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)value;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies = 500.0F;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders = 2.0F;
  obj->pFilter.isSetupComplete = false;
  obj->pFilter.isInitialized = 1;
  c_TwoBandCrossoverFilter_setupI(&obj->pFilter);
  obj->pFilter.isSetupComplete = true;
  obj->pFilter.TunablePropsChanged = false;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies = obj->pCrossoverFrequencies;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders = obj->pOrders;
  if (obj->pFilter.isInitialized == 1) {
    memset(&obj->pFilter.pCR1_States_LP[0], 0, sizeof(float) << 4);
    memset(&obj->pFilter.pCR1_States_HP[0], 0, sizeof(float) << 4);
    tuneCrossoverFilterCoefficients(obj->pFilter.CrossoverFrequencies,
      obj->pFilter.CrossoverOrders, obj->pFilter.SampleRate,
      obj->pFilter.pCR1_B_LP, obj->pFilter.pCR1_A_LP, obj->pFilter.pCR1_B_HP,
      AH1, &obj->pFilter.pPhaseMult);
    for (i1 = 0; i1 < 12; i1++) {
      obj->pFilter.pCR1_A_HP[i1] = AH1[i1];
    }
  }

  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
  crossoverFilter_resetImpl(obj);
}

/*
 * Arguments    : expander *obj
 * Return Type  : void
 */
static void c_SystemCore_checkTunableProps(expander *obj)
{
  double Fs;
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    Fs = obj->pSampleRateDialog;
    if (obj->AttackTime != 0.0) {
      obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
    } else {
      obj->pAlphaA = 0.0F;
    }

    if (obj->ReleaseTime != 0.0) {
      obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
    } else {
      obj->pAlphaR = 0.0F;
    }

    obj->pHoldTimeSamples = floorf((float)obj->HoldTime * (float)
      obj->pSampleRateDialog);
  }
}

/*
 * Arguments    : compressor *obj
 *                const int varargin_1_size[1]
 * Return Type  : bool
 */
static bool c_SystemCore_detectInputSizeCha(compressor *obj, const int
  varargin_1_size[1])
{
  bool anyInputSizeChanged;
  short inSize[8];
  int k;
  bool exitg1;
  anyInputSizeChanged = false;
  inSize[0] = (short)varargin_1_size[0];
  inSize[1] = 1;
  for (k = 0; k < 6; k++) {
    inSize[k + 2] = 1;
  }

  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 8)) {
    if (obj->inputVarSize[0].f1[k] != (unsigned int)inSize[k]) {
      anyInputSizeChanged = true;
      for (k = 0; k < 8; k++) {
        obj->inputVarSize[0].f1[k] = (unsigned int)inSize[k];
      }

      exitg1 = true;
    } else {
      k++;
    }
  }

  return anyInputSizeChanged;
}

/*
 * Arguments    : expander *obj
 * Return Type  : void
 */
static void c_SystemCore_setupAndReset(expander *obj)
{
  double Fs;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  obj->pLevelDetectionState = 0.0F;
  Fs = obj->pSampleRateDialog;
  if (obj->AttackTime != 0.0) {
    obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
  } else {
    obj->pAlphaA = 0.0F;
  }

  if (obj->ReleaseTime != 0.0) {
    obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
  } else {
    obj->pAlphaR = 0.0F;
  }

  obj->pNumChannels = 1.0;
  obj->pHoldTimeSamples = floorf((float)obj->HoldTime * (float)
    obj->pSampleRateDialog);
  obj->pHoldTimeState[0] = 0.0F;
  obj->pHoldTimeState[1] = 0.0F;
  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
  obj->pHoldTimeSamples = floorf((float)obj->HoldTime * (float)
    obj->pSampleRateDialog);
  obj->pLevelDetectionState = 0.0F;
  Fs = obj->pSampleRateDialog;
  if (obj->AttackTime != 0.0) {
    obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
  } else {
    obj->pAlphaA = 0.0F;
  }

  if (obj->ReleaseTime != 0.0) {
    obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
  } else {
    obj->pAlphaR = 0.0F;
  }

  obj->pHoldTimeState[0] = 0.0F;
  obj->pHoldTimeState[1] = 0.0F;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : void
 */
static void d_SystemCore_checkTunableProps(compressor *obj)
{
  double Fs;
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    Fs = obj->pSampleRateDialog;
    if (obj->AttackTime != 0.0) {
      obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
    } else {
      obj->pAlphaA = 0.0F;
    }

    if (obj->ReleaseTime != 0.0) {
      obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
    } else {
      obj->pAlphaR = 0.0F;
    }

    obj->pMakeUpGain = (float)obj->MakeUpGain;
  }
}

/*
 * Arguments    : crossoverFilter_1 *obj
 *                const int varargin_1_size[2]
 * Return Type  : bool
 */
static bool d_SystemCore_detectInputSizeCha(crossoverFilter_1 *obj, const int
  varargin_1_size[2])
{
  bool anyInputSizeChanged;
  short inSize[8];
  int k;
  bool exitg1;
  anyInputSizeChanged = false;
  inSize[0] = (short)varargin_1_size[0];
  inSize[1] = (short)varargin_1_size[1];
  for (k = 0; k < 6; k++) {
    inSize[k + 2] = 1;
  }

  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 8)) {
    if (obj->inputVarSize[0].f1[k] != (unsigned int)inSize[k]) {
      anyInputSizeChanged = true;
      for (k = 0; k < 8; k++) {
        obj->inputVarSize[0].f1[k] = (unsigned int)inSize[k];
      }

      exitg1 = true;
    } else {
      k++;
    }
  }

  return anyInputSizeChanged;
}

/*
 * Arguments    : compressor *obj
 *                const int varargin_1_size[1]
 * Return Type  : void
 */
static void d_SystemCore_setupAndReset(compressor *obj, const int
  varargin_1_size[1])
{
  int i8;
  double Fs;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  obj->inputVarSize[0].f1[0] = (unsigned int)varargin_1_size[0];
  obj->inputVarSize[0].f1[1] = 1U;
  for (i8 = 0; i8 < 6; i8++) {
    obj->inputVarSize[0].f1[i8 + 2] = 1U;
  }

  obj->pLevelDetectionState = 0.0F;
  Fs = obj->pSampleRateDialog;
  if (obj->AttackTime != 0.0) {
    obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
  } else {
    obj->pAlphaA = 0.0F;
  }

  if (obj->ReleaseTime != 0.0) {
    obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
  } else {
    obj->pAlphaR = 0.0F;
  }

  obj->pNumChannels = 1.0;
  obj->pMakeUpGain = (float)obj->MakeUpGain;
  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
  obj->pLevelDetectionState = 0.0F;
  Fs = obj->pSampleRateDialog;
  if (obj->AttackTime != 0.0) {
    obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
  } else {
    obj->pAlphaA = 0.0F;
  }

  if (obj->ReleaseTime != 0.0) {
    obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
  } else {
    obj->pAlphaR = 0.0F;
  }
}

/*
 * Arguments    : crossoverFilter_1 *obj
 * Return Type  : void
 */
static void e_SystemCore_checkTunableProps(crossoverFilter_1 *obj)
{
  double freq[3];
  int iidx[3];
  float F0;
  float FsByTwo_tmp;
  bool flag;
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    freq[0] = obj->pFreq[0];
    freq[1] = obj->pFreq[1];
    freq[2] = obj->pFreq[2];
    sort(freq, iidx);
    F0 = (float)freq[0];
    FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
    if ((float)freq[0] >= FsByTwo_tmp) {
      F0 = FsByTwo_tmp - 1.0F;
    }

    if ((float)freq[0] == 0.0F) {
      F0 = 1.0F;
    }

    obj->pCrossoverFrequencies[0] = F0;
    obj->pOrders[0] = (float)obj->pSlopes[0] / 6.0F;
    F0 = (float)freq[1];
    FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
    if ((float)freq[1] >= FsByTwo_tmp) {
      F0 = FsByTwo_tmp - 1.0F;
    }

    if ((float)freq[1] == 0.0F) {
      F0 = 1.0F;
    }

    obj->pCrossoverFrequencies[1] = F0;
    obj->pOrders[1] = (float)obj->pSlopes[1] / 6.0F;
    F0 = (float)freq[2];
    FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
    if ((float)freq[2] >= FsByTwo_tmp) {
      F0 = FsByTwo_tmp - 1.0F;
    }

    if ((float)freq[2] == 0.0F) {
      F0 = 1.0F;
    }

    obj->pCrossoverFrequencies[2] = F0;
    obj->pOrders[2] = (float)obj->pSlopes[2] / 6.0F;
    FsByTwo_tmp = obj->pOrders[iidx[1] - 1];
    F0 = obj->pOrders[iidx[2] - 1];
    obj->pOrders[0] = obj->pOrders[iidx[0] - 1];
    obj->pOrders[1] = FsByTwo_tmp;
    obj->pOrders[2] = F0;
    flag = (obj->pFilter.isInitialized == 1);
    if (flag) {
      obj->pFilter.TunablePropsChanged = true;
    }

    obj->pFilter.CrossoverFrequencies[0] = obj->pCrossoverFrequencies[0];
    obj->pFilter.CrossoverFrequencies[1] = obj->pCrossoverFrequencies[1];
    obj->pFilter.CrossoverFrequencies[2] = obj->pCrossoverFrequencies[2];
    flag = (obj->pFilter.isInitialized == 1);
    if (flag) {
      obj->pFilter.TunablePropsChanged = true;
    }

    obj->pFilter.CrossoverOrders[0] = obj->pOrders[0];
    obj->pFilter.CrossoverOrders[1] = obj->pOrders[1];
    obj->pFilter.CrossoverOrders[2] = obj->pOrders[2];
    flag = (obj->pFilter.isInitialized == 1);
    if (flag) {
      obj->pFilter.TunablePropsChanged = true;
    }

    obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
  }
}

/*
 * Arguments    : crossoverFilter_1 *obj
 *                const int varargin_1_size[2]
 * Return Type  : void
 */
static void e_SystemCore_setupAndReset(crossoverFilter_1 *obj, const int
  varargin_1_size[2])
{
  int i9;
  double freq[3];
  int iidx[3];
  float F0;
  float FsByTwo_tmp;
  double value;
  bool flag;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  obj->inputVarSize[0].f1[0] = (unsigned int)varargin_1_size[0];
  obj->inputVarSize[0].f1[1] = (unsigned int)varargin_1_size[1];
  for (i9 = 0; i9 < 6; i9++) {
    obj->inputVarSize[0].f1[i9 + 2] = 1U;
  }

  obj->pNumChannels = 2.0;
  freq[0] = obj->pFreq[0];
  freq[1] = obj->pFreq[1];
  freq[2] = obj->pFreq[2];
  sort(freq, iidx);
  obj->pCrossoverFrequencies[0] = (float)freq[0];
  F0 = obj->pCrossoverFrequencies[0];
  FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
  obj->pCrossoverFrequencies[0] = F0;
  if (F0 >= FsByTwo_tmp) {
    obj->pCrossoverFrequencies[0] = FsByTwo_tmp - 1.0F;
  }

  if (F0 == 0.0F) {
    obj->pCrossoverFrequencies[0] = 1.0F;
  }

  obj->pOrders[0] = (float)obj->pSlopes[0] / 6.0F;
  obj->pCrossoverFrequencies[1] = (float)freq[1];
  F0 = obj->pCrossoverFrequencies[1];
  FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
  obj->pCrossoverFrequencies[1] = F0;
  if (F0 >= FsByTwo_tmp) {
    obj->pCrossoverFrequencies[1] = FsByTwo_tmp - 1.0F;
  }

  if (F0 == 0.0F) {
    obj->pCrossoverFrequencies[1] = 1.0F;
  }

  obj->pOrders[1] = (float)obj->pSlopes[1] / 6.0F;
  obj->pCrossoverFrequencies[2] = (float)freq[2];
  F0 = obj->pCrossoverFrequencies[2];
  FsByTwo_tmp = (float)obj->pSampleRateDialog / 2.0F;
  obj->pCrossoverFrequencies[2] = F0;
  if (F0 >= FsByTwo_tmp) {
    obj->pCrossoverFrequencies[2] = FsByTwo_tmp - 1.0F;
  }

  if (F0 == 0.0F) {
    obj->pCrossoverFrequencies[2] = 1.0F;
  }

  obj->pOrders[2] = (float)obj->pSlopes[2] / 6.0F;
  FsByTwo_tmp = obj->pOrders[iidx[1] - 1];
  F0 = obj->pOrders[iidx[2] - 1];
  obj->pOrders[0] = obj->pOrders[iidx[0] - 1];
  obj->pOrders[1] = FsByTwo_tmp;
  obj->pOrders[2] = F0;
  value = obj->pSampleRateDialog;
  obj->pFilter.isInitialized = 0;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)value;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies[0] = 300.0F;
  obj->pFilter.CrossoverFrequencies[1] = 400.0F;
  obj->pFilter.CrossoverFrequencies[2] = 500.0F;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders[0] = 2.0F;
  obj->pFilter.CrossoverOrders[1] = 2.0F;
  obj->pFilter.CrossoverOrders[2] = 2.0F;
  SystemCore_setup(&obj->pFilter);
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies[0] = obj->pCrossoverFrequencies[0];
  obj->pFilter.CrossoverFrequencies[1] = obj->pCrossoverFrequencies[1];
  obj->pFilter.CrossoverFrequencies[2] = obj->pCrossoverFrequencies[2];
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders[0] = obj->pOrders[0];
  obj->pFilter.CrossoverOrders[1] = obj->pOrders[1];
  obj->pFilter.CrossoverOrders[2] = obj->pOrders[2];
  if (obj->pFilter.isInitialized == 1) {
    c_FourBandCrossoverFilter_reset(&obj->pFilter);
  }

  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies[0] = obj->pCrossoverFrequencies[0];
  obj->pFilter.CrossoverFrequencies[1] = obj->pCrossoverFrequencies[1];
  obj->pFilter.CrossoverFrequencies[2] = obj->pCrossoverFrequencies[2];
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders[0] = obj->pOrders[0];
  obj->pFilter.CrossoverOrders[1] = obj->pOrders[1];
  obj->pFilter.CrossoverOrders[2] = obj->pOrders[2];
  if (obj->pFilter.isInitialized == 1) {
    c_FourBandCrossoverFilter_reset(&obj->pFilter);
  }
}

/*
 * Arguments    : c_audio_internal_FourBandCrosso *obj
 * Return Type  : void
 */
static void f_SystemCore_checkTunableProps(c_audio_internal_FourBandCrosso *obj)
{
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    c_FourBandCrossoverFilter_tuneC(obj);
  }
}

/*
 * Arguments    : crossoverFilter *obj
 *                const float varargin_1[4096]
 *                float varargout_1[4096]
 *                float varargout_2[4096]
 * Return Type  : void
 */
void SystemCore_parenReference(crossoverFilter *obj, const float varargin_1[4096],
  float varargout_1[4096], float varargout_2[4096])
{
  if (obj->isInitialized != 1) {
    SystemCore_setupAndReset(obj);
  }

  SystemCore_checkTunableProps(obj);
  crossoverFilter_stepImpl(obj, varargin_1, varargout_1, varargout_2);
}

/*
 * Arguments    : c_audio_internal_TwoBandCrossov *obj
 * Return Type  : void
 */
void b_SystemCore_checkTunableProps(c_audio_internal_TwoBandCrossov *obj)
{
  float AH1[12];
  int i7;
  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies,
      obj->CrossoverOrders, obj->SampleRate, obj->pCR1_B_LP, obj->pCR1_A_LP,
      obj->pCR1_B_HP, AH1, &obj->pPhaseMult);
    for (i7 = 0; i7 < 12; i7++) {
      obj->pCR1_A_HP[i7] = AH1[i7];
    }
  }
}

/*
 * Arguments    : expander *obj
 *                const float varargin_1_data[]
 *                const int varargin_1_size[1]
 *                float varargout_1_data[]
 *                int varargout_1_size[1]
 *                float varargout_2_data[]
 *                int varargout_2_size[1]
 * Return Type  : void
 */
void b_SystemCore_parenReference(expander *obj, const float varargin_1_data[],
  const int varargin_1_size[1], float varargout_1_data[], int varargout_1_size[1],
  float varargout_2_data[], int varargout_2_size[1])
{
  if (obj->isInitialized != 1) {
    c_SystemCore_setupAndReset(obj);
  }

  c_SystemCore_checkTunableProps(obj);
  expander_stepImpl(obj, varargin_1_data, varargin_1_size, varargout_1_data,
                    varargout_1_size, varargout_2_data, varargout_2_size);
}

/*
 * Arguments    : c_audio_internal_TwoBandCrossov *obj
 * Return Type  : void
 */
void b_SystemCore_setupAndReset(c_audio_internal_TwoBandCrossov *obj)
{
  float AH1[12];
  int i6;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  c_TwoBandCrossoverFilter_setupI(obj);
  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
  memset(&obj->pCR1_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR1_States_HP[0], 0, sizeof(float) << 4);
  tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies,
    obj->CrossoverOrders, obj->SampleRate, obj->pCR1_B_LP, obj->pCR1_A_LP,
    obj->pCR1_B_HP, AH1, &obj->pPhaseMult);
  for (i6 = 0; i6 < 12; i6++) {
    obj->pCR1_A_HP[i6] = AH1[i6];
  }
}

/*
 * Arguments    : compressor *obj
 *                const float varargin_1_data[]
 *                const int varargin_1_size[1]
 *                float varargout_1_data[]
 *                int varargout_1_size[1]
 *                float varargout_2_data[]
 *                int varargout_2_size[1]
 * Return Type  : void
 */
void c_SystemCore_parenReference(compressor *obj, const float varargin_1_data[],
  const int varargin_1_size[1], float varargout_1_data[], int varargout_1_size[1],
  float varargout_2_data[], int varargout_2_size[1])
{
  if (obj->isInitialized != 1) {
    d_SystemCore_setupAndReset(obj, varargin_1_size);
  }

  d_SystemCore_checkTunableProps(obj);
  c_SystemCore_detectInputSizeCha(obj, varargin_1_size);
  CompressorBase_stepImpl(obj, varargin_1_data, varargin_1_size,
    varargout_1_data, varargout_1_size, varargout_2_data, varargout_2_size);
}

/*
 * Arguments    : crossoverFilter_1 *obj
 *                const float varargin_1_data[]
 *                const int varargin_1_size[2]
 *                float varargout_1_data[]
 *                int varargout_1_size[2]
 *                float varargout_2_data[]
 *                int varargout_2_size[2]
 *                float varargout_3_data[]
 *                int varargout_3_size[2]
 *                float varargout_4_data[]
 *                int varargout_4_size[2]
 * Return Type  : void
 */
void d_SystemCore_parenReference(crossoverFilter_1 *obj, const float
  varargin_1_data[], const int varargin_1_size[2], float varargout_1_data[], int
  varargout_1_size[2], float varargout_2_data[], int varargout_2_size[2], float
  varargout_3_data[], int varargout_3_size[2], float varargout_4_data[], int
  varargout_4_size[2])
{
  if (obj->isInitialized != 1) {
    e_SystemCore_setupAndReset(obj, varargin_1_size);
  }

  e_SystemCore_checkTunableProps(obj);
  d_SystemCore_detectInputSizeCha(obj, varargin_1_size);
  if (obj->pFilter.isInitialized != 1) {
    SystemCore_setup(&obj->pFilter);
    c_FourBandCrossoverFilter_reset(&obj->pFilter);
  }

  f_SystemCore_checkTunableProps(&obj->pFilter);
  c_FourBandCrossoverFilter_stepI(&obj->pFilter, varargin_1_data,
    varargin_1_size, varargout_1_data, varargout_1_size, varargout_2_data,
    varargout_2_size, varargout_3_data, varargout_3_size, varargout_4_data,
    varargout_4_size);
}

/*
 * Arguments    : limiter *obj
 *                const float varargin_1_data[]
 *                float varargout_1_data[]
 *                int varargout_1_size[2]
 * Return Type  : void
 */
void e_SystemCore_parenReference(limiter *obj, const float varargin_1_data[],
  float varargout_1_data[], int varargout_1_size[2])
{
  double Fs;
  if (obj->isInitialized != 1) {
    obj->isSetupComplete = false;
    obj->isInitialized = 1;
    obj->pLevelDetectionState[0] = 0.0F;
    obj->pLevelDetectionState[1] = 0.0F;
    Fs = obj->pSampleRateDialog;
    if (obj->AttackTime != 0.0) {
      obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
    } else {
      obj->pAlphaA = 0.0F;
    }

    if (obj->ReleaseTime != 0.0) {
      obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
    } else {
      obj->pAlphaR = 0.0F;
    }

    obj->pNumChannels = 2.0;
    obj->pMakeUpGain = (float)obj->MakeUpGain;
    obj->isSetupComplete = true;
    obj->TunablePropsChanged = false;
    obj->pLevelDetectionState[0] = 0.0F;
    obj->pLevelDetectionState[1] = 0.0F;
    Fs = obj->pSampleRateDialog;
    if (obj->AttackTime != 0.0) {
      obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
    } else {
      obj->pAlphaA = 0.0F;
    }

    if (obj->ReleaseTime != 0.0) {
      obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
    } else {
      obj->pAlphaR = 0.0F;
    }
  }

  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
    Fs = obj->pSampleRateDialog;
    if (obj->AttackTime != 0.0) {
      obj->pAlphaA = expf(-2.19722462F / ((float)obj->AttackTime * (float)Fs));
    } else {
      obj->pAlphaA = 0.0F;
    }

    if (obj->ReleaseTime != 0.0) {
      obj->pAlphaR = expf(-2.19722462F / ((float)obj->ReleaseTime * (float)Fs));
    } else {
      obj->pAlphaR = 0.0F;
    }

    obj->pMakeUpGain = (float)obj->MakeUpGain;
  }

  b_CompressorBase_stepImpl(obj, varargin_1_data, varargout_1_data,
    varargout_1_size);
}

/*
 * Arguments    : crossoverFilter *obj
 *                const float varargin_1_data[]
 *                float varargout_1_data[]
 *                int varargout_1_size[2]
 *                float varargout_2_data[]
 *                int varargout_2_size[2]
 * Return Type  : void
 */
void f_SystemCore_parenReference(crossoverFilter *obj, const float
  varargin_1_data[], float varargout_1_data[], int varargout_1_size[2], float
  varargout_2_data[], int varargout_2_size[2])
{
  if (obj->isInitialized != 1) {
    SystemCore_setupAndReset(obj);
  }

  SystemCore_checkTunableProps(obj);
  if (obj->pFilter.isInitialized != 1) {
    b_SystemCore_setupAndReset(&obj->pFilter);
  }

  b_SystemCore_checkTunableProps(&obj->pFilter);
  varargout_2_size[0] = 2048;
  varargout_2_size[1] = 2;
  memcpy(&varargout_2_data[0], &varargin_1_data[0], sizeof(float) << 12);
  b_TwoBandCrossoverFilter_stepIm(&obj->pFilter, varargout_2_data,
    varargout_2_size, varargout_1_data, varargout_1_size);
}

/*
 * File trailer for SystemCore.c
 *
 * [EOF]
 */

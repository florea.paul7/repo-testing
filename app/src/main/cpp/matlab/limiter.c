/*
 * File: limiter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "limiter.h"
#include "power.h"
#include "abs.h"

/* Function Definitions */

/*
 * Arguments    : const limiter *obj
 *                const float xG_data[]
 *                float G_data[]
 *                int G_size[2]
 * Return Type  : void
 */
void limiter_computeGain(const limiter *obj, const float xG_data[], float
  G_data[], int G_size[2])
{
  float W;
  float T;
  int i;
  float ind2_tmp_data[4096];
  float y_data[4096];
  int y_size[2];
  float y;
  int trueCount;
  int partialTrueCount;
  bool b0;
  bool ind3_data[4096];
  float z;
  short tmp_data[4096];
  int xG_size[1];
  int tmp_size[1];
  W = (float)obj->KneeWidth;
  T = (float)obj->Threshold;
  for (i = 0; i < 4096; i++) {
    G_data[i] = xG_data[i];
    y = xG_data[i] - T;
    ind2_tmp_data[i] = y;
    if (2.0F * y > W) {
      G_data[i] = T;
    }
  }

  if (W != 0.0F) {
    c_abs(ind2_tmp_data, y_data, y_size);
    trueCount = 0;
    for (i = 0; i < 4096; i++) {
      b0 = (2.0F * y_data[i] <= W);
      ind3_data[i] = b0;
      if (b0) {
        trueCount++;
      }
    }

    partialTrueCount = 0;
    for (i = 0; i < 4096; i++) {
      if (ind3_data[i]) {
        tmp_data[partialTrueCount] = (short)(i + 1);
        partialTrueCount++;
      }
    }

    z = W / 2.0F;
    y = 2.0F * W;
    xG_size[0] = trueCount;
    for (partialTrueCount = 0; partialTrueCount < trueCount; partialTrueCount++)
    {
      ind2_tmp_data[partialTrueCount] = (xG_data[tmp_data[partialTrueCount] - 1]
        - T) + z;
    }

    power(ind2_tmp_data, xG_size, y_data, tmp_size);
    i = tmp_size[0];
    for (partialTrueCount = 0; partialTrueCount < i; partialTrueCount++) {
      y_data[partialTrueCount] /= y;
    }

    partialTrueCount = 0;
    for (i = 0; i < 4096; i++) {
      if (ind3_data[i]) {
        G_data[i] = xG_data[i] - y_data[partialTrueCount];
        partialTrueCount++;
      }
    }
  }

  G_size[0] = 2048;
  G_size[1] = 2;
  for (partialTrueCount = 0; partialTrueCount < 4096; partialTrueCount++) {
    G_data[partialTrueCount] -= xG_data[partialTrueCount];
  }
}

/*
 * File trailer for limiter.c
 *
 * [EOF]
 */

/*
 * File: FourBandCrossoverFilter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "FourBandCrossoverFilter.h"
#include "filter.h"
#include "tuneCrossoverFilterCoefficients.h"

/* Function Definitions */

/*
 * Arguments    : c_audio_internal_FourBandCrosso *obj
 * Return Type  : void
 */
void c_FourBandCrossoverFilter_reset(c_audio_internal_FourBandCrosso *obj)
{
  memset(&obj->pCR1_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR1_States_HP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR2_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR2_States_HP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR3_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pCR3_States_HP[0], 0, sizeof(float) << 4);
  memset(&obj->pAP1_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pAP1_States_HP[0], 0, sizeof(float) << 4);
  memset(&obj->pAP2_States_LP[0], 0, sizeof(float) << 4);
  memset(&obj->pAP2_States_HP[0], 0, sizeof(float) << 4);
  c_FourBandCrossoverFilter_tuneC(obj);
}

/*
 * Arguments    : c_audio_internal_FourBandCrosso *obj
 *                const float x_data[]
 *                const int x_size[2]
 *                float z1_data[]
 *                int z1_size[2]
 *                float z2_data[]
 *                int z2_size[2]
 *                float z3_data[]
 *                int z3_size[2]
 *                float z4_data[]
 *                int z4_size[2]
 * Return Type  : void
 */
void c_FourBandCrossoverFilter_stepI(c_audio_internal_FourBandCrosso *obj, const
  float x_data[], const int x_size[2], float z1_data[], int z1_size[2], float
  z2_data[], int z2_size[2], float z3_data[], int z3_size[2], float z4_data[],
  int z4_size[2])
{
  int B1L_tmp;
  float B1L[12];
  float A1L[12];
  int loop_ub_tmp;
  float B1H[12];
  float A1H[12];
  float B2L[12];
  float A2L[12];
  float B2H[12];
  int b_index;
  float S[16];
  float A2H[12];
  float B3L[12];
  float b_B1L[3];
  float A3L[12];
  float b_A1L[3];
  float B3H[12];
  float A3H[12];
  float b_S;
  int b_z2_size[2];
  int outLL_size[2];
  float b_z2_data[4098];
  float outLL_data[4098];
  float c_S[4];
  int b_z4_size[2];
  int b_outLL_size[2];
  float b_A3H[3];
  int c_z2_size[2];
  int b_z1_size[2];
  int d_z2_size[2];
  int c_outLL_size[2];
  int c_z4_size[2];
  int b_z3_size[2];
  int d_z4_size[2];
  for (B1L_tmp = 0; B1L_tmp < 12; B1L_tmp++) {
    B1L[B1L_tmp] = obj->pCR1_B_LP[B1L_tmp];
    A1L[B1L_tmp] = obj->pCR1_A_LP[B1L_tmp];
    B1H[B1L_tmp] = obj->pCR1_B_HP[B1L_tmp];
    A1H[B1L_tmp] = obj->pCR1_A_HP[B1L_tmp];
    B2L[B1L_tmp] = obj->pCR2_B_LP[B1L_tmp];
    A2L[B1L_tmp] = obj->pCR2_A_LP[B1L_tmp];
    B2H[B1L_tmp] = obj->pCR2_B_HP[B1L_tmp];
    A2H[B1L_tmp] = obj->pCR2_A_HP[B1L_tmp];
    B3L[B1L_tmp] = obj->pCR3_B_LP[B1L_tmp];
    A3L[B1L_tmp] = obj->pCR3_A_LP[B1L_tmp];
    B3H[B1L_tmp] = obj->pCR3_B_HP[B1L_tmp];
    A3H[B1L_tmp] = obj->pCR3_A_HP[B1L_tmp];
  }

  z2_size[0] = x_size[0];
  z2_size[1] = 2;
  loop_ub_tmp = x_size[0] * x_size[1];
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&z2_data[0], &x_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    S[B1L_tmp] = obj->pCR1_States_LP[B1L_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B1L[0] = B1L[3 * b_index];
    b_A1L[0] = A1L[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_B1L[1] = B1L[B1L_tmp];
    b_A1L[1] = A1L[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_B1L[2] = B1L[B1L_tmp];
    b_A1L[2] = A1L[B1L_tmp];
    b_z2_size[0] = z2_size[0];
    b_z2_size[1] = 2;
    B1L_tmp = z2_size[0] * z2_size[1];
    if (0 <= B1L_tmp - 1) {
      memcpy(&b_z2_data[0], &z2_data[0], (unsigned int)(B1L_tmp * (int)sizeof
              (float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_B1L, b_A1L, b_z2_data, b_z2_size, c_S, z2_data, z2_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    obj->pCR1_States_LP[B1L_tmp] = S[B1L_tmp];
    b_S = obj->pCR1_States_HP[B1L_tmp];
    S[B1L_tmp] = b_S;
  }

  z4_size[0] = x_size[0];
  z4_size[1] = 2;
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&z4_data[0], &x_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B1H[3 * b_index];
    b_B1L[0] = A1H[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B1H[B1L_tmp];
    b_B1L[1] = A1H[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B1H[B1L_tmp];
    b_B1L[2] = A1H[B1L_tmp];
    b_z4_size[0] = z4_size[0];
    b_z4_size[1] = 2;
    loop_ub_tmp = z4_size[0] * z4_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z4_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, b_z4_size, c_S, z4_data, z4_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    obj->pCR1_States_HP[B1L_tmp] = S[B1L_tmp];
    b_S = obj->pAP2_States_LP[B1L_tmp];
    S[B1L_tmp] = b_S;
  }

  outLL_size[0] = z2_size[0];
  outLL_size[1] = 2;
  loop_ub_tmp = z2_size[0] * z2_size[1];
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&outLL_data[0], &z2_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B3L[3 * b_index];
    b_B1L[0] = A3L[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B3L[B1L_tmp];
    b_B1L[1] = A3L[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B3L[B1L_tmp];
    b_B1L[2] = A3L[B1L_tmp];
    b_outLL_size[0] = outLL_size[0];
    b_outLL_size[1] = 2;
    loop_ub_tmp = outLL_size[0] * outLL_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &outLL_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, b_outLL_size, c_S, outLL_data, outLL_size,
             *(float (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pAP2_States_LP[0], &S[0], sizeof(float) << 4);
  for (B1L_tmp = 0; B1L_tmp < 12; B1L_tmp++) {
    B1L[B1L_tmp] = B3H[B1L_tmp];
  }

  b_S = obj->pPhaseMult[2];
  B1L[0] = B3H[0] * b_S;
  B1L[1] = B3H[1] * b_S;
  B1L[2] = B3H[2] * b_S;
  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    S[B1L_tmp] = obj->pAP2_States_HP[B1L_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B1L[0] = B1L[3 * b_index];
    b_A3H[0] = A3H[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_B1L[1] = B1L[B1L_tmp];
    b_A3H[1] = A3H[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_B1L[2] = B1L[B1L_tmp];
    b_A3H[2] = A3H[B1L_tmp];
    c_z2_size[0] = z2_size[0];
    c_z2_size[1] = 2;
    loop_ub_tmp = z2_size[0] * z2_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z2_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_B1L, b_A3H, b_z2_data, c_z2_size, c_S, z2_data, z2_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pAP2_States_HP[0], &S[0], sizeof(float) << 4);
  z2_size[0] = outLL_size[0];
  z2_size[1] = 2;
  loop_ub_tmp = outLL_size[0] * outLL_size[1] - 1;
  for (B1L_tmp = 0; B1L_tmp <= loop_ub_tmp; B1L_tmp++) {
    z2_data[B1L_tmp] += outLL_data[B1L_tmp];
  }

  z1_size[0] = outLL_size[0];
  z1_size[1] = 2;
  loop_ub_tmp = outLL_size[0] << 1;
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&z1_data[0], &z2_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    S[B1L_tmp] = obj->pCR2_States_LP[B1L_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B2L[3 * b_index];
    b_B1L[0] = A2L[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B2L[B1L_tmp];
    b_B1L[1] = A2L[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B2L[B1L_tmp];
    b_B1L[2] = A2L[B1L_tmp];
    b_z1_size[0] = z1_size[0];
    b_z1_size[1] = 2;
    loop_ub_tmp = z1_size[0] * z1_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z1_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, b_z1_size, c_S, z1_data, z1_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    obj->pCR2_States_LP[B1L_tmp] = S[B1L_tmp];
    b_S = obj->pCR2_States_HP[B1L_tmp];
    S[B1L_tmp] = b_S;
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B2H[3 * b_index];
    b_B1L[0] = A2H[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B2H[B1L_tmp];
    b_B1L[1] = A2H[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B2H[B1L_tmp];
    b_B1L[2] = A2H[B1L_tmp];
    d_z2_size[0] = z2_size[0];
    d_z2_size[1] = 2;
    loop_ub_tmp = z2_size[0] * z2_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z2_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, d_z2_size, c_S, z2_data, z2_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pCR2_States_HP[0], &S[0], sizeof(float) << 4);
  outLL_size[0] = z4_size[0];
  outLL_size[1] = 2;
  loop_ub_tmp = z4_size[0] * z4_size[1];
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&outLL_data[0], &z4_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    S[B1L_tmp] = obj->pAP1_States_LP[B1L_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B2L[3 * b_index];
    b_B1L[0] = A2L[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B2L[B1L_tmp];
    b_B1L[1] = A2L[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B2L[B1L_tmp];
    b_B1L[2] = A2L[B1L_tmp];
    c_outLL_size[0] = outLL_size[0];
    c_outLL_size[1] = 2;
    loop_ub_tmp = outLL_size[0] * outLL_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &outLL_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, c_outLL_size, c_S, outLL_data, outLL_size,
             *(float (*)[4])&S[b_index << 2]);
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    obj->pAP1_States_LP[B1L_tmp] = S[B1L_tmp];
    b_S = obj->pAP1_States_HP[B1L_tmp];
    S[B1L_tmp] = b_S;
  }

  b_S = obj->pPhaseMult[0];
  B2H[0] *= b_S;
  B2H[1] *= b_S;
  B2H[2] *= b_S;
  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B2H[3 * b_index];
    b_B1L[0] = A2H[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B2H[B1L_tmp];
    b_B1L[1] = A2H[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B2H[B1L_tmp];
    b_B1L[2] = A2H[B1L_tmp];
    c_z4_size[0] = z4_size[0];
    c_z4_size[1] = 2;
    loop_ub_tmp = z4_size[0] * z4_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z4_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, c_z4_size, c_S, z4_data, z4_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pAP1_States_HP[0], &S[0], sizeof(float) << 4);
  b_S = obj->pPhaseMult[1];
  z4_size[0] = outLL_size[0];
  z4_size[1] = 2;
  loop_ub_tmp = outLL_size[0] * outLL_size[1] - 1;
  for (B1L_tmp = 0; B1L_tmp <= loop_ub_tmp; B1L_tmp++) {
    z4_data[B1L_tmp] = b_S * (outLL_data[B1L_tmp] + z4_data[B1L_tmp]);
  }

  z3_size[0] = outLL_size[0];
  z3_size[1] = 2;
  loop_ub_tmp = outLL_size[0] << 1;
  if (0 <= loop_ub_tmp - 1) {
    memcpy(&z3_data[0], &z4_data[0], (unsigned int)(loop_ub_tmp * (int)sizeof
            (float)));
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    S[B1L_tmp] = obj->pCR3_States_LP[B1L_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B3L[3 * b_index];
    b_B1L[0] = A3L[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B3L[B1L_tmp];
    b_B1L[1] = A3L[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B3L[B1L_tmp];
    b_B1L[2] = A3L[B1L_tmp];
    b_z3_size[0] = z3_size[0];
    b_z3_size[1] = 2;
    loop_ub_tmp = z3_size[0] * z3_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z3_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_B1L, b_z2_data, b_z3_size, c_S, z3_data, z3_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  for (B1L_tmp = 0; B1L_tmp < 16; B1L_tmp++) {
    obj->pCR3_States_LP[B1L_tmp] = S[B1L_tmp];
    b_S = obj->pCR3_States_HP[B1L_tmp];
    S[B1L_tmp] = b_S;
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_A1L[0] = B3H[3 * b_index];
    b_A3H[0] = A3H[3 * b_index];
    B1L_tmp = 1 + 3 * b_index;
    b_A1L[1] = B3H[B1L_tmp];
    b_A3H[1] = A3H[B1L_tmp];
    B1L_tmp = 2 + 3 * b_index;
    b_A1L[2] = B3H[B1L_tmp];
    b_A3H[2] = A3H[B1L_tmp];
    d_z4_size[0] = z4_size[0];
    d_z4_size[1] = 2;
    loop_ub_tmp = z4_size[0] * z4_size[1];
    if (0 <= loop_ub_tmp - 1) {
      memcpy(&b_z2_data[0], &z4_data[0], (unsigned int)(loop_ub_tmp * (int)
              sizeof(float)));
    }

    B1L_tmp = b_index << 2;
    c_S[0] = S[B1L_tmp];
    c_S[1] = S[1 + B1L_tmp];
    c_S[2] = S[2 + B1L_tmp];
    c_S[3] = S[3 + B1L_tmp];
    b_filter(b_A1L, b_A3H, b_z2_data, d_z4_size, c_S, z4_data, z4_size, *(float
              (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pCR3_States_HP[0], &S[0], sizeof(float) << 4);
  b_S = obj->pPhaseMult[0];
  B1L_tmp = z2_size[0] * z2_size[1];
  z2_size[1] = 2;
  loop_ub_tmp = B1L_tmp - 1;
  for (B1L_tmp = 0; B1L_tmp <= loop_ub_tmp; B1L_tmp++) {
    z2_data[B1L_tmp] *= b_S;
  }

  b_S = obj->pPhaseMult[2];
  B1L_tmp = z4_size[0] * z4_size[1];
  z4_size[1] = 2;
  loop_ub_tmp = B1L_tmp - 1;
  for (B1L_tmp = 0; B1L_tmp <= loop_ub_tmp; B1L_tmp++) {
    z4_data[B1L_tmp] *= b_S;
  }
}

/*
 * Arguments    : c_audio_internal_FourBandCrosso *obj
 * Return Type  : void
 */
void c_FourBandCrossoverFilter_tuneC(c_audio_internal_FourBandCrosso *obj)
{
  tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies[1],
    obj->CrossoverOrders[1], obj->SampleRate, obj->pCR1_B_LP, obj->pCR1_A_LP,
    obj->pCR1_B_HP, obj->pCR1_A_HP, &obj->pPhaseMult[1]);
  tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies[0],
    obj->CrossoverOrders[0], obj->SampleRate, obj->pCR2_B_LP, obj->pCR2_A_LP,
    obj->pCR2_B_HP, obj->pCR2_A_HP, &obj->pPhaseMult[0]);
  tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies[2],
    obj->CrossoverOrders[2], obj->SampleRate, obj->pCR3_B_LP, obj->pCR3_A_LP,
    obj->pCR3_B_HP, obj->pCR3_A_HP, &obj->pPhaseMult[2]);
}

/*
 * File trailer for FourBandCrossoverFilter.c
 *
 * [EOF]
 */

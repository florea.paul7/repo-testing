/*
 * File: compressor.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 08-Apr-2019 16:15:10
 */

/* Include Files */
#include <string.h>
#include "callAllFunctions.h"
#include "compressor.h"
#include "power.h"
#include "abs.h"

/* Function Definitions */

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *b_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.45;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0003;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 19.8;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *c_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.45;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0003;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 19.8;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.45;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0003;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 19.8;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : const compressor *obj
 *                const float xG_data[]
 *                const int xG_size[1]
 *                float G_data[]
 *                int G_size[1]
 * Return Type  : void
 */
void compressor_computeGain(const compressor *obj, const float xG_data[], const
  int xG_size[1], float G_data[], int G_size[1])
{
  float W;
  float R;
  float T;
  int ind2_tmp_size[1];
  int loop_ub;
  int i;
  float ind2_tmp_data[2049];
  boolean_T ind2_data[2049];
  int trueCount;
  int partialTrueCount;
  short tmp_data[2049];
  float z_data[4096];
  float b_tmp_data[2049];
  int tmp_size[1];
  float z;
  float b_z;
  short c_tmp_data[2049];
  int b_xG_size[1];
  W = (float)obj->KneeWidth;
  R = (float)obj->Ratio;
  T = (float)obj->Threshold;
  G_size[0] = xG_size[0];
  if (0 <= xG_size[0] - 1) {
    memcpy(&G_data[0], &xG_data[0], (unsigned int)(xG_size[0] * (int)sizeof
            (float)));
  }

  ind2_tmp_size[0] = xG_size[0];
  loop_ub = xG_size[0];
  for (i = 0; i < loop_ub; i++) {
    ind2_tmp_data[i] = xG_data[i] - T;
  }

  loop_ub = xG_size[0];
  for (i = 0; i < loop_ub; i++) {
    ind2_data[i] = (2.0F * ind2_tmp_data[i] > W);
  }

  loop_ub = xG_size[0] - 1;
  trueCount = 0;
  for (i = 0; i <= loop_ub; i++) {
    if (ind2_data[i]) {
      trueCount++;
    }
  }

  partialTrueCount = 0;
  for (i = 0; i <= loop_ub; i++) {
    if (ind2_data[i]) {
      tmp_data[partialTrueCount] = (short)(i + 1);
      partialTrueCount++;
    }
  }

  for (i = 0; i < trueCount; i++) {
    z_data[i] = (xG_data[tmp_data[i] - 1] - T) / R;
  }

  loop_ub = xG_size[0];
  partialTrueCount = 0;
  for (i = 0; i < loop_ub; i++) {
    if (ind2_data[i]) {
      G_data[i] = T + z_data[partialTrueCount];
      partialTrueCount++;
    }
  }

  if (W != 0.0F) {
    b_abs(ind2_tmp_data, ind2_tmp_size, b_tmp_data, tmp_size);
    loop_ub = tmp_size[0];
    for (i = 0; i < loop_ub; i++) {
      ind2_data[i] = (2.0F * b_tmp_data[i] <= W);
    }

    loop_ub = tmp_size[0] - 1;
    trueCount = 0;
    for (i = 0; i <= loop_ub; i++) {
      if (ind2_data[i]) {
        trueCount++;
      }
    }

    partialTrueCount = 0;
    for (i = 0; i <= loop_ub; i++) {
      if (ind2_data[i]) {
        c_tmp_data[partialTrueCount] = (short)(i + 1);
        partialTrueCount++;
      }
    }

    z = 1.0F / R;
    b_z = W / 2.0F;
    R = 2.0F * W;
    b_xG_size[0] = trueCount;
    for (i = 0; i < trueCount; i++) {
      ind2_tmp_data[i] = (xG_data[c_tmp_data[i] - 1] - T) + b_z;
    }

    power(ind2_tmp_data, b_xG_size, z_data, ind2_tmp_size);
    loop_ub = ind2_tmp_size[0];
    for (i = 0; i < loop_ub; i++) {
      z_data[i] = (z - 1.0F) * z_data[i] / R;
    }

    loop_ub = tmp_size[0];
    partialTrueCount = 0;
    for (i = 0; i < loop_ub; i++) {
      if (ind2_data[i]) {
        G_data[i] = xG_data[i] + z_data[partialTrueCount];
        partialTrueCount++;
      }
    }
  }

  loop_ub = xG_size[0];
  for (i = 0; i < loop_ub; i++) {
    G_data[i] -= xG_data[i];
  }
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *d_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *e_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *f_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *g_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.3;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.008;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *h_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.3;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.008;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *i_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -7.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.3;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.008;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *j_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -6.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *k_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -6.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *l_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -6.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *m_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -8.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.99;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *n_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -8.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.99;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *o_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -8.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.002;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.15;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 2.99;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *p_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.38;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 17.89;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *q_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.38;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 17.89;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *r_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -65.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 1.38;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.25;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 17.89;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *s_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -9.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.1;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *t_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -9.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.1;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : compressor *obj
 * Return Type  : compressor *
 */
compressor *u_compressor_compressor(compressor *obj)
{
  compressor *b_obj;
  boolean_T flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -9.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.2;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.1;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 12.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->MakeUpGain = 4.9;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * File trailer for compressor.c
 *
 * [EOF]
 */

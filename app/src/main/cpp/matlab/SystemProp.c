/*
 * File: SystemProp.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "SystemProp.h"
#include "BACCHcxBuff.h"
#include "callAllFunctions_rtwutil.h"

/* Function Definitions */

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : void
 */
void SystemProp_parseInputs(crossoverFilter *obj)
{
  if (obj->isInitialized == 1) {
    obj->TunablePropsChanged = true;
  }

  obj->pFreq[0] = 40.0;
  if (obj->isInitialized == 1) {
    obj->TunablePropsChanged = true;
  }

  obj->pSlopes[0] = 12.0;
  obj->pSlopes[0] = fmin(6.0 * rt_roundd_snf(obj->pSlopes[0] / 6.0), 48.0);
  if (obj->pSlopes[0] == 0.0) {
    obj->pSlopes[0] = 6.0;
  }

  if (obj->isInitialized == 1) {
    obj->TunablePropsChanged = true;
  }

  obj->pSampleRateDialog = 44100.0;
}

/*
 * File trailer for SystemProp.c
 *
 * [EOF]
 */

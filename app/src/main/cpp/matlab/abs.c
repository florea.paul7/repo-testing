/*
 * File: abs.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "abs.h"

/* Function Definitions */

/*
 * Arguments    : const float x_data[]
 *                const int x_size[1]
 *                float y_data[]
 *                int y_size[1]
 * Return Type  : void
 */
void b_abs(const float x_data[], const int x_size[1], float y_data[], int
           y_size[1])
{
  int nx;
  int k;
  nx = x_size[0];
  y_size[0] = (short)x_size[0];
  for (k = 0; k < nx; k++) {
    y_data[k] = fabsf(x_data[k]);
  }
}

/*
 * Arguments    : const float x_data[]
 *                float y_data[]
 *                int y_size[2]
 * Return Type  : void
 */
void c_abs(const float x_data[], float y_data[], int y_size[2])
{
  int k;
  y_size[0] = 2048;
  y_size[1] = 2;
  for (k = 0; k < 4096; k++) {
    y_data[k] = fabsf(x_data[k]);
  }
}

/*
 * File trailer for abs.c
 *
 * [EOF]
 */

/*
 * File: BACCHcxBuff.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef BACCHCXBUFF_H
#define BACCHCXBUFF_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void BACCHcxBuff(const float yIn[4096], int sampleRate, float yOut_data[],
  int yOut_size[2], float cOut_data[], int cOut_size[2]);
extern void BACCHcxBuff_free(void);
extern void BACCHcxBuff_init(void);
extern void runBefore_not_empty_init(void);

#endif

/*
 * File trailer for BACCHcxBuff.h
 *
 * [EOF]
 */

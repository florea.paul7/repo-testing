/*
 * File: compMe1Band_HP.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "callAllFunctions.h"
#include "compMe1Band_HP.h"
#include "power.h"
#include "SystemCore.h"
#include "abs.h"
#include "db2mag.h"
#include "compressor.h"
#include "expander.h"
#include "crossoverFilter.h"

/* Variable Definitions */
static boolean_T b_runbefore_not_empty;
static int b_oldfs;
static crossoverFilter xOver44;
static crossoverFilter xOver48;
static crossoverFilter xOver96;
static expander e1_44;
static expander e1_48;
static expander e1_96;
static compressor c1_44;
static compressor c1_48;
static compressor c1_96;
static emxArray_real32_T_2048 g1;
static emxArray_real32_T_2048 g1mag;
static emxArray_real32_T_2048x2 b_y1;
static emxArray_real32_T_2049x2 y1Padded;
static emxArray_real32_T_109x2 y1Pad44;
static emxArray_real32_T_119x2 y1Pad48;
static emxArray_real32_T_238x2 y1Pad96;
static double padlen44;
static double padlen48;
static double padlen96;

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void b_runbefore_not_empty_init(void)
{
  b_runbefore_not_empty = false;
}

/*
 * HeadPhone Compressor - Expander + 1 Band Compressor (w/ Lookahead)
 *    FINAL: 190131-Samsung Tab S4
 *    ~ Nicholas Millias
 *
 *    yin     - input signal
 *    preGain - boost before compressor
 *    fs      - samples per second
 *
 *    1. Define Compressor Properties
 *        T:  Compression Threshold [dB]
 *        R:  Compression Ratio
 *        AT: Attack Time [seconds]
 *        RT: Release Time [seconds]
 *        PG: Post Gain [dB]
 *        knee: Range of db soft knee is applied default:6 [dB]
 *    2. Define Expander Properties
 *    3. Crossover Frequency sets LF cut off point
 *
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * Arguments    : float yin[4096]
 *                float preGain
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void compMe1Band_HP(float yin[4096], float preGain, int fs, float yout_data[],
                    int yout_size[2])
{
  float u0;
  int k;
  float unusedU6[4096];
  float e_y1[4096];
  int tmp_size[1];
  float tmp_data[2048];
  float varargin_1_data[2049];
  int varargin_1_size[1];
  int b_tmp_size[1];
  float varargin_2_data[2049];
  short csz_idx_0;
  int maxval_size[1];
  int nx;
  float maxval_data[2049];
  float unusedU7_data[2048];
  float u1;
  int b_varargin_1_size[1];
  int c_tmp_size[1];
  short b_tmp_data[2048];
  int d_tmp_size[1];
  int loop_ub;
  int c_varargin_1_size[1];
  short c_tmp_data[2049];

  /*  First Time Initialization */
  if (!b_runbefore_not_empty) {
    b_oldfs = fs;
    b_runbefore_not_empty = true;

    /* lookahead time in seconds, affects drc and padlen */
    /* Comp Parameters */
    /* dB */
    /* Compression Ratio */
    /* 0.0001; %Attack time in s */
    /* 0.100; %Release Time in s */
    /* v1(20) .  %13%14.9 - RC4 %Post Compression Make-Up Gain */
    /* Expander Parameters  */
    /* Threshold */
    /* Compression Ratio */
    /* .001 %Attack time in s */
    /* Release Time in s */
    /* Crossover Parameters */
    /* TODO:24db induces glitches on sin wave */
    /* Crossover */
    crossoverFilter_crossoverFilter(&xOver44);
    b_crossoverFilter_crossoverFilt(&xOver48);
    c_crossoverFilter_crossoverFilt(&xOver96);

    /* Expander (First Band) */
    /*  %Threshold (dB), Compression Ratio */
    /*          %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    /*         %0.0200 */
    expander_expander(&e1_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    b_expander_expander(&e1_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    c_expander_expander(&e1_96);

    /* limit output to -0.1 dB ); */
    /* Compressors (First Band) */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    compressor_compressor(&c1_44);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    b_compressor_compressor(&c1_48);

    /* limit output to -0.1 dB ); */
    /*  %Threshold (dB), Compression Ration */
    /*              %Attack time is 80% of the lookahead time. (This is the time to go from 10% to 90% of the attack) */
    c_compressor_compressor(&c1_96);

    /* limit output to -0.1 dB ); */
    /* Declare persistent vars used Stage 1 in block processing */
  }

  /*  Reset */
  if (fs != b_oldfs) {
    /* Is this Necessary to zero out buffers or are they simply overwritten? */
    padlen44 = 108.0;
    padlen48 = 118.0;
    padlen96 = 237.0;
    b_y1.size[0] = 2048;
    b_y1.size[1] = 2;
    memset(&b_y1.data[0], 0, sizeof(float) << 12);
    y1Pad44.size[0] = 108;
    y1Pad44.size[1] = 2;
    memset(&y1Pad44.data[0], 0, 216U * sizeof(float));
    y1Pad48.size[0] = 118;
    y1Pad48.size[1] = 2;
    memset(&y1Pad48.data[0], 0, 236U * sizeof(float));
    y1Pad96.size[0] = 237;
    y1Pad96.size[1] = 2;
    memset(&y1Pad96.data[0], 0, 474U * sizeof(float));
    y1Padded.size[0] = 2048;
    y1Padded.size[1] = 2;
    memset(&y1Padded.data[0], 0, sizeof(float) << 12);
    g1.size[0] = 2048;
    g1mag.size[0] = 2048;
    memset(&g1.data[0], 0, sizeof(float) << 11);
    memset(&g1mag.data[0], 0, sizeof(float) << 11);
  }

  /*  Process */
  if ((float)fabs(preGain) > 0.0001) {
    u0 = db2mag(preGain);
    for (k = 0; k < 4096; k++) {
      yin[k] *= u0;
    }

    /* Pre-Gain */
  }

  /* Compression */
  if (fs < 46000) {
    /* SR = 44100 */
    /* Crossover */
    SystemCore_parenReference(&xOver44, yin, unusedU6, e_y1);
    b_y1.size[0] = 2048;
    b_y1.size[1] = 2;
    memcpy(&b_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    b_SystemCore_parenReference(&e1_44, maxval_data, maxval_size, unusedU7_data,
      tmp_size, g1.data, g1.size);

    /* Construct the expanded signal */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    /* Get Gains     */
    c_tmp_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      u0 = b_y1.data[k] * g1mag.data[k];
      b_y1.data[k] = u0;
      u1 = b_y1.data[k + 2048];
      unusedU7_data[k] = u1;
      b_y1.data[k + 2048] = u1 * g1mag.data[k];
      tmp_data[k] = u0;
    }

    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    c_SystemCore_parenReference(&c1_44, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] * (int)
              sizeof(float)));
    }

    /* Delay Input */
    nx = (int)(2048.0 - padlen44);
    y1Padded.size[0] = y1Pad44.size[0] + nx;
    y1Padded.size[1] = 2;
    loop_ub = y1Pad44.size[0];
    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k] = y1Pad44.data[k];
    }

    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k + y1Padded.size[0]] = y1Pad44.data[k + y1Pad44.size[0]];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[k + y1Pad44.size[0]] = b_y1.data[k];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[(k + y1Pad44.size[0]) + y1Padded.size[0]] = b_y1.data[k +
        2048];
    }

    /* Construct the output signal */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    if (0 <= y1Padded.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y1Padded.data[0], (unsigned int)
             (y1Padded.size[0] * (int)sizeof(float)));
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k]] = unusedU7_data[k];
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      varargin_1_data[k] = y1Padded.data[k + y1Padded.size[0]];
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k] + y1Padded.size[0]] = unusedU7_data[k];
    }

    /* Fill Pads for Next Time */
    k = (int)((2048.0 - padlen44) + 1.0);
    loop_ub = 2049 - k;
    y1Pad44.size[0] = loop_ub;
    y1Pad44.size[1] = 2;
    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad44.data[nx] = b_y1.data[(k + nx) - 1];
    }

    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad44.data[nx + y1Pad44.size[0]] = b_y1.data[((int)((2048.0 - padlen44)
        + 1.0) + nx) + 2047];
    }
  } else if (fs < 64000) {
    /* SR = 48000 */
    /* Crossover */
    SystemCore_parenReference(&xOver48, yin, unusedU6, e_y1);
    b_y1.size[0] = 2048;
    b_y1.size[1] = 2;
    memcpy(&b_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    b_SystemCore_parenReference(&e1_48, maxval_data, maxval_size, unusedU7_data,
      tmp_size, g1.data, g1.size);

    /* construct the expanded signal */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    /* Get Gains    */
    c_tmp_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      u0 = b_y1.data[k] * g1mag.data[k];
      b_y1.data[k] = u0;
      u1 = b_y1.data[k + 2048];
      unusedU7_data[k] = u1;
      b_y1.data[k + 2048] = u1 * g1mag.data[k];
      tmp_data[k] = u0;
    }

    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    c_SystemCore_parenReference(&c1_48, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] * (int)
              sizeof(float)));
    }

    /* Delay Input */
    nx = (int)(2048.0 - padlen48);
    y1Padded.size[0] = y1Pad48.size[0] + nx;
    y1Padded.size[1] = 2;
    loop_ub = y1Pad48.size[0];
    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k] = y1Pad48.data[k];
    }

    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k + y1Padded.size[0]] = y1Pad48.data[k + y1Pad48.size[0]];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[k + y1Pad48.size[0]] = b_y1.data[k];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[(k + y1Pad48.size[0]) + y1Padded.size[0]] = b_y1.data[k +
        2048];
    }

    /* Construct Output */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    if (0 <= y1Padded.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y1Padded.data[0], (unsigned int)
             (y1Padded.size[0] * (int)sizeof(float)));
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k]] = unusedU7_data[k];
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      varargin_1_data[k] = y1Padded.data[k + y1Padded.size[0]];
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k] + y1Padded.size[0]] = unusedU7_data[k];
    }

    /* Fill Pads for Next Time */
    k = (int)((2048.0 - padlen48) + 1.0);
    loop_ub = 2049 - k;
    y1Pad48.size[0] = loop_ub;
    y1Pad48.size[1] = 2;
    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad48.data[nx] = b_y1.data[(k + nx) - 1];
    }

    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad48.data[nx + y1Pad48.size[0]] = b_y1.data[((int)((2048.0 - padlen48)
        + 1.0) + nx) + 2047];
    }
  } else {
    /* SR = 96000  */
    /* Crossover */
    SystemCore_parenReference(&xOver96, yin, unusedU6, e_y1);
    b_y1.size[0] = 2048;
    b_y1.size[1] = 2;
    memcpy(&b_y1.data[0], &e_y1[0], sizeof(float) << 12);

    /* Expand */
    tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, tmp_size, varargin_1_data, varargin_1_size);
    b_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, b_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    b_SystemCore_parenReference(&e1_96, maxval_data, maxval_size, unusedU7_data,
      tmp_size, g1.data, g1.size);

    /* construct the expanded signal */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    b_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, b_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    for (k = 0; k < 2048; k++) {
      b_y1.data[k] = yin[k] * g1mag.data[k];
      b_tmp_data[k] = (short)k;
    }

    for (k = 0; k < 2048; k++) {
      b_y1.data[b_tmp_data[k] + 2048] = yin[2048 + k] * g1mag.data[k];
    }

    c_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[0], sizeof(float) << 11);
    b_abs(tmp_data, c_tmp_size, varargin_1_data, varargin_1_size);
    d_tmp_size[0] = 2048;
    memcpy(&tmp_data[0], &b_y1.data[2048], sizeof(float) << 11);
    b_abs(tmp_data, d_tmp_size, varargin_2_data, tmp_size);
    if (varargin_1_size[0] <= tmp_size[0]) {
      csz_idx_0 = (short)varargin_1_size[0];
    } else {
      csz_idx_0 = (short)tmp_size[0];
    }

    maxval_size[0] = csz_idx_0;
    nx = csz_idx_0;
    for (k = 0; k < nx; k++) {
      u0 = varargin_1_data[k];
      u1 = varargin_2_data[k];
      if (u0 > u1) {
        u1 = u0;
      }

      maxval_data[k] = u1;
    }

    c_SystemCore_parenReference(&c1_96, maxval_data, maxval_size,
      varargin_1_data, varargin_1_size, varargin_2_data, tmp_size);
    g1.size[0] = tmp_size[0];
    if (0 <= tmp_size[0] - 1) {
      memcpy(&g1.data[0], &varargin_2_data[0], (unsigned int)(tmp_size[0] * (int)
              sizeof(float)));
    }

    /* Delay Input */
    nx = (int)(2048.0 - padlen96);
    y1Padded.size[0] = y1Pad96.size[0] + nx;
    y1Padded.size[1] = 2;
    loop_ub = y1Pad96.size[0];
    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k] = y1Pad96.data[k];
    }

    for (k = 0; k < loop_ub; k++) {
      y1Padded.data[k + y1Padded.size[0]] = y1Pad96.data[k + y1Pad96.size[0]];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[k + y1Pad96.size[0]] = b_y1.data[k];
    }

    for (k = 0; k < nx; k++) {
      y1Padded.data[(k + y1Pad96.size[0]) + y1Padded.size[0]] = b_y1.data[k +
        2048];
    }

    /* Construct Output */
    memcpy(&varargin_1_data[0], &g1.data[0], sizeof(float) << 11);
    c_varargin_1_size[0] = 2048;
    for (k = 0; k < 2048; k++) {
      maxval_data[k] = varargin_1_data[k] / 20.0F;
    }

    b_power(maxval_data, c_varargin_1_size, varargin_1_data, varargin_1_size);
    g1mag.size[0] = varargin_1_size[0];
    if (0 <= varargin_1_size[0] - 1) {
      memcpy(&g1mag.data[0], &varargin_1_data[0], (unsigned int)
             (varargin_1_size[0] * (int)sizeof(float)));
    }

    if (0 <= y1Padded.size[0] - 1) {
      memcpy(&varargin_1_data[0], &y1Padded.data[0], (unsigned int)
             (y1Padded.size[0] * (int)sizeof(float)));
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k]] = unusedU7_data[k];
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      varargin_1_data[k] = y1Padded.data[k + y1Padded.size[0]];
    }

    loop_ub = (short)y1Padded.size[0] - 1;
    for (k = 0; k <= loop_ub; k++) {
      c_tmp_data[k] = (short)k;
    }

    loop_ub = y1Padded.size[0];
    for (k = 0; k < loop_ub; k++) {
      unusedU7_data[k] = varargin_1_data[k] * g1mag.data[k];
    }

    for (k = 0; k < 2048; k++) {
      y1Padded.data[c_tmp_data[k] + y1Padded.size[0]] = unusedU7_data[k];
    }

    /* Fill Pads for Next Time */
    k = (int)((2048.0 - padlen96) + 1.0);
    loop_ub = 2049 - k;
    y1Pad96.size[0] = loop_ub;
    y1Pad96.size[1] = 2;
    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad96.data[nx] = b_y1.data[(k + nx) - 1];
    }

    for (nx = 0; nx < loop_ub; nx++) {
      y1Pad96.data[nx + y1Pad96.size[0]] = b_y1.data[((int)((2048.0 - padlen96)
        + 1.0) + nx) + 2047];
    }
  }

  /* Output */
  yout_size[0] = y1Padded.size[0];
  yout_size[1] = 2;
  loop_ub = y1Padded.size[0] * y1Padded.size[1];
  if (0 <= loop_ub - 1) {
    memcpy(&yout_data[0], &y1Padded.data[0], (unsigned int)(loop_ub * (int)
            sizeof(float)));
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe1Band_HP_free(void)
{
  crossoverFilter *obj;
  obj = &xOver44;
  if (!xOver44.matlabCodegenIsDeleted) {
    xOver44.matlabCodegenIsDeleted = true;
    if (xOver44.isInitialized == 1) {
      xOver44.isInitialized = 2;
      if (xOver44.isSetupComplete) {
        xOver44.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &xOver48;
  if (!xOver48.matlabCodegenIsDeleted) {
    xOver48.matlabCodegenIsDeleted = true;
    if (xOver48.isInitialized == 1) {
      xOver48.isInitialized = 2;
      if (xOver48.isSetupComplete) {
        xOver48.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  obj = &xOver96;
  if (!xOver96.matlabCodegenIsDeleted) {
    xOver96.matlabCodegenIsDeleted = true;
    if (xOver96.isInitialized == 1) {
      xOver96.isInitialized = 2;
      if (xOver96.isSetupComplete) {
        xOver96.pNumChannels = -1.0;
        if (obj->pFilter.isInitialized == 1) {
          obj->pFilter.isInitialized = 2;
        }
      }
    }
  }

  if (!e1_44.matlabCodegenIsDeleted) {
    e1_44.matlabCodegenIsDeleted = true;
    if (e1_44.isInitialized == 1) {
      e1_44.isInitialized = 2;
      if (e1_44.isSetupComplete) {
        e1_44.pNumChannels = -1.0;
      }
    }
  }

  if (!e1_48.matlabCodegenIsDeleted) {
    e1_48.matlabCodegenIsDeleted = true;
    if (e1_48.isInitialized == 1) {
      e1_48.isInitialized = 2;
      if (e1_48.isSetupComplete) {
        e1_48.pNumChannels = -1.0;
      }
    }
  }

  if (!e1_96.matlabCodegenIsDeleted) {
    e1_96.matlabCodegenIsDeleted = true;
    if (e1_96.isInitialized == 1) {
      e1_96.isInitialized = 2;
      if (e1_96.isSetupComplete) {
        e1_96.pNumChannels = -1.0;
      }
    }
  }

  if (!c1_44.matlabCodegenIsDeleted) {
    c1_44.matlabCodegenIsDeleted = true;
    if (c1_44.isInitialized == 1) {
      c1_44.isInitialized = 2;
      if (c1_44.isSetupComplete) {
        c1_44.pNumChannels = -1.0;
      }
    }
  }

  if (!c1_48.matlabCodegenIsDeleted) {
    c1_48.matlabCodegenIsDeleted = true;
    if (c1_48.isInitialized == 1) {
      c1_48.isInitialized = 2;
      if (c1_48.isSetupComplete) {
        c1_48.pNumChannels = -1.0;
      }
    }
  }

  if (!c1_96.matlabCodegenIsDeleted) {
    c1_96.matlabCodegenIsDeleted = true;
    if (c1_96.isInitialized == 1) {
      c1_96.isInitialized = 2;
      if (c1_96.isSetupComplete) {
        c1_96.pNumChannels = -1.0;
      }
    }
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void compMe1Band_HP_init(void)
{
  y1Pad96.size[1] = 0;
  y1Pad48.size[1] = 0;
  y1Pad44.size[1] = 0;
  y1Padded.size[1] = 0;
  b_y1.size[1] = 0;
  g1mag.size[0] = 0;
  g1.size[0] = 0;
  c1_96.matlabCodegenIsDeleted = true;
  c1_48.matlabCodegenIsDeleted = true;
  c1_44.matlabCodegenIsDeleted = true;
  e1_96.matlabCodegenIsDeleted = true;
  e1_48.matlabCodegenIsDeleted = true;
  e1_44.matlabCodegenIsDeleted = true;
  xOver96.matlabCodegenIsDeleted = true;
  xOver48.matlabCodegenIsDeleted = true;
  xOver44.matlabCodegenIsDeleted = true;
  padlen44 = 109.0;
  padlen48 = 119.0;
  padlen96 = 238.0;
  b_y1.size[0] = 2048;
  b_y1.size[1] = 2;
  memset(&b_y1.data[0], 0, sizeof(float) << 12);
  y1Pad44.size[0] = 109;
  y1Pad44.size[1] = 2;
  memset(&y1Pad44.data[0], 0, 218U * sizeof(float));
  y1Pad48.size[0] = 119;
  y1Pad48.size[1] = 2;
  memset(&y1Pad48.data[0], 0, 238U * sizeof(float));
  y1Pad96.size[0] = 238;
  y1Pad96.size[1] = 2;
  memset(&y1Pad96.data[0], 0, 476U * sizeof(float));
  y1Padded.size[0] = 2048;
  y1Padded.size[1] = 2;
  memset(&y1Padded.data[0], 0, sizeof(float) << 12);
  g1.size[0] = 2048;
  g1mag.size[0] = 2048;
  memset(&g1.data[0], 0, sizeof(float) << 11);
  memset(&g1mag.data[0], 0, sizeof(float) << 11);
}

/*
 * File trailer for compMe1Band_HP.c
 *
 * [EOF]
 */

/*
 * File: callAllFunctions.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

#ifndef CALLALLFUNCTIONS_H
#define CALLALLFUNCTIONS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void callAllFunctions(const emxArray_real32_T *yin, float preGainT, int
  Fs, emxArray_real32_T *yout);

#endif

/*
 * File trailer for callAllFunctions.h
 *
 * [EOF]
 */

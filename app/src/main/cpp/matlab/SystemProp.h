/*
 * File: SystemProp.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef SYSTEMPROP_H
#define SYSTEMPROP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void SystemProp_parseInputs(crossoverFilter *obj);

#endif

/*
 * File trailer for SystemProp.h
 *
 * [EOF]
 */

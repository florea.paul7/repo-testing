/*
 * File: callAllFunctions_data.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "callAllFunctions_data.h"

/* Variable Definitions */
creal32_T minimag;

/*
 * File trailer for callAllFunctions_data.c
 *
 * [EOF]
 */

/*
 * File: ExpanderBase.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "ExpanderBase.h"

/* Function Definitions */

/*
 * Arguments    : expander *obj
 *                const float x_data[]
 *                float yout_data[]
 *                int yout_size[1]
 * Return Type  : void
 */
void ExpanderBase_detectLevel(expander *obj, const float x_data[], float
  yout_data[], int yout_size[1])
{
  float y_data[2049];
  float alphaA;
  float alphaR;
  float atck_count;
  float rel_count;
  float lim;
  int i;
  float f1;
  memset(&y_data[0], 0, 2049U * sizeof(float));
  y_data[0] = obj->pLevelDetectionState;
  alphaA = obj->pAlphaA;
  alphaR = obj->pAlphaR;
  atck_count = obj->pHoldTimeState[0];
  rel_count = obj->pHoldTimeState[1];
  lim = obj->pHoldTimeSamples;
  yout_size[0] = 2048;
  for (i = 0; i < 2048; i++) {
    if (x_data[i] == y_data[i]) {
      f1 = y_data[i];
      y_data[i + 1] = y_data[i];
    } else if (x_data[i] < y_data[i]) {
      rel_count = 0.0F;
      if (atck_count < lim) {
        atck_count++;
        f1 = y_data[i];
        y_data[i + 1] = y_data[i];
      } else {
        f1 = alphaA * y_data[i] + (1.0F - alphaA) * x_data[i];
        y_data[i + 1] = f1;
      }
    } else {
      atck_count = 0.0F;
      if (rel_count < lim) {
        rel_count++;
        f1 = y_data[i];
        y_data[i + 1] = y_data[i];
      } else {
        f1 = alphaR * y_data[i] + (1.0F - alphaR) * x_data[i];
        y_data[i + 1] = f1;
      }
    }

    yout_data[i] = f1;
  }

  obj->pLevelDetectionState = y_data[2048];
  obj->pHoldTimeState[0] = atck_count;
  obj->pHoldTimeState[1] = rel_count;
}

/*
 * File trailer for ExpanderBase.c
 *
 * [EOF]
 */

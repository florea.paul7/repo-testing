/*
 * File: db2mag.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

#ifndef DB2MAG_H
#define DB2MAG_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern float db2mag(float ydb);

#endif

/*
 * File trailer for db2mag.h
 *
 * [EOF]
 */

/*
 * File: hpCompress.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <string.h>
#include "callAllFunctions.h"
#include "hpCompress.h"
#include "limitMe.h"
#include "compMe4Band_HP.h"
#include "compMe1Band_HP.h"

/* Variable Definitions */
static boolean_T runbefore_not_empty;
static emxArray_real32_T_2049x2 yout1;
static emxArray_real32_T_2048x2 yout2;
static int oldfs;

/* Function Definitions */

/*
 * HPCOMPRESS - Headphone Compression Block
 *    Calls Expander + 1Band Compressor followed by 4 band compressor
 *    followed by a limiter
 * Arguments    : const float yin[4096]
 *                float preGain0
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void hpCompress(const float yin[4096], float preGain0, int fs, float yout_data[],
                int yout_size[2])
{
  float b_yin[4096];
  if (!runbefore_not_empty) {
    oldfs = fs;
    runbefore_not_empty = true;
  }

  if (oldfs != fs) {
    yout1.size[0] = 2048;
    yout1.size[1] = 2;
    yout2.size[0] = 2048;
    yout2.size[1] = 2;
    memset(&yout1.data[0], 0, sizeof(float) << 12);
    memset(&yout2.data[0], 0, sizeof(float) << 12);
  }

  /* Process Audio */
  memcpy(&b_yin[0], &yin[0], sizeof(float) << 12);
  compMe1Band_HP(b_yin, preGain0, fs, yout1.data, yout1.size);
  compMe4Band_HP(yout1.data, yout1.size, fs, yout2.data, yout2.size);
  limitMe(yout2.data, fs, yout_data, yout_size);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void hpCompress_init(void)
{
  yout2.size[1] = 0;
  yout1.size[1] = 0;
  yout1.size[0] = 2048;
  yout1.size[1] = 2;
  yout2.size[0] = 2048;
  yout2.size[1] = 2;
  memset(&yout1.data[0], 0, sizeof(float) << 12);
  memset(&yout2.data[0], 0, sizeof(float) << 12);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void runbefore_not_empty_init(void)
{
  runbefore_not_empty = false;
}

/*
 * File trailer for hpCompress.c
 *
 * [EOF]
 */

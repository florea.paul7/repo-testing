/*
 * File: SystemCore.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef SYSTEMCORE_H
#define SYSTEMCORE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void SystemCore_parenReference(crossoverFilter *obj, const float
  varargin_1[4096], float varargout_1[4096], float varargout_2[4096]);
extern void b_SystemCore_checkTunableProps(c_audio_internal_TwoBandCrossov *obj);
extern void b_SystemCore_parenReference(expander *obj, const float
  varargin_1_data[], const int varargin_1_size[1], float varargout_1_data[], int
  varargout_1_size[1], float varargout_2_data[], int varargout_2_size[1]);
extern void b_SystemCore_setupAndReset(c_audio_internal_TwoBandCrossov *obj);
extern void c_SystemCore_parenReference(compressor *obj, const float
  varargin_1_data[], const int varargin_1_size[1], float varargout_1_data[], int
  varargout_1_size[1], float varargout_2_data[], int varargout_2_size[1]);
extern void d_SystemCore_parenReference(crossoverFilter_1 *obj, const float
  varargin_1_data[], const int varargin_1_size[2], float varargout_1_data[], int
  varargout_1_size[2], float varargout_2_data[], int varargout_2_size[2], float
  varargout_3_data[], int varargout_3_size[2], float varargout_4_data[], int
  varargout_4_size[2]);
extern void e_SystemCore_parenReference(limiter *obj, const float
  varargin_1_data[], float varargout_1_data[], int varargout_1_size[2]);
extern void f_SystemCore_parenReference(crossoverFilter *obj, const float
  varargin_1_data[], float varargout_1_data[], int varargout_1_size[2], float
  varargout_2_data[], int varargout_2_size[2]);

#endif

/*
 * File trailer for SystemCore.h
 *
 * [EOF]
 */

/*
 * File: TwoBandCrossoverFilter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <arm_neon.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "TwoBandCrossoverFilter.h"
#include "filter.h"
#include "tuneCrossoverFilterCoefficients.h"

/* Function Definitions */

/*
 * Arguments    : c_audio_internal_TwoBandCrossov *obj
 *                float x[4096]
 *                float outL[4096]
 * Return Type  : void
 */
void TwoBandCrossoverFilter_stepImpl(c_audio_internal_TwoBandCrossov *obj, float
  x[4096], float outL[4096])
{
  int B_tmp;
  float B[12];
  float A[12];
  int b_index;
  float S[16];
  float b_B[3];
  float b_A[3];
  float b;
  float b_outL[4096];
  float b_S[4];
  __attribute__((aligned(16))) float fv3[4096];
  float32x4_t r2;
  float32x4_t r3;
  memcpy(&outL[0], &x[0], sizeof(float) << 12);
  for (B_tmp = 0; B_tmp < 12; B_tmp++) {
    B[B_tmp] = obj->pCR1_B_LP[B_tmp];
    A[B_tmp] = obj->pCR1_A_LP[B_tmp];
  }

  for (B_tmp = 0; B_tmp < 16; B_tmp++) {
    S[B_tmp] = obj->pCR1_States_LP[B_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B[0] = B[3 * b_index];
    b_A[0] = A[3 * b_index];
    B_tmp = 1 + 3 * b_index;
    b_B[1] = B[B_tmp];
    b_A[1] = A[B_tmp];
    B_tmp = 2 + 3 * b_index;
    b_B[2] = B[B_tmp];
    b_A[2] = A[B_tmp];
    memcpy(&b_outL[0], &outL[0], sizeof(float) << 12);
    B_tmp = b_index << 2;
    b_S[0] = S[B_tmp];
    b_S[1] = S[1 + B_tmp];
    b_S[2] = S[2 + B_tmp];
    b_S[3] = S[3 + B_tmp];
    filter(b_B, b_A, b_outL, b_S, outL, *(float (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pCR1_States_LP[0], &S[0], sizeof(float) << 4);
  for (B_tmp = 0; B_tmp < 12; B_tmp++) {
    B[B_tmp] = obj->pCR1_B_HP[B_tmp];
    A[B_tmp] = obj->pCR1_A_HP[B_tmp];
  }

  for (B_tmp = 0; B_tmp < 16; B_tmp++) {
    S[B_tmp] = obj->pCR1_States_HP[B_tmp];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B[0] = B[3 * b_index];
    b_A[0] = A[3 * b_index];
    B_tmp = 1 + 3 * b_index;
    b_B[1] = B[B_tmp];
    b_A[1] = A[B_tmp];
    B_tmp = 2 + 3 * b_index;
    b_B[2] = B[B_tmp];
    b_A[2] = A[B_tmp];
    memcpy(&b_outL[0], &x[0], sizeof(float) << 12);
    B_tmp = b_index << 2;
    b_S[0] = S[B_tmp];
    b_S[1] = S[1 + B_tmp];
    b_S[2] = S[2 + B_tmp];
    b_S[3] = S[3 + B_tmp];
    filter(b_B, b_A, b_outL, b_S, x, *(float (*)[4])&S[b_index << 2]);
  }

  memcpy(&obj->pCR1_States_HP[0], &S[0], sizeof(float) << 4);
  b = obj->pPhaseMult;
  for (B_tmp = 0; B_tmp <= 4092; B_tmp += 4) {
    r2 = vld1q_f32(*(float (*)[4])&x[B_tmp]);
    r3 = vmulq_n_f32(r2, b);
    vst1q_f32(&fv3[B_tmp], r3);
  }

  memcpy(&x[0], &fv3[0], sizeof(float) << 12);
}

/*
 * Arguments    : c_audio_internal_TwoBandCrossov *obj
 *                float x_data[]
 *                int x_size[2]
 *                float outL_data[]
 *                int outL_size[2]
 * Return Type  : void
 */
void b_TwoBandCrossoverFilter_stepIm(c_audio_internal_TwoBandCrossov *obj, float
  x_data[], int x_size[2], float outL_data[], int outL_size[2])
{
  int i16;
  float B[12];
  float A[12];
  int b_index;
  float S[16];
  float b_B[3];
  float b_A[3];
  int B_tmp;
  float b;
  float b_S[4];
  float b_outL_data[4098];
  int b_outL_size[2];
  outL_size[0] = 2048;
  outL_size[1] = 2;
  memcpy(&outL_data[0], &x_data[0], sizeof(float) << 12);
  for (i16 = 0; i16 < 12; i16++) {
    B[i16] = obj->pCR1_B_LP[i16];
    A[i16] = obj->pCR1_A_LP[i16];
  }

  for (i16 = 0; i16 < 16; i16++) {
    S[i16] = obj->pCR1_States_LP[i16];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B[0] = B[3 * b_index];
    b_A[0] = A[3 * b_index];
    B_tmp = 1 + 3 * b_index;
    b_B[1] = B[B_tmp];
    b_A[1] = A[B_tmp];
    B_tmp = 2 + 3 * b_index;
    b_B[2] = B[B_tmp];
    b_A[2] = A[B_tmp];
    B_tmp = b_index << 2;
    b_S[0] = S[B_tmp];
    b_S[1] = S[1 + B_tmp];
    b_S[2] = S[2 + B_tmp];
    b_S[3] = S[3 + B_tmp];
    b_filter(b_B, b_A, outL_data, outL_size, b_S, b_outL_data, b_outL_size,
             *(float (*)[4])&S[b_index << 2]);
    outL_size[0] = b_outL_size[0];
    outL_size[1] = 2;
    B_tmp = b_outL_size[0] * b_outL_size[1];
    if (0 <= B_tmp - 1) {
      memcpy(&outL_data[0], &b_outL_data[0], (unsigned int)(B_tmp * (int)sizeof
              (float)));
    }
  }

  memcpy(&obj->pCR1_States_LP[0], &S[0], sizeof(float) << 4);
  for (i16 = 0; i16 < 12; i16++) {
    B[i16] = obj->pCR1_B_HP[i16];
    A[i16] = obj->pCR1_A_HP[i16];
  }

  for (i16 = 0; i16 < 16; i16++) {
    S[i16] = obj->pCR1_States_HP[i16];
  }

  for (b_index = 0; b_index < 4; b_index++) {
    b_B[0] = B[3 * b_index];
    b_A[0] = A[3 * b_index];
    B_tmp = 1 + 3 * b_index;
    b_B[1] = B[B_tmp];
    b_A[1] = A[B_tmp];
    B_tmp = 2 + 3 * b_index;
    b_B[2] = B[B_tmp];
    b_A[2] = A[B_tmp];
    B_tmp = b_index << 2;
    b_S[0] = S[B_tmp];
    b_S[1] = S[1 + B_tmp];
    b_S[2] = S[2 + B_tmp];
    b_S[3] = S[3 + B_tmp];
    b_filter(b_B, b_A, x_data, x_size, b_S, b_outL_data, b_outL_size, *(float (*)
              [4])&S[b_index << 2]);
    B_tmp = b_outL_size[0];
    x_size[0] = b_outL_size[0];
    x_size[1] = 2;
    for (i16 = 0; i16 < B_tmp; i16++) {
      x_data[i16] = b_outL_data[i16];
    }

    for (i16 = 0; i16 < B_tmp; i16++) {
      x_data[i16 + 2048] = b_outL_data[i16 + b_outL_size[0]];
    }
  }

  memcpy(&obj->pCR1_States_HP[0], &S[0], sizeof(float) << 4);
  b = obj->pPhaseMult;
  x_size[0] = 2048;
  x_size[1] = 2;
  for (i16 = 0; i16 < 2; i16++) {
    for (B_tmp = 0; B_tmp < 2048; B_tmp++) {
      b_index = B_tmp + (i16 << 11);
      x_data[b_index] *= b;
    }
  }
}

/*
 * Arguments    : c_audio_internal_TwoBandCrossov *obj
 * Return Type  : void
 */
void c_TwoBandCrossoverFilter_setupI(c_audio_internal_TwoBandCrossov *obj)
{
  int jtilecol;
  int ibtile;
  signed char b[12];
  float AH1[12];
  for (jtilecol = 0; jtilecol < 4; jtilecol++) {
    ibtile = jtilecol * 3;
    b[ibtile] = 1;
    b[ibtile + 1] = 0;
    b[ibtile + 2] = 0;
  }

  for (jtilecol = 0; jtilecol < 12; jtilecol++) {
    obj->pCR1_B_LP[jtilecol] = b[jtilecol];
    obj->pCR1_A_LP[jtilecol] = b[jtilecol];
    obj->pCR1_B_HP[jtilecol] = b[jtilecol];
    obj->pCR1_A_HP[jtilecol] = b[jtilecol];
  }

  obj->pPhaseMult = 0.0F;
  tuneCrossoverFilterCoefficients(obj->CrossoverFrequencies,
    obj->CrossoverOrders, obj->SampleRate, obj->pCR1_B_LP, obj->pCR1_A_LP,
    obj->pCR1_B_HP, AH1, &obj->pPhaseMult);
  for (jtilecol = 0; jtilecol < 12; jtilecol++) {
    obj->pCR1_A_HP[jtilecol] = AH1[jtilecol];
  }
}

/*
 * File trailer for TwoBandCrossoverFilter.c
 *
 * [EOF]
 */

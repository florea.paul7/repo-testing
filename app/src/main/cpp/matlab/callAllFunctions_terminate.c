/*
 * File: callAllFunctions_terminate.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "callAllFunctions_terminate.h"
#include "compMe1Band_HP.h"
#include "compMe4Band_HP.h"
#include "compMe1Band_SPK.h"
#include "BACCHcxBuff.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void callAllFunctions_terminate(void)
{
  BACCHcxBuff_free();
  compMe1Band_SPK_free();
  compMe4Band_HP_free();
  compMe1Band_HP_free();
}

/*
 * File trailer for callAllFunctions_terminate.c
 *
 * [EOF]
 */

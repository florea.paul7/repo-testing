/*
 * File: mltwinMod.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef MLTWINMOD_H
#define MLTWINMOD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_mltwinMod(double outWin[4096]);
extern void mltwinMod(double outWin[2048]);

#endif

/*
 * File trailer for mltwinMod.h
 *
 * [EOF]
 */

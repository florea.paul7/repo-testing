/*
 * File: limitMe.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "limitMe.h"
#include "SystemCore.h"

/* Variable Definitions */
static bool d_runbefore_not_empty;
static limiter l1_44;
static limiter l1_48;
static limiter l1_96;

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void d_runbefore_not_empty_init(void)
{
  d_runbefore_not_empty = false;
}

/*
 * Limiter (IN PROGRESS)
 *    yin     - input signal (MONO)
 *    fs      - samples per second
 *
 *    1. Define Limiter Properties
 *        T:  Limiter Threshold [dB]
 *        AT: Attack Time [seconds]
 *        RT: Release Time [seconds]
 *        PG: Make Up Gain [dB]
 *        K1: Range of db soft knee is applied default:6 [dB]
 *
 * %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 * Arguments    : const float yin_data[]
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void limitMe(const float yin_data[], int fs, float yout_data[], int yout_size[2])
{
  /* coder directive to prevent in-lining of this function so that it is easy to call in the C project */
  /*  First Time Initialization */
  if (!d_runbefore_not_empty) {
    d_runbefore_not_empty = true;

    /* Limiter Parameters */
    /* dB */
    /* Attack time in s */
    /* Release Time in s */
    /* Post Limiter Make-Up Gain */
    /* Range of DB for Knee */
    /* Limiters */
    l1_44.pNumChannels = -1.0;
    l1_44.isInitialized = 0;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.Threshold = -0.05;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.pSampleRateDialog = 44100.0;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.AttackTime = 1.0E-5;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.ReleaseTime = 0.1;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.KneeWidth = 0.0;
    if (l1_44.isInitialized == 1) {
      l1_44.TunablePropsChanged = true;
    }

    l1_44.MakeUpGain = 0.0;
    l1_44.matlabCodegenIsDeleted = false;
    l1_48.pNumChannels = -1.0;
    l1_48.isInitialized = 0;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.Threshold = -0.05;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.pSampleRateDialog = 48000.0;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.AttackTime = 1.0E-5;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.ReleaseTime = 0.1;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.KneeWidth = 0.0;
    if (l1_48.isInitialized == 1) {
      l1_48.TunablePropsChanged = true;
    }

    l1_48.MakeUpGain = 0.0;
    l1_48.matlabCodegenIsDeleted = false;
    l1_96.pNumChannels = -1.0;
    l1_96.isInitialized = 0;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.Threshold = -0.05;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.pSampleRateDialog = 96000.0;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.AttackTime = 1.0E-5;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.ReleaseTime = 0.1;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.KneeWidth = 0.0;
    if (l1_96.isInitialized == 1) {
      l1_96.TunablePropsChanged = true;
    }

    l1_96.MakeUpGain = 0.0;
    l1_96.matlabCodegenIsDeleted = false;
  }

  /*  Process - Limiter */
  if (fs < 46000) {
    /* SR = 44100    */
    e_SystemCore_parenReference(&l1_44, yin_data, yout_data, yout_size);
  } else if (fs < 64000) {
    /* SR = 48000 */
    e_SystemCore_parenReference(&l1_48, yin_data, yout_data, yout_size);
  } else {
    /* SR = 96000  */
    e_SystemCore_parenReference(&l1_96, yin_data, yout_data, yout_size);
  }
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void limitMe_init(void)
{
  l1_96.matlabCodegenIsDeleted = true;
  l1_48.matlabCodegenIsDeleted = true;
  l1_44.matlabCodegenIsDeleted = true;
}

/*
 * File trailer for limitMe.c
 *
 * [EOF]
 */

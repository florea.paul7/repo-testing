/*
 * File: designLPHPFilter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include <float.h>
#include "callAllFunctions.h"
#include "designLPHPFilter.h"

/* Function Declarations */
static void b_hpeq(double N, float BW, float B_data[], int B_size[2], float
                   A_data[], int A_size[2]);
static void hpeq(double N, float BW, float B_data[], int B_size[2], float
                 A_data[], int A_size[2]);
static float rt_remf_snf(float u0, float u1);

/* Function Definitions */

/*
 * Arguments    : double N
 *                float BW
 *                float B_data[]
 *                int B_size[2]
 *                float A_data[]
 *                int A_size[2]
 * Return Type  : void
 */
static void b_hpeq(double N, float BW, float B_data[], int B_size[2], float
                   A_data[], int A_size[2])
{
  float r;
  float L;
  float WB;
  float a;
  int offset;
  int Ba_size_idx_0;
  int idx;
  float Ba_data[12];
  int Aa_size_idx_0;
  float Aa_data[12];
  int y_size_idx_1;
  float y_data[3];
  int k;
  float i_data[3];
  float si_data[3];
  float b_a;
  float Bhat_data[12];
  float a_tmp;
  float Ahat_data[12];
  float B2_data[4];
  float A2_data[4];
  bool x_data[4];
  bool exitg1;
  signed char ii_data[4];
  r = rt_remf_snf((float)N, 2.0F);
  L = ((float)N - r) / 2.0F;
  WB = tanf(3.14159274F * BW / 2.0F);
  a = powf(0.99999994F, 1.0F / (float)N);
  offset = !(r == 0.0F);
  Ba_size_idx_0 = (int)(L + (float)offset);
  idx = (int)(L + (float)offset) * 3;
  if (0 <= idx - 1) {
    memset(&Ba_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  Aa_size_idx_0 = (int)(L + (float)offset);
  idx = (int)(L + (float)offset) * 3;
  if (0 <= idx - 1) {
    memset(&Aa_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  if (r != 0.0F) {
    Ba_data[0] = 0.0F * WB;
    Ba_data[Ba_size_idx_0] = a;
    Ba_data[Ba_size_idx_0 << 1] = 0.0F;
    Aa_data[0] = WB;
    Aa_data[Aa_size_idx_0] = a;
    Aa_data[Aa_size_idx_0 << 1] = 0.0F;
  }

  if (L > 0.0F) {
    if (L < 1.0F) {
      y_size_idx_1 = 0;
    } else if (rtIsInfF(L) && (1.0F == L)) {
      y_size_idx_1 = 1;
      y_data[0] = rtNaNF;
    } else {
      idx = (int)floor(L - 1.0);
      y_size_idx_1 = idx + 1;
      for (k = 0; k <= idx; k++) {
        y_data[k] = (float)(1.0 + (double)k);
      }
    }

    if (0 <= y_size_idx_1 - 1) {
      memcpy(&i_data[0], &y_data[0], (unsigned int)(y_size_idx_1 * (int)sizeof
              (float)));
    }

    for (k = 0; k < y_size_idx_1; k++) {
      si_data[k] = 3.14159274F * ((2.0F * i_data[k] - 1.0F) / (float)N) / 2.0F;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      si_data[k] = sinf(si_data[k]);
    }

    for (k = 0; k < y_size_idx_1; k++) {
      i_data[k] += (float)offset;
    }

    r = WB * WB;
    b_a = 0.0F * r;
    a_tmp = a * a;
    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Ba_data[(int)i_data[k] - 1] = b_a;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      Ba_data[((int)i_data[k] + Ba_size_idx_0) - 1] = 0.0F * si_data[k] * WB;
    }

    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Ba_data[((int)i_data[k] + (Ba_size_idx_0 << 1)) - 1] = a_tmp;
    }

    a *= 2.0F;
    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Aa_data[(int)i_data[k] - 1] = r;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      Aa_data[((int)i_data[k] + Aa_size_idx_0) - 1] = a * si_data[k] * WB;
    }

    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Aa_data[((int)i_data[k] + (Aa_size_idx_0 << 1)) - 1] = a_tmp;
    }
  }

  B_size[0] = Ba_size_idx_0;
  B_size[1] = 5;
  idx = Ba_size_idx_0 * 5;
  if (0 <= idx - 1) {
    memset(&B_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  A_size[0] = Ba_size_idx_0;
  A_size[1] = 5;
  idx = Ba_size_idx_0 * 5;
  if (0 <= idx - 1) {
    memset(&A_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  idx = Ba_size_idx_0 * 3;
  if (0 <= idx - 1) {
    memset(&Bhat_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  idx = Ba_size_idx_0 * 3;
  if (0 <= idx - 1) {
    memset(&Ahat_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    B2_data[k] = Ba_data[k + (Ba_size_idx_0 << 1)];
  }

  for (k = 0; k < Aa_size_idx_0; k++) {
    A2_data[k] = Aa_data[k + (Aa_size_idx_0 << 1)];
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    x_data[k] = (((Ba_data[k + Ba_size_idx_0] != 0.0F) || (Aa_data[k +
      Aa_size_idx_0] != 0.0F)) && ((B2_data[k] == 0.0F) && (A2_data[k] == 0.0F)));
  }

  idx = 0;
  y_size_idx_1 = Ba_size_idx_0;
  offset = 0;
  exitg1 = false;
  while ((!exitg1) && (offset <= Ba_size_idx_0 - 1)) {
    if (x_data[offset]) {
      idx++;
      ii_data[idx - 1] = (signed char)(offset + 1);
      if (idx >= Ba_size_idx_0) {
        exitg1 = true;
      } else {
        offset++;
      }
    } else {
      offset++;
    }
  }

  if (Ba_size_idx_0 == 1) {
    if (idx == 0) {
      y_size_idx_1 = 0;
    }
  } else if (1 > idx) {
    y_size_idx_1 = 0;
  } else {
    y_size_idx_1 = idx;
  }

  if (y_size_idx_1 != 0) {
    idx = ii_data[0] - 1;
    r = Aa_data[idx] + Aa_data[(ii_data[0] + Aa_size_idx_0) - 1];
    Bhat_data[idx] = (Ba_data[idx] + Ba_data[(ii_data[0] + Ba_size_idx_0) - 1]) /
      r;
    Bhat_data[(ii_data[0] + Ba_size_idx_0) - 1] = (Ba_data[ii_data[0] - 1] -
      Ba_data[(ii_data[0] + Ba_size_idx_0) - 1]) / r;
    Ahat_data[idx] = 1.0F;
    Ahat_data[(ii_data[0] + Ba_size_idx_0) - 1] = (Aa_data[ii_data[0] - 1] -
      Aa_data[(ii_data[0] + Aa_size_idx_0) - 1]) / r;
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    x_data[k] = ((B2_data[k] != 0.0F) || (A2_data[k] != 0.0F));
  }

  idx = 0;
  y_size_idx_1 = Ba_size_idx_0;
  offset = 0;
  exitg1 = false;
  while ((!exitg1) && (offset <= Ba_size_idx_0 - 1)) {
    if (x_data[offset]) {
      idx++;
      ii_data[idx - 1] = (signed char)(offset + 1);
      if (idx >= Ba_size_idx_0) {
        exitg1 = true;
      } else {
        offset++;
      }
    } else {
      offset++;
    }
  }

  if (Ba_size_idx_0 == 1) {
    if (idx == 0) {
      y_size_idx_1 = 0;
    }
  } else if (1 > idx) {
    y_size_idx_1 = 0;
  } else {
    y_size_idx_1 = idx;
  }

  for (k = 0; k < y_size_idx_1; k++) {
    B2_data[k] = ii_data[k];
  }

  for (k = 0; k < y_size_idx_1; k++) {
    idx = (int)B2_data[k];
    offset = idx - 1;
    r = (Aa_data[offset] + Aa_data[(idx + Aa_size_idx_0) - 1]) + Aa_data[(idx +
      (Aa_size_idx_0 << 1)) - 1];
    Bhat_data[offset] = ((Ba_data[offset] + Ba_data[(idx + Ba_size_idx_0) - 1])
                         + Ba_data[(idx + (Ba_size_idx_0 << 1)) - 1]) / r;
    Bhat_data[(idx + Ba_size_idx_0) - 1] = 2.0F * (Ba_data[(int)B2_data[k] - 1]
      - Ba_data[(idx + (Ba_size_idx_0 << 1)) - 1]) / r;
    Bhat_data[(idx + (Ba_size_idx_0 << 1)) - 1] = ((Ba_data[(int)B2_data[k] - 1]
      - Ba_data[(idx + Ba_size_idx_0) - 1]) + Ba_data[(idx + (Ba_size_idx_0 << 1))
      - 1]) / r;
    Ahat_data[offset] = 1.0F;
    Ahat_data[(idx + Ba_size_idx_0) - 1] = 2.0F * (Aa_data[(int)B2_data[k] - 1]
      - Aa_data[(idx + (Aa_size_idx_0 << 1)) - 1]) / r;
    Ahat_data[(idx + (Ba_size_idx_0 << 1)) - 1] = ((Aa_data[(int)B2_data[k] - 1]
      - Aa_data[(idx + Aa_size_idx_0) - 1]) + Aa_data[(idx + (Aa_size_idx_0 << 1))
      - 1]) / r;
  }

  for (k = 0; k < 3; k++) {
    for (idx = 0; idx < Ba_size_idx_0; idx++) {
      B_data[idx + Ba_size_idx_0 * k] = Bhat_data[idx + Ba_size_idx_0 * k];
    }

    for (idx = 0; idx < Ba_size_idx_0; idx++) {
      A_data[idx + Ba_size_idx_0 * k] = Ahat_data[idx + Ba_size_idx_0 * k];
    }
  }
}

/*
 * Arguments    : double N
 *                float BW
 *                float B_data[]
 *                int B_size[2]
 *                float A_data[]
 *                int A_size[2]
 * Return Type  : void
 */
static void hpeq(double N, float BW, float B_data[], int B_size[2], float
                 A_data[], int A_size[2])
{
  float r;
  float L;
  float WB;
  float a;
  int offset;
  int Ba_size_idx_0;
  int idx;
  float Ba_data[12];
  int Aa_size_idx_0;
  float Aa_data[12];
  int y_size_idx_1;
  float y_data[3];
  int k;
  float i_data[3];
  float si_data[3];
  float b_a;
  float Bhat_data[12];
  float a_tmp;
  float Ahat_data[12];
  float B2_data[4];
  float A2_data[4];
  bool x_data[4];
  bool exitg1;
  signed char ii_data[4];
  r = rt_remf_snf((float)N, 2.0F);
  L = ((float)N - r) / 2.0F;
  WB = tanf(3.14159274F * BW / 2.0F);
  a = powf(0.99999994F, 1.0F / (float)N);
  offset = !(r == 0.0F);
  Ba_size_idx_0 = (int)(L + (float)offset);
  idx = (int)(L + (float)offset) * 3;
  if (0 <= idx - 1) {
    memset(&Ba_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  Aa_size_idx_0 = (int)(L + (float)offset);
  idx = (int)(L + (float)offset) * 3;
  if (0 <= idx - 1) {
    memset(&Aa_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  if (r != 0.0F) {
    Ba_data[0] = 0.0F * WB;
    Ba_data[Ba_size_idx_0] = a;
    Ba_data[Ba_size_idx_0 << 1] = 0.0F;
    Aa_data[0] = WB;
    Aa_data[Aa_size_idx_0] = a;
    Aa_data[Aa_size_idx_0 << 1] = 0.0F;
  }

  if (L > 0.0F) {
    if (L < 1.0F) {
      y_size_idx_1 = 0;
    } else if (rtIsInfF(L) && (1.0F == L)) {
      y_size_idx_1 = 1;
      y_data[0] = rtNaNF;
    } else {
      idx = (int)floor(L - 1.0);
      y_size_idx_1 = idx + 1;
      for (k = 0; k <= idx; k++) {
        y_data[k] = (float)(1.0 + (double)k);
      }
    }

    if (0 <= y_size_idx_1 - 1) {
      memcpy(&i_data[0], &y_data[0], (unsigned int)(y_size_idx_1 * (int)sizeof
              (float)));
    }

    for (k = 0; k < y_size_idx_1; k++) {
      si_data[k] = 3.14159274F * ((2.0F * i_data[k] - 1.0F) / (float)N) / 2.0F;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      si_data[k] = sinf(si_data[k]);
    }

    for (k = 0; k < y_size_idx_1; k++) {
      i_data[k] += (float)offset;
    }

    r = WB * WB;
    b_a = 0.0F * r;
    a_tmp = a * a;
    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Ba_data[(int)i_data[k] - 1] = b_a;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      Ba_data[((int)i_data[k] + Ba_size_idx_0) - 1] = 0.0F * si_data[k] * WB;
    }

    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Ba_data[((int)i_data[k] + (Ba_size_idx_0 << 1)) - 1] = a_tmp;
    }

    a *= 2.0F;
    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Aa_data[(int)i_data[k] - 1] = r;
    }

    for (k = 0; k < y_size_idx_1; k++) {
      Aa_data[((int)i_data[k] + Aa_size_idx_0) - 1] = a * si_data[k] * WB;
    }

    idx = (int)L;
    for (k = 0; k < idx; k++) {
      Aa_data[((int)i_data[k] + (Aa_size_idx_0 << 1)) - 1] = a_tmp;
    }
  }

  B_size[0] = Ba_size_idx_0;
  B_size[1] = 5;
  idx = Ba_size_idx_0 * 5;
  if (0 <= idx - 1) {
    memset(&B_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  A_size[0] = Ba_size_idx_0;
  A_size[1] = 5;
  idx = Ba_size_idx_0 * 5;
  if (0 <= idx - 1) {
    memset(&A_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  idx = Ba_size_idx_0 * 3;
  if (0 <= idx - 1) {
    memset(&Bhat_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  idx = Ba_size_idx_0 * 3;
  if (0 <= idx - 1) {
    memset(&Ahat_data[0], 0, (unsigned int)(idx * (int)sizeof(float)));
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    B2_data[k] = Ba_data[k + (Ba_size_idx_0 << 1)];
  }

  for (k = 0; k < Aa_size_idx_0; k++) {
    A2_data[k] = Aa_data[k + (Aa_size_idx_0 << 1)];
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    x_data[k] = (((Ba_data[k + Ba_size_idx_0] != 0.0F) || (Aa_data[k +
      Aa_size_idx_0] != 0.0F)) && ((B2_data[k] == 0.0F) && (A2_data[k] == 0.0F)));
  }

  idx = 0;
  y_size_idx_1 = Ba_size_idx_0;
  offset = 0;
  exitg1 = false;
  while ((!exitg1) && (offset <= Ba_size_idx_0 - 1)) {
    if (x_data[offset]) {
      idx++;
      ii_data[idx - 1] = (signed char)(offset + 1);
      if (idx >= Ba_size_idx_0) {
        exitg1 = true;
      } else {
        offset++;
      }
    } else {
      offset++;
    }
  }

  if (Ba_size_idx_0 == 1) {
    if (idx == 0) {
      y_size_idx_1 = 0;
    }
  } else if (1 > idx) {
    y_size_idx_1 = 0;
  } else {
    y_size_idx_1 = idx;
  }

  if (y_size_idx_1 != 0) {
    idx = ii_data[0] - 1;
    r = Aa_data[idx] + Aa_data[(ii_data[0] + Aa_size_idx_0) - 1];
    Bhat_data[idx] = (Ba_data[idx] + Ba_data[(ii_data[0] + Ba_size_idx_0) - 1]) /
      r;
    Bhat_data[(ii_data[0] + Ba_size_idx_0) - 1] = (Ba_data[ii_data[0] - 1] -
      Ba_data[(ii_data[0] + Ba_size_idx_0) - 1]) / r;
    Ahat_data[idx] = 1.0F;
    Ahat_data[(ii_data[0] + Ba_size_idx_0) - 1] = (Aa_data[ii_data[0] - 1] -
      Aa_data[(ii_data[0] + Aa_size_idx_0) - 1]) / r;
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    x_data[k] = ((B2_data[k] != 0.0F) || (A2_data[k] != 0.0F));
  }

  idx = 0;
  y_size_idx_1 = Ba_size_idx_0;
  offset = 0;
  exitg1 = false;
  while ((!exitg1) && (offset <= Ba_size_idx_0 - 1)) {
    if (x_data[offset]) {
      idx++;
      ii_data[idx - 1] = (signed char)(offset + 1);
      if (idx >= Ba_size_idx_0) {
        exitg1 = true;
      } else {
        offset++;
      }
    } else {
      offset++;
    }
  }

  if (Ba_size_idx_0 == 1) {
    if (idx == 0) {
      y_size_idx_1 = 0;
    }
  } else if (1 > idx) {
    y_size_idx_1 = 0;
  } else {
    y_size_idx_1 = idx;
  }

  for (k = 0; k < y_size_idx_1; k++) {
    B2_data[k] = ii_data[k];
  }

  for (k = 0; k < y_size_idx_1; k++) {
    idx = (int)B2_data[k];
    offset = idx - 1;
    r = (Aa_data[offset] + Aa_data[(idx + Aa_size_idx_0) - 1]) + Aa_data[(idx +
      (Aa_size_idx_0 << 1)) - 1];
    Bhat_data[offset] = ((Ba_data[offset] + Ba_data[(idx + Ba_size_idx_0) - 1])
                         + Ba_data[(idx + (Ba_size_idx_0 << 1)) - 1]) / r;
    Bhat_data[(idx + Ba_size_idx_0) - 1] = 2.0F * (Ba_data[(int)B2_data[k] - 1]
      - Ba_data[(idx + (Ba_size_idx_0 << 1)) - 1]) / r;
    Bhat_data[(idx + (Ba_size_idx_0 << 1)) - 1] = ((Ba_data[(int)B2_data[k] - 1]
      - Ba_data[(idx + Ba_size_idx_0) - 1]) + Ba_data[(idx + (Ba_size_idx_0 << 1))
      - 1]) / r;
    Ahat_data[offset] = 1.0F;
    Ahat_data[(idx + Ba_size_idx_0) - 1] = 2.0F * (Aa_data[(int)B2_data[k] - 1]
      - Aa_data[(idx + (Aa_size_idx_0 << 1)) - 1]) / r;
    Ahat_data[(idx + (Ba_size_idx_0 << 1)) - 1] = ((Aa_data[(int)B2_data[k] - 1]
      - Aa_data[(idx + Aa_size_idx_0) - 1]) + Aa_data[(idx + (Aa_size_idx_0 << 1))
      - 1]) / r;
  }

  for (k = 0; k < 3; k++) {
    for (idx = 0; idx < Ba_size_idx_0; idx++) {
      B_data[idx + Ba_size_idx_0 * k] = Bhat_data[idx + Ba_size_idx_0 * k];
    }

    for (idx = 0; idx < Ba_size_idx_0; idx++) {
      A_data[idx + Ba_size_idx_0 * k] = Ahat_data[idx + Ba_size_idx_0 * k];
    }
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    B2_data[k] = -B_data[k + Ba_size_idx_0];
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    B_data[k + Ba_size_idx_0] = B2_data[k];
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    B2_data[k] = -A_data[k + Ba_size_idx_0];
  }

  for (k = 0; k < Ba_size_idx_0; k++) {
    A_data[k + Ba_size_idx_0] = B2_data[k];
  }
}

/*
 * Arguments    : float u0
 *                float u1
 * Return Type  : float
 */
static float rt_remf_snf(float u0, float u1)
{
  float y;
  float q;
  if (rtIsNaNF(u0) || rtIsInfF(u0) || (rtIsNaNF(u1) || rtIsInfF(u1))) {
    y = rtNaNF;
  } else if ((u1 != 0.0F) && (u1 != truncf(u1))) {
    q = fabsf(u0 / u1);
    if (fabsf(q - floorf(q + 0.5F)) <= FLT_EPSILON * q) {
      y = 0.0F * u0;
    } else {
      y = fmodf(u0, u1);
    }
  } else {
    y = fmodf(u0, u1);
  }

  return y;
}

/*
 * Arguments    : double N
 *                float BW
 *                float B_data[]
 *                int B_size[2]
 *                float A_data[]
 *                int A_size[2]
 * Return Type  : void
 */
void b_designLPHPFilter(double N, float BW, float B_data[], int B_size[2], float
  A_data[], int A_size[2])
{
  double No2;
  int B_size_tmp;
  int loop_ub;
  int i4;
  float Bf_data[20];
  int Bf_size[2];
  float Af_data[20];
  int Af_size[2];
  No2 = N / 2.0;
  B_size[0] = 3;
  B_size_tmp = (int)No2;
  B_size[1] = B_size_tmp;
  loop_ub = 3 * B_size_tmp;
  if (0 <= loop_ub - 1) {
    memset(&B_data[0], 0, (unsigned int)(loop_ub * (int)sizeof(float)));
  }

  for (i4 = 0; i4 < B_size_tmp; i4++) {
    B_data[3 * i4] = 1.0F;
  }

  A_size[0] = 2;
  A_size[1] = B_size_tmp;
  loop_ub = B_size_tmp << 1;
  if (0 <= loop_ub - 1) {
    memset(&A_data[0], 0, (unsigned int)(loop_ub * (int)sizeof(float)));
  }

  b_hpeq(No2, BW, Bf_data, Bf_size, Af_data, Af_size);
  loop_ub = Bf_size[0] - 1;
  for (i4 = 0; i4 <= loop_ub; i4++) {
    B_data[3 * i4] = Bf_data[i4];
    B_data[1 + 3 * i4] = Bf_data[i4 + Bf_size[0]];
    B_data[2 + 3 * i4] = Bf_data[i4 + (Bf_size[0] << 1)];
  }

  loop_ub = Af_size[0] - 1;
  for (i4 = 0; i4 <= loop_ub; i4++) {
    B_size_tmp = i4 << 1;
    A_data[B_size_tmp] = Af_data[i4 + Af_size[0]];
    A_data[1 + B_size_tmp] = Af_data[i4 + Af_size[0] * 2];
  }
}

/*
 * Arguments    : double N
 *                float BW
 *                float B_data[]
 *                int B_size[2]
 *                float A_data[]
 *                int A_size[2]
 * Return Type  : void
 */
void designLPHPFilter(double N, float BW, float B_data[], int B_size[2], float
                      A_data[], int A_size[2])
{
  double No2;
  int B_size_tmp;
  int loop_ub;
  int i3;
  float Bf_data[20];
  int Bf_size[2];
  float Af_data[20];
  int Af_size[2];
  No2 = N / 2.0;
  B_size[0] = 3;
  B_size_tmp = (int)No2;
  B_size[1] = B_size_tmp;
  loop_ub = 3 * B_size_tmp;
  if (0 <= loop_ub - 1) {
    memset(&B_data[0], 0, (unsigned int)(loop_ub * (int)sizeof(float)));
  }

  for (i3 = 0; i3 < B_size_tmp; i3++) {
    B_data[3 * i3] = 1.0F;
  }

  A_size[0] = 2;
  A_size[1] = B_size_tmp;
  loop_ub = B_size_tmp << 1;
  if (0 <= loop_ub - 1) {
    memset(&A_data[0], 0, (unsigned int)(loop_ub * (int)sizeof(float)));
  }

  hpeq(No2, BW, Bf_data, Bf_size, Af_data, Af_size);
  loop_ub = Bf_size[0] - 1;
  for (i3 = 0; i3 <= loop_ub; i3++) {
    B_data[3 * i3] = Bf_data[i3];
    B_data[1 + 3 * i3] = Bf_data[i3 + Bf_size[0]];
    B_data[2 + 3 * i3] = Bf_data[i3 + (Bf_size[0] << 1)];
  }

  loop_ub = Af_size[0] - 1;
  for (i3 = 0; i3 <= loop_ub; i3++) {
    B_size_tmp = i3 << 1;
    A_data[B_size_tmp] = Af_data[i3 + Af_size[0]];
    A_data[1 + B_size_tmp] = Af_data[i3 + Af_size[0] * 2];
  }
}

/*
 * File trailer for designLPHPFilter.c
 *
 * [EOF]
 */

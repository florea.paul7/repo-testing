/*
 * File: sort1.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "sort1.h"

/* Function Definitions */

/*
 * Arguments    : double x[3]
 *                int idx[3]
 * Return Type  : void
 */
void sort(double x[3], int idx[3])
{
  double tmp;
  if ((x[0] <= x[1]) || rtIsNaN(x[1])) {
    if ((x[1] <= x[2]) || rtIsNaN(x[2])) {
      idx[0] = 1;
      idx[1] = 2;
      idx[2] = 3;
    } else if ((x[0] <= x[2]) || rtIsNaN(x[2])) {
      idx[0] = 1;
      idx[1] = 3;
      idx[2] = 2;
      tmp = x[1];
      x[1] = x[2];
      x[2] = tmp;
    } else {
      idx[0] = 3;
      idx[1] = 1;
      idx[2] = 2;
      tmp = x[2];
      x[2] = x[1];
      x[1] = x[0];
      x[0] = tmp;
    }
  } else if ((x[0] <= x[2]) || rtIsNaN(x[2])) {
    idx[0] = 2;
    idx[1] = 1;
    idx[2] = 3;
    tmp = x[0];
    x[0] = x[1];
    x[1] = tmp;
  } else if ((x[1] <= x[2]) || rtIsNaN(x[2])) {
    idx[0] = 2;
    idx[1] = 3;
    idx[2] = 1;
    tmp = x[0];
    x[0] = x[1];
    x[1] = x[2];
    x[2] = tmp;
  } else {
    idx[0] = 3;
    idx[1] = 2;
    idx[2] = 1;
    tmp = x[0];
    x[0] = x[2];
    x[2] = tmp;
  }
}

/*
 * File trailer for sort1.c
 *
 * [EOF]
 */

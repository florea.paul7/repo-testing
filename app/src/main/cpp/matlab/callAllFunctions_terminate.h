/*
 * File: callAllFunctions_terminate.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef CALLALLFUNCTIONS_TERMINATE_H
#define CALLALLFUNCTIONS_TERMINATE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void callAllFunctions_terminate(void);

#endif

/*
 * File trailer for callAllFunctions_terminate.h
 *
 * [EOF]
 */

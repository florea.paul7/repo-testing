/*
 * File: db2mag.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <math.h>
#include "callAllFunctions.h"
#include "db2mag.h"

/* Function Definitions */

/*
 * Arguments    : float ydb
 * Return Type  : float
 */
float db2mag(float ydb)
{
  return (float)pow(10.0, ydb / 20.0F);
}

/*
 * File trailer for db2mag.c
 *
 * [EOF]
 */

/*
 * File: callAllFunctions_types.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef CALLALLFUNCTIONS_TYPES_H
#define CALLALLFUNCTIONS_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_c_audio_internal_FourBandCrosso
#define typedef_c_audio_internal_FourBandCrosso

typedef struct {
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  float CrossoverFrequencies[3];
  float CrossoverOrders[3];
  float SampleRate;
  float pCR1_B_LP[12];
  float pCR1_A_LP[12];
  float pCR1_B_HP[12];
  float pCR1_A_HP[12];
  float pCR2_B_LP[12];
  float pCR2_A_LP[12];
  float pCR2_B_HP[12];
  float pCR2_A_HP[12];
  float pCR3_B_LP[12];
  float pCR3_A_LP[12];
  float pCR3_B_HP[12];
  float pCR3_A_HP[12];
  float pPhaseMult[3];
  float pCR1_States_LP[16];
  float pCR1_States_HP[16];
  float pCR2_States_LP[16];
  float pCR2_States_HP[16];
  float pCR3_States_LP[16];
  float pCR3_States_HP[16];
  float pAP1_States_LP[16];
  float pAP1_States_HP[16];
  float pAP2_States_LP[16];
  float pAP2_States_HP[16];
} c_audio_internal_FourBandCrosso;

#endif                                 /*typedef_c_audio_internal_FourBandCrosso*/

#ifndef typedef_c_audio_internal_TwoBandCrossov
#define typedef_c_audio_internal_TwoBandCrossov

typedef struct {
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  float CrossoverFrequencies;
  float CrossoverOrders;
  float SampleRate;
  float pCR1_B_LP[12];
  float pCR1_A_LP[12];
  float pCR1_B_HP[12];
  float pCR1_A_HP[12];
  float pPhaseMult;
  float pCR1_States_LP[16];
  float pCR1_States_HP[16];
} c_audio_internal_TwoBandCrossov;

#endif                                 /*typedef_c_audio_internal_TwoBandCrossov*/

#ifndef typedef_cell_wrap_3
#define typedef_cell_wrap_3

typedef struct {
  unsigned int f1[8];
} cell_wrap_3;

#endif                                 /*typedef_cell_wrap_3*/

#ifndef typedef_compressor
#define typedef_compressor

typedef struct {
  bool matlabCodegenIsDeleted;
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[1];
  double pSampleRateDialog;
  double Threshold;
  double AttackTime;
  double ReleaseTime;
  double pNumChannels;
  float pAlphaA;
  float pAlphaR;
  float pLevelDetectionState;
  double MakeUpGain;
  double KneeWidth;
  float pMakeUpGain;
  double Ratio;
} compressor;

#endif                                 /*typedef_compressor*/

#ifndef typedef_crossoverFilter
#define typedef_crossoverFilter

typedef struct {
  bool matlabCodegenIsDeleted;
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[1];
  double pSampleRateDialog;
  float pOrders;
  float pCrossoverFrequencies;
  double pNumChannels;
  double pFreq[4];
  double pSlopes[4];
  c_audio_internal_TwoBandCrossov pFilter;
} crossoverFilter;

#endif                                 /*typedef_crossoverFilter*/

#ifndef typedef_crossoverFilter_1
#define typedef_crossoverFilter_1

typedef struct {
  bool matlabCodegenIsDeleted;
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[1];
  double pSampleRateDialog;
  float pOrders[3];
  float pCrossoverFrequencies[3];
  double pNumChannels;
  double pFreq[4];
  double pSlopes[4];
  c_audio_internal_FourBandCrosso pFilter;
} crossoverFilter_1;

#endif                                 /*typedef_crossoverFilter_1*/

#ifndef struct_dsp_Delay_0
#define struct_dsp_Delay_0

struct dsp_Delay_0
{
  int S0_isInitialized;
  int W0_CIRC_BUF_IDX;
  float W1_IC_BUFF[1024];
  int W2_PrevNumChan;
  bool W3_NeedsToInit;
  float P0_IC;
};

#endif                                 /*struct_dsp_Delay_0*/

#ifndef typedef_dsp_Delay_0
#define typedef_dsp_Delay_0

typedef struct dsp_Delay_0 dsp_Delay_0;

#endif                                 /*typedef_dsp_Delay_0*/

#ifndef struct_dsp_Delay_1
#define struct_dsp_Delay_1

struct dsp_Delay_1
{
  int S0_isInitialized;
  int W0_CIRC_BUF_IDX;
  float W1_IC_BUFF[2048];
  int W2_PrevNumChan;
  bool W3_NeedsToInit;
  float P0_IC;
};

#endif                                 /*struct_dsp_Delay_1*/

#ifndef typedef_dsp_Delay_1
#define typedef_dsp_Delay_1

typedef struct dsp_Delay_1 dsp_Delay_1;

#endif                                 /*typedef_dsp_Delay_1*/

#ifndef struct_dsp_IFFT_5
#define struct_dsp_IFFT_5

struct dsp_IFFT_5
{
  int S0_isInitialized;
  float P0_TwiddleTable[3072];
};

#endif                                 /*struct_dsp_IFFT_5*/

#ifndef typedef_dsp_IFFT_5
#define typedef_dsp_IFFT_5

typedef struct dsp_IFFT_5 dsp_IFFT_5;

#endif                                 /*typedef_dsp_IFFT_5*/

#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T

struct emxArray_real32_T
{
  float *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  bool canFreeData;
};

#endif                                 /*struct_emxArray_real32_T*/

#ifndef typedef_emxArray_real32_T
#define typedef_emxArray_real32_T

typedef struct emxArray_real32_T emxArray_real32_T;

#endif                                 /*typedef_emxArray_real32_T*/

#ifndef struct_emxArray_real32_T_109x2
#define struct_emxArray_real32_T_109x2

struct emxArray_real32_T_109x2
{
  float data[218];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_109x2*/

#ifndef typedef_emxArray_real32_T_109x2
#define typedef_emxArray_real32_T_109x2

typedef struct emxArray_real32_T_109x2 emxArray_real32_T_109x2;

#endif                                 /*typedef_emxArray_real32_T_109x2*/

#ifndef struct_emxArray_real32_T_119x2
#define struct_emxArray_real32_T_119x2

struct emxArray_real32_T_119x2
{
  float data[238];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_119x2*/

#ifndef typedef_emxArray_real32_T_119x2
#define typedef_emxArray_real32_T_119x2

typedef struct emxArray_real32_T_119x2 emxArray_real32_T_119x2;

#endif                                 /*typedef_emxArray_real32_T_119x2*/

#ifndef struct_emxArray_real32_T_2048
#define struct_emxArray_real32_T_2048

struct emxArray_real32_T_2048
{
  float data[2048];
  int size[1];
};

#endif                                 /*struct_emxArray_real32_T_2048*/

#ifndef typedef_emxArray_real32_T_2048
#define typedef_emxArray_real32_T_2048

typedef struct emxArray_real32_T_2048 emxArray_real32_T_2048;

#endif                                 /*typedef_emxArray_real32_T_2048*/

#ifndef struct_emxArray_real32_T_2048x2
#define struct_emxArray_real32_T_2048x2

struct emxArray_real32_T_2048x2
{
  float data[4096];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_2048x2*/

#ifndef typedef_emxArray_real32_T_2048x2
#define typedef_emxArray_real32_T_2048x2

typedef struct emxArray_real32_T_2048x2 emxArray_real32_T_2048x2;

#endif                                 /*typedef_emxArray_real32_T_2048x2*/

#ifndef struct_emxArray_real32_T_2049x2
#define struct_emxArray_real32_T_2049x2

struct emxArray_real32_T_2049x2
{
  float data[4098];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_2049x2*/

#ifndef typedef_emxArray_real32_T_2049x2
#define typedef_emxArray_real32_T_2049x2

typedef struct emxArray_real32_T_2049x2 emxArray_real32_T_2049x2;

#endif                                 /*typedef_emxArray_real32_T_2049x2*/

#ifndef struct_emxArray_real32_T_238x2
#define struct_emxArray_real32_T_238x2

struct emxArray_real32_T_238x2
{
  float data[476];
  int size[2];
};

#endif                                 /*struct_emxArray_real32_T_238x2*/

#ifndef typedef_emxArray_real32_T_238x2
#define typedef_emxArray_real32_T_238x2

typedef struct emxArray_real32_T_238x2 emxArray_real32_T_238x2;

#endif                                 /*typedef_emxArray_real32_T_238x2*/

#ifndef typedef_expander
#define typedef_expander

typedef struct {
  bool matlabCodegenIsDeleted;
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[1];
  double pSampleRateDialog;
  double Threshold;
  double AttackTime;
  double ReleaseTime;
  double pNumChannels;
  float pAlphaA;
  float pAlphaR;
  float pLevelDetectionState;
  double HoldTime;
  float pHoldTimeState[2];
  float pHoldTimeSamples;
  double Ratio;
  double KneeWidth;
} expander;

#endif                                 /*typedef_expander*/

#ifndef typedef_limiter
#define typedef_limiter

typedef struct {
  bool matlabCodegenIsDeleted;
  int isInitialized;
  bool isSetupComplete;
  bool TunablePropsChanged;
  cell_wrap_3 inputVarSize[1];
  double pSampleRateDialog;
  double Threshold;
  double AttackTime;
  double ReleaseTime;
  double pNumChannels;
  float pAlphaA;
  float pAlphaR;
  float pLevelDetectionState[2];
  double MakeUpGain;
  double KneeWidth;
  float pMakeUpGain;
} limiter;

#endif                                 /*typedef_limiter*/
#endif

/*
 * File trailer for callAllFunctions_types.h
 *
 * [EOF]
 */

/*
 * File: TwoBandCrossoverFilter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef TWOBANDCROSSOVERFILTER_H
#define TWOBANDCROSSOVERFILTER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void TwoBandCrossoverFilter_stepImpl(c_audio_internal_TwoBandCrossov *obj,
  float x[4096], float outL[4096]);
extern void b_TwoBandCrossoverFilter_stepIm(c_audio_internal_TwoBandCrossov *obj,
  float x_data[], int x_size[2], float outL_data[], int outL_size[2]);
extern void c_TwoBandCrossoverFilter_setupI(c_audio_internal_TwoBandCrossov *obj);

#endif

/*
 * File trailer for TwoBandCrossoverFilter.h
 *
 * [EOF]
 */

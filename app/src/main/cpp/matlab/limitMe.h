/*
 * File: limitMe.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef LIMITME_H
#define LIMITME_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void d_runbefore_not_empty_init(void);

extern void limitMe(const float yin_data[], int fs, float yout_data[], int
yout_size[2]);

extern void limitMe_init(void);

#ifdef __cplusplus
};
#endif

#endif

/*
 * File trailer for limitMe.h
 *
 * [EOF]
 */

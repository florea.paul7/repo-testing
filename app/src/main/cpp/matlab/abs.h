/*
 * File: abs.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef ABS_H
#define ABS_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_abs(const float x_data[], const int x_size[1], float y_data[], int
                  y_size[1]);
extern void c_abs(const float x_data[], float y_data[], int y_size[2]);

#endif

/*
 * File trailer for abs.h
 *
 * [EOF]
 */

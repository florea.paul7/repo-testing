/*
 * File: compMe1Band_SPK.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef COMPME1BAND_SPK_H
#define COMPME1BAND_SPK_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void compMe1Band_SPK(const float yin[4096], int fs, float yout_data[],
  int yout_size[2]);
extern void compMe1Band_SPK_free(void);
extern void compMe1Band_SPK_init(void);
extern void f_runbefore_not_empty_init(void);

#ifdef __cplusplus
};
#endif

#endif



/*
 * File trailer for compMe1Band_SPK.h
 *
 * [EOF]
 */

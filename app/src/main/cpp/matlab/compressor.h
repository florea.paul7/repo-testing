/*
 * File: compressor.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 11-Mar-2019 16:33:55
 */

#ifndef COMPRESSOR_H
#define COMPRESSOR_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern compressor *b_compressor_compressor(compressor *obj);
extern compressor *c_compressor_compressor(compressor *obj);
extern compressor *compressor_compressor(compressor *obj);
extern void compressor_computeGain(const compressor *obj, const float xG_data[],
  const int xG_size[1], float G_data[], int G_size[1]);
extern compressor *d_compressor_compressor(compressor *obj);
extern compressor *e_compressor_compressor(compressor *obj);
extern compressor *f_compressor_compressor(compressor *obj);
extern compressor *g_compressor_compressor(compressor *obj);
extern compressor *h_compressor_compressor(compressor *obj);
extern compressor *i_compressor_compressor(compressor *obj);
extern compressor *j_compressor_compressor(compressor *obj);
extern compressor *k_compressor_compressor(compressor *obj);
extern compressor *l_compressor_compressor(compressor *obj);
extern compressor *m_compressor_compressor(compressor *obj);
extern compressor *n_compressor_compressor(compressor *obj);
extern compressor *o_compressor_compressor(compressor *obj);
extern compressor *p_compressor_compressor(compressor *obj);
extern compressor *q_compressor_compressor(compressor *obj);
extern compressor *r_compressor_compressor(compressor *obj);
extern compressor *s_compressor_compressor(compressor *obj);
extern compressor *t_compressor_compressor(compressor *obj);
extern compressor *u_compressor_compressor(compressor *obj);

#endif

/*
 * File trailer for compressor.h
 *
 * [EOF]
 */

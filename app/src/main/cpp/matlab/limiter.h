/*
 * File: limiter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef LIMITER_H
#define LIMITER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void limiter_computeGain(const limiter *obj, const float xG_data[], float
  G_data[], int G_size[2]);

#endif

/*
 * File trailer for limiter.h
 *
 * [EOF]
 */

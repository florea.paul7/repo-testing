/*
 * File: compMe1Band_HP.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

#ifndef COMPME1BAND_HP_H
#define COMPME1BAND_HP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_runbefore_not_empty_init(void);
extern void compMe1Band_HP(float yin[4096], float preGain, int fs, float
  yout_data[], int yout_size[2]);
extern void compMe1Band_HP_free(void);
extern void compMe1Band_HP_init(void);

#endif

/*
 * File trailer for compMe1Band_HP.h
 *
 * [EOF]
 */

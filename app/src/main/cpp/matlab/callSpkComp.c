/*
 * File: callSpkComp.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <math.h>
#include "callAllFunctions.h"
#include "callSpkComp.h"
#include "spkCompress.h"

/* Function Definitions */

/*
 * testSpkComp - Calls Speaker Compression
 *
 *    Generated Code Signature:
 *    extern void spkCompress(const float yin[4096], float fs, float yout_data[], int
 *        yout_size[2]);
 * Arguments    : const emxArray_real32_T *yin
 *                int Fs
 * Return Type  : void
 */
void callSpkComp(const emxArray_real32_T *yin, int Fs)
{
  int numBlocks;
  int b;
  short b_b;
  int i15;
  short curBlk_tmp[2048];
  float b_yin[4096];
  emxArray_real32_T_2048x2 c_yin;
  int i16;

  /*  Parameters - Define Input Types */
  /* Compressor Settings */
  /*  Processing Loop */
  numBlocks = yin->size[0];
  if (numBlocks <= 2) {
    numBlocks = 2;
  }

  if (yin->size[0] == 0) {
    numBlocks = 0;
  }

  numBlocks = (int)floor((double)numBlocks / 2048.0);
  for (b = 0; b < numBlocks; b++) {
    b_b = (short)((short)((short)b << 11) + 1);
    for (i15 = 0; i15 < 2048; i15++) {
      curBlk_tmp[i15] = (short)(i15 + b_b);
    }

    for (i15 = 0; i15 < 2; i15++) {
      for (i16 = 0; i16 < 2048; i16++) {
        b_yin[i16 + (i15 << 11)] = yin->data[(curBlk_tmp[i16] + yin->size[0] *
          i15) - 1];
      }
    }

    spkCompress(b_yin, Fs, c_yin.data, c_yin.size);
  }
}

/*
 * File trailer for callSpkComp.c
 *
 * [EOF]
 */

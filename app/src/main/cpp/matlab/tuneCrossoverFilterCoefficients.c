/*
 * File: tuneCrossoverFilterCoefficients.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "tuneCrossoverFilterCoefficients.h"
#include "designLPHPFilter.h"
#include "mod.h"

/* Function Definitions */

/*
 * Arguments    : float freq
 *                float order
 *                float Fs
 *                float BL[12]
 *                float AL[12]
 *                float BH[12]
 *                float AH[12]
 *                float *phaseMult
 * Return Type  : void
 */
void tuneCrossoverFilterCoefficients(float freq, float order, float Fs, float
  BL[12], float AL[12], float BH[12], float AH[12], float *phaseMult)
{
  float wt;
  float FsbyTwo;
  float N;
  bool isEven;
  int ord;
  float B0L_data[21];
  int B0L_size[2];
  float A0L_data[14];
  int A0L_size[2];
  float B0H_data[21];
  float A0H_data[14];
  int i2;
  int ibtile;
  signed char b[12];
  int loop_ub;
  wt = freq;
  FsbyTwo = Fs / 2.0F;
  if (freq >= FsbyTwo) {
    wt = FsbyTwo - 1.0F;
  }

  wt /= FsbyTwo;
  N = order;
  if ((order == 2.0F) || (order == 6.0F)) {
    *phaseMult = -1.0F;
  } else {
    *phaseMult = 1.0F;
  }

  isEven = false;
  if (b_mod(order) == 0.0F) {
    N = order / 2.0F;
    isEven = true;
  }

  switch ((int)N) {
   case 1:
    ord = 2;
    break;

   case 2:
    ord = 4;
    break;

   case 3:
    ord = 6;
    break;

   case 4:
    ord = 8;
    break;

   case 5:
    ord = 10;
    break;

   case 6:
    ord = 12;
    break;

   case 7:
    ord = 14;
    break;

   default:
    ord = 2;
    break;
  }

  designLPHPFilter(ord, 1.0F - wt, B0L_data, B0L_size, A0L_data, A0L_size);
  b_designLPHPFilter(ord, wt, B0H_data, B0L_size, A0H_data, A0L_size);
  for (ord = 0; ord < 4; ord++) {
    ibtile = ord * 3;
    b[ibtile] = 1;
    b[ibtile + 1] = 0;
    b[ibtile + 2] = 0;
  }

  for (i2 = 0; i2 < 12; i2++) {
    BL[i2] = b[i2];
    AL[i2] = b[i2];
    BH[i2] = b[i2];
    AH[i2] = b[i2];
  }

  if (isEven) {
    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      BL[3 * i2] = B0L_data[3 * i2];
      ord = 1 + 3 * i2;
      BL[ord] = B0L_data[ord];
      ord = 2 + 3 * i2;
      BL[ord] = B0L_data[ord];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    if (3.0F > (3.0F + ceilf(N / 2.0F)) - 1.0F) {
      i2 = 0;
    } else {
      i2 = 2;
    }

    for (ibtile = 0; ibtile <= loop_ub; ibtile++) {
      BL[3 * (i2 + ibtile)] = B0L_data[3 * ibtile];
      BL[1 + 3 * (i2 + ibtile)] = B0L_data[1 + 3 * ibtile];
      BL[2 + 3 * (i2 + ibtile)] = B0L_data[2 + 3 * ibtile];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      ord = i2 << 1;
      AL[3 * i2 + 1] = A0L_data[ord];
      AL[3 * i2 + 2] = A0L_data[1 + ord];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    if (3.0F > (3.0F + ceilf(N / 2.0F)) - 1.0F) {
      i2 = 0;
    } else {
      i2 = 2;
    }

    for (ibtile = 0; ibtile <= loop_ub; ibtile++) {
      ord = ibtile << 1;
      AL[3 * (i2 + ibtile) + 1] = A0L_data[ord];
      AL[3 * (i2 + ibtile) + 2] = A0L_data[1 + ord];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      BH[3 * i2] = B0H_data[3 * i2];
      ord = 1 + 3 * i2;
      BH[ord] = B0H_data[ord];
      ord = 2 + 3 * i2;
      BH[ord] = B0H_data[ord];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    if (3.0F > (3.0F + ceilf(N / 2.0F)) - 1.0F) {
      i2 = 0;
    } else {
      i2 = 2;
    }

    for (ibtile = 0; ibtile <= loop_ub; ibtile++) {
      BH[3 * (i2 + ibtile)] = B0H_data[3 * ibtile];
      BH[1 + 3 * (i2 + ibtile)] = B0H_data[1 + 3 * ibtile];
      BH[2 + 3 * (i2 + ibtile)] = B0H_data[2 + 3 * ibtile];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      ord = i2 << 1;
      AH[3 * i2 + 1] = A0H_data[ord];
      AH[3 * i2 + 2] = A0H_data[1 + ord];
    }

    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    if (3.0F > (3.0F + ceilf(N / 2.0F)) - 1.0F) {
      i2 = 0;
    } else {
      i2 = 2;
    }

    for (ibtile = 0; ibtile <= loop_ub; ibtile++) {
      ord = ibtile << 1;
      AH[3 * (i2 + ibtile) + 1] = A0H_data[ord];
      AH[3 * (i2 + ibtile) + 2] = A0H_data[1 + ord];
    }
  } else {
    FsbyTwo = ceilf(N / 2.0F);
    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      BL[3 * i2] = B0L_data[3 * i2];
      ord = 1 + 3 * i2;
      BL[ord] = B0L_data[ord];
      ord = 2 + 3 * i2;
      BL[ord] = B0L_data[ord];
    }

    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      ord = i2 << 1;
      AL[3 * i2 + 1] = A0L_data[ord];
      AL[3 * i2 + 2] = A0L_data[1 + ord];
    }

    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      BH[3 * i2] = B0H_data[3 * i2];
      ord = 1 + 3 * i2;
      BH[ord] = B0H_data[ord];
      ord = 2 + 3 * i2;
      BH[ord] = B0H_data[ord];
    }

    if (1.0F > FsbyTwo) {
      loop_ub = -1;
    } else {
      loop_ub = (int)FsbyTwo - 1;
    }

    for (i2 = 0; i2 <= loop_ub; i2++) {
      ord = i2 << 1;
      AH[3 * i2 + 1] = A0H_data[ord];
      AH[3 * i2 + 2] = A0H_data[1 + ord];
    }
  }
}

/*
 * File trailer for tuneCrossoverFilterCoefficients.c
 *
 * [EOF]
 */

/*
 * File: log10.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "log10.h"

/* Function Definitions */

/*
 * Arguments    : float x_data[]
 *                int x_size[1]
 * Return Type  : void
 */
void b_log10(float x_data[], int x_size[1])
{
  int nx;
  int k;
  nx = x_size[0];
  for (k = 0; k < nx; k++) {
    x_data[k] = log10f(x_data[k]);
  }
}

/*
 * Arguments    : float x_data[]
 * Return Type  : void
 */
void c_log10(float x_data[])
{
  int k;
  for (k = 0; k < 4096; k++) {
    x_data[k] = log10f(x_data[k]);
  }
}

/*
 * File trailer for log10.c
 *
 * [EOF]
 */

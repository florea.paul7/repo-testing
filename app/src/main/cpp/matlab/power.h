/*
 * File: power.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef POWER_H
#define POWER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_power(const float b_data[], const int b_size[1], float y_data[],
                    int y_size[1]);
extern void c_power(const float b_data[], float y_data[], int y_size[2]);
extern void power(const float a_data[], const int a_size[1], float y_data[], int
                  y_size[1]);

#endif

/*
 * File trailer for power.h
 *
 * [EOF]
 */

/*
 * File: expander.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef EXPANDER_H
#define EXPANDER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern expander *b_expander_expander(expander *obj);
extern expander *c_expander_expander(expander *obj);
extern expander *d_expander_expander(expander *obj);
extern expander *e_expander_expander(expander *obj);
extern expander *expander_expander(expander *obj);
extern void expander_stepImpl(expander *obj, const float x_data[], const int
  x_size[1], float y_data[], int y_size[1], float G_data[], int G_size[1]);
extern expander *f_expander_expander(expander *obj);

#endif

/*
 * File trailer for expander.h
 *
 * [EOF]
 */

/*
 * File: CompressorBase.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <string.h>
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "CompressorBase.h"
#include "power.h"
#include "limiter.h"
#include "log10.h"
#include "abs.h"
#include "compressor.h"

/* Function Declarations */
static void CompressorBase_detectLevel(compressor *obj, const float x_data[],
  const int x_size[1], float yout_data[], int yout_size[1]);
static void b_CompressorBase_detectLevel(limiter *obj, const float x_data[],
  float yout_data[], int yout_size[2]);

/* Function Definitions */

/*
 * Arguments    : compressor *obj
 *                const float x_data[]
 *                const int x_size[1]
 *                float yout_data[]
 *                int yout_size[1]
 * Return Type  : void
 */
static void CompressorBase_detectLevel(compressor *obj, const float x_data[],
  const int x_size[1], float yout_data[], int yout_size[1])
{
  float y_data[2050];
  float alphaA;
  float alphaR;
  int loop_ub;
  int i;
  if (0 <= x_size[0]) {
    memset(&y_data[0], 0, (unsigned int)((x_size[0] + 1) * (int)sizeof(float)));
  }

  y_data[0] = obj->pLevelDetectionState;
  alphaA = obj->pAlphaA;
  alphaR = obj->pAlphaR;
  loop_ub = x_size[0];
  for (i = 0; i < loop_ub; i++) {
    if (x_data[i] <= y_data[i]) {
      y_data[i + 1] = alphaA * y_data[i] + (1.0F - alphaA) * x_data[i];
    } else {
      y_data[i + 1] = alphaR * y_data[i] + (1.0F - alphaR) * x_data[i];
    }
  }

  yout_size[0] = x_size[0];
  if (0 <= x_size[0] - 1) {
    memset(&yout_data[0], 0, (unsigned int)(x_size[0] * (int)sizeof(float)));
  }

  loop_ub = x_size[0] - 1;
  if (0 <= loop_ub) {
    memcpy(&yout_data[0], &y_data[1], (unsigned int)((loop_ub + 1) * (int)sizeof
            (float)));
  }

  obj->pLevelDetectionState = y_data[x_size[0]];
}

/*
 * Arguments    : limiter *obj
 *                const float x_data[]
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
static void b_CompressorBase_detectLevel(limiter *obj, const float x_data[],
  float yout_data[], int yout_size[2])
{
  float y_data[4098];
  float alphaA;
  float alphaR;
  int i;
  memset(&y_data[0], 0, 4098U * sizeof(float));
  y_data[0] = obj->pLevelDetectionState[0];
  y_data[2049] = obj->pLevelDetectionState[1];
  alphaA = obj->pAlphaA;
  alphaR = obj->pAlphaR;
  for (i = 0; i < 2048; i++) {
    if (x_data[i] <= y_data[i]) {
      y_data[i + 1] = alphaA * y_data[i] + (1.0F - alphaA) * x_data[i];
    } else {
      y_data[i + 1] = alphaR * y_data[i] + (1.0F - alphaR) * x_data[i];
    }

    if (x_data[i + 2048] <= y_data[i + 2049]) {
      y_data[i + 2050] = alphaA * y_data[i + 2049] + (1.0F - alphaA) * x_data[i
        + 2048];
    } else {
      y_data[i + 2050] = alphaR * y_data[i + 2049] + (1.0F - alphaR) * x_data[i
        + 2048];
    }
  }

  yout_size[0] = 2048;
  yout_size[1] = 2;
  memset(&yout_data[0], 0, sizeof(float) << 12);
  memcpy(&yout_data[0], &y_data[1], sizeof(float) << 11);
  obj->pLevelDetectionState[0] = y_data[2048];
  memcpy(&yout_data[2048], &y_data[2050], sizeof(float) << 11);
  obj->pLevelDetectionState[1] = y_data[4097];
}

/*
 * Arguments    : compressor *obj
 *                const float x_data[]
 *                const int x_size[1]
 *                float y_data[]
 *                int y_size[1]
 *                float G_data[]
 *                int G_size[1]
 * Return Type  : void
 */
void CompressorBase_stepImpl(compressor *obj, const float x_data[], const int
  x_size[1], float y_data[], int y_size[1], float G_data[], int G_size[1])
{
  float varargin_1_data[2049];
  int varargin_1_size[1];
  int maxval_size[1];
  int nx;
  int k;
  float maxval_data[2049];
  int tmp_size[1];
  int b_G_size[1];
  int c_G_size[1];
  b_abs(x_data, x_size, varargin_1_data, varargin_1_size);
  maxval_size[0] = (short)varargin_1_size[0];
  nx = (short)varargin_1_size[0];
  for (k = 0; k < nx; k++) {
    maxval_data[k] = fmaxf(varargin_1_data[k], 1.1920929E-7F);
  }

  b_log10(maxval_data, maxval_size);
  tmp_size[0] = maxval_size[0];
  nx = maxval_size[0];
  for (k = 0; k < nx; k++) {
    varargin_1_data[k] = 20.0F * maxval_data[k];
  }

  compressor_computeGain(obj, varargin_1_data, tmp_size, G_data, G_size);
  b_G_size[0] = G_size[0];
  if (0 <= G_size[0] - 1) {
    memcpy(&varargin_1_data[0], &G_data[0], (unsigned int)(G_size[0] * (int)
            sizeof(float)));
  }

  CompressorBase_detectLevel(obj, varargin_1_data, b_G_size, G_data, G_size);
  nx = G_size[0];
  for (k = 0; k < nx; k++) {
    G_data[k] += obj->pMakeUpGain;
  }

  c_G_size[0] = G_size[0];
  nx = G_size[0];
  for (k = 0; k < nx; k++) {
    varargin_1_data[k] = G_data[k] / 20.0F;
  }

  b_power(varargin_1_data, c_G_size, y_data, y_size);
  y_size[0] = x_size[0];
  nx = x_size[0];
  for (k = 0; k < nx; k++) {
    y_data[k] *= x_data[k];
  }
}

/*
 * Arguments    : limiter *obj
 *                const float x_data[]
 *                float y_data[]
 *                int y_size[2]
 * Return Type  : void
 */
void b_CompressorBase_stepImpl(limiter *obj, const float x_data[], float y_data[],
  int y_size[2])
{
  float G_data[4096];
  int G_size[2];
  int k;
  float maxval_data[4096];
  float tmp_data[4096];
  float b_G_data;
  c_abs(x_data, G_data, G_size);
  for (k = 0; k < 4096; k++) {
    maxval_data[k] = fmaxf(G_data[k], 1.1920929E-7F);
  }

  c_log10(maxval_data);
  for (k = 0; k < 4096; k++) {
    tmp_data[k] = 20.0F * maxval_data[k];
  }

  limiter_computeGain(obj, tmp_data, G_data, G_size);
  memcpy(&maxval_data[0], &G_data[0], sizeof(float) << 12);
  b_CompressorBase_detectLevel(obj, maxval_data, G_data, G_size);
  for (k = 0; k < 4096; k++) {
    b_G_data = G_data[k] + obj->pMakeUpGain;
    maxval_data[k] = b_G_data / 20.0F;
    G_data[k] = b_G_data;
  }

  c_power(maxval_data, y_data, y_size);
  y_size[0] = 2048;
  y_size[1] = 2;
  for (k = 0; k < 4096; k++) {
    y_data[k] *= x_data[k];
  }
}

/*
 * File trailer for CompressorBase.c
 *
 * [EOF]
 */

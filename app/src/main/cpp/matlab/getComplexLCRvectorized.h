/*
 * File: getComplexLCRvectorized.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef GETCOMPLEXLCRVECTORIZED_H
#define GETCOMPLEXLCRVECTORIZED_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_runBefore_not_empty_init(void);
extern void getComplexLCRvectorized(const creal32_T alpha_data[], const int
  alpha_size[1], const creal32_T beta_data[], const int beta_size[1], creal32_T
  alphaHat_data[], int alphaHat_size[1], creal32_T betaHat_data[], int
  betaHat_size[1], creal32_T gammaHat_data[], int gammaHat_size[1]);
extern void getComplexLCRvectorized_init(void);

#endif

/*
 * File trailer for getComplexLCRvectorized.h
 *
 * [EOF]
 */

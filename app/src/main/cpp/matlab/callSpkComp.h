/*
 * File: callSpkComp.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

#ifndef CALLSPKCOMP_H
#define CALLSPKCOMP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void callSpkComp(const emxArray_real32_T *yin, int Fs);

#endif

/*
 * File trailer for callSpkComp.h
 *
 * [EOF]
 */

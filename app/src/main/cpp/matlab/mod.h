/*
 * File: mod.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef MOD_H
#define MOD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern float b_mod(float x);

#endif

/*
 * File trailer for mod.h
 *
 * [EOF]
 */

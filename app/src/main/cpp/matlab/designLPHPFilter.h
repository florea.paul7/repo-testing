/*
 * File: designLPHPFilter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef DESIGNLPHPFILTER_H
#define DESIGNLPHPFILTER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_designLPHPFilter(double N, float BW, float B_data[], int B_size[2],
  float A_data[], int A_size[2]);
extern void designLPHPFilter(double N, float BW, float B_data[], int B_size[2],
  float A_data[], int A_size[2]);

#endif

/*
 * File trailer for designLPHPFilter.h
 *
 * [EOF]
 */

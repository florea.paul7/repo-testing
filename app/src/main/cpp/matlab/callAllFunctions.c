/*
 * File: callAllFunctions.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <math.h>
#include "callAllFunctions.h"
#include "callAllFunctions_emxutil.h"
#include "BACCHcxBuff.h"
#include "callSpkComp.h"
#include "callHPComp.h"

/* Function Definitions */

/*
 * Calls All Functions to build as one project
 *  this file itself is called by ScriptThatCalls_CallBothTest2s.m
 * Arguments    : const emxArray_real32_T *yin
 *                float preGainT
 *                int Fs
 *                emxArray_real32_T *yout
 * Return Type  : void
 */
void callAllFunctions(const emxArray_real32_T *yin, float preGainT, int Fs,
                      emxArray_real32_T *yout)
{
  int numBlocks;
  int b;
  short b_b;
  int i0;
  short curBlk_tmp[2048];
  static float b_yin[4096];
  static float resBlkSt_data[8192];
  int resBlkSt_size[2];
  static float resBlkC_data[4096];
  int resBlkC_size[2];
  int i1;

  /* Parameters - Need to be defined here and passed all the way through chain */
  /* to show up as part of function declarations */
  /* Function Calls */
  callHPComp(yin, preGainT, Fs);

  /*  Call HP Compressor */
  callSpkComp(yin, Fs);

  /*  Call SPK Compressor */
  /*  testBACCHcx - Calls BACCHcx center extraction */
  /*  */
  /*    Generated Code Signature: */
  /*    extern void BACCHcxBuff(const float yIn[4096], int sampleRate, float yOut_data[], */
  /*        int yOut_size[2], float cOut_data[], int cOut_size[2]); */
  /*  */
  /*  Parameters - Define Input Types */
  /*  Processing Loop */
  numBlocks = yin->size[0];
  if (numBlocks <= 2) {
    numBlocks = 2;
  }

  if (yin->size[0] == 0) {
    numBlocks = 0;
  }

  numBlocks = (int)floor((double)numBlocks / 2048.0);
  for (b = 0; b < numBlocks; b++) {
    b_b = (short)((short)((short)b << 11) + 1);
    for (i0 = 0; i0 < 2048; i0++) {
      curBlk_tmp[i0] = (short)(i0 + b_b);
    }

    for (i0 = 0; i0 < 2; i0++) {
      for (i1 = 0; i1 < 2048; i1++) {
        b_yin[i1 + (i0 << 11)] = yin->data[(curBlk_tmp[i1] + yin->size[0] * i0)
          - 1];
      }
    }

    BACCHcxBuff(b_yin, Fs, resBlkSt_data, resBlkSt_size, resBlkC_data,
                resBlkC_size);
  }

  /*  Call BACCHcx */
  i0 = yout->size[0] * yout->size[1];
  yout->size[0] = yin->size[0];
  yout->size[1] = 2;
  emxEnsureCapacity_real32_T(yout, i0);
  numBlocks = yin->size[0] * yin->size[1];
  for (i0 = 0; i0 < numBlocks; i0++) {
    yout->data[i0] = yin->data[i0];
  }
}

/*
 * File trailer for callAllFunctions.c
 *
 * [EOF]
 */

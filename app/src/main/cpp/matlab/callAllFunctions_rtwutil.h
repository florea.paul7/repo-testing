/*
 * File: callAllFunctions_rtwutil.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef CALLALLFUNCTIONS_RTWUTIL_H
#define CALLALLFUNCTIONS_RTWUTIL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void MWDSPCG_R2DIT_TBLS_C(creal32_T y[], int nChans, int nRows, int
  fftLen, int offset, const float tablePtr[], int twiddleStep, bool isInverse);
extern int div_s32_floor(int numerator, int denominator);
extern double rt_roundd_snf(double b_u);

#endif

/*
 * File trailer for callAllFunctions_rtwutil.h
 *
 * [EOF]
 */

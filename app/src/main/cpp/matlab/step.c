/*
 * File: step.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "step.h"
#include "callAllFunctions_rtwutil.h"

/* Function Declarations */
static void MWDSPCG_IFFT_DblLen_S_XC(const creal32_T x[], float y[], int nRows,
  int nChans, int fftLen, const float twiddleTable[], int twiddleStep);

/* Function Definitions */

/*
 * Arguments    : const creal32_T x[]
 *                float y[]
 *                int nRows
 *                int nChans
 *                int fftLen
 *                const float twiddleTable[]
 *                int twiddleStep
 * Return Type  : void
 */
static void MWDSPCG_IFFT_DblLen_S_XC(const creal32_T x[], float y[], int nRows,
  int nChans, int fftLen, const float twiddleTable[], int twiddleStep)
{
  creal32_T *output;
  int uIdx;
  int yIdx;
  float accRe;
  float accIm;
  int accRe_tmp_tmp;
  int accRe_tmp;
  int tempOut0Re_tmp;
  float tempOut0Re;
  float tempOut0Im;
  float tempOut1Re;
  int prod_re_tmp_tmp;
  int b_prod_re_tmp_tmp;
  int jbr_f;
  int jbr_b;
  int k;
  int b_accRe_tmp;
  int i;

  /* In-place "double-length" data recovery
     Table-based twiddle computation

     Used to recover linear-ordered length-N point complex FFT result
     from a linear-ordered complex length-N/2 point FFT, performed
     on N interleaved real values.
   */
  output = (creal32_T *)y;
  uIdx = nRows * (nChans - 1);
  yIdx = uIdx >> 1;
  accRe = x[uIdx].re;
  accIm = x[uIdx].re;
  accRe_tmp_tmp = fftLen >> 1;
  accRe_tmp = uIdx + accRe_tmp_tmp;
  accRe += x[accRe_tmp].re;
  accIm -= x[accRe_tmp].re;
  output[yIdx].re = accRe;
  output[yIdx].im = accIm;
  if (accRe_tmp_tmp > 1) {
    accRe = x[uIdx + 1].re;
    tempOut0Re_tmp = accRe_tmp - 1;
    tempOut0Re = accRe + x[tempOut0Re_tmp].re;
    accIm = x[uIdx + 1].im;
    tempOut0Im = accIm - x[tempOut0Re_tmp].im;
    accRe = x[uIdx + 1].re;
    tempOut1Re = accRe - x[tempOut0Re_tmp].re;
    accIm = x[uIdx + 1].im;
    accRe = accIm + x[tempOut0Re_tmp].im;
    prod_re_tmp_tmp = fftLen >> 2;
    b_prod_re_tmp_tmp = prod_re_tmp_tmp * twiddleStep;
    tempOut0Re_tmp = b_prod_re_tmp_tmp - twiddleStep;
    accIm = twiddleTable[twiddleStep] * tempOut1Re - twiddleTable[tempOut0Re_tmp]
      * accRe;
    accRe = twiddleTable[twiddleStep] * accRe + twiddleTable[tempOut0Re_tmp] *
      tempOut1Re;
    tempOut0Re_tmp = yIdx + (fftLen >> 2);
    output[tempOut0Re_tmp].re = tempOut0Re + (0.0F - accRe);
    output[tempOut0Re_tmp].im = tempOut0Im + accIm;
    if (prod_re_tmp_tmp > 1) {
      tempOut0Re_tmp = (yIdx + (fftLen >> 1)) - 1;
      output[tempOut0Re_tmp].re = tempOut0Re - (0.0F - accRe);
      output[tempOut0Re_tmp].im = accIm - tempOut0Im;
      b_accRe_tmp = uIdx + prod_re_tmp_tmp;
      accRe = x[b_accRe_tmp].re;
      accIm = 0.0F - x[b_accRe_tmp].im;
      accRe *= 2.0F;
      accIm *= 2.0F;
      output[yIdx + 1].re = accRe;
      output[yIdx + 1].im = accIm;
    }

    jbr_f = fftLen >> 3;
    jbr_b = prod_re_tmp_tmp - 1;
    k = twiddleStep << 1;
    for (i = 2; i < prod_re_tmp_tmp; i++) {
      b_accRe_tmp = uIdx + i;
      accRe = x[b_accRe_tmp].re;
      tempOut0Re_tmp = accRe_tmp - i;
      tempOut0Re = accRe + x[tempOut0Re_tmp].re;
      accIm = x[b_accRe_tmp].im;
      tempOut0Im = accIm - x[tempOut0Re_tmp].im;
      accRe = x[b_accRe_tmp].re;
      tempOut1Re = accRe - x[tempOut0Re_tmp].re;
      accIm = x[b_accRe_tmp].im;
      accRe = accIm + x[tempOut0Re_tmp].im;
      tempOut0Re_tmp = b_prod_re_tmp_tmp - k;
      accIm = twiddleTable[k] * tempOut1Re - twiddleTable[tempOut0Re_tmp] *
        accRe;
      accRe = twiddleTable[k] * accRe + twiddleTable[tempOut0Re_tmp] *
        tempOut1Re;
      tempOut0Re_tmp = yIdx + jbr_f;
      output[tempOut0Re_tmp].re = tempOut0Re + (0.0F - accRe);
      output[tempOut0Re_tmp].im = tempOut0Im + accIm;
      tempOut0Re_tmp = yIdx + jbr_b;
      output[tempOut0Re_tmp].re = tempOut0Re - (0.0F - accRe);
      output[tempOut0Re_tmp].im = accIm - tempOut0Im;

      /* Compute next bit-reversed destination index */
      tempOut0Re_tmp = accRe_tmp_tmp;
      do {
        tempOut0Re_tmp = (int)((unsigned int)tempOut0Re_tmp >> 1);
        jbr_f ^= tempOut0Re_tmp;
      } while ((jbr_f & tempOut0Re_tmp) == 0);

      /* Compute next bit-reversed destination index */
      tempOut0Re_tmp = accRe_tmp_tmp;
      do {
        tempOut0Re_tmp = (int)((unsigned int)tempOut0Re_tmp >> 1);
        jbr_b ^= tempOut0Re_tmp;
      } while ((jbr_b & tempOut0Re_tmp) != 0);

      k += twiddleStep;
    }
  }
}

/*
 * Arguments    : dsp_Delay_1 *obj
 * Return Type  : void
 */
void InitializeConditions(dsp_Delay_1 *obj)
{
  int j;
  int delayIdx;

  /* System object Initialization function: dsp.Delay */
  obj->W3_NeedsToInit = false;
  obj->W0_CIRC_BUF_IDX = 0;
  j = 0;
  for (delayIdx = 0; delayIdx < 2048; delayIdx++) {
    obj->W1_IC_BUFF[j] = obj->P0_IC;
    j++;
  }

  obj->W2_PrevNumChan = -1;
}

/*
 * Arguments    : const dsp_IFFT_5 *obj
 *                const creal32_T U0_data[]
 *                const int U0_size[2]
 *                float Y0_data[]
 *                int Y0_size[2]
 * Return Type  : void
 */
void Outputs(const dsp_IFFT_5 *obj, const creal32_T U0_data[], const int
             U0_size[2], float Y0_data[], int Y0_size[2])
{
  bool hasDimError;
  int nChans;
  int twiddleStep;
  int nOutRows;
  int mNOutRows2_tmp;
  int offset;

  /* System object Outputs function: dsp.IFFT */
  Y0_size[0] = U0_size[0];
  Y0_size[1] = U0_size[1];
  if ((U0_size[0] != 0) && (U0_size[1] != 0)) {
    hasDimError = (((U0_size[0] & (U0_size[0] - 1U)) != 0U) || ((unsigned int)
      U0_size[0] == 0U));
    if (!hasDimError) {
      nChans = U0_size[1];
      twiddleStep = div_s32_floor(4096, U0_size[0]);
      nOutRows = U0_size[0];
      if (nOutRows == 1) {
        twiddleStep = 0;
        while (twiddleStep < nChans) {
          Y0_data[0] = U0_data[0].re;
          twiddleStep = 1;
        }
      } else {
        if (nChans % 2 != 0) {
          MWDSPCG_IFFT_DblLen_S_XC((creal32_T *)&U0_data[0U], (float *)&Y0_data
            [0U], nOutRows, nChans, nOutRows, (float *)&obj->P0_TwiddleTable[0U],
            twiddleStep);
          if (nOutRows > 2) {
            mNOutRows2_tmp = nOutRows >> 1;
            offset = (nOutRows * (nChans - 1)) >> 1;
            MWDSPCG_R2DIT_TBLS_C((creal32_T *)&Y0_data[0U], 1, mNOutRows2_tmp,
                                 mNOutRows2_tmp, offset, (float *)
                                 &obj->P0_TwiddleTable[0U], twiddleStep << 1,
                                 true);
          }
        }

        /* Scale inverse transformation */
        for (twiddleStep = 0; twiddleStep < nOutRows * nChans; twiddleStep++) {
          Y0_data[twiddleStep] /= (float)nOutRows;
        }
      }
    }
  }
}

/*
 * Arguments    : dsp_Delay_0 *obj
 * Return Type  : void
 */
void b_InitializeConditions(dsp_Delay_0 *obj)
{
  int j;
  int delayIdx;

  /* System object Initialization function: dsp.Delay */
  obj->W3_NeedsToInit = false;
  obj->W0_CIRC_BUF_IDX = 0;
  j = 0;
  for (delayIdx = 0; delayIdx < 1024; delayIdx++) {
    obj->W1_IC_BUFF[j] = obj->P0_IC;
    j++;
  }

  obj->W2_PrevNumChan = -1;
}

/*
 * File trailer for step.c
 *
 * [EOF]
 */

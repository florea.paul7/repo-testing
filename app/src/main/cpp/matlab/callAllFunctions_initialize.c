/*
 * File: callAllFunctions_initialize.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "callAllFunctions_initialize.h"
#include "getComplexLCRvectorized.h"
#include "spkCompress.h"
#include "hpCompress.h"
#include "BACCHcxBuff.h"
#include "compMe1Band_SPK.h"
#include "limitMe.h"
#include "compMe4Band_HP.h"
#include "compMe1Band_HP.h"
#include "callAllFunctions_data.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void callAllFunctions_initialize(void)
{
  rt_InitInfAndNaN(8U);
  b_runBefore_not_empty_init();
  minimag.re = 0.0F;
  minimag.im = 1.17549435E-38F;
  runBefore_not_empty_init();
  f_runbefore_not_empty_init();
  e_runbefore_not_empty_init();
  d_runbefore_not_empty_init();
  c_runbefore_not_empty_init();
  b_runbefore_not_empty_init();
  runbefore_not_empty_init();
  compMe1Band_HP_init();
  compMe4Band_HP_init();
  limitMe_init();
  compMe1Band_SPK_init();
  BACCHcxBuff_init();
  hpCompress_init();
  spkCompress_init();
  getComplexLCRvectorized_init();
}

/*
 * File trailer for callAllFunctions_initialize.c
 *
 * [EOF]
 */

/*
 * File: crossoverFilter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef CROSSOVERFILTER_H
#define CROSSOVERFILTER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern crossoverFilter *b_crossoverFilter_crossoverFilt(crossoverFilter *obj);
extern crossoverFilter *c_crossoverFilter_crossoverFilt(crossoverFilter *obj);
extern void c_crossoverFilter_processTunedP(crossoverFilter *obj);
extern crossoverFilter *crossoverFilter_crossoverFilter(crossoverFilter *obj);
extern void crossoverFilter_resetImpl(crossoverFilter *obj);
extern void crossoverFilter_stepImpl(crossoverFilter *obj, const float x[4096],
  float varargout_1[4096], float varargout_2[4096]);
extern crossoverFilter_1 *d_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj);
extern crossoverFilter_1 *e_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj);
extern crossoverFilter_1 *f_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj);

#endif

/*
 * File trailer for crossoverFilter.h
 *
 * [EOF]
 */

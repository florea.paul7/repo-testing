/*
 * File: crossoverFilter.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "crossoverFilter.h"
#include "tuneCrossoverFilterCoefficients.h"
#include "TwoBandCrossoverFilter.h"
#include "SystemCore.h"
#include "BACCHcxBuff.h"
#include "SystemProp.h"
#include "callAllFunctions_rtwutil.h"

/* Function Definitions */

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : crossoverFilter *
 */
crossoverFilter *b_crossoverFilter_crossoverFilt(crossoverFilter *obj)
{
  crossoverFilter *b_obj;
  bool flag;
  double x;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pFreq[0] = 40.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSlopes[0] = 12.0;
  x = b_obj->pSlopes[0] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[0] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[0] == 0.0) {
    b_obj->pSlopes[0] = 6.0;
  }

  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : crossoverFilter *
 */
crossoverFilter *c_crossoverFilter_crossoverFilt(crossoverFilter *obj)
{
  crossoverFilter *b_obj;
  bool flag;
  double x;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pFreq[0] = 40.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSlopes[0] = 12.0;
  x = b_obj->pSlopes[0] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[0] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[0] == 0.0) {
    b_obj->pSlopes[0] = 6.0;
  }

  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : void
 */
void c_crossoverFilter_processTunedP(crossoverFilter *obj)
{
  double freqVect;
  float b_freqVect;
  float FsByTwo;
  bool flag;
  freqVect = obj->pFreq[0];
  b_freqVect = (float)freqVect;
  FsByTwo = (float)obj->pSampleRateDialog / 2.0F;
  if ((float)freqVect >= FsByTwo) {
    b_freqVect = FsByTwo - 1.0F;
  }

  if ((float)freqVect == 0.0F) {
    b_freqVect = 1.0F;
  }

  obj->pCrossoverFrequencies = b_freqVect;
  obj->pOrders = (float)obj->pSlopes[0] / 6.0F;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies = obj->pCrossoverFrequencies;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders = obj->pOrders;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
}

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : crossoverFilter *
 */
crossoverFilter *crossoverFilter_crossoverFilter(crossoverFilter *obj)
{
  crossoverFilter *b_obj;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  SystemProp_parseInputs(b_obj);
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : crossoverFilter *obj
 * Return Type  : void
 */
void crossoverFilter_resetImpl(crossoverFilter *obj)
{
  bool flag;
  float AH1[12];
  int i5;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.SampleRate = (float)obj->pSampleRateDialog;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverFrequencies = obj->pCrossoverFrequencies;
  flag = (obj->pFilter.isInitialized == 1);
  if (flag) {
    obj->pFilter.TunablePropsChanged = true;
  }

  obj->pFilter.CrossoverOrders = obj->pOrders;
  if (obj->pFilter.isInitialized == 1) {
    memset(&obj->pFilter.pCR1_States_LP[0], 0, sizeof(float) << 4);
    memset(&obj->pFilter.pCR1_States_HP[0], 0, sizeof(float) << 4);
    tuneCrossoverFilterCoefficients(obj->pFilter.CrossoverFrequencies,
      obj->pFilter.CrossoverOrders, obj->pFilter.SampleRate,
      obj->pFilter.pCR1_B_LP, obj->pFilter.pCR1_A_LP, obj->pFilter.pCR1_B_HP,
      AH1, &obj->pFilter.pPhaseMult);
    for (i5 = 0; i5 < 12; i5++) {
      obj->pFilter.pCR1_A_HP[i5] = AH1[i5];
    }
  }
}

/*
 * Arguments    : crossoverFilter *obj
 *                const float x[4096]
 *                float varargout_1[4096]
 *                float varargout_2[4096]
 * Return Type  : void
 */
void crossoverFilter_stepImpl(crossoverFilter *obj, const float x[4096], float
  varargout_1[4096], float varargout_2[4096])
{
  if (obj->pFilter.isInitialized != 1) {
    b_SystemCore_setupAndReset(&obj->pFilter);
  }

  b_SystemCore_checkTunableProps(&obj->pFilter);
  memcpy(&varargout_2[0], &x[0], sizeof(float) << 12);
  TwoBandCrossoverFilter_stepImpl(&obj->pFilter, varargout_2, varargout_1);
}

/*
 * Arguments    : crossoverFilter_1 *obj
 * Return Type  : crossoverFilter_1 *
 */
crossoverFilter_1 *d_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj)
{
  crossoverFilter_1 *b_obj;
  bool flag;
  double x;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pFreq[0] = 150.0;
  b_obj->pFreq[1] = 700.0;
  b_obj->pFreq[2] = 5000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  x = b_obj->pSlopes[0] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[0] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[0] == 0.0) {
    b_obj->pSlopes[0] = 6.0;
  }

  x = b_obj->pSlopes[1] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[1] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[1] == 0.0) {
    b_obj->pSlopes[1] = 6.0;
  }

  x = b_obj->pSlopes[2] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[2] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[2] == 0.0) {
    b_obj->pSlopes[2] = 6.0;
  }

  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : crossoverFilter_1 *obj
 * Return Type  : crossoverFilter_1 *
 */
crossoverFilter_1 *e_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj)
{
  crossoverFilter_1 *b_obj;
  bool flag;
  double x;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pFreq[0] = 150.0;
  b_obj->pFreq[1] = 700.0;
  b_obj->pFreq[2] = 5000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  x = b_obj->pSlopes[0] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[0] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[0] == 0.0) {
    b_obj->pSlopes[0] = 6.0;
  }

  x = b_obj->pSlopes[1] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[1] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[1] == 0.0) {
    b_obj->pSlopes[1] = 6.0;
  }

  x = b_obj->pSlopes[2] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[2] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[2] == 0.0) {
    b_obj->pSlopes[2] = 6.0;
  }

  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : crossoverFilter_1 *obj
 * Return Type  : crossoverFilter_1 *
 */
crossoverFilter_1 *f_crossoverFilter_crossoverFilt(crossoverFilter_1 *obj)
{
  crossoverFilter_1 *b_obj;
  bool flag;
  double x;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->pFreq[0] = 100.0;
  b_obj->pFreq[1] = 800.0;
  b_obj->pFreq[2] = 1500.0;
  b_obj->pFreq[3] = 3000.0;
  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  b_obj->pSlopes[3] = 12.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pFreq[0] = 150.0;
  b_obj->pFreq[1] = 700.0;
  b_obj->pFreq[2] = 5000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSlopes[0] = 12.0;
  b_obj->pSlopes[1] = 12.0;
  b_obj->pSlopes[2] = 12.0;
  x = b_obj->pSlopes[0] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[0] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[0] == 0.0) {
    b_obj->pSlopes[0] = 6.0;
  }

  x = b_obj->pSlopes[1] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[1] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[1] == 0.0) {
    b_obj->pSlopes[1] = 6.0;
  }

  x = b_obj->pSlopes[2] / 6.0;
  x = rt_roundd_snf(x);
  b_obj->pSlopes[2] = fmin(6.0 * x, 48.0);
  if (b_obj->pSlopes[2] == 0.0) {
    b_obj->pSlopes[2] = 6.0;
  }

  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * File trailer for crossoverFilter.c
 *
 * [EOF]
 */

/*
 * File: power.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "power.h"

/* Function Definitions */

/*
 * Arguments    : const float b_data[]
 *                const int b_size[1]
 *                float y_data[]
 *                int y_size[1]
 * Return Type  : void
 */
void b_power(const float b_data[], const int b_size[1], float y_data[], int
             y_size[1])
{
  int nx;
  int k;
  y_size[0] = (short)b_size[0];
  nx = (short)b_size[0];
  for (k = 0; k < nx; k++) {
    y_data[k] = powf(10.0F, b_data[k]);
  }
}

/*
 * Arguments    : const float b_data[]
 *                float y_data[]
 *                int y_size[2]
 * Return Type  : void
 */
void c_power(const float b_data[], float y_data[], int y_size[2])
{
  int k;
  y_size[0] = 2048;
  y_size[1] = 2;
  for (k = 0; k < 4096; k++) {
    y_data[k] = powf(10.0F, b_data[k]);
  }
}

/*
 * Arguments    : const float a_data[]
 *                const int a_size[1]
 *                float y_data[]
 *                int y_size[1]
 * Return Type  : void
 */
void power(const float a_data[], const int a_size[1], float y_data[], int
           y_size[1])
{
  int nx;
  int k;
  y_size[0] = (short)a_size[0];
  nx = (short)a_size[0];
  for (k = 0; k < nx; k++) {
    y_data[k] = a_data[k] * a_data[k];
  }
}

/*
 * File trailer for power.c
 *
 * [EOF]
 */

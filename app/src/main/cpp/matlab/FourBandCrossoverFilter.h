/*
 * File: FourBandCrossoverFilter.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef FOURBANDCROSSOVERFILTER_H
#define FOURBANDCROSSOVERFILTER_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void c_FourBandCrossoverFilter_reset(c_audio_internal_FourBandCrosso *obj);
extern void c_FourBandCrossoverFilter_stepI(c_audio_internal_FourBandCrosso *obj,
  const float x_data[], const int x_size[2], float z1_data[], int z1_size[2],
  float z2_data[], int z2_size[2], float z3_data[], int z3_size[2], float
  z4_data[], int z4_size[2]);
extern void c_FourBandCrossoverFilter_tuneC(c_audio_internal_FourBandCrosso *obj);

#endif

/*
 * File trailer for FourBandCrossoverFilter.h
 *
 * [EOF]
 */

/*
 * File: callAllFunctions_rtwutil.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "callAllFunctions_rtwutil.h"

/* Function Definitions */

/*
 * Arguments    : creal32_T y[]
 *                int nChans
 *                int nRows
 *                int fftLen
 *                int offset
 *                const float tablePtr[]
 *                int twiddleStep
 *                bool isInverse
 * Return Type  : void
 */
void MWDSPCG_R2DIT_TBLS_C(creal32_T y[], int nChans, int nRows, int fftLen, int
  offset, const float tablePtr[], int twiddleStep, bool isInverse)
{
  int nHalf;
  int nQtr;
  int fwdInvFactor;
  int offsetCh;
  int iCh;
  int ix;
  int idelta;
  float tmp_re;
  int k;
  float tmp_im;
  int kratio;
  int i1;
  int istart;
  int i2;
  int j;
  float twidRe;
  float twidIm;

  /* DSP System Toolbox Decimation in Time FFT  */
  /* Computation performed using table lookup  */
  /* Output type: complex real32_T */
  nHalf = (fftLen >> 1) * twiddleStep;
  nQtr = nHalf >> 1;
  if (isInverse) {
    fwdInvFactor = -1;
  } else {
    fwdInvFactor = 1;
  }

  /* For each channel */
  offsetCh = offset;
  for (iCh = 0; iCh < nChans; iCh++) {
    /* Perform butterflies for the first stage, where no multiply is required. */
    for (ix = offsetCh; ix < (fftLen + offsetCh) - 1; ix += 2) {
      tmp_re = y[ix + 1].re;
      tmp_im = y[ix + 1].im;
      y[ix + 1].re = y[ix].re - tmp_re;
      y[ix + 1].im = y[ix].im - tmp_im;
      y[ix].re += tmp_re;
      y[ix].im += tmp_im;
    }

    idelta = 2;
    k = fftLen >> 2;
    kratio = k * twiddleStep;
    while (k > 0) {
      i1 = offsetCh;

      /* Perform the first butterfly in each remaining stage, where no multiply is required. */
      for (ix = 0; ix < k; ix++) {
        i2 = i1 + idelta;
        tmp_re = y[i2].re;
        tmp_im = y[i2].im;
        y[i2].re = y[i1].re - tmp_re;
        y[i2].im = y[i1].im - tmp_im;
        y[i1].re += tmp_re;
        y[i1].im += tmp_im;
        i1 += idelta << 1;
      }

      istart = offsetCh + 1;

      /* Perform remaining butterflies */
      for (j = kratio; j < nHalf; j += kratio) {
        i1 = istart;
        twidRe = tablePtr[j];
        twidIm = (float)fwdInvFactor * tablePtr[j + nQtr];
        for (ix = 0; ix < k; ix++) {
          i2 = i1 + idelta;
          tmp_re = y[i2].re * twidRe - y[i2].im * twidIm;
          tmp_im = y[i2].re * twidIm + y[i2].im * twidRe;
          y[i2].re = y[i1].re - tmp_re;
          y[i2].im = y[i1].im - tmp_im;
          y[i1].re += tmp_re;
          y[i1].im += tmp_im;
          i1 += idelta << 1;
        }

        istart++;
      }

      idelta <<= 1;
      k >>= 1;
      kratio >>= 1;
    }

    /* Point to next channel */
    offsetCh += nRows;
  }
}

/*
 * Arguments    : int numerator
 *                int denominator
 * Return Type  : int
 */
int div_s32_floor(int numerator, int denominator)
{
  int quotient;
  unsigned int absNumerator;
  unsigned int absDenominator;
  bool quotientNeedsNegation;
  unsigned int tempAbsQuotient;
  if (denominator == 0) {
    if (numerator >= 0) {
      quotient = MAX_int32_T;
    } else {
      quotient = MIN_int32_T;
    }
  } else {
    if (numerator < 0) {
      absNumerator = ~(unsigned int)numerator + 1U;
    } else {
      absNumerator = (unsigned int)numerator;
    }

    if (denominator < 0) {
      absDenominator = ~(unsigned int)denominator + 1U;
    } else {
      absDenominator = (unsigned int)denominator;
    }

    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    tempAbsQuotient = absNumerator / absDenominator;
    if (quotientNeedsNegation) {
      absNumerator %= absDenominator;
      if (absNumerator > 0U) {
        tempAbsQuotient++;
      }

      quotient = -(int)tempAbsQuotient;
    } else {
      quotient = (int)tempAbsQuotient;
    }
  }

  return quotient;
}

/*
 * Arguments    : double b_u
 * Return Type  : double
 */
double rt_roundd_snf(double b_u)
{
  double y;
  if (fabs(b_u) < 4.503599627370496E+15) {
    if (b_u >= 0.5) {
      y = floor(b_u + 0.5);
    } else if (b_u > -0.5) {
      y = b_u * 0.0;
    } else {
      y = ceil(b_u - 0.5);
    }
  } else {
    y = b_u;
  }

  return y;
}

/*
 * File trailer for callAllFunctions_rtwutil.c
 *
 * [EOF]
 */

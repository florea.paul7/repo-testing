/*
 * File: callHPComp.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 18:53:34
 */

/* Include Files */
#include <math.h>
#include "callAllFunctions.h"
#include "callHPComp.h"
#include "hpCompress.h"

/* Function Definitions */

/*
 * testHPComp - Applies Headphone Compression
 *
 *    Generated Code Signature:
 *    extern void hpCompress(const float yin[4096], float fs, float yout_data[], int
 *        yout_size[2]);
 * Arguments    : const emxArray_real32_T *yin
 *                float preGain
 *                int Fs
 * Return Type  : void
 */
void callHPComp(const emxArray_real32_T *yin, float preGain, int Fs)
{
  int numBlocks;
  int b;
  short b_b;
  int i2;
  short curBlk_tmp[2048];
  static float b_yin[4096];
  static emxArray_real32_T_2048x2 c_yin;
  int i3;

  /*  Parameters - Define Input Types */
  /* Compressor Settings */
  /*  Processing Loop */
  numBlocks = yin->size[0];
  if (numBlocks <= 2) {
    numBlocks = 2;
  }

  if (yin->size[0] == 0) {
    numBlocks = 0;
  }

  numBlocks = (int)floor((double)numBlocks / 2048.0);
  for (b = 0; b < numBlocks; b++) {
    b_b = (short)((short)((short)b << 11) + 1);
    for (i2 = 0; i2 < 2048; i2++) {
      curBlk_tmp[i2] = (short)(i2 + b_b);
    }

    for (i2 = 0; i2 < 2; i2++) {
      for (i3 = 0; i3 < 2048; i3++) {
        b_yin[i3 + (i2 << 11)] = yin->data[(curBlk_tmp[i3] + yin->size[0] * i2)
          - 1];
      }
    }

    hpCompress(b_yin, preGain, Fs, c_yin.data, c_yin.size);
  }
}

/*
 * File trailer for callHPComp.c
 *
 * [EOF]
 */

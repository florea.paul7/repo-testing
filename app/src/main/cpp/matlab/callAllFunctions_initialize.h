/*
 * File: callAllFunctions_initialize.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef CALLALLFUNCTIONS_INITIALIZE_H
#define CALLALLFUNCTIONS_INITIALIZE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void callAllFunctions_initialize(void);

#endif

/*
 * File trailer for callAllFunctions_initialize.h
 *
 * [EOF]
 */

/*
 * File: spkCompress.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

/* Include Files */
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "spkCompress.h"
#include "limitMe.h"
#include "compMe1Band_SPK.h"

/* Variable Definitions */
static bool e_runbefore_not_empty;
static emxArray_real32_T_2048x2 b_yout1;
static int d_oldfs;

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void e_runbefore_not_empty_init(void)
{
  e_runbefore_not_empty = false;
}

/*
 * SPKCOMPRESS - Speaker Compression Block
 *    Calls Expander + 2 Stage 1 Band Compressor followed by a limiter
 * Arguments    : const float yin[4096]
 *                int fs
 *                float yout_data[]
 *                int yout_size[2]
 * Return Type  : void
 */
void spkCompress(const float yin[4096], int fs, float yout_data[], int
                 yout_size[2])
{
  if (!e_runbefore_not_empty) {
    d_oldfs = fs;
    e_runbefore_not_empty = true;
  }

  if (d_oldfs != fs) {
    b_yout1.size[0] = 2048;
    b_yout1.size[1] = 2;
    memset(&b_yout1.data[0], 0, sizeof(float) << 12);
  }

  /* Process Audio */
  compMe1Band_SPK(yin, fs, b_yout1.data, b_yout1.size);
  limitMe(b_yout1.data, fs, yout_data, yout_size);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void spkCompress_init(void)
{
  b_yout1.size[1] = 0;
  b_yout1.size[0] = 2048;
  b_yout1.size[1] = 2;
  memset(&b_yout1.data[0], 0, sizeof(float) << 12);
}

/*
 * File trailer for spkCompress.c
 *
 * [EOF]
 */

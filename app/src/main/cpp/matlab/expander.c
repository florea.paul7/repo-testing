/*
 * File: expander.c
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "callAllFunctions.h"
#include "expander.h"
#include "power.h"
#include "ExpanderBase.h"
#include "abs.h"
#include "log10.h"

/* Function Declarations */
static void expander_computeGain(const expander *obj, const float xG_data[],
  float G_data[], int G_size[1]);

/* Function Definitions */

/*
 * Arguments    : const expander *obj
 *                const float xG_data[]
 *                float G_data[]
 *                int G_size[1]
 * Return Type  : void
 */
static void expander_computeGain(const expander *obj, const float xG_data[],
  float G_data[], int G_size[1])
{
  float W;
  float R;
  float T;
  int ind1_tmp_size[1];
  int partialTrueCount;
  float ind1_tmp_data[2049];
  int trueCount;
  bool ind1_data[2048];
  int i;
  short tmp_data[2048];
  float b_tmp_data[2048];
  float c_tmp_data[2049];
  int tmp_size[1];
  float z;
  short d_tmp_data[2048];
  int xG_size[1];
  float z_data[4096];
  W = (float)obj->KneeWidth;
  R = (float)obj->Ratio;
  T = (float)obj->Threshold;
  ind1_tmp_size[0] = 2048;
  for (partialTrueCount = 0; partialTrueCount < 2048; partialTrueCount++) {
    G_data[partialTrueCount] = xG_data[partialTrueCount];
    ind1_tmp_data[partialTrueCount] = xG_data[partialTrueCount] - T;
  }

  for (partialTrueCount = 0; partialTrueCount < 2048; partialTrueCount++) {
    ind1_data[partialTrueCount] = (2.0F * ind1_tmp_data[partialTrueCount] <= -W);
  }

  trueCount = 0;
  for (i = 0; i < 2048; i++) {
    if (ind1_data[i]) {
      trueCount++;
    }
  }

  partialTrueCount = 0;
  for (i = 0; i < 2048; i++) {
    if (ind1_data[i]) {
      tmp_data[partialTrueCount] = (short)(i + 1);
      partialTrueCount++;
    }
  }

  for (partialTrueCount = 0; partialTrueCount < trueCount; partialTrueCount++) {
    b_tmp_data[partialTrueCount] = (xG_data[tmp_data[partialTrueCount] - 1] - T)
      * R;
  }

  partialTrueCount = 0;
  for (i = 0; i < 2048; i++) {
    if (ind1_data[i]) {
      G_data[i] = T + b_tmp_data[partialTrueCount];
      partialTrueCount++;
    }
  }

  if (W != 0.0F) {
    b_abs(ind1_tmp_data, ind1_tmp_size, c_tmp_data, tmp_size);
    i = tmp_size[0];
    for (partialTrueCount = 0; partialTrueCount < i; partialTrueCount++) {
      ind1_data[partialTrueCount] = (2.0F * c_tmp_data[partialTrueCount] <= W);
    }

    trueCount = 0;
    for (i = 0; i < 2048; i++) {
      if (ind1_data[i]) {
        trueCount++;
      }
    }

    partialTrueCount = 0;
    for (i = 0; i < 2048; i++) {
      if (ind1_data[i]) {
        d_tmp_data[partialTrueCount] = (short)(i + 1);
        partialTrueCount++;
      }
    }

    z = W / 2.0F;
    W *= 2.0F;
    xG_size[0] = trueCount;
    for (partialTrueCount = 0; partialTrueCount < trueCount; partialTrueCount++)
    {
      b_tmp_data[partialTrueCount] = (xG_data[d_tmp_data[partialTrueCount] - 1]
        - T) - z;
    }

    power(b_tmp_data, xG_size, z_data, ind1_tmp_size);
    i = ind1_tmp_size[0];
    for (partialTrueCount = 0; partialTrueCount < i; partialTrueCount++) {
      z_data[partialTrueCount] = (1.0F - R) * z_data[partialTrueCount] / W;
    }

    partialTrueCount = 0;
    for (i = 0; i < 2048; i++) {
      if (ind1_data[i]) {
        G_data[i] = xG_data[i] + z_data[partialTrueCount];
        partialTrueCount++;
      }
    }
  }

  G_size[0] = 2048;
  for (partialTrueCount = 0; partialTrueCount < 2048; partialTrueCount++) {
    G_data[partialTrueCount] -= xG_data[partialTrueCount];
  }
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *b_expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.01;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *c_expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.01;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *d_expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *e_expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 48000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 44100.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.01;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * Arguments    : expander *obj
 *                const float x_data[]
 *                const int x_size[1]
 *                float y_data[]
 *                int y_size[1]
 *                float G_data[]
 *                int G_size[1]
 * Return Type  : void
 */
void expander_stepImpl(expander *obj, const float x_data[], const int x_size[1],
  float y_data[], int y_size[1], float G_data[], int G_size[1])
{
  float varargin_1_data[2049];
  int varargin_1_size[1];
  int maxval_size[1];
  int nx;
  int k;
  float maxval_data[2049];
  float tmp_data[2049];
  float b_G_data[2048];
  int b_G_size[1];
  b_abs(x_data, x_size, varargin_1_data, varargin_1_size);
  maxval_size[0] = (short)varargin_1_size[0];
  nx = (short)varargin_1_size[0];
  for (k = 0; k < nx; k++) {
    maxval_data[k] = fmaxf(varargin_1_data[k], 1.1920929E-7F);
  }

  b_log10(maxval_data, maxval_size);
  nx = maxval_size[0];
  for (k = 0; k < nx; k++) {
    tmp_data[k] = 20.0F * maxval_data[k];
  }

  expander_computeGain(obj, tmp_data, G_data, G_size);
  memcpy(&b_G_data[0], &G_data[0], sizeof(float) << 11);
  ExpanderBase_detectLevel(obj, b_G_data, G_data, G_size);
  b_G_size[0] = 2048;
  for (k = 0; k < 2048; k++) {
    tmp_data[k] = G_data[k] / 20.0F;
  }

  b_power(tmp_data, b_G_size, varargin_1_data, varargin_1_size);
  y_size[0] = 2048;
  for (k = 0; k < 2048; k++) {
    y_data[k] = x_data[k] * varargin_1_data[k];
  }
}

/*
 * Arguments    : expander *obj
 * Return Type  : expander *
 */
expander *f_expander_expander(expander *obj)
{
  expander *b_obj;
  bool flag;
  b_obj = obj;
  b_obj->pNumChannels = -1.0;
  b_obj->isInitialized = 0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Threshold = -73.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->Ratio = 2.5;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->pSampleRateDialog = 96000.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->AttackTime = 0.0005;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->ReleaseTime = 0.001;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->KneeWidth = 15.0;
  flag = (b_obj->isInitialized == 1);
  if (flag) {
    b_obj->TunablePropsChanged = true;
  }

  b_obj->HoldTime = 0.0;
  b_obj->matlabCodegenIsDeleted = false;
  return b_obj;
}

/*
 * File trailer for expander.c
 *
 * [EOF]
 */

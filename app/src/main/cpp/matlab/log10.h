/*
 * File: log10.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 16:46:25
 */

#ifndef LOG10_H
#define LOG10_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

/* Function Declarations */
extern void b_log10(float x_data[], int x_size[1]);
extern void c_log10(float x_data[]);

#endif

/*
 * File trailer for log10.h
 *
 * [EOF]
 */

/*
 * File: compMe4Band_HP.h
 *
 * MATLAB Coder version            : 4.1
 * C/C++ source code generated on  : 10-Mar-2019 12:43:17
 */

#ifndef COMPME4BAND_HP_H
#define COMPME4BAND_HP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "callAllFunctions_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Declarations */
extern void c_runbefore_not_empty_init(void);
extern void compMe4Band_HP(const float yin_data[], const int yin_size[2], int fs,
  float yout_data[], int yout_size[2]);
extern void compMe4Band_HP_free(void);
extern void compMe4Band_HP_init(void);

#ifdef __cplusplus
};
#endif

#endif



/*
 * File trailer for compMe4Band_HP.h
 *
 * [EOF]
 */

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/inc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/modules/dsp

LOCAL_MODULE    := ne10_static

LOCAL_ARM_NEON	:= true
LOCAL_ARM_MODE := arm

LOCAL_CFLAGS    += -D__ARM_NEON

ne10_srcs_cpp 	+= modules/dsp/NE10_init_dsp.c
ne10_srcs_cpp 	+= modules/dsp/NE10_fft.c
ne10_srcs_cpp 	+= modules/dsp/NE10_fft_float32.c
ne10_srcs_cpp 	+= modules/dsp/NE10_fft_float32.neonintrinsic.c
ne10_srcs_cpp 	+= modules/dsp/NE10_fft_generic_float32.c
ne10_srcs_cpp 	+= modules/dsp/NE10_fft_generic_float32.neonintrinsic.cpp
ne10_srcs_cpp   += modules/math/NE10_init_math.c
ne10_srcs_cpp   += modules/math/NE10_mul.c
ne10_srcs_cpp   += modules/math/NE10_add.c
ne10_srcs_cpp   += modules/math/NE10_mla.c
ne10_srcs_cpp   += modules/math/NE10_setc.c
ne10_srcs_cpp   += modules/math/NE10_setc.neon.c


LOCAL_SRC_FILES := $(ne10_srcs_cpp)

include $(BUILD_STATIC_LIBRARY)
#ifndef __BACCH_EQ_CONSTS_INCLUDE__
#define __BACCH_EQ_CONSTS_INCLUDE__

#define EQ_NUM_IRS       1

#define EQ_SPAN          16.00f

static int EQ_IR_LENGTHS[3] = { 1024, 1024, 2048 };

static int EQ_IR_NUM_BLOCKS[3] = { 1, 1, 2 };

#endif //__BACCH_EQ_CONSTS_INCLUDE__

#ifndef __BACCH_VIRT_SHORT_CONSTS_INCLUDE__
#define __BACCH_VIRT_SHORT_CONSTS_INCLUDE__

#define VIRT_SHORT_NUM_IRS       7

static int VIRT_SHORT_IR_LENGTHS[3] = { 300, 300, 600 };

static int VIRT_SHORT_IR_NUM_BLOCKS[3] = { 1, 1, 1 };

#endif //__BACCH_VIRT_SHORT_CONSTS_INCLUDE__

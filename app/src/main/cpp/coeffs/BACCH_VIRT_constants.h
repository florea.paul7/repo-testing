#ifndef __BACCH_VIRT_CONSTS_INCLUDE__
#define __BACCH_VIRT_CONSTS_INCLUDE__

#define VIRT_NUM_IRS       7

static int VIRT_IR_LENGTHS[3] = { 8192, 8192, 16384 };

static int VIRT_IR_NUM_BLOCKS[3] = { 8, 8, 16 };

#endif //__BACCH_VIRT_CONSTS_INCLUDE__

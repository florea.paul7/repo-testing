/*
 * BACCH_library.h
 *
 *  Created on: Sep 11, 2012
 *      Author: Sam Caldwell
 *  Contains various constants used by BACCH modules
 *
 * Changelist:
 *		05.04.2018: added fade up and fade down array indexes
 *		05.01.2018: added state transition time
 *		04.09.2018: added state_t enum for module state management
 *		04.09.2018: added BACCH_samplerates enum  
 *		03.30.2018: rename and cleanup re move to new project
 */

#ifndef BACCH_LIBRARY_H_
#define BACCH_LIBRARY_H_

typedef enum  {
	VIRT_MODE_FIRST = 0,
	VIRT_MODE_DTS = 0,
	VIRT_MODE_DOLBY,
	VIRT_MODE_LAST
} virt_7_1_mode_t;

typedef enum {
    VIRT_IR_LENGTH_INVALID = -1,
    VIRT_IR_LENGTH_SHORT = 0,
    VIRT_IR_LENGTH_LONG = 1,
    VIRT_IR_LENGTH_CNT,
    VIRT_IR_LENGTH_LAST = VIRT_IR_LENGTH_CNT - 1,
} virt_ir_length_t;


typedef enum {
    BACCH_PARAM_FEATURE_ENABLE = 0x7F00,
    BACCH_PARAM_FEATURE_DISABLE,
    BACCH_PARAM_FEATURE_STATUS,
    BACCH_PARAM_SP_OFFSET,
    BACCH_PARAM_AUDIO_ORIENTATION,
    BACCH_PARAM_HP_HEADING,
    BACCH_PARAM_HEADTRACKING,
    BACCH_PARAM_VOLUME,
    BACCH_PARAM_PRESET,
} bacch_audio_params_t;

typedef enum {
    BACCH_FEATURE_INVALID = -1,
    BACCH_FEATURE_LIB,
    BACCH_FEATURE_HP,
    BACCH_FEATURE_SP,
	BACCH_FEATURE_LCEX,
	BACCH_FEATURE_ISACTIVE = 4,
    BACCH_FEATURE_CNT,
    BACCH_FEATURE_LAST = BACCH_FEATURE_CNT - 1
} bacch_feature_t;

typedef enum {
    BACCH_LIB_STATUS_SHIFT = 0,
    BACCH_HP_STATUS_SHIFT,
    BACCH_SP_STATUS_SHIFT,
	BACCH_LCEX_STATUS_SHIFT,
	BACCH_ISACTIVE_STATUS_SHIFT,
} bacch_feature_status_shift_t;

typedef enum {
    BACCH_FEATURE_OFF = 0,
    BACCH_FEATURE_ON,
} bacch_feature_status_t;

typedef enum {
    BACCH_SEQUENCE_PRESET_1 = 0,
    BACCH_SEQUENCE_PRESET_2 = 1,
    // and so on
    BACCH_SEQUENCE_PRESET_MAX = 10,
} bacch_audio_preset_t;

typedef enum {
    HEADTRACKING_ON = 0x1000,
    HEADTRACKING_OFF = 0x2000,
} bacch_headtracking_t;

// these must match BACCH_sp, used to configure BACCH_sp
typedef enum {
    BACCH_AUDIO_ORIENTATION_INVALID = -1,
    BACCH_AUDIO_ORIENTATION_LANDSCAPE,
    BACCH_AUDIO_ORIENTATION_LANDSCAPE_REVERSE,
    BACCH_AUDIO_ORIENTATION_PORTRAIT,
    BACCH_AUDIO_ORIENTATION_PORTRAIT_REVERSE,
    BACCH_AUDIO_ORIENTATION_CNT,
    BACCH_AUDIO_ORIENTATION_LAST = BACCH_AUDIO_ORIENTATION_CNT - 1
} bacch_audio_orientation_t;


#endif // BACCH_LIBRARY_H_

/*
 * BACCH_constants.h
 *
 *  Created on: Sep 11, 2012
 *      Author: Sam Caldwell
 *  Contains various constants used by BACCH modules
 *
 * Changelist:
 *		05.04.2018: added fade up and fade down array indexes
 *		05.01.2018: added state transition time
 *		04.09.2018: added state_t enum for module state management
 *		04.09.2018: added BACCH_samplerates enum  
 *		03.30.2018: rename and cleanup re move to new project
 */

#ifndef BACCH_CONSTANTS_H_
#define BACCH_CONSTANTS_H_

// Macros
#define FLOAT_IS_ZERO(x)		((fabsf(x))<0.001f)	
#define FLOAT_IS_ONE(x)			((fabsf(1.0f-(x)))<0.001f)

// Math constants
#define PI						3.1415926535897932384626433832795

#define	NORM_IN_16 				(0.000030518F)
#define NORM_OUT_16 			(32767.0F)
#define NORM_16					(32767.0F)
#define NORM_IN_32				(4.65661287E-10F)
#define NORM_OUT_32 			(2147483648.0F)

// IO Constants
#define IN_OUT_SIZE				8192
#define BACCH_FRAME_SIZE		2048
#define BACCH_FFT_SIZE			BACCH_FRAME_SIZE * 2
#define BACCH_FFT_ORDER			12
#define COMPLEX_WORDS			2

// transition time used in smoothly changing module states
#define STATE_TRANSITION_TIME_SEC		0.25f

#define COMPRESSOR_GAIN_FOR_HP  0.0f
#define COMPRESSOR_GAIN_FOR_SP  33.0f

// IO Configuration
#define IO_NOT_INTERLEAVED      0
#define IO_INTERLEAVED          1
#define IO_NOT_INPLACE          0
#define IO_INPLACE              1

#define DATA_TYPE_SIGNED16      0
#define DATA_TYPE_SIGNED24      1
#define DATA_TYPE_SIGNED32      2
#define DATA_TYPE_FLOAT32       3

#define MAX_IN_CHANNELS			8	// 7.1 input
#define MIN_IN_CHANNELS			2

#define NUM_OUT_CHANNELS		2

// Sample Rate defines
#define FS_44_1k	            44100
#define	FS_48k		            48000
#define	FS_96k		            96000
#define	FS_192k		            192000

// IR array channel indexes
#define LEFT_TO_LEFT			0
#define LEFT_TO_RIGHT			1
#define RIGHT_TO_LEFT			2
#define RIGHT_TO_RIGHT			3

#define CHANNEL_TO_LEFT         0
#define CHANNEL_TO_RIGHT        1

#define IR_HEAD					0


// heading/offset directionIsDown of change
#define DELTA_DIRECTION_UP		0
#define DELTA_DIRECTION_DOWN	1

// used as index for fade mix buffers
// always apply array BACCH_PROCESSED to the audio processed by BACCH
// apply array BACCH_BYPASS to unprocessed audio
#define BACCH_PROCESSED	        0
#define BACCH_BYPASS	        1
#define BACCH_TRANSITION_ON     0
#define BACCH_TRANSITION_OFF    1

// min heading/offset change to respond to, filter out small changes
#define MIN_FILTER_DELTA            2.0f

enum status_t {
    STATUS_OK = 0,
    STATUS_FAIL,
    STATUS_ENOMEM,
    STATUS_BUF_FULL,
    STATUS_BUF_EMPTY,
    STATUS_INVALID_ARG,
};

enum channel_t {
	BACCH_LEFT = 0,
	BACCH_RIGHT,
	BACCH_CENTER,
	BACCH_LFE,
	BACCH_LEFT_SURROUND,
	BACCH_RIGHT_SURROUND,
	BACCH_LEFT_REAR,
	BACCH_RIGHT_REAR
};

enum ir_channel_t {
    BACCH_IR_LEFT = 0,
    BACCH_IR_RIGHT,
    BACCH_IR_CENTER,
    BACCH_IR_LEFT_SURROUND,
    BACCH_IR_RIGHT_SURROUND,
    BACCH_IR_LEFT_REAR,
    BACCH_IR_RIGHT_REAR
};

enum buffer_op_t {
    BACCH_BUFFER_OP_INVALID = -1,
    BACCH_BUFFER_OP_WRITE = 0,
    BACCH_BUFFER_OP_READ = 1,
    BACCH_BUFFER_OP_LAST,
};

// Filter state.
enum state_t {
	// Bypass.
	STATE_BYPASS = 0x01,
	// In the process of smooth transition to bypass state.
	STATE_TRANSITION_TO_BYPASS = 0x02,
	// In the process of smooth transition to normal (enabled) state.
	STATE_TRANSITION_TO_NORMAL = 0x04,
	// In normal (enabled) state.
	STATE_NORMAL = 0x05,
	// A bit-mask for determining whether the filter is enabled or disabled
	// in the eyes of the client.
	STATE_ENABLED_MASK = 0x04
};

#endif /* BC_CONSTANTS_H_ */

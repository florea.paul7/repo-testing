#ifndef __BACCH_HP_INCLUDE__
#define __BACCH_HP_INCLUDE__

// supports only 2 channels in and 2 channels out

#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_HP_constants.h"
//#include <audio_effects/effect_bacch_audio.h>

#define HP_DEGREES_PER_IR	(float)HP_SPAN / (float)(HP_NUM_IRS-1)	

#define HP_NUM_IR_SETS 1

enum hp_ir_direction_t {
	HP_L_TO_L	= 0,
	HP_L_TO_R,
	HP_R_TO_L,
	HP_R_TO_R,
};


class BACCH_hp {

public:

	// Constructor
	// samplerate: audio samplerate (determines filter samplerate)
	// filterNum: which filter to load in the filter bank
	BACCH_hp(int samplerate);

	// Destructor
	~BACCH_hp();

	// process a frame of audio with length samples - must be block length (1024 samples)
	void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	// configure the sp instance:
	// load a new filter, clears internal buffers
	// input definition same as constructor
	// will cause discontinuity in output
	void configure(int samplerate);

	// Resets the internal state of the filter.
	// tail buffers are cleared state becomes disabled.
	// This change happens immediately and will cause discontinuities in the output.
	void reset();

	// Clears the tail buffers.
	// This change happens immediately and might cause discontinuities in the
	// output.
	void clear();

	BACCH_buffer			**mPad;
	int						*mPadIdx;

private:
	BACCH_fft				*fft;

	int						mFilterNum;
	int						mSampleRate;
	int						mSampleRateIdx;
	int						mNumIRBlocks;



	// complex buffers are setup with the following arrangement
	// TFs[a][b]
	// a - 1 for each input channel, aka 2 for stereo
	// b - fft block number - aka head TF or tail TF(s)
	BACCH_complex_buffer	***TFs;

	BACCH_complex_buffer	*ScratchIn;
	BACCH_complex_buffer	**ScratchFFT;

	void FilterLoad(void);

};

#endif //__BACCH_HP_INCLUDE__

#ifndef __BACCH_HP_XTC_INCLUDE__
#define __BACCH_HP_XTC_INCLUDE__

// Multichannel virtualizer convolution filter
// supports 4 to 8 channels in and 2 channels out

// the filter consists of separate head and tail processes
// separated in case a separate thread is required to close timing

// description:
// The head processor is responsible for processing the current input block to the current 
// output (minimal latency)
// The tail processor will process the 'tail' of the IR against current and previous input
//
// The virtualizer assumes input of 1024 sample frames though it can be modified to accept
// larger input sizes with the requirement that input be a power of 2
// The virtualizer processes up to 16 blocks in up to 16 stages (as required by the length of the IR)
// Completed in head processing:
// first stage: process first 1024 sample frame, accumulated with first frame in pad buffer 
// Completed in tail processing:
// subsequent stages: process first 1024 sample frame, accumulate with nth frame in pad buffer, starting at 2nd

#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_HP_XTC_constants.h"
#include "BACCH_library.h"

#define HP_XTC_MAX_FILTERNUM		1

class BACCH_hp_xtc {

public:

	// Constructor
	// samplerate: audio samplerate
	// filterNum: which filter to load in the filter bank
	BACCH_hp_xtc(int samplerate, int filterNum);

	// Destructor
	~BACCH_hp_xtc();

	// process a frame of audio with length samples - must be block length (1024 samples)
	void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	// configure the sp instance:
	// load a new filter, clears internal buffers
	// input definition same as constructor
	// will cause discontinuity in output
	void configure(int samplerate, int filterNum);


	// Resets the internal state of the filter.
	// tail buffers are cleared.
	// This change happens immediately and will cause discontinuities in the output.
	void reset();

	// Clears the tail buffers.
	// This change happens immediately and might cause discontinuities in the
	// output.
	void clear();

private:
	BACCH_fft*              fft;

	// filter number for use with multiple filters if available
	// currently is set to 0
	int						mVolumeIdx;
	
	// index into filter arrays and enums for given sample rate
	int						mSampleRateIdx;

	// number of IR blocks given the sample rate
	int						mNumIRBlocks;

	BACCH_complex_buffer	***TFs;

	// buffers and scratch for processing
	//BACCH_buffer			*mOut;
	BACCH_buffer			**mPad;
	int						*mPadIdx;

	BACCH_complex_buffer	*ScratchIn;
	BACCH_complex_buffer	**ScratchFFT;

	void FilterLoad(void);

};

#endif //__BACCH_HP_XTC_INCLUDE__

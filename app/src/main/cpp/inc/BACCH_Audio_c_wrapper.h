#pragma  once
/*
 * BACCH_Audio_c_wrapper.h
 *
 *  Created on: Sep 8, 2018
 *      Author: Samuel Caldwell
 */
/* =========================================================================
   Copyright (c) 2018 BACCH Laboratories Inc.
   All rights reserved. BACCH Laboratories Proprietary and Confidential.
   ========================================================================= */


#include "BACCH_library.h"
#include "BACCH_types.h"

#include "BACCH_Audio.h"
#include "BACCH_sp.h"

typedef unsigned char _bool;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _BACCH_AUDIO_EXPORTS_
#define BACCH_AUDIO_API __declspec(dllexport)
#else
#define BACCH_AUDIO_API
#endif


	//typedef struct BACCH_Audio BACCH_Audio_Object;

	typedef void* bufHndl;

	typedef void* baHandle;

	BACCH_AUDIO_API baHandle get_BA_instance(int sampleRate,
		int channelsIn,
		int orientation,
		const char* sp_id);

	BACCH_AUDIO_API void delete_BA_instance(baHandle mBA);

	BACCH_AUDIO_API void configure_BA(baHandle mBA, int channelsIn, int sampleRate, int orientation);

	BACCH_AUDIO_API void configure_VirtMode_BA(baHandle mBA, bool isDolby);

	BACCH_AUDIO_API void configure_UBACCH(baHandle mBA, float distance_speaker_to_speaker, float distance_listener_to_speaker, bool isMetricUnits);

	BACCH_AUDIO_API void reset_BA(baHandle mBA);

	BACCH_AUDIO_API void setBACCHEnable_BA(baHandle mBA, _bool enabled, _bool isHP, _bool immediate);

	BACCH_AUDIO_API void setHPEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate);

	BACCH_AUDIO_API void setSPEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate);

    BACCH_AUDIO_API void setLCexEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate);

	BACCH_AUDIO_API void setUBACCHEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate);

	BACCH_AUDIO_API int process_float_buf(baHandle mBA, float *in, float *out, int numsamples, int numchannelsin);

	BACCH_AUDIO_API int process_float_ptrs(baHandle mBA, float **in, float **out, int numsamples, int numchannelsin);

	BACCH_AUDIO_API int resample(baHandle mBA, int16 *in, int16 *out, int numsamples, int numchannelsin, int srIn, int *srOut);


void updateOrientation_BA(baHandle mBA, bacch_audio_orientation_t orientation);

bacch_audio_orientation_t getOrientation_BA(baHandle mBA);

void clear_BA(baHandle mBA);

void setEQEnable_BA(baHandle mBA, _bool enabled, _bool immediate);

int getState_BA(baHandle mBA);

int process_block_BA(baHandle mBA, bufHndl inbuffer, bufHndl outbuffer);

#ifdef BUF_WRAPPERS
// buffer class wrappers
bufHndl getBA_buffer(int numChannels, int lengthInSamples);
void delete_BA_buffer(bufHndl baBuf);
int getNumFrames_BA_buffer(bufHndl baBuf);
int readFramesFrom_BA_buffer(bufHndl baBuf, float** out, int numChannels, int numSamples);
int writeFramesTo_BA_buffer(bufHndl baBuf, float** in, int numChannels, int numSamples);
int write_s16Frames_To_f32buffer(bufHndl baBuf, int16_t** in, int numChannels, int numSamples);

int write_f32buffer_To_s16buffer(bufHndl f32in, bufHndl s16out, int numSamples);

bufHndl getBA_s16buffer(int numChannels, int lengthInSamples);
void delete_BA_s16buffer(bufHndl baBuf);
int getNumFrames_BA_s16buffer(bufHndl baBuf);
int readFramesFrom_BA_s16buffer(bufHndl buf, int16_t** out, int numChannels, int numSamples);
int writeFramesTo_BA_s16buffer(bufHndl buf, int16_t** in, int numChannels, int numSamples);

long BACCH_Audio_c_wrapper_getSize();


#endif
#ifdef __cplusplus
}
#endif


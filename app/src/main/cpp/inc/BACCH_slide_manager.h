#pragma once

#ifndef BACCH_SLIDE_MGR_H_
#define BACCH_SLIDE_MGR_H_

#include "BACCH_HP_constants.h"
#include "BACCH_GALAXY_TABS_constants.h"
#include "BACCH_types.h"
#include "BACCH_constants.h"
#include "BACCH_supporting.h"

// define as 0 for ramps, 1 for sin/cos curves
#define  USE_CURVES         0

#define  IR_INACTIVE        1000

// transition directionIsDown used as index to array
#define SLIDE_UP			false
#define SLIDE_DOWN			true

#define IR_LOW              0
#define IR_HIGH             1

#define RAMP_UP             0
#define RAMP_DOWN           1
#define RAMP_UP_DOWN		2

enum slide_module_t {
    SLIDE_HP = 0,
    SLIDE_SP,
    SLIDE_EQ,
    SLIDE_LAST
};

// in the event that the position referred to by slide_info aligns exactly with an IR
// lowIR will always reflect that IR (as will singleIR) and highIR will refer to the next higher IR (even if it is invalid)
class slide_info {
public:
    float   degrees = 0;
    float   degreesPerIR = 0;
    float   ir = 0;
    float   halfSpan = 0;      // half of span in degrees
    bool    onIR = false;
    bool    initialized = false;
    int     lowIR = 0;
    float   lowIRGain = 0;
    int     highIR = 0;
    float   highIRGain = 0;
    int     singleIR = 0;
	int		numIRs = 0;

    slide_info() noexcept;
    slide_info(int numIrs, float span, float degrees);
    void setInfo(slide_info *info);
    void setDegrees(float val);
	void setIR(float val);
};

class BACCH_slide_manager
{
public:
    BACCH_slide_manager(slide_module_t val, int sampleRateIndex);
    ~BACCH_slide_manager();

    // sets the currentDegrees to 0.0f and updates accordingly
    void reset();

    // this sets the target for the module to slide to
    // in degrees where negative indicates to the left
    void setTarget(float targetInDegrees, bool force);

    // step by one IR toward the target
    // returns:
    //   true if this step reaches target
    //   false if more steps required
    bool slide();

    float getCurrentDegrees();

    float getCurrentIR();

    float getTargetIR();

    bool  isDirty();

    bool  isInProgress();


    // will be used by BACCH_Filter to apply gain to outputting convolvers during slides
    float *slidingGain[3] = { 0 };

    // will be used by BACCH_Filter to apply gain to outputting convolvers when not sliding
    float staticGain[2] = { 0 };

    // a list of active IRs by Filter number
    int   activeIRs[41] = { 0 };

    // a list of IRs contributing to process' output
    int   outputIRs[4] = { 0 };

private:
    slide_info * currentIR;
    slide_info  * targetIR;

    bool			dirty = false;          // update required
    bool			inProgress = false;    // update in progress
    bool            directionIsDown = false;
    bool            targetReached = false;
    bool            isStatic = false;

	bool			isSP = false;

    int             numIRs = 0;       // number of Impulse Responses for associated module
    float           degreesPerIR[2] = { 0 };   // degrees per IR
    int             numPlusMinusIRs = 0; // number of IRs to process on either side of current IR(s)

    slide_module_t  module;

    // populated with ramps/curves for use in building the slide gain multipliers
    float           *mFade[3] = { 0 };

    void            setStaticMode();
    void            setActiveIRs(int lowIR, int irCnt);
    int buildRamps(slide_info *last, slide_info *next);
    void buildRamp(float *buf, float startVal, float endVal, int shape, bool zerosFirst = false, float zerosTo = 0.0f);

};

#endif // BACCH_SLIDE_MGR_H_
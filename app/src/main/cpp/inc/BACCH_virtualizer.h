#ifndef __BACCH_VIRT_INCLUDE__
#define __BACCH_VIRT_INCLUDE__

// Multichannel virtualizer convolution filter
// supports 4 to 8 channels in and 2 channels out

// the filter consists of separate head and tail processes
// separated in case a separate thread is required to close timing

// description:
// The head processor is responsible for processing the current input block to the current 
// output (minimal latency)
// The tail processor will process the 'tail' of the IR against current and previous input
//
// The virtualizer assumes input of 1024 sample frames though it can be modified to accept
// larger input sizes with the requirement that input be a power of 2
// The virtualizer processes up to 16 blocks in up to 16 stages (as required by the length of the IR)
// Completed in head processing:
// first stage: process first 1024 sample frame, accumulated with first frame in pad buffer 
// Completed in tail processing:
// subsequent stages: process first 1024 sample frame, accumulate with nth frame in pad buffer, starting at 2nd

#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_VIRT_constants.h"
#include "BACCH_VIRT_SHORT_constants.h"
#include "BACCH_library.h"

#define VIRT_MIN_CHANNELS_IN	4
#define VIRT_MAX_CHANNELS_IN	8
#define VIRT_NUM_CHANNELS_OUT	2

#define VIRT_MAX_FILTERNUM		1

class BACCH_virtualizer {

public:

	// Constructor
	// samplerate: audio samplerate index - see validateSampleRate in BACCH_supporting.cpp
    // numchannelsin: number of input channels, must be >=4 and <= 8
	// filterNum: which filter to load in the filter bank
    // rIR: true = include room impulse response
    // vMode: for 7.1 - DTS or Dolby
	BACCH_virtualizer(int samplerate, int numChannels, int filternum, bool rIR, virt_7_1_mode_t vMode);

	// Destructor
	~BACCH_virtualizer();

	// process a frame of audio: must be block length (1024 samples)
	void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	// configure the sp instance:
	// load a new filter, clears internal buffers
	// input definition same as constructor
	// will cause discontinuity in output
	void configure(int samplerate, int numChannels, int filterNum, bool rIR, virt_7_1_mode_t mode = VIRT_MODE_DTS);

    // set the irLength
    void setRIR(bool includeRIR);

	// set the irLength
	bool setMode(virt_7_1_mode_t mode);

	// Resets the internal state of the filter.
	// tail buffers are cleared.
	// This change happens immediately and will cause discontinuities in the output.
	void reset();

	// Clears the tail buffers.
	// This change happens immediately and might cause discontinuities in the
	// output.
	void clear();

private:
	BACCH_fft*              fft;

	// filter number for use with multiple room filters if available
	// currently only one room filter so is set to 0
	int						mFilterNum;

	// number of channels of input to the virtualizer, 4 or greater and interpreted as:
	// 4 = quadraphonic
	// 6 = 5.1 surround with L, R, C, LFE, Lsurround, Rsurround
	// 8 = 7.1 surround with L, R, C, LFE, Lsurround, Rsurround, Lrear, Rrear (in MODE_DTS) or
	//     7.1 surround with L, R, C, LFE, Lrear, Rrear, Lsurround, Rsurround (in MODE_DOLBY)
	int						mNumChannelsIn;
    int                     mNumCh2Proc;

    int                     mCh2IRMap[7];
    int                     mChMap[7];

	// see mNumChannelsIn description, only impacts 7.1 processing
	virt_7_1_mode_t			mMode;

    // use short IRs (headphone mode) or long IRs (speaker mode)
    bool                    mIncludeRIR;

    int                     mSampleRate;
	// index into filter arrays and enums for given sample rate
	int						mSampleRateIndex;

	// number of IR blocks given the sample rate
	int						mNumIRBlocks;

	BACCH_complex_buffer	***TFs;

	// buffers and scratch for first pass head processing, allows quick return for minimal latency
	BACCH_buffer			*mOut;
	BACCH_buffer			**mPad;
	int						*mPadIdx;

	BACCH_complex_buffer	*ScratchIn;
	BACCH_complex_buffer	**ScratchFFT;

	void FilterLoad(void);
	void convolve_head(const BACCH_complex_buffer *in, BACCH_complex_buffer ** const *tf, BACCH_buffer *out);
	void convolve_tail(const BACCH_complex_buffer *in, BACCH_complex_buffer ** const *tf, BACCH_buffer **out);

    void setChMap();
};

#endif //__BACCH_VIRT_INCLUDE__

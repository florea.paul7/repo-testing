// FFT wrapper for:
// Windows and MAC: IPP libraries - add __USE_IPP__ to C/C++ Preprocessor Definition
// Android: NE10 library - add __USE_NE10__ to Android.mk
// Not specified: Natural C FFT functionality

#ifndef COM_BACCH_MATH_H_
#define COM_BACCH_MATH_H_

#include "BACCH_types.h"
#include "BACCH_complex_buffer.h"

//#ifdef __USE_IPP__
//#undef __USE_IPP__
//#endif

#ifdef __USE_IPP__
#include "ipp.h"
#else // __USE_IPP__
#ifdef __USE_NE10__
#include "NE10_types.h"
#endif // __USE_NE10__
#endif // __USE_IPP__

#define kPI					3.141592653589793f
#define kPI_double			3.141592653589793

#define BL_FFT_FWD			0
#define BL_FFT_INV			1

//enum VECTOR_OPERATIONS_T {
//	ADD = 1,
//	SUB = 2,
//	MUL = 3,
//	MUL_ACCUM = 4,
//	DIV = 5
//};

class BACCH_fft{

public:

	// initialize the fft engine instance
	// FFT size is determined by BACCH_FFT_SIZE/BACCH_FFT_ORDER in BACCH_constants.h
    BACCH_fft() noexcept;
	BACCH_fft(int fftLength) noexcept;
    ~BACCH_fft();


	// all FFT operations act on BACCH_FFT_SIZE number of samples
	// Forward fft complex buffer
    void Forward(FFTComplex* data);
	// Inverse fft complex buffer
    void Inverse(FFTComplex* data);

	// Forward fft all channels of complex buffer
	void Forward(BACCH_complex_buffer *data);
	// Inverse fft all channels of complex buffer
	void Inverse(BACCH_complex_buffer *data);



private:

	void fft_Init(int fftLength);

#ifdef __USE_IPP__
	IppsFFTSpec_C_32fc*		ipp_plan;
	Ipp8u*					ipp_plan_mem;
	Ipp8u*					ipp_work_buffer;
#else // __USE_IPP__

#ifdef __USE_NE10__
	ne10_fft_cfg_float32_t cfg;
    ne10_fft_cpx_float32_t *scratchBuf;
#else // __USE_NE10

 	unsigned int	**reversetable; 

#endif // __USE_NE10__
#endif // __USE_IPP__

	int mFFTLength;

	void process(FFTComplex *data, int numsamples, int forward);
};

// vector math
void vector_add(float** a, float** b, float**dst, int numChannels, int numFrames);
void vector_add_i(float** a, float** srcdst, int numChannels, int numFrames);
void vector_add(BACCH_buffer* a, BACCH_buffer* b, BACCH_buffer* dst, int numChannels, int numFrames);
void vector_add_i(BACCH_buffer* a, BACCH_buffer* srcdst, int numChannels, int numFrames);
void vector_mul(float** a, float** b, float** dst, int numChannels, int numFrames);
void vector_mul_i(float** a, float** srcdst, int numChannels, int numFrames);
void vector_mul_c(const FFTComplex *a, const FFTComplex *b, FFTComplex* dst, int numFrames);
void vector_mul_i_c(const FFTComplex *a, FFTComplex* srcdst, int numFrames);
void vector_mul_accum_i_c(const FFTComplex *a, const FFTComplex *b, FFTComplex* srcdst, int numFrames);

// buffer math
void bufComplexMultiply(BACCH_complex_buffer* const a, BACCH_complex_buffer* const b, BACCH_complex_buffer* dst, int numChannels, int numFrames);


#endif  // COM_BACCH_MATH_H_

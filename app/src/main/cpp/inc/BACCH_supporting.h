#pragma once


#include "BACCH_types.h"
#include <string.h>

#ifdef _MSC_VER
#include <Windows.h>
#include <math.h>
#else
#include <stdint.h>
#endif


// definitions
//#define MAX_IR_GAIN									60	// +-6 = 18 dB
//#define OUTPUT_CHANNELS								2
//#define IR_FILE_MAX_CHANNELS						4
//#define MAX_IMPULSE_LENGTH_SECONDS					10
//#define MAX_IMPULSE_LENGTH							44100*MAX_IMPULSE_LENGTH_SECONDS		// approx 6 million taps
//#define MIN_IMPULSE_LENGTH							22050
//#define MAX_CHARS_TO_PRINT 							52
//#define MIN_IR_LENGTH_BACCH							512
//#define MAX_BACCH_192000							131072
//#define MAX_BACCH_96000								65536
//#define MAX_BACCH_48000								32768
//#define MAX_NUM_IR									100
// for 1.008 only
//#define MIN_BACCH_SLIDE_RATE_HZ						30

#if 0 //ndef _MSC_VER
// math
float log2(float x);
double log2(double x);
float log4(float x);
double log4(double x);
float log10(float x);
float round(float x);
double round(double x);

float floor(float x);
float ceil(float x);
float fabsf(float x);
float pow(float x, float y);

float cos(float x);
float sin(float x);

template <typename T> inline void swap_tpl(T& a, T& b)
{
    T temp = a;
    a = b;
    b = temp;
}
template <typename T> inline T round(T a)
{
    return floor(a + 0.5);
}
template <typename T> inline T log2(T a)
{
    return log10(a) / log10(2.0);
}

#endif



#ifdef MULTI_THREAD

class crit_section {
private:
#ifdef _MSC_VER
	CRITICAL_SECTION		csMutex;
#else
	std::mutex				csMutex;
#endif

public:
	crit_section();
	~crit_section();
	void Enter(void);
	void Leave(void);
};
#endif // MULTI_THREAD
// have to move to separate class file

// functions... global scope

int validateSampleRate(int samplerate);

#ifdef _MSC_VER
void mThrow(std::string msg);
#else
void mThrow(char *msg);
#endif




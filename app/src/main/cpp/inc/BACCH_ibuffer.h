#pragma once

#include "BACCH_types.h"
#include "BACCH_constants.h"
#include "stdbool.h"

class IBuffer_t {
public:
    int16_t 	*s16;
    int32_t	readIndx;
    int32_t 	writeIndx;
    int32_t	framesCnt;
    int32_t	framesCapacity;
    int32_t    numChannels;

    IBuffer_t(int32_t numChannels, int32_t capacityInFrames);

    ~IBuffer_t();

    void clear();

    int32_t readBuf(int16_t *dest, int32_t numFrames, bool accum);

    int32_t writeBuf(int16_t *src, int32_t numFrames);

    // if reading/writing data directly to internal buffer, be sure to subsequently call updateBuffer
    int16_t *writePtr();

    int16_t *readPtr();

    status_t updateBuffer(int32_t numFrames, buffer_op_t read);

    int getNumFrames();
};

//
//#ifdef __cplusplus
//extern "C" {
//#endif
//
//    iBuffer_t *new_iBuffer(uint32_t capacityInFrames, uint32_t numChannels);
//
//    void delete_iBuffer(iBuffer_t *ibuf);
//
//    uint32_t readBuf(iBuffer_t *me, int16_t *dest, uint32_t numFrames, bool accum);
//
//    uint32_t writeBuf(iBuffer_t* me, int16_t *src, uint32_t numFrames);
//
//    // if reading/writing data directly to internal buffer, be sure to subsequently call updateBuffer
//    int16_t *writePtr(iBuffer_t *me);
//
//    int16_t *readPtr(iBuffer_t *me);
//
//    void updateBuffer(iBuffer_t *me, int numFrames, bool read);
//
//    int getNumFrames(iBuffer_t *me);
//
//#ifdef __cplusplus
//}
//#endif
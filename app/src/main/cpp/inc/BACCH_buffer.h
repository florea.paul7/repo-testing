// buffer is generic buffer for use with BACCH filter

#ifndef BACCH_BUFFER_H_
#define BACCH_BUFFER_H_

#include "BACCH_types.h"
#include "BACCH_constants.h"

class BACCH_buffer {

public:
	// Constructor.
	BACCH_buffer();
    // numchannels - number of channels in buffer
	// length - length of each channel in samples
	BACCH_buffer(SInt32 numchannels, SInt32 length);

	BACCH_buffer(SInt32 numchannels, SInt32 length, float *memPtr);

	BACCH_buffer(SInt32 numchannels, SInt32 length, float **block_ptrs);

	~BACCH_buffer();
    
	// resets the buffer with empty buffers of numchannels and size length
	void reset(SInt32 numchannels, SInt32 length);

    // reset the buffer to empty, no change in channels or length
    void reset();

	// empties the buffers
	void clear();

    // move data to beginning of buffer memory
    void reAlign(void);

    int getNumSamplesInBuffer();

   // public access to raw data buffers
	float *chunk;
    float *data[16] = { 0 };
	int mNumChannels;
	int mBufferLen;
	int mReadIndex;
	int mWriteIndex;
	int mNumSamplesInBuffer;

	bool externalMem;
	bool chunkValid;
	
};


#endif  // BACCH_BUFFER_H_

#ifndef __BACCH_COMPRESSOR_INCLUDE__
#define __BACCH_COMPRESSOR_INCLUDE__

// Wrapper for Compressor generated by Matlab Coder

#include "BACCH_buffer.h"
#include <stdlib.h>

//==============================================================================
/**
*/
class BACCH_compressor
{
public:
    //==============================================================================
    // Constructor
    // samplerate: audio samplerate
    // preset: which preset configuration to use
    BACCH_compressor(int samplerate);

    // Destructor
    ~BACCH_compressor();

    // process a frame of audio
    void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, bool isHP);

    // configure the compressor instance:
    // load a new filter, clears internal buffers
    // input definition same as constructor
    // will cause discontinuity in output
    void configure(int samplerate);

    // Resets the internal state of the filter.
    // tail buffers are cleared.
    // This change happens immediately and will cause discontinuities in the output.
    void reset();

    // Clears the tail buffers.
    // This change happens immediately and might cause discontinuities in the
    // output.
    void clear(); 

private:

    int                     mSampleRate;

    float                   *inChunk;
    float                   *tmpChunk;
    float                   *outChunk;

};

#endif //__BACCH_COMPRESSOR_INCLUDE__

#pragma once
#ifndef __BACCH_CX_INCLUDE__
#define __BACCH_CX_INCLUDE__

// TRUE STEREO CONVOLUTION FILTER
// supports only 2 channels in and 2 channels out

#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"

class BACCH_cx {

public:

	// Constructor
	// samplerate: audio samplerate (determines filter samplerate)
	// filterNum: which filter to load in the filter bank
	BACCH_cx(int samplerate);

	// Destructor
	~BACCH_cx();

	// process a frame of audio - must be block length (1024 samples)
	void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, BACCH_buffer* coutbuffer);

	// configure the cx instance:
	// clears internal buffers, regens necessary buffers
	// input definition same as constructor
	// will cause discontinuity in output
	void configure(int samplerate);


	// Resets the internal state of the filter.
	// tail buffers are cleared state becomes disabled.
	// This change happens immediately and will cause discontinuities in the output.
	void reset();

	// Clears the tail buffers.
	// This change happens immediately and might cause discontinuities in the
	// output.
	void clear();


private:
	BACCH_fft				*fft;

	int						mSampleRate;
	int						mSampleRateIdx;

	int						mBlockSize;

	BACCH_complex_buffer	**ScratchIn;
	BACCH_complex_buffer	**cScratchFFT;
	BACCH_complex_buffer	**ScratchFFT;
	BACCH_complex_buffer	**Intermediate;

	BACCH_buffer			**Input;
	BACCH_buffer			*Window;
	BACCH_buffer			**Scratch;
	BACCH_buffer			**ScratchOut;

	BACCH_buffer			*centerHoldBuf;

	void mltwinMod(float *win, int length);
	void getCX(BACCH_complex_buffer *in, BACCH_complex_buffer *out, int numsamples);

};

#endif  // BACCH_CX_INCLUDE
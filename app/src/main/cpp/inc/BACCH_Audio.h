/*
 * BACCHFilter.h
 *
 *  Created on: September 19, 2017
 *      Author: Sam Caldwell
 */

#ifndef BACCH_AUDIO_H_
#define BACCH_AUDIO_H_

#include "BACCH_equalizer.h"
#include "BACCH_EQ_constants.h"
#include "BACCH_hp.h"
#include "BACCH_HP_constants.h"
#include "BACCH_hp_xtc.h"
#include "BACCH_HP_XTC_constants.h"
#include "BACCH_sp.h"
#include "BACCH_GALAXY_TABS_constants.h"
#include "BACCH_LCRex.h"
#include "BACCH_types.h"
#include "BACCH_compressor.h"
#include "BACCH_constants.h"
#include "BACCH_buffer.h"
#include "BACCH_library.h"
#include "BACCH_supporting.h"
#include "BACCH_slide_manager.h"
#include "BACCH_virtualizer.h"

//#include <functional>
//#include <atomic>

//#define _FDEBUG
#ifdef _FDEBUG
#include <stdio.h>
#endif

//
//#define BACCH_AUDIO_NUM_PRESETS                 3
//#define BACCH_AUDIO_MAX_NUM_SEQUENCED           10
//const int BACCH_AUDIO_PRESETS[BACCH_AUDIO_NUM_PRESETS][BACCH_AUDIO_MAX_NUM_SEQUENCED] = {
//    { 3, BACCH_FEATURE_SP | BACCH_HEADTRACKING_OFF, BACCH_FEATURE_EQ, BACCH_FEATURE_COMPRESSOR },
//    { 2, BACCH_FEATURE_SP | BACCH_HEADTRACKING_OFF, BACCH_FEATURE_EQ },
//    { 2,  BACCH_FEATURE_EQ, BACCH_FEATURE_SP | BACCH_HEADTRACKING_ON },
//};
//
//#define CALL_MEMBER_FN(object, ptrToMember) ((object).*(ptrToMember))


class BACCH_Audio {

public:

    // Constructor.  Only supports 2 channel output
    // channelsIn    Number of input channels: min 2 max 8
    // sampleRate   Sample rate, in Hz.
    BACCH_Audio(int sampleRate, int channelsIn, bacch_sp_orientation_t orientation, const char* sp_id);
	~BACCH_Audio();

    // Reconfiguration of the filter. Implies clear().
    // channelsIn    Number of input channels
    // sampleRate   Sample rate, in Hz.
    void configure(int sampleRate, int channelsIn, bacch_sp_orientation_t orientation);

	void configure_LCRex(int sampleRate, float width, float gain);

	bool configure_UBACCH(float f1, float f2, bool b1);

	void configure_VIRT_Mode(virt_7_1_mode_t mode);

    // changes speaker processing to support the current device orientation
    // landscape & landscape reverse
    void updateOrientation(bacch_sp_orientation_t orientation);
    bacch_sp_orientation_t getOrientation();

    // Resets the internal state of the filter.
    // Heading is set to zero (straight ahead), tail buffers are cleared,
    // state becomes disabled.
    // This change happens immediately and might cause discontinuities in the output.
    void reset();

    // Clears the tail buffers.
    // This change happens immediately and might cause discontinuities in the
    // output.
    void clear();

	void setEnabled(bool enabled , bool isHP, bool immediate);

    // Set headphone filter state.
    // immediate    If true, transitions to new state smoothly, without
    //              introducing discontinuities in the output. Otherwise,
    //              transitions immediately.
    void setHPEnable(bool enabled, bool isUser, bool immediate = false);

    // Set speaker filter state.
    // immediate    If true, transitions to new state smoothly, without
    //              introducing discontinuities in the output. Otherwise,
    //              transitions immediately.
    void setSPEnable(bool enabled, bool isUser, bool immediate = false);

	void setLCexEnable(bool enabled, bool immediate = false);

    bool getLCexEnable();

    // Enables (activates) the equalizer.
    // immediate    If true, transitions to new state smoothly, without
    //              introducing discontinuities in the output. Otherwise,
    //              transitions immediately.
    void setEQEnable(bool enabled, bool immediate = false);

    bool getEQEnable();

    int getState();

    int process_array(float *inbuffer, float *outbuffer, int numsamples, int numchannels);

	int process_block(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

    // for debug - do not release with these functions included
    void test_deinterleave(const int16_t *intrlvd_in, float** out, float gain, int numChannels, int numSamples);
    void test_interleave(float** data, int16_t* out, float postGain, int numChannelsOut, int numSampleFrames);

private:
#ifdef _FDEBUG
	FILE *dfile;
#endif
    int processHP(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

    int processSP(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	int processXTC(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

    int processEQ(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	int processCex(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, BACCH_buffer* coutbuffer);

    int processComp(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, bool isHP);

    int processVirt(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

    // BACCH headphone instance
    BACCH_hp		*bhp;
	float			*hpStateTransition[2] = { 0 };		// per sample output multiplier ptr for state transition

    // BACCH speaker instance
    BACCH_sp        *bsp;
	float			*spStateTransition[2] = { 0 };		// per sample output multiplier ptr for state transition

    // BACCH equalizer instance
    BACCH_equalizer *beq[EQ_NUM_IRS] = { 0 };
    float			*eqStateTransition[2] = { 0 };		// per sample output multiplier ptr for state transition

	// BACCH cross talk cancellation for headphones
	BACCH_hp_xtc	*bxtc = { 0 };

    // BACCH Center Channel Extraction instance
	BACCH_LCRex		*bcex;

	// BACCH compressor for Galaxy S
	BACCH_compressor	*bcomp;

	BACCH_virtualizer   *bv;

	BACCH_buffer*	scratchFade = { 0 };				// storage for IR slide fader gain curves

    // sample rate of audio
	int	            mSampleRate;

	// index used to determine number of IR blocks given sample rate
	int				mSampleRateIndex;

	int 			mNumChannelsIn;

    bacch_sp_orientation_t    mOrientation;

    virt_7_1_mode_t       mVirtMode;

	float			mCGain;
	float			mCWidth;
	BACCH_buffer	*mCoutBuffer;

    // These filter IDs are intended to support IR sets for different devices
    // they have nothing to do with the IR sliding mechanism
    int             mEQFilterNum;

    BACCH_slide_manager *eqSlide = { 0 };

	// debug info
	int				blockNum;

	// table lookup used for smooth transition between states
	int				mNumBlocksToTransition;
	BACCH_buffer	*mTransitionFade = { 0 };

	// transition support
	int				bhpBlocksInTransition;
	int				bhpOutputGoodCnt;
	int				bspBlocksInTransition;
	int				bspOutputGoodCnt;
	int				blcBlocksInTransition;
	int				blcOutputGoodCnt;
    int				beqBlocksInTransition;
    int				beqOutputGoodCnt;

    // These buffers can be optimized to minimize memory consumption if necessary
	// buffer of convolution input buffer objects
    BACCH_buffer*	beq_parallel_ins[EQ_NUM_IRS] = { 0 };
    BACCH_buffer*	beq_parallel_outs[EQ_NUM_IRS] = { 0 };

    BACCH_buffer*	processBuf = { 0 };

	BACCH_buffer	*inFBuf = { 0 };
	BACCH_buffer	*tmpFBuf = { 0 };
	BACCH_buffer	*outFBuf = { 0 };

	BACCH_buffer    *virtBuf;

    // Current states
    state_t bhpState;
    state_t bspState;
	state_t lastHPState;
	state_t lastSPState;
	state_t blcState;
    state_t beqState;

    float   mRequestedVolume[NUM_OUT_CHANNELS] = { 0 };

	void setBHPStateTransition();	// set BACCH hp mode transition gain
	void setBSPStateTransition();	// set BACCH sp mode transition gain
	void setStateTransition(state_t state, int blocksInTransition, float** fadeArray);
};

#endif /* COM_BACCH_FILTER_H_ */

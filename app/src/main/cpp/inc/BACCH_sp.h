#ifndef __BACCH_SP_INCLUDE__
#define __BACCH_SP_INCLUDE__

// TRUE STEREO CONVOLUTION FILTER
// supports only 2 channels in and 2 channels out

#include <cstdio>
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"

#include "BACCH_GALAXY_TABS_constants.h"
#define SP_DEGREES_PER_IR	(float)SP_SPAN / (float)(GALAXY_TABS_NUM_IRS-1)

#define SP_NUM_IR_SETS          1

typedef enum {
    BACCH_SP_ORIENTATION_INVALID = -1,
    BACCH_SP_ORIENTATION_LANDSCAPE,
    BACCH_SP_ORIENTATION_LANDSCAPE_REVERSE,
    BACCH_SP_ORIENTATION_CNT,
    BACCH_SP_ORIENTATION_PORTRAIT,
    BACCH_SP_ORIENTATION_PORTRAIT_REVERSE,
    BACCH_SP_ORIENTATION_LAST = BACCH_SP_ORIENTATION_CNT - 1
} bacch_sp_orientation_t;

enum ir_direction_t {
	SP_L_TO_L	= 0,
	SP_L_TO_R,
	SP_R_TO_L,
	SP_R_TO_R,
};

class BACCH_sp {

public:

	// Constructor
	// samplerate: audio samplerate (determines filter samplerate)
	// filterNum: which filter to load in the filter bank
	BACCH_sp(int samplerate, bacch_sp_orientation_t orientation);

	// Destructor
	~BACCH_sp();

	// process a frame of audio - must be block length (1024 samples)
	void process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer);

	// configure the sp instance:
	// load a new filter, clears internal buffers
	// input definition same as constructor
	// will cause discontinuity in output
	void configure(int samplerate, bacch_sp_orientation_t orientation);

    // update sp processing to accomodate device orientation change
    void updateOrientation(bacch_sp_orientation_t orientation);

	// Resets the internal state of the filter.
	// tail buffers are cleared state becomes disabled.
	// This change happens immediately and will cause discontinuities in the output.
	void reset();

	// Clears the tail buffers.
	// This change happens immediately and might cause discontinuities in the
	// output.
	void clear();

	BACCH_buffer			**mPad;
	int						mPadIdx[8] = { 0 };

private:
	BACCH_fft				*fft;

//#define _SPFDEBUG
#ifdef _SPFDEBUG
	FILE *spfile;
#endif

	int						mFilterNum;
	int						mSampleRate;
	int						mSampleRateIdx;
	int						mNumIRBlocks;
	int						mIRLength;

    bacch_sp_orientation_t  mOrientation;
    bool                    mHTEnabled;
    
	// complex buffers are setup with the following arrangement
	// TFs[a][b]
	// a - 1 for each input channel, aka 2 for stereo
	// b - fft block number - aka head TF or tail TF(s)
	BACCH_complex_buffer	***TFs;

	BACCH_complex_buffer	*ScratchIn;
	BACCH_complex_buffer	**ScratchFFT;

	void FilterLoad(void);

};

#endif //__BACCH_SP_INCLUDE__

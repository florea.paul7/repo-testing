//
// Created by Samuel Caldwell on 1/2/2019.
//

#ifndef BACCH_AUDIO_BACCHAUDIO_H
#define BACCH_AUDIO_BACCHAUDIO_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

// ********************* Java Native Interface ***************************

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_createEngine
        (JNIEnv *env, jclass obj,
         jint sampleRate,
         jint channelsIn,
         jint orientation);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_destroyEngine
        (JNIEnv *env, jclass obj);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configVirt
        ( JNIEnv *env, jclass obj,
          jboolean isDolby);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configBA
        ( JNIEnv *env, jclass obj,
          jint sampleRate,
          jint channelsIn,
          jint orientation);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configUBACCH
        ( JNIEnv *env, jclass obj,
          jfloat distance_speaker_to_speaker,
          jfloat distance_listener_to_speaker,
          jboolean isMetricUnits);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_reset
        ( JNIEnv *env, jclass obj);

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_enable
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isHP,
          jboolean immediate);

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableHP
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate);

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableSP
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate);

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableLCex
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate);

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableUBACCH
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate);

JNIEXPORT jint JNICALL Java_bacch_audio_BACCHAudio_process
        ( JNIEnv *env, jclass obj,
          jobject in_buf,
          jobject out_buf,
          jint numsamples,
          jint numchannelsin);

JNIEXPORT jint JNICALL Java_bacch_audio_BACCHAudio_resample
        ( JNIEnv *env, jclass obj,
          jobject in_buf,
          jobject out_buf,
          jint numsamples,
          jint numchannelsin,
          jint srIn,
          jint srOut);

#ifdef __cplusplus
}
#endif

#endif //BACCH_AUDIO_BACCHAUDIO_H

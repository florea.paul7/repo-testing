// buffer is generic buffer for use with BACCH filter

#ifndef BACCH_COMPLEX_BUFFER_H_
#define BACCH_COMPLEX_BUFFER_H_

#include "BACCH_types.h"
#include "BACCH_buffer.h"

class BACCH_complex_buffer {

public:
	// Constructor.
	// numChannels - number of channels to support
	// length - length of each channel in samples, must be power of 2
	BACCH_complex_buffer(SInt32 numchannels, SInt32 length);

	~BACCH_complex_buffer();

	// resets the buffer with empty buffers of size length
	void reset(SInt32 numchannels, SInt32 length);

    void reset();

	// empties the buffers
	void clear();

    // move data to front and update indexes
    void reAlign();
        
    FFTComplex *data[16] = { 0 };

	float		*chunk;

	int mNumChannels;
    int mBufferLen;
	int mNumSamplesInBuffer;
	int mReadIndex;
	int mWriteIndex;


};


#endif  // BACCH_COMPLEX_BUFFER_H_

/*
 * bacch_types.h
 *
 *  Created on: Jun 1, 2012
 *      Author: srcald34
 */

#ifndef TYPES_H_
#define TYPES_H_

#include "BACCH_constants.h"
#ifndef _MSC_VER
#include <stdint.h>
#endif

#ifndef NULL
#define NULL 0x0000
#endif


typedef unsigned char		uint8;
typedef unsigned short 		uint16;
//typedef unsigned int 		uint32;
typedef unsigned long long	uint64;
//typedef char				int8;
typedef short				int16;
//typedef int				int32;
typedef long long	        int64;
typedef float				float32;
typedef double				float64;

#ifndef uint8_t
typedef unsigned char		uint8_t;
#endif
#ifndef uint16_t
typedef unsigned short 		uint16_t;
#endif
#ifndef uint32_t
typedef unsigned int 		uint32_t;
#endif

#ifndef int16_t
typedef short				int16_t;
#endif

#ifndef int32_t
typedef signed int			int32_t;
#endif

#ifndef UInt32
typedef unsigned int		UInt32;
#endif

#ifndef SInt32
typedef signed int			SInt32;
#endif

#ifdef _MSC_VER
#ifndef int64_t
typedef long long           int64_t;
#endif
#endif

#ifndef _MSC_VER
#ifndef true
#define true    			1
#endif

#ifndef false
#define false   			0
#endif
#endif // _MSC_VER

#ifdef _MSC_VER
#define do_inline __inline
#define CRIT_SECT			CRITICAL_SECTION
#define _HANDLE				HANDLE
#else
#define do_inline inline
#define CRIT_SECT			void * //std::mutex
#define _HANDLE				void*
#endif

#define JNI_FALSE			0
#define JNI_TRUE			1

typedef float FFTSample;

typedef struct FFTComplex {
	FFTSample re, im;
} FFTComplex;

// union types used by interleave and deinterleave
// uab4x4_t usefule for 2 & 4 channel interleave and deinterleave
typedef union {
    int64_t wide;
    struct {
        int16_t a1;
        int16_t b1;
        int16_t a2;
        int16_t b2;
    } narrow;
} ab2x2_t;

typedef union {
    int64_t wide;
    struct {
        int16_t a1;
        int16_t b1;
        int16_t c1;
        int16_t d1;
    } narrow;
} ab1x4_t;

typedef union {
    int64_t wide[3];
    struct {
        int16_t a1;
        int16_t b1;
        int16_t c1;
        int16_t d1;
        int16_t e1;
        int16_t f1;
        int16_t a2;
        int16_t b2;
        int16_t c2;
        int16_t d2;
        int16_t e2;
        int16_t f2;
    } narrow;
} ab2x6_t;

typedef union {
    int64_t wide[2];
    struct {
        int16_t a1;
        int16_t b1;
        int16_t c1;
        int16_t d1;
        int16_t e1;
        int16_t f1;
        int16_t g1;
        int16_t h1;
    } narrow;
} ab1x8_t;

// in terms of blocks, will have 21 of these and another 
typedef union {
    int64_t wide[3];
    struct {
        int16_t a1;
        int16_t b1;
        int16_t c1;
        int16_t a2;
        int16_t b2;
        int16_t c2;
        int16_t a3;
        int16_t b3;
        int16_t c3;
        int16_t a4;
        int16_t b4;
        int16_t c4;
    } narrow;
} abc3x4_t;

#endif /* TYPES_H_ */

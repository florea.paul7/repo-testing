#ifndef BACCH_BIQUAD_H_
#define BACCH_BIQUAD_H_

typedef double coeff;

typedef struct sFilter {
    coeff a0;
    coeff a1;
    coeff a2;
    coeff b0;
    coeff b1;
    coeff b2;
    coeff *yN_1;
    coeff *yN_2;
}sFilter_t;

typedef struct Biquad_t {
	sFilter_t bq;
	//double b0, b1, b2;
	//double a0, a1, a2;
	double wNMinus2;
	double wNMinus1;
	struct Biquad_t* next;
}Biquad_t;


class BACCH_biquad {
private:
    sFilter_t   mBQ;
    int         mNumChannels;
    int         mSampleRate;

    BACCH_biquad(int samplerate, int numChannels, int filternum);
    ~BACCH_biquad();

    void reset();

    void process(float** in, float** out, int numSamples);
};



#endif  // BACCH_BIQUAD_H

#pragma once
#include "BACCH_types.h"

#ifdef _MSC_VER
#include <Windows.h>
#endif

// memory allocation
void* blMalloc(SInt32 elementSize, SInt32 length, SInt32 alignment);
void blFree(void*);


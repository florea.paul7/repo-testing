#pragma once

#include "BACCH_types.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_ibuffer.h"

#ifdef _cplusplus
extern "C" {
#endif

// only needs to be run once, no issue if run multiple times though
void init_buffer_ops();

void delete_buffer_ops();

/************* BUFFER OPERATIONS ********************************/

// reset dst to empty without erasing data
void bufReset(BACCH_buffer *dst);

// clear all data in dst and reset to empty
void bufClear(BACCH_buffer *dst);

// copy data from src to dst, consuming data in src
void bufCopy(BACCH_buffer *src, BACCH_buffer *dst, int numSamples);

// copy data from src float array to dst
void bufBlockToBuf(float** src, BACCH_buffer* dst, int numChannels, int numSamples);

// copy src to dst float array, consuming data in src
void bufBufToBlock(BACCH_buffer* src, float** dst, int numChannels, int numSamples);

// clone src to dst overwriting dst but not consuming src
void bufDuplicate(BACCH_buffer *src, BACCH_buffer *dst, int numSamples);

// add src to srcdst, consuming data in src
void bufAdd_I(BACCH_buffer *src, BACCH_buffer *srcdst, int numSamples);

// multiply src and srcdst, consuming data in src
void bufMul_I(BACCH_buffer *src, BACCH_buffer *srcdst, int numSamples);

// interleave from BACCH_buffer to BACCH_ibuffer, consumes in, increments out by numsamples
int bufInterleave(BACCH_buffer* in, IBuffer_t* intrlvd_out, float gain, int numChannels, int numSamples, bool accum);

int bufDeinterleave(const int16_t* intrlvd_in, BACCH_buffer* out, float gain, int numChannels, int numSamples);

/***************** COMPLEX BUFFER OPERATIONS ***************************/


int cbufCopy(BACCH_complex_buffer *src, BACCH_complex_buffer *dst, int numchannels, int numSamples);

// write interleaved data 'in' to buffer
// imaginary portion of buffer is set to 0
int cbufIntrlvdToCplx(float *intrlvd_src, BACCH_complex_buffer *dst, int numchannels, int numSamples);

// put real data from buffer to interleaved 'out'
int cbufCplxToIntrlvd(BACCH_complex_buffer *src, float *intrlvd_dst, int numchannels, int numSamples);

// write block data 'in' to the buffer
// imaginary portion of buffer is set to 0
int cbufRealtoCplx(BACCH_buffer* src, BACCH_complex_buffer *dst, int numchannels, int numSamples);

// put real portion of buffer data  to block 'out'
int cbufCplxtoReal(BACCH_complex_buffer *src, BACCH_buffer* out, int numchannels, int copyLen, bool accum);


/************* VECTOR INIT FUNCTIONS ********************************/

// clear the vector, set length values to 0.0f
void vClear(float* dst, int length);

// set the first numSamples of vector to val
int vSet(float* dst, float val, int numSamples);

// set first numSamples of dst vector to src vector, can overlap
int vCopy(float* src, float* dst, int numSamples);

// add src1 and src2 with output to dst
int vAdd(float* src1, float* src2, float* dst, int numSamples);

// add src1 and src2 with output to dst
int vAdd_I(float* src1, float* srcdst, int numSamples);

/************* VECTOR MANIPULATION FUNCTIONS *************************/

// add product of src1 and Constant to dst
int vAddProductC(float* src1, float c, float* dst, int numSamples);

// add product of src1 and src2 to dst
int vAddProduct(float* src1, float *src2, float* dst, int numSamples);

// dst = product of src1 and src2
int vMul(float *src1, float *src2, float* dst, int numSamples);

// srcdst = product of src1 and srcdst
int vMul_I(float *src1, float *srcdst, int numSamples);

// dst = product of src1 and constant
int vMulC(float *src1, float c, float* dst, int numSamples);

// srcdst = product of srcdst and constant
int vMulC_I(float *srcdst, float c, int numSamples);

// put real values from complex array to float array
int vComplexToReal(FFTComplex* in, float* out, int numSamples, bool accum);

// get float array to real values in complex array
int vRealToComplex(float* in, FFTComplex* out, int numSamples);

// convert multichannel float array to interleaved shorts
int vBlockToInterleaved_32f_16s(float **in, int16_t *intrlvd_out, float gain, int numChannels, int numSamples, bool accum);

// convert multichannel interleaved shorts to float array
int vIntrlvdToBlock_16s_32f(const int16_t* intrlvd_in, float** out, float gain, int numChannels, int numSamples);


#ifdef _CPLUS_PLUS
}
#endif

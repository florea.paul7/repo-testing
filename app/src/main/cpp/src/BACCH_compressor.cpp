#include "BACCH_compressor.h"
#include "BACCH_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_mem.h"
#include "limitMe.h"
#include "hpCompress.h"
#include "spkCompress.h"


BACCH_compressor::BACCH_compressor(int sampleRate)
{
    int i = 0, j = 0;
    float pregain = 13.0f;

    int sampleRateIndex = validateSampleRate(sampleRate);
    int yout[2] = {0};

    mSampleRate = sampleRate;

    limitMe_init();
    hpCompress_init();
    spkCompress_init();


    inChunk = (float*)blMalloc(sizeof(float), 4096, 64);
    tmpChunk = (float*)blMalloc(sizeof(float), 4096, 64);
	outChunk = (float*)blMalloc(sizeof(float), 4096, 64);

	vClear(inChunk, 4096);
	vClear(outChunk, 4096);

    hpCompress(inChunk, pregain, (float)mSampleRate, outChunk, yout);

    vClear(inChunk, 4096);

	limitMe(inChunk, (float)mSampleRate, outChunk, yout);

	//vClear(inChunk, 4096);

	spkCompress(inChunk, (float)mSampleRate, outChunk, yout);


	//vClear(inChunk, 4096);



}

void BACCH_compressor::configure(int samplerate)
{
	int samplerateIndex = validateSampleRate(samplerate);

    mSampleRate = samplerate;
	//runbefore_not_empty_init();
}

void BACCH_compressor::reset()
{
	clear();
}

void BACCH_compressor::clear()
{
    // do nothing
}

BACCH_compressor::~BACCH_compressor()
{
    //S9crossfs_free();
    blFree(inChunk);
    blFree(outChunk);
}

void BACCH_compressor::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, bool isHP) {
    status_t mStatus = STATUS_OK;
    int yout[2] = {1024, 2};
    int cExpects = BACCH_FRAME_SIZE;
    float pregain = 13;

    vCopy(inbuffer->data[0], inChunk, cExpects);
    vCopy(inbuffer->data[1], &inChunk[cExpects], cExpects);
    if (isHP) {
        hpCompress(inChunk, (float)pregain, (float)mSampleRate, outChunk, yout);
    }
    else {
        spkCompress(inChunk, (float)mSampleRate, tmpChunk, yout);
        limitMe(tmpChunk,(float) mSampleRate, outChunk, yout);
    }

    vCopy(outChunk, outbuffer->data[0], cExpects);
    vCopy(&outChunk[cExpects], outbuffer->data[1], cExpects);

        /******************** SPECIAL CASE BUFFER MGMT ***********************/
    // process chain above does not manipulate the inbuffer or outbuffer buffer metrics
    inbuffer->reset();
    outbuffer->mWriteIndex = outbuffer->mNumSamplesInBuffer = BACCH_FRAME_SIZE;

    return;
}


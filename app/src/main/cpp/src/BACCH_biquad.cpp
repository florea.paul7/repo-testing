#include "stdafx.h"

/*
 ============================================================================
 Name        : Test.c
 Author      : Balin
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "BACCH_types.h"
#include "BACCH_constants.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"
#include "BACCH_biquad.h"
#include "BACCH_biquad_coeffs.h"


BACCH_biquad::BACCH_biquad(int samplerate, int numChannels, int filternum) {

    int samplerateIdx = validateSampleRate(samplerate);
    if (samplerateIdx > 0) {
        mSampleRate = samplerate;
        mNumChannels = numChannels;
        mBQ.a0 = 1.0; 
        mBQ.a1 = bq_coeffs[samplerateIdx][filternum][0];
        mBQ.a2 = bq_coeffs[samplerateIdx][filternum][1];
        mBQ.b0 = bq_coeffs[samplerateIdx][filternum][2];
        mBQ.b1 = bq_coeffs[samplerateIdx][filternum][3];
        mBQ.b2 = bq_coeffs[samplerateIdx][filternum][4];

        mBQ.yN_1 = (double*)blMalloc(sizeof(double), numChannels, 64);
        mBQ.yN_2 = (double*)blMalloc(sizeof(double), numChannels, 64);
        for (int i = 0; i < numChannels; i++) {
            mBQ.yN_1[i] = 0.0;
            mBQ.yN_2[i] = 0.0;
        }
    }
}

BACCH_biquad::~BACCH_biquad() {

}

void BACCH_biquad::reset() {
    for (int i = 0; i < mNumChannels; i++) {
        mBQ.yN_1[i] = 0.0;
        mBQ.yN_2[i] = 0.0;
    }
}

// currently setup to be efficient for numChannels = 2
void BACCH_biquad::process(float** in, float** out, int numSamples) {

    float *inPtr[MAX_IN_CHANNELS];
    float *outPtr[MAX_IN_CHANNELS];
    int   cnt = numSamples;
 

    for (int i = 0; i < mNumChannels; i++) {
        inPtr[i] = in[i];
        outPtr[i] = out[i];
    }

    double wN0, wN1;

    if (mNumChannels == 2) {
        for (int i=0;i<numSamples;i++) {
            wN0 = inPtr[0][i] - (mBQ.a1 * mBQ.yN_1[0]) - (mBQ.a2 * mBQ.yN_2[0]);
            wN1 = inPtr[1][i] - (mBQ.a1 * mBQ.yN_1[1]) - (mBQ.a2 * mBQ.yN_2[1]);
            outPtr[0][i] = (float)((mBQ.b0 * wN0) + (mBQ.b1 * mBQ.yN_1[0]) + (mBQ.b2 * mBQ.yN_2[0]));
            outPtr[1][i] = (float)((mBQ.b0 * wN1) + (mBQ.b1 * mBQ.yN_1[1]) + (mBQ.b2 * mBQ.yN_2[1]));
            mBQ.yN_2[0] = mBQ.yN_1[0]; 
            mBQ.yN_2[1] = mBQ.yN_1[1];
            mBQ.yN_1[0] = wN0;
            mBQ.yN_1[1] = wN1;
        }
    }
    else {
        for (int i = 0; i < mNumChannels; i++) {
            cnt = numSamples;
            while (cnt--) {
                wN0 = *inPtr[i]++ - (mBQ.a1 * mBQ.yN_1[i]) - (mBQ.a2 * mBQ.yN_2[i]);
                *outPtr[i]++ = (float)((mBQ.b0 * wN0) + (mBQ.b1 * mBQ.yN_1[i]) + (mBQ.b2 * mBQ.yN_2[i]));
                mBQ.yN_2[i] = mBQ.yN_1[i];
                mBQ.yN_1[i] = wN0;
            }
        }
    }
}

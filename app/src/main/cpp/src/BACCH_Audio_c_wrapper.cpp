/*
 * BACCH_Audio_c_wrapper.cpp
 *
 *  Created on: Sep 8, 2018
 *      Author: Samuel Caldwell
 */

/* =========================================================================
   Copyright (c) 2018 BACCH Laboratories Inc.
   All rights reserved. BACCH Laboratories Proprietary and Confidential.
   ========================================================================= */

#include "BACCH_Audio_c_wrapper.h"
#include "BACCH_Audio.h"
#include "BACCH_sp.h"
#include "BACCH_library.h"
#include "BACCH_buffer.h"
#include "BACCH_buffer_ops.h"

#ifdef __hexagon__
#include "HAP_farf.h"

void msg(const char* msg) {
	FARF(HIGH, msg);
}
#else
void msg(const char* msg) {};
#endif

#ifdef _MSC_VER
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	printf("The DLL has been called with %d\n", ul_reason_for_call);
	return TRUE;
}
#endif



baHandle get_BA_instance(int sampleRate,
						 int channelsIn,
						 int orientation,
						 const char* sp_id){

	msg("BACCH audio c wrapper: calling init!");
	return new BACCH_Audio(sampleRate, channelsIn, (bacch_sp_orientation_t)orientation, sp_id);
}

void delete_BA_instance(baHandle mBA) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	delete mBAPtr;
}

void configure_BA(baHandle mBA, int sampleRate, int channelsIn, int orientation) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->configure(sampleRate, channelsIn, (bacch_sp_orientation_t)orientation);
}

void configure_VirtMode_BA(baHandle mBA, bool isDolby) {
    BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
    mBAPtr->configure_VIRT_Mode((isDolby)? VIRT_MODE_DOLBY : VIRT_MODE_DTS);
}

void configure_UBACCH(baHandle mBA, float distance_speaker_to_speaker, float distance_listener_to_speaker, bool isMetricUnits) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	//mBAPtr->configure_UBACCH(distance_speaker_to_speaker, distance_listener_to_speaker, isMetricUnits);
}

void updateOrientation_BA(baHandle mBA, bacch_audio_orientation_t orientation) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->updateOrientation((bacch_sp_orientation_t)orientation);
}

bacch_audio_orientation_t getOrientation_BA(baHandle mBA) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	return (bacch_audio_orientation_t)mBAPtr->getOrientation();
}

void reset_BA(baHandle mBA) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->reset();
}

void clear_BA(baHandle mBA) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->clear();
}

void setBACCHEnable_BA(baHandle mBA, _bool enabled, _bool isHP, _bool immediate) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->setEnabled(enabled, isHP, immediate);
}

void setHPEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->setHPEnable(enabled, isUser, immediate);
}

void setSPEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->setSPEnable(enabled, isUser, immediate);
}

void setUBACCHEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate) {

	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	//mBAPtr->setUBACCHEnable(enabled, isUser, immediate);
}

void setEQEnable_BA(baHandle mBA, _bool enabled, _bool immediate) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	mBAPtr->setEQEnable(enabled, immediate);
}

void setLCexEnable_BA(baHandle mBA, _bool enabled, _bool isUser, _bool immediate) {
    BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
    mBAPtr->setLCexEnable(enabled, immediate);
}

int getState_BA(baHandle mBA) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	return mBAPtr->getState();
}

int process_float_buf(baHandle mBA, float *in, float *out, int numsamples, int numchannelsin) {

	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;

	return mBAPtr->process_array(in, out, numsamples, numchannelsin);
}

int process_float_ptrs(baHandle mBA, float **in, float **out, int numsamples, int numchannelsin) {

	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	BACCH_buffer *inBB = new BACCH_buffer(numchannelsin, numsamples, in);
	inBB->mNumSamplesInBuffer = inBB->mWriteIndex = numsamples;
	BACCH_buffer *outBB = new BACCH_buffer(2, numsamples, out);

	return mBAPtr->process_block(inBB, outBB);

}

int process_block_BA(baHandle mBA, bufHndl inbuffer, bufHndl outbuffer) {
	BACCH_Audio* mBAPtr = (BACCH_Audio*)mBA;
	BACCH_buffer* in = (BACCH_buffer*)inbuffer;
	BACCH_buffer* out = (BACCH_buffer*)outbuffer;
	return mBAPtr->process_block(in, out);
}

int resample(baHandle mBA, int16 *in, int16 *out, int numsamples, int numchannelsin, int srIn, int *srOut) {

	int numSamplesToSkip = 1;
	int i = 0;
	int j = 0;
	int iRet = 0;

	switch (srIn) {
		case 88200: {
			*srOut = 44100;
			numSamplesToSkip = 1;
		}break;
		case 96000: {
			*srOut = 48000;
			numSamplesToSkip = 1;
		} break;
		case 176400: {
			*srOut = 44100;
			numSamplesToSkip = 3;
		}break;
		case 192000: {
			*srOut = 48000;
			numSamplesToSkip = 3;
		}break;
		default: {
			*srOut = 0;
			return 0;
		}
	}

	iRet = numsamples/(1+numSamplesToSkip);

	switch (numchannelsin) {
		case 2: {
			for (i=0,j=0;i<numsamples*numchannelsin;i+=2,j+=2) {
				out[i] = in[j];
				out[i + 1] = in[j + 1];
				j += 2*numSamplesToSkip;
			}
		}break;
		case 6: {
			for (i=0,j=0;i<numsamples*numchannelsin;i+=6,j+=6) {
				out[i] = in[j];
				out[i + 1] = in[j + 1];
				out[i + 2] = in[j + 2];
				out[i + 3] = in[j + 3];
				out[i + 4] = in[j + 4];
				out[i + 5] = in[j + 5];
				j+= 6*numSamplesToSkip;
			}
		}break;
		case 8: {
			for (i=0,j=0;i<numsamples*numchannelsin;i+=8,j+=8) {
				out[i] = in[j];
				out[i + 1] = in[j + 1];
				out[i + 2] = in[j + 2];
				out[i + 3] = in[j + 3];
				out[i + 4] = in[j + 4];
				out[i + 5] = in[j + 5];
				out[i + 6] = in[j + 6];
				out[i + 7] = in[j + 7];
				j+= 8*numSamplesToSkip;
			}
		}break;
		default:
			return 0;

	}
	return iRet;
}
#ifdef BUF_WRAPPERS

// Buffer wrappers
bufHndl getBA_buffer(int numChannels, int lengthInSamples) {
	return new BACCH_buffer(numChannels, lengthInSamples);
}

void delete_BA_buffer(bufHndl baBuf) {
	BACCH_buffer* buf = (BACCH_buffer*)baBuf;
	delete buf;
}

int getNumFrames_BA_buffer(bufHndl baBuf) {
	BACCH_buffer *buf = (BACCH_buffer*)baBuf;
	return buf->getNumSamplesInBuffer();
}

int readFramesFrom_BA_buffer(bufHndl baBuf, float** out, int numChannels, int numSamples){
	BACCH_buffer *buf = (BACCH_buffer*)baBuf;
	bufBufToBlock(buf, out, numChannels, numSamples);
	return numSamples;
}

int writeFramesTo_BA_buffer(bufHndl baBuf, float** in, int numChannels, int numSamples) {
	BACCH_buffer *buf = (BACCH_buffer*)baBuf;
	bufBlockToBuf(in, buf, numChannels, numSamples);
	return numSamples;
}

int write_s16Frames_To_f32buffer(bufHndl baBuf, int16_t** in, int numChannels, int numSamples) {
	BACCH_buffer *buf = (BACCH_buffer*)baBuf;
	bufBlockToBuf_s16_f32(in, buf, numChannels, numSamples);
	return numSamples;
}

int write_f32buffer_To_s16buffer(bufHndl f32in, bufHndl s16out, int numSamples) {
	BACCH_buffer* bufIn = (BACCH_buffer*)f32in;
	BACCH_s16buffer* bufOut = (BACCH_s16buffer*)s16out;
	bufCopy_f32_s16(bufIn, bufOut, numSamples);
	return numSamples;
}

bufHndl getBA_s16buffer(int numChannels, int lengthInSamples) {
	 return new BACCH_s16buffer(numChannels, lengthInSamples);
}

void delete_BA_s16buffer(bufHndl baBuf) {
	BACCH_s16buffer* buf = (BACCH_s16buffer*)baBuf;
	delete buf;
}

int getNumFrames_BA_s16buffer(bufHndl baBuf){
	BACCH_s16buffer *buf = (BACCH_s16buffer*)baBuf;
	return buf->getNumSamplesInBuffer();
}

int readFramesFrom_BA_s16buffer(bufHndl baBuf, int16_t** out, int numChannels, int numSamples) {
	BACCH_s16buffer *buf = (BACCH_s16buffer*)baBuf;
	bufBufToBlock_s16(buf, out, numChannels, numSamples);
	return numSamples;
}

int writeFramesTo_BA_s16buffer(bufHndl baBuf, int16_t** in, int numChannels, int numSamples) {
	BACCH_s16buffer *buf = (BACCH_s16buffer*)baBuf;
	bufBlockToBuf_s16(in, buf, numChannels, numSamples);
	return numSamples;
}

#endif // BUF_WRAPPERS

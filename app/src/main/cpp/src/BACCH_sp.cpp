#include "BACCH_sp.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"

#include "BACCH_GALAXY_TABS_coeffs.h"
#include "BACCH_GALAXY_TABS_constants.h"

#include <android/log.h>

#define LOG_TAG	"BACCH_sp"

#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

#ifdef _SPFDEBUG
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <cerrno>

#endif


// sp filters are:
// float[samplerate][orientation][offset][channel_to_channel][coeffs]



void BACCH_sp::FilterLoad() {
	int irBlockIdx = 0, orientIdx = 0;
	int offset = 0;
	//const float *data[2];

	int srIdx = 1; //mSampleRateIdx;

	// sp ir file is array of IRs by samplerate, offset(1-41) aka filterNum,
	// the LtoL, RtoL, LtoR, RtoR
    offset = 0;
    for (irBlockIdx = 0; irBlockIdx < GALAXY_TABS_IR_NUM_BLOCKS[srIdx]; irBlockIdx++) {
        // clear the transfer function buffers
        TFs[BACCH_LEFT][irBlockIdx]->clear();
        TFs[BACCH_RIGHT][irBlockIdx]->clear();

        // load the impulse response data
        vRealToComplex((float*)&ts_irs[srIdx][0][LEFT_TO_LEFT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);
        vRealToComplex((float*)&ts_irs[srIdx][0][LEFT_TO_RIGHT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);

        vRealToComplex((float*)&ts_irs[srIdx][0][RIGHT_TO_LEFT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);
        vRealToComplex((float*)&ts_irs[srIdx][0][RIGHT_TO_RIGHT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);

        // fft the impulse responses
        fft->Forward(TFs[BACCH_LEFT][irBlockIdx]);
        fft->Forward(TFs[BACCH_RIGHT][irBlockIdx]);
        offset += BACCH_FRAME_SIZE;

        // IRs validated via file output and comparison of TF to Matlab plot(fft(ir,4096))
/*#ifdef _SPFDEBUG
        if (spfile != NULL) {
            int numchannels = 2;
            BACCH_buffer *debugBuf = new BACCH_buffer(numchannels, BACCH_FFT_SIZE*2);
            int16_t *i16buf = (int16_t *) blMalloc(sizeof(int16_t), numchannels * BACCH_FFT_SIZE * 2, 64);

            vCopy((float*)TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], debugBuf->data[0], BACCH_FFT_SIZE*2);
            vCopy((float*)TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], debugBuf->data[1], BACCH_FFT_SIZE*2);
            //vBlockToInterleaved_32f_16s(debugBuf->data, i16buf, 1.0f, numchannels, BACCH_FFT_SIZE*2, false);
            fwrite((char *) debugBuf->data[0], 1, (long long) ( BACCH_FFT_SIZE * 2 * sizeof(float)), spfile);
            fwrite((char *) debugBuf->data[1], 1, (long long) ( BACCH_FFT_SIZE * 2 * sizeof(float)), spfile);

            vCopy((float*)TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], debugBuf->data[0], BACCH_FFT_SIZE*2);
            vCopy((float*)TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], debugBuf->data[1], BACCH_FFT_SIZE*2);
            //vBlockToInterleaved_32f_16s(debugBuf->data, i16buf, 1.0f, numchannels, BACCH_FFT_SIZE * 2, false);
            fwrite((char *) debugBuf->data[0], 1, (long long) ( BACCH_FFT_SIZE * 2 * sizeof(float)), spfile);
            fwrite((char *) debugBuf->data[1], 1, (long long) ( BACCH_FFT_SIZE * 2 * sizeof(float)), spfile);

            fclose(spfile);

            blFree(i16buf);
            delete debugBuf;
        }
#endif*/
    }
}

void BACCH_sp::configure(int samplerate, bacch_sp_orientation_t orientation)
{
    if ((samplerate != mSampleRate) || (orientation != mOrientation)) {
        mSampleRate = samplerate;
        mSampleRateIdx = validateSampleRate(mSampleRate);
        if (mSampleRateIdx < 0) {
            mSampleRateIdx = 0;
        }
        mNumIRBlocks = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIdx];
		mIRLength = GALAXY_TABS_IR_LENGTHS[mSampleRateIdx];
        updateOrientation(orientation);
        FilterLoad();
        reset();
    }
}

void BACCH_sp::updateOrientation(bacch_sp_orientation_t val) {
    if ((val > BACCH_SP_ORIENTATION_INVALID) && (val <= BACCH_SP_ORIENTATION_CNT)) {
		mOrientation = val;
    }
}

void BACCH_sp::reset()
{
    int irBlockIdx = 0;

    //mPad->clear();
    for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
        mPad[irBlockIdx]->clear();
        mPadIdx[irBlockIdx] = irBlockIdx;
    }
}

void BACCH_sp::clear()
{
    reset();
}

BACCH_sp::BACCH_sp(int samplerate, bacch_sp_orientation_t orientation)
{        
	int irBlockIdx = 0, channelIdx = 0;

#ifdef TABS4
	LOGI("Loading Filters for TAB S4 with aarch64");
#else
	LOGI("Loading Filters for TAB S with armv7");
#endif


#ifdef _SPFDEBUG
    spfile = fopen("/sdcard/Music/spdebug.raw", "w");
	if (spfile == NULL) {
	    int *error = __errno();
	    char *strerr = strerror(*error);
	    LOGI("Error returned by fopen: %d : %s", *error, strerr);
	}
#endif
	fft = new BACCH_fft();
	//if ((mFilterNum < 0) || (mFilterNum >= GALAXY_TABS_NUM_IRS))
	//	mThrow("Invalid Filter Number passed to BACCH_SP instance");

	mSampleRate = samplerate;
	mSampleRateIdx = validateSampleRate(mSampleRate);
	//if (mSampleRateIdx < 0)
	//	mThrow("Invalid Sample Rate passed to BACCH_SP");

	mNumIRBlocks = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIdx];
	
    TFs = (BACCH_complex_buffer***)blMalloc(sizeof(BACCH_complex_buffer**), NUM_OUT_CHANNELS, 32);

	// transfer function buffers
	for (channelIdx = 0; channelIdx < NUM_OUT_CHANNELS; channelIdx++) {
        TFs[channelIdx] = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), mNumIRBlocks, 32);
        for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
            TFs[channelIdx][irBlockIdx] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
        }
    }

	ScratchIn = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
	mPad = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*),mNumIRBlocks, 32);
	ScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), mNumIRBlocks, 32);

    // pad, scratch buffers
	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		mPad[irBlockIdx] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		ScratchFFT[irBlockIdx] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		mPadIdx[irBlockIdx] = irBlockIdx;
	}

    updateOrientation(orientation);

	// load transfer functions
	FilterLoad(); 
}


BACCH_sp::~BACCH_sp()
{
	int channelIdx = 0, irBlockIdx = 0, orientIdx = 0;

	delete fft;
	delete ScratchIn;

    for (channelIdx = 0; channelIdx < NUM_OUT_CHANNELS; channelIdx++) {
        for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
            delete TFs[channelIdx][irBlockIdx];
        }
        blFree(TFs[channelIdx]);
    }

	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		delete mPad[irBlockIdx];
		delete ScratchFFT[irBlockIdx];
	}

	blFree(mPad);
	//blFree(mPadIdx);
	blFree(TFs);
	blFree(ScratchFFT);
}

// this call updates the inbuffer and the outbuffer which will have n samples in it
// the outbuffer must have available space the same or greater than length of input data
void BACCH_sp::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer)
{
	int irBlockIdx = 0, inChannelIdx = 0, padIdx = 0;
    status_t mStatus = STATUS_OK;
	int numSamples = inbuffer->getNumSamplesInBuffer();

	// clear scratch buffer
	ScratchIn->clear();
		
	// copy input data to scratch buffer real values
	cbufRealtoCplx(inbuffer, ScratchIn, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);

    /*if (spfile != NULL) {
        BACCH_buffer *debugBuf = new BACCH_buffer(2, BACCH_FFT_SIZE);

        int16_t *i16buf = (int16_t *) blMalloc(sizeof(int16_t),2 * BACCH_FFT_SIZE, 64);

        vCopy( inbuffer->data[0], debugBuf->data[0], BACCH_FRAME_SIZE);
        vCopy( inbuffer->data[1], debugBuf->data[1], BACCH_FRAME_SIZE);

        vBlockToInterleaved_32f_16s(debugBuf->data, i16buf, 1.0f, 2, BACCH_FRAME_SIZE, false);

        fwrite((char *) i16buf, 1, (long long) (2 * BACCH_FRAME_SIZE * 2), spfile);

        //fwrite((char *) debugBuf->data[0], 1, (long long) ( BACCH_FFT_SIZE * sizeof(float)), spfile);
        //fwrite((char *) debugBuf->data[1], 1, (long long) ( BACCH_FFT_SIZE * sizeof(float)), spfile);
        free(i16buf);
        delete debugBuf;
    }*/

    bufCopy(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);
    // zero out this buffer, subsequent operations are accumulates
	mPad[mPadIdx[0]]->clear();

	// lets swap the pad index to next buffer indicating last one consumed
	for (padIdx = 0; padIdx < mNumIRBlocks; padIdx++) {
		mPadIdx[padIdx] --;
		if (mPadIdx[padIdx] < 0)
			mPadIdx[padIdx] = mNumIRBlocks-1;
	}

	fft->Forward(ScratchIn);

    irBlockIdx = 0;
	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
        ScratchFFT[irBlockIdx]->clear();

        // convolve left with left => left scratch and left with right => right scratch
        vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
        vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right

        // convolve and accumulate right with left => left scratch and right with right => right scratch
        vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // + right to left
        vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // + right to right

        // inverse fft
        fft->Inverse(ScratchFFT[irBlockIdx]);

        // store results in pad buffers
        cbufCplxtoReal(ScratchFFT[irBlockIdx], mPad[mPadIdx[0]], NUM_OUT_CHANNELS, BACCH_FFT_SIZE, false);
	}

    // add current output to last run's tail
    bufAdd_I(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);
    
}


//#include "stdafx.h"

#include "BACCH_cx.h"
#include "BACCH_types.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"

#define CX_LAST		0
#define CX_CURRENT	1

#define CX_ALPHA	0
#define CX_BETA		1
#define CX_GAMMA	2

void getDC(FFTComplex left, FFTComplex right, BACCH_complex_buffer *out) {
	
	FFTComplex kappaDC;
	kappaDC.im = left.re + right.re;

	float ai = left.im + right.im;
	if (ai == 0.0F) {
		kappaDC.re = kappaDC.im / 2.0F;
		kappaDC.im = 0.0F;
	}
	else if (kappaDC.im == 0.0F) {
		kappaDC.re = 0.0F;
		kappaDC.im = ai / 2.0F;
	}
	else {
		kappaDC.re = kappaDC.im / 2.0F;
		kappaDC.im = ai / 2.0F;
	}

	out->data[CX_GAMMA][0].re = kappaDC.re * 1.41421354F;
	out->data[CX_GAMMA][0].im = kappaDC.im * 1.41421354F;

	out->data[CX_ALPHA][0].re = (left.re - kappaDC.re);
	out->data[CX_ALPHA][0].im = (left.im - kappaDC.im);
	out->data[CX_BETA][0].re = (right.re - kappaDC.re);
	out->data[CX_BETA][0].im = (right.im - kappaDC.im);
}

inline bool checkZero(FFTComplex *a, FFTComplex *b) {
	if (a->re == 0 && a->im == 0 && b->re == 0 && b->im == 0) {
		return true;
	}
	return false;
}

inline void setZero(FFTComplex **a, int index) {
	a[0][index].re = a[0][index].im = 0.0f;
	a[1][index].re = a[1][index].im = 0.0f;
	a[2][index].re = a[2][index].im = 0.0f;
}

inline bool checkEqual(FFTComplex *a, FFTComplex *b) {
	if ((a->re == b->re) && (a->im == b->im)) {
		return true;
	}
	return false;
}

inline void setToCenter(FFTComplex **a, FFTComplex *alpha, int index) {
	a[CX_ALPHA][index].re = a[CX_ALPHA][index].im = 0.0f;
	a[CX_BETA][index].re = a[CX_BETA][index].im = 0.0f;
	a[CX_GAMMA][index].re = 1.41421354F * alpha->re;
	a[CX_GAMMA][index].im = 1.41421354F * alpha->im;
}

static float rt_hypotf(float u0, float u1)
{
	float y;
	float a;
	float b;
	a = fabsf(u0);
	b = fabsf(u1);
	if (a < b) {
		a /= b;
		y = b * sqrtf(a * a + 1.0F);
	}
	else if (a > b) {
		b /= a;
		y = a * sqrtf(b * b + 1.0F);
	}
	else {
		y = a * 1.41421354F;
	}

	return y;
}

void getUandV(FFTComplex *alpha, FFTComplex *beta, FFTComplex *u, FFTComplex *v, FFTComplex *utmp) {
	/* Define U and V */
/* 'getComplexLCRwidth0:30' u = (beta - alpha) / norm(beta - alpha); */
	utmp->re = beta->re - alpha->re;
	utmp->im = beta->im - alpha->im;
	u->im = rt_hypotf(utmp->re, utmp->im);
	if (utmp->im == 0.0F) {
		u->re = utmp->re / u->im;
		u->im = 0.0F;
	}
	else if (utmp->re == 0.0F) {
		u->re = 0.0F;
		u->im = utmp->im / u->im;
	}
	else {
		u->re = utmp->re / u->im;
		u->im = utmp->im / u->im;
	}

	/* 'getComplexLCRwidth0:31' v = u * 1i; */
	v->re = 0.0F - u->im;
	v->im = u->re;
}

void BACCH_cx::getCX(BACCH_complex_buffer *in, BACCH_complex_buffer *out, int numsamples) {


	//vFabs((float*)in[CX_LAST]->data[BACCH_LEFT], (float*)cScratchFFT[CX_LAST]->data[BACCH_LEFT], numsamples*2);
	//vFabs((float*)in[CX_LAST]->data[BACCH_RIGHT], (float*)cScratchFFT[CX_LAST]->data[BACCH_RIGHT], numsamples*2);

	float m2[2];
	float r;
	float t;
	float y[4];
	int r1;
	int r2;
	FFTComplex utmp;
	FFTComplex *alpha;
	FFTComplex *beta;
	FFTComplex *alphaHat;
	FFTComplex *betaHat;
	FFTComplex *gammaHat;
	utmp.re = 0;
	utmp.im = 0;

	getDC(in->data[BACCH_LEFT][0], in->data[BACCH_RIGHT][0], out);

	for (int i = 1; i < numsamples; i++) {
		alpha = &in->data[BACCH_LEFT][i];
		beta = &in->data[BACCH_RIGHT][i];
		alphaHat = &out->data[CX_ALPHA][i];
		betaHat = &out->data[CX_BETA][i];
		gammaHat = &out->data[CX_GAMMA][i];

		if (checkZero(alpha, beta)) {
			setZero(out->data, i);
			continue;
		}
		if (checkEqual(alpha, beta)) {
			setToCenter(out->data, alpha, i);
			continue;
		}
		/* Define U and V */
	/* 'getComplexLCRwidth0:30' u = (beta - alpha) / norm(beta - alpha); */
		FFTComplex u, v;
		getUandV(alpha, beta, &u, &v, &utmp);

		m2[0] = alpha->re;
		m2[1] = alpha->im;

		/* Solve for Inverse */
		/* 'getComplexLCRwidth0:38' coeff = inv(m1)\m2; */
		if (fabsf(-u.im) > fabsf(-u.re)) {
			r = -u.re / -u.im;
			t = 1.0F / (r * u.re - (0.0F - u.im));
			y[0] = u.re / -u.im * t;
			y[1] = -t;
			y[2] = -(0.0F - u.im) / -u.im * t;
			y[3] = r * t;
		}
		else {
			r = -u.im / -u.re;
			t = 1.0F / (u.re - r * (0.0F - u.im));
			y[0] = u.re / -u.re * t;
			y[1] = -r * t;
			y[2] = -(0.0F - u.im) / -u.re * t;
			y[3] = t;
		}

		if (fabsf(y[1]) > fabsf(y[0])) {
			r1 = 1;
			r2 = 0;
		}
		else {
			r1 = 0;
			r2 = 1;
		}

		u.im = y[r2] / y[r1];
		r = y[2 + r1];
		t = (m2[r2] - m2[r1] * u.im) / (y[2 + r2] - u.im * r);
		u.im = (m2[r1] - t * r) / y[r1];

		/* Get Coefficients */
		/* 'getComplexLCRwidth0:41' c = coeff(1); */
		/* 'getComplexLCRwidth0:42' d = coeff(2); */
		/* Check for Cases */
		/* 'getComplexLCRwidth0:45' if c < 0 */
		if (u.im < 0.0F) {
			/* 'getComplexLCRwidth0:46' if alpha == 0 */
			if ((alpha->re == 0.0F) && (alpha->im == 0.0F)) {
				/* 'getComplexLCRwidth0:47' v = single(0) + minimag; */
				v.re = 0.0f;
				v.im = 0.0f;

				/* 'getComplexLCRwidth0:48' d = norm(alpha); */
				t = 0.0F;
			}
			else {
				/* 'getComplexLCRwidth0:49' else */
				/* 'getComplexLCRwidth0:50' v = alpha / norm(alpha); */
				t = rt_hypotf(alpha->re, alpha->im);
				if (alpha->im == 0.0F) {
					v.re = alpha->re / t;
					v.im = 0.0F;
				}
				else if (alpha->re == 0.0F) {
					v.re = 0.0F;
					v.im = alpha->im / t;
				}
				else {
					v.re = alpha->re / t;
					v.im = alpha->im / t;
				}

				/* 'getComplexLCRwidth0:51' d = norm(alpha); */
			}
		}

		/* 'getComplexLCRwidth0:54' if c > norm(beta - alpha) */
		if (u.im > rt_hypotf(utmp.re, utmp.im)) {
			/* 'getComplexLCRwidth0:55' if beta == 0 */
			if ((beta->re == 0.0F) && (beta->im == 0.0F)) {
				/* 'getComplexLCRwidth0:56' v = single(0) + minimag; */
				v.re = 0.0f;
				v.im = 0.0f;

				/* 'getComplexLCRwidth0:57' d = norm(beta); */
				t = 0.0F;
			}
			else {
				/* 'getComplexLCRwidth0:58' else */
				/* 'getComplexLCRwidth0:59' v = beta / norm(beta); */
				t = rt_hypotf(beta->re, beta->im);
				if (beta->im == 0.0F) {
					v.re = beta->re / t;
					v.im = 0.0F;
				}
				else if (beta->re == 0.0F) {
					v.re = 0.0F;
					v.im = beta->im / t;
				}
				else {
					v.re = beta->re / t;
					v.im = beta->im / t;
				}

				/* 'getComplexLCRwidth0:60' d = norm(beta); */
			}
		}

		/* Remap Width Parameter */
		/* w = widthDoubleMap(width, alpha, beta); %width==0 so w=1 */
		/* 'getComplexLCRwidth0:66' kappa = d * v; */
		u.re = t * v.re;
		u.im = t * v.im;

		/* kappa = w * d * v; %w=1 for this case; */
		/* 'getComplexLCRwidth0:68' alphaHat = alpha - kappa; */
		alphaHat->re = alpha->re - u.re;
		alphaHat->im = alpha->im - u.im;

		/* Left */
		/* 'getComplexLCRwidth0:69' betaHat  = beta  - kappa; */
		betaHat->re = beta->re - u.re;
		betaHat->im = beta->im - u.im;

		/* Right */
		/* 'getComplexLCRwidth0:70' gammaHat = sqrt(2) * kappa; */
		gammaHat->re = 1.41421354F * u.re;
		gammaHat->im = 1.41421354F * u.im;
	}
}

void BACCH_cx::configure(int samplerate)
{
    if (samplerate != mSampleRate) {
        mSampleRate = samplerate;
        
        reset();
    }
}

void BACCH_cx::reset()
{
	
}

void BACCH_cx::clear()
{
    reset();
}

BACCH_cx::BACCH_cx(int samplerate)
{      
	fft = new BACCH_fft();

	mSampleRate = samplerate;
	mSampleRateIdx = validateSampleRate(mSampleRate);
	
	mBlockSize = BACCH_FRAME_SIZE;

	ScratchIn = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 2, 64);
	cScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 2, 64);
	ScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 2, 64); 
	Intermediate = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 2, 64);

	Input = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*), 2, 64);
	Scratch = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*), 2, 64);
	ScratchOut = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*), 2, 64);

	for (int i = 0; i < 2; i++) {
		ScratchIn[i] = new BACCH_complex_buffer(2, mBlockSize * 2);
		cScratchFFT[i] = new BACCH_complex_buffer(2, mBlockSize * 2);
		ScratchFFT[i] = new BACCH_complex_buffer(2, mBlockSize * 2);

		Input[i] = new BACCH_buffer(2, mBlockSize);
		Scratch[i] = new BACCH_buffer(2, mBlockSize);

		Intermediate[i] = new BACCH_complex_buffer(3, mBlockSize * 2);
		ScratchOut[i] = new BACCH_buffer(3, mBlockSize);
	}

	centerHoldBuf = new BACCH_buffer(1, mBlockSize*2);

	Window = new BACCH_buffer(1, mBlockSize);

	mltwinMod(Window->data[0], mBlockSize);

}


BACCH_cx::~BACCH_cx()
{

	for (int i = 0; i < 2; i++) {
		delete ScratchIn[i];
		delete cScratchFFT[i];
		delete ScratchFFT[i];
		delete Intermediate[i];

		delete Input[i];
		delete Scratch[i];
	}

	if (NULL != centerHoldBuf) delete centerHoldBuf;

	blFree(ScratchIn);
	blFree(cScratchFFT);
	blFree(ScratchFFT);
	blFree(Intermediate);
	blFree(Input);
	blFree(Scratch);
	blFree(Window);
}

// this call updates the inbuffer and the outbuffer which will have n samples in it
// the outbuffer must have available space the same or greater than length of input data
void BACCH_cx::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, BACCH_buffer* coutbuffer)
{
	
	int numSamples = inbuffer->getNumSamplesInBuffer();
	FFTComplex kappaDC[2];

	ScratchOut[CX_LAST]->clear();
	ScratchFFT[CX_LAST]->clear();
	ScratchFFT[CX_CURRENT]->clear();
	outbuffer->clear();
	coutbuffer->clear();

	// copy last run's tail
	vCopy(&ScratchOut[CX_CURRENT]->data[CX_ALPHA][mBlockSize / 2], outbuffer->data[CX_ALPHA], mBlockSize / 2);
	vCopy(&ScratchOut[CX_CURRENT]->data[CX_BETA][mBlockSize / 2], outbuffer->data[CX_BETA], mBlockSize / 2);
	// put last run center channel tail in center hold buf (outgoing data) with delay of 1/2 block
	// there is a 1/2 block of delayed data already in the buffer
	vCopy(&ScratchOut[CX_CURRENT]->data[CX_GAMMA][mBlockSize / 2], &centerHoldBuf->data[0][mBlockSize/2], mBlockSize/2); //coutbuffer->data[0], mBlockSize / 2);

	ScratchOut[CX_CURRENT]->clear(); 

	// copy the last run's input
	vCopy(&Input[CX_CURRENT]->data[0][mBlockSize / 2], Input[CX_LAST]->data[0], mBlockSize / 2);
	vCopy(&Input[CX_CURRENT]->data[1][mBlockSize / 2], Input[CX_LAST]->data[1], mBlockSize / 2);

	Input[CX_CURRENT]->reset();

	// copy new input data
	bufCopy(inbuffer, Input[CX_CURRENT], mBlockSize);

	// finish the 'last run' buffer with first half of current input
	vCopy(Input[CX_CURRENT]->data[0], &Input[CX_LAST]->data[0][mBlockSize / 2], mBlockSize / 2);
	vCopy(Input[CX_CURRENT]->data[1], &Input[CX_LAST]->data[1][mBlockSize / 2], mBlockSize / 2);

	// apply window to copy 
	vMul(Input[CX_LAST]->data[0], Window->data[0], Scratch[CX_LAST]->data[0], mBlockSize);
	vMul(Input[CX_LAST]->data[1], Window->data[0], Scratch[CX_LAST]->data[1], mBlockSize);
	vMul(Input[CX_CURRENT]->data[0], Window->data[0], Scratch[CX_CURRENT]->data[0], mBlockSize);
	vMul(Input[CX_CURRENT]->data[1], Window->data[0], Scratch[CX_CURRENT]->data[1], mBlockSize);

	Scratch[CX_LAST]->mNumSamplesInBuffer = Scratch[CX_LAST]->mWriteIndex = mBlockSize;
	Scratch[CX_CURRENT]->mNumSamplesInBuffer = Scratch[CX_CURRENT]->mWriteIndex = mBlockSize;

	cbufRealtoCplx(Scratch[CX_LAST], ScratchFFT[CX_LAST], 2, mBlockSize);
	cbufRealtoCplx(Scratch[CX_CURRENT], ScratchFFT[CX_CURRENT], 2, mBlockSize);

	fft->Forward(ScratchFFT[CX_LAST]);
	fft->Forward(ScratchFFT[CX_CURRENT]);

	Intermediate[CX_LAST]->clear();
	Intermediate[CX_CURRENT]->clear();

	getCX(ScratchFFT[CX_LAST], Intermediate[CX_LAST], mBlockSize*2);
	getCX(ScratchFFT[CX_CURRENT], Intermediate[CX_CURRENT], mBlockSize * 2);

	fft->Inverse(Intermediate[CX_LAST]);
	fft->Inverse(Intermediate[CX_CURRENT]);

	Intermediate[CX_LAST]->mNumSamplesInBuffer = Intermediate[CX_LAST]->mWriteIndex = mBlockSize;
	Intermediate[CX_CURRENT]->mNumSamplesInBuffer = Intermediate[CX_CURRENT]->mWriteIndex = mBlockSize;

	cbufCplxtoReal(Intermediate[CX_LAST], ScratchOut[CX_LAST], 3, mBlockSize, false);
	cbufCplxtoReal(Intermediate[CX_CURRENT], ScratchOut[CX_CURRENT], 3, mBlockSize, false);

	//fft->Inverse(Intermediate[CX_LAST]);
	//fft->Inverse(Intermediate[CX_CURRENT]);

	//cbufCplxtoReal(Intermediate[CX_LAST], ScratchOut[CX_LAST], 3, mBlockSize, false);
	//cbufCplxtoReal(Intermediate[CX_CURRENT], ScratchOut[CX_CURRENT], 3, mBlockSize, false);

	//vCopy(Scratch[CX_LAST]->data[0], ScratchOut[CX_LAST]->data[0], mBlockSize);
	//vCopy(Scratch[CX_LAST]->data[1], ScratchOut[CX_LAST]->data[1], mBlockSize);
	//vCopy(Scratch[CX_CURRENT]->data[0], ScratchOut[CX_CURRENT]->data[0], mBlockSize);
	//vCopy(Scratch[CX_CURRENT]->data[1], ScratchOut[CX_CURRENT]->data[1], mBlockSize);

	vMul_I(Window->data[0], ScratchOut[CX_LAST]->data[0], mBlockSize);
	vMul_I(Window->data[0], ScratchOut[CX_LAST]->data[1], mBlockSize);
	vMul_I(Window->data[0], ScratchOut[CX_LAST]->data[2], mBlockSize);
	vMulC_I(ScratchOut[CX_LAST]->data[2], 0.707945784384138f, mBlockSize);
	vMul_I(Window->data[0], ScratchOut[CX_CURRENT]->data[0], mBlockSize);
	vMul_I(Window->data[0], ScratchOut[CX_CURRENT]->data[1], mBlockSize);
	vMul_I(Window->data[0], ScratchOut[CX_CURRENT]->data[2], mBlockSize);
	vMulC_I(ScratchOut[CX_CURRENT]->data[2], 0.707945784384138f, mBlockSize);

	vCopy(ScratchOut[CX_CURRENT]->data[CX_ALPHA], &outbuffer->data[0][mBlockSize / 2], mBlockSize/2);
	vCopy(ScratchOut[CX_CURRENT]->data[CX_BETA], &outbuffer->data[1][mBlockSize / 2], mBlockSize/2);
	// copy extracted center channel to center hold buffer with 1/2 block delay (others already 1/2 block back, so whole block back)
	vCopy(ScratchOut[CX_CURRENT]->data[CX_GAMMA], &centerHoldBuf->data[0][mBlockSize], mBlockSize / 2);
	//vCopy(ScratchOut[CX_CURRENT]->data[CX_GAMMA], &coutbuffer->data[0][mBlockSize / 2], mBlockSize/2);

	vAdd_I(ScratchOut[CX_LAST]->data[CX_ALPHA], outbuffer->data[0], mBlockSize);
	vAdd_I(ScratchOut[CX_LAST]->data[CX_BETA], outbuffer->data[1], mBlockSize);
	// add to the center hold buffer - again delayed by half a block
	vAdd_I(ScratchOut[CX_LAST]->data[CX_GAMMA], &centerHoldBuf->data[0][mBlockSize/2], mBlockSize);
	//vAdd_I(ScratchOut[CX_LAST]->data[CX_GAMMA], coutbuffer->data[0], mBlockSize);

	// move delayed center channel output to outbuffer center channel
	vCopy(centerHoldBuf->data[0], coutbuffer->data[0], mBlockSize);

	// move remaining center channel (1/2 block) to beginning of hold buffer
	centerHoldBuf->mNumSamplesInBuffer = mBlockSize / 2;
	centerHoldBuf->mReadIndex = mBlockSize;
	centerHoldBuf->reAlign();

	outbuffer->mNumSamplesInBuffer = outbuffer->mWriteIndex = mBlockSize;
	coutbuffer->mNumSamplesInBuffer = coutbuffer->mWriteIndex = mBlockSize;
    
}

void BACCH_cx::mltwinMod(float *win, int length)
{
	int k;
	float ex;
	float d0;

	/*   Joe Henning - Jan 2014 */
	/* 'mltwinMod:15' assert(isa(N,'double')); */
	/* 'mltwinMod:16' assert(size(N,1) <=8192); */
	/* 'mltwinMod:18' outWin = zeros(N,1); */
	/* 'mltwinMod:20' for k = 0:N-1 */
	for (k = 0; k < length; k++) {
		/* 'mltwinMod:21' outWin(k+1) = sin((k+0.5)*pi/N); */
		win[k] = (float)sin(((double)k + 0.5) * 3.1415926535897931 / double(length));
	}

	/*  normalize */
	/* 'mltwinMod:25' outWin = outWin./max(outWin); */
	ex = win[0];
	for (k = 0; k < length-1; k++) {
		d0 = win[k + 1];
		if (ex < d0) {
			ex = d0;
		}
	}

	for (k = 0; k < 2048; k++) {
		win[k] = win[k] / ex;
	}
}


#include "BACCH_hp.h"
#include "../coeffs/BACCH_HP_coeffs.h"
#include "../coeffs/BACCH_HP_constants.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"
#include "BACCH_buffer_ops.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif


void BACCH_hp::FilterLoad() {
	int irBlockIdx = 0;
	int offset = 0;

	// sp ir file is array of IRs by samplerate, offset(1-41) aka filterNum,
	// then LtoL, RtoL, LtoR, RtoR
	for (irBlockIdx = 0; irBlockIdx < HP_IR_NUM_BLOCKS[mSampleRateIdx]; irBlockIdx++) {
		// clear the transfer function buffers
		TFs[BACCH_LEFT][irBlockIdx]->clear();
		TFs[BACCH_RIGHT][irBlockIdx]->clear();
			
        vRealToComplex((float*)&hp_irs[mSampleRateIdx][0][LEFT_TO_LEFT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);// ->write_block((float**)data, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
        vRealToComplex((float*)&hp_irs[mSampleRateIdx][0][LEFT_TO_RIGHT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);// ->write_block((float**)data, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);

        vRealToComplex((float*)&hp_irs[mSampleRateIdx][0][RIGHT_TO_LEFT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);// ->write_block((float**)data, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
        vRealToComplex((float*)&hp_irs[mSampleRateIdx][0][RIGHT_TO_RIGHT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);// ->write_block((float**)data, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);


		// fft the impulse responses
		fft->Forward(TFs[BACCH_LEFT][irBlockIdx]);
		fft->Forward(TFs[BACCH_RIGHT][irBlockIdx]);
		offset += BACCH_FRAME_SIZE;
	}
}

BACCH_hp::BACCH_hp(int samplerate)
{        
	int irBlockIdx = 0, channelIdx = 0;
	
	fft = new BACCH_fft(BACCH_FFT_SIZE);
    mFilterNum =0;
	//if ((mFilterNum<0) || (mFilterNum>=HP_NUM_IRS))
	//	mThrow("Invalid Filter Number passed to BACCH_HP instance");

	mSampleRate = samplerate;
	mSampleRateIdx = validateSampleRate(mSampleRate);

	mNumIRBlocks = HP_IR_NUM_BLOCKS[mSampleRateIdx];
	
	mPad = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*),mNumIRBlocks, 32);
	TFs = (BACCH_complex_buffer***)blMalloc(sizeof(BACCH_complex_buffer**), NUM_OUT_CHANNELS, 32);
	ScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), mNumIRBlocks, 32);
	mPadIdx = (int*)blMalloc(sizeof(int), 32, 32); // new int[mNumIRBlocks];

	// transfer function buffers
	for (channelIdx = 0; channelIdx < NUM_OUT_CHANNELS; channelIdx++) {
		TFs[channelIdx] = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), mNumIRBlocks, 32);
		for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
			TFs[channelIdx][irBlockIdx] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		}
	}

	// mPad, scratch buffers
	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		
		mPad[irBlockIdx] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		
		mPadIdx[irBlockIdx] = irBlockIdx;
	
		ScratchFFT[irBlockIdx] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
	}

	ScratchIn = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
	
	// load transfer functions
	FilterLoad();
}


BACCH_hp::~BACCH_hp()
{
	int channelIdx = 0, irBlockIdx = 0;

	delete fft;
	delete ScratchIn;
	blFree(mPadIdx);
	for (channelIdx = 0; channelIdx < NUM_OUT_CHANNELS; channelIdx++) {
		for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
			delete TFs[channelIdx][irBlockIdx];
		}
		blFree(TFs[channelIdx]);
	}
	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		delete mPad[irBlockIdx];
		delete ScratchFFT[irBlockIdx];
	}
	blFree(mPad);
	blFree(TFs);
	blFree(ScratchFFT);
}

void BACCH_hp::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer)
{
    status_t mStatus = STATUS_OK;
	int irBlockIdx = 0, inChannelIdx = 0, padIdx = 0; 

    // clear scratch buffer
    ScratchIn->clear();

    // copy input data to scratch buffer real values
    cbufRealtoCplx(inbuffer, ScratchIn, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);

    bufCopy(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

    // has to clear, subsequent use is accumulate
	mPad[mPadIdx[0]]->clear();

	// lets swap the mPad index to next buffer since current idx=0 is consumed
	for (padIdx = 0; padIdx < mNumIRBlocks; padIdx++) {
		mPadIdx[padIdx] --;
		if (mPadIdx[padIdx] < 0)
			mPadIdx[padIdx] = mNumIRBlocks - 1;
	}

	fft->Forward(ScratchIn);

	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		ScratchFFT[irBlockIdx]->clear();

		// first vector complex multiply sets the data in ScratchFFT for the current IR Block Index
		vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
		vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right
		
		vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // + right to left
		vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // + right to right		

		// inverse fft
		fft->Inverse(ScratchFFT[irBlockIdx]);

		// store results in mPad buffers
        cbufCplxtoReal(ScratchFFT[irBlockIdx], mPad[mPadIdx[irBlockIdx]], NUM_OUT_CHANNELS, BACCH_FFT_SIZE, true);
	}

    // add current output to last run's tail
    bufAdd_I(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

}

void BACCH_hp::configure(int samplerate)
{
	if ((samplerate != mSampleRate)) {
		mSampleRate = samplerate;
		mSampleRateIdx = validateSampleRate(mSampleRate);
		mNumIRBlocks = HP_IR_NUM_BLOCKS[mSampleRateIdx];
		FilterLoad();
		reset();
	}
}

void BACCH_hp::reset()
{
	int irBlockIdx = 0;

	for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
		mPad[irBlockIdx]->clear();
		mPadIdx[irBlockIdx] = irBlockIdx;
	}
}

void BACCH_hp::clear()
{
	reset();
}


//#include "stdafx.h"

// RingBuffer is specifically used for headphone case
#include "BACCH_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include <malloc.h>
#include "BACCH_mem.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif

BACCH_buffer::BACCH_buffer() {
	mBufferLen = BACCH_FRAME_SIZE;
	mNumChannels = 2;
	mReadIndex = mWriteIndex = 0;
	mNumSamplesInBuffer = 0;
	externalMem = true;
	chunkValid = false;
}


BACCH_buffer::BACCH_buffer(SInt32 numchannels, SInt32 length) {

	mBufferLen = length;
	mNumChannels = numchannels;
    mReadIndex = mWriteIndex = 0;
    mNumSamplesInBuffer = 0;
	externalMem = false;
	chunkValid = true;
    
    chunk = (float*)blMalloc(sizeof(float), mBufferLen*mNumChannels, 64);
    for (int i = 0; i < mNumChannels; i++) {
        data[i] = (float*)&chunk[i*mBufferLen];
    }

	clear();
}

BACCH_buffer::BACCH_buffer(SInt32 numchannels, SInt32 length, float *memPtr) {
	mBufferLen = length;
	mNumChannels = numchannels;
	mReadIndex = mWriteIndex = 0;
	mNumSamplesInBuffer = 0;
	externalMem = true;
	chunkValid = true;

	chunk = memPtr;
	for (int i = 0; i < mNumChannels; i++) {
		data[i] = (float*)&chunk[i*mBufferLen];
	}
}

BACCH_buffer::BACCH_buffer(SInt32 numchannels, SInt32 length, float **block_ptrs) {
	mBufferLen = length;
	mNumChannels = numchannels;
	mReadIndex = mWriteIndex = 0;
	mNumSamplesInBuffer = 0;
	externalMem = true;
	chunkValid = false;

	for (int i = 0; i < mNumChannels; i++) {
		data[i] = block_ptrs[i];
	}
}

BACCH_buffer::~BACCH_buffer() {
	if (!externalMem)
		blFree(chunk);
}

void BACCH_buffer::reset() {

    mReadIndex = mWriteIndex = 0;
    mNumSamplesInBuffer = 0;
}

void BACCH_buffer::reset(SInt32 numchannels, SInt32 length) {

	if (length != mBufferLen && !externalMem) {

        blFree(chunk);

		mBufferLen = length;
		mNumChannels = numchannels;

        chunk = (float*)blMalloc(sizeof(float), mBufferLen*mNumChannels, 64);
        for (int i = 0; i < mNumChannels; i++) {
            data[i] = (float*)&chunk[i*mBufferLen];
        }
	}
		
    mReadIndex = mWriteIndex = 0;
    mNumSamplesInBuffer = 0;
}

void BACCH_buffer::clear() {
	if (chunkValid)
		vClear(chunk, mNumChannels*mBufferLen);
	else {
		for (int i = 0; i < mNumChannels; i++) {
			vClear(data[i], mBufferLen);
		}
	}

	mReadIndex = mWriteIndex = 0;
	mNumSamplesInBuffer = 0;
}

void BACCH_buffer::reAlign(void) {

    if (mNumSamplesInBuffer > 0) {
        if (mReadIndex != 0) {
            for (int i = 0; i < mNumChannels; i++) {
                vCopy(&data[i][mReadIndex], data[i], mNumSamplesInBuffer);
            }
        }
    }
    mReadIndex = 0;
    mWriteIndex = mNumSamplesInBuffer;
}

int BACCH_buffer::getNumSamplesInBuffer() {
    return mNumSamplesInBuffer;
}

#include "BACCH_math.h"
#include "BACCH_types.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"
#include <stdlib.h>


#ifndef _MSC_VER
#include <android/log.h>
#endif

#ifdef __USE_IPP__

#include "ipp.h"
#include "ipps.h"
#include "ippcore.h"

#else

#ifdef __USE_NE10__
#include "NE10_dsp.h"
#include "NE10_math.h"
#endif // __USE_NE10__

#endif // __USE_IPP__

#include <cmath>

inline void swap(FFTComplex *a, FFTComplex *b) { 
	FFTComplex t;
	t.re = a->re;
	t.im = a->im;
	a->re = b->re;
	a->im = b->im;
	b->re = t.re;
	b->im = t.im;
}

inline void Set_C(FFTComplex *me, float _re, float _im)
{
	me->re = _re;
	me->im = _im;
}

inline void Set_U(FFTComplex *me, const FFTComplex c)
{
	me->re = c.re;
	me->im = c.im;
}

inline static void Scale(const FFTComplex a, float b, FFTComplex *result)
{
	result->re = a.re * b;
	result->im = a.im * b;
}

inline static void Mul(const FFTComplex a, const FFTComplex b, FFTComplex *result)
{
	// Store temporarily in case a or b reference the same memory as result
	float t = a.re * b.im + a.im * b.re;
	result->re = a.re * b.re - a.im * b.im;
	result->im = t;
}

inline static void Add(const FFTComplex a, const FFTComplex b, FFTComplex *result)
{
	result->re = a.re + b.re;
	result->im = a.im + b.im;
}

inline static void Sub(const FFTComplex a, const FFTComplex b, FFTComplex *result)
{
	result->re = a.re - b.re;
	result->im = a.im - b.im;
}

inline static void MulAdd(const FFTComplex a, const FFTComplex b, const FFTComplex c, FFTComplex *result)
{
	// Store temporarily in case a or b reference the same memory as result
	float t = a.re * b.im + a.im * b.re;
	result->re = c.re + a.re * b.re - a.im * b.im;
	result->im = c.im + t;
}

BACCH_fft::BACCH_fft() noexcept {
	fft_Init(BACCH_FFT_SIZE);
}

BACCH_fft::BACCH_fft(int fftLength) noexcept {
	fft_Init(fftLength);
}

void BACCH_fft::fft_Init(int fftLength) {

#ifdef __USE_IPP__
	int Status = 0, spec_size = 0, init_size = 0, buffer_size = 0;

	int order = (int)log2(fftLength);

	Ipp8u* initBuf;

	mFFTLength = fftLength;

	Status = ippsFFTGetSize_C_32f(order, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, &spec_size, &init_size, &buffer_size);
	if (Status != ippStsNoErr) mThrow("Error in ipps FFT Get Size #1a");

	// allocate memory for the fft spec and work buffers
	ipp_plan_mem = (Ipp8u*)blMalloc(spec_size, 1, 64);
	initBuf = (Ipp8u*)blMalloc(init_size, 1, 64);
	ipp_work_buffer = (Ipp8u*)blMalloc(buffer_size, 1, 64);

	Status = ippsFFTInit_C_32fc(&ipp_plan, order, IPP_FFT_DIV_INV_BY_N, ippAlgHintNone, ipp_plan_mem, initBuf);
	if (Status != ippStsNoErr) return; // mThrow("Block allocation FFT error in allocate_convolution_memory #1b");

	// free init buffer
	if (init_size > 0) blFree(initBuf);
#else

	mFFTLength = fftLength;

#ifdef __USE_NE10__

	cfg = ne10_fft_alloc_c2c_float32(fftLength);
	scratchBuf = (ne10_fft_cpx_float32_t*)blMalloc(sizeof(ne10_fft_cpx_float32_t), fftLength, 64);

#else

	reversetable = (unsigned int**)calloc(1, 32 * sizeof(unsigned int*));
	if (NULL == reversetable)
		return; // mThrow("FFT failed to allocate memory");
#endif// __USE_NE10__
#endif //__USE_IPP__
}

BACCH_fft::~BACCH_fft() {

#ifdef __USE_IPP__
	blFree(ipp_plan);

	blFree(ipp_work_buffer);
#else
#ifdef __USE_NE10__
	ne10_fft_destroy_c2c_float32(cfg);
    blFree(scratchBuf);
#else
    for(int i=0;i<32;i++) {
        if (NULL != reversetable[i]) free(reversetable[i]);
    }
#endif //__USE_NE10__
#endif //__USE_IPP__
}

void BACCH_fft::process(FFTComplex *data, int numsamples, int reverse)
{
	int Status = 0;
#ifdef __USE_IPP__
	if (reverse == BL_FFT_FWD) {
		Status = ippsFFTFwd_CToC_32fc_I((Ipp32fc*)data, ipp_plan, ipp_work_buffer);
		if (Status != ippStsNoErr) mThrow("FFT error in process fwd");
	}
	else if (reverse == BL_FFT_INV) {
		Status = ippsFFTInv_CToC_32fc_I((Ipp32fc*)data, ipp_plan, ipp_work_buffer);
		if (Status != ippStsNoErr) mThrow("FFT error in process inv");
	}
#else

#ifdef __USE_NE10__	
	ne10_fft_cpx_float32_t *mdata = (ne10_fft_cpx_float32_t*)data;
    ne10_fft_cpx_float32_t *dst; // [numsamples];
    dst = scratchBuf;

    ne10_fft_c2c_1d_float32(dst, mdata, cfg, reverse);

    for (int i = 0; i < mFFTLength; i++) {
	    mdata[i].r = dst[i].r;
	    mdata[i].i = dst[i].i;
    }
#else
	unsigned int numbits = 0;
	int count = 1;
	int n, j;
	
	while (count < numsamples)
	{
		count += count;
		++numbits;
	}

	unsigned int* tbl = reversetable[numbits];
	if (tbl == NULL)
	{
		tbl = (unsigned int*)calloc(1, numsamples * sizeof(unsigned int));

		//if (NULL == tbl)
		//	mThrow("FFT failed to allocate memory");

		for ( n = 0; n < numsamples; n++)
		{
			unsigned int j = 1, k = 0, m = numsamples >> 1;
			while (m > 0)
			{
				if (n & m)
					k |= j;
				j += j;
				m >>= 1;
			}
			tbl[n] = k;
		}

		reversetable[numbits] = tbl;
	}

	for (int i = 0; i < numsamples; i++)
	{
		int j = tbl[i];
		if (i < j)
		{
			swap(&data[i], &data[j]);
		}
	}

	float w0 = (reverse == 1) ? kPI : -kPI;
	for (j = 1; j < numsamples; j += j)
	{
		FFTComplex wr, wd;
		wr.re = cos(w0);
		wr.im = sin(w0);
		wd.re = 1.0;
		wd.im = 0.0;
		int step = j + j;
		for (int m = 0; m < j; ++m)
		{
			for (int i = m; i < numsamples; i += step)
			{
				FFTComplex t;
				Mul(wd, data[i + j], &t);
				Sub(data[i], t, &data[i + j]);
				Add(data[i], t, &data[i]);
			}
			Mul(wd, wr, &wd);
		}
		w0 *= 0.5f;
	}
#endif // __USE_NE10__
#endif // __USE_IPP__
}


void BACCH_fft::Forward(FFTComplex* data)
{
	process(data, mFFTLength, BL_FFT_FWD);
}

void BACCH_fft::Inverse(FFTComplex* data )
{	
	process(data, mFFTLength, BL_FFT_INV);

#if !defined __USE_IPP__ && !defined __USE_NE10__
	const float scale = 1.0f / (float)BACCH_FFT_SIZE;
	for (int n = 0; n < BACCH_FFT_SIZE; n++)
	{
		data[n].re *= scale;
		data[n].im *= scale;
	}
#endif // __USE_IPP__
}

void BACCH_fft::Forward(BACCH_complex_buffer *buf) {
	int i = 0;

	for (i = 0; i < buf->mNumChannels; i++) {
		process(buf->data[i], mFFTLength, BL_FFT_FWD);
	}

    buf->mReadIndex = 0;
    buf->mWriteIndex = buf->mNumSamplesInBuffer = mFFTLength;
}

void BACCH_fft::Inverse(BACCH_complex_buffer *buf) {
	int i = 0;
#if !defined __USE_IPP__ && !defined __USE_NE10__
	const float scale = 1.0f / (float)BACCH_FFT_SIZE;
#endif

	for (i = 0; i < buf->mNumChannels; i++) {
		process(buf->data[i], mFFTLength, BL_FFT_INV);
#if !defined __USE_IPP__ && !defined __USE_NE10__
		for (int n = 0; n < mFFTLength; n++)
		{
			buf->data[i][n].re *= scale;
			buf->data[i][n].im *= scale;
		}
#endif // __USE_IPP__
	}
    buf->mReadIndex = 0;
    buf->mWriteIndex = buf->mNumSamplesInBuffer = mFFTLength;
} // BACCH_fft class definition

void vector_add(float** a, float** b, float**dst, int numChannels, int numFrames) {
	int channelIdx = 0, frameIdx = 0;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
#ifdef __USE_IPP__
		ippsAdd_32f((const Ipp32f*)a[channelIdx], (const Ipp32f*)b[channelIdx], (Ipp32f*)dst[channelIdx], numFrames);
#else
#ifdef __USE_NE10__
		ne10_add_float(dst[channelIdx], a[channelIdx], b[channelIdx], numFrames);
#else
		for (frameIdx = 0; frameIdx < numFrames; frameIdx++) {
			dst[channelIdx][frameIdx] = a[channelIdx][frameIdx] + b[channelIdx][frameIdx];
		}
#endif //__USE_NE10__
#endif // __USE_IPP__	
	}
}

void vector_add_i(float** a, float**srcdst, int numChannels, int numFrames) {
	int channelIdx = 0, frameIdx = 0;
#ifdef __USE_IPP__	
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		ippsAdd_32f_I((const Ipp32f*)a[channelIdx], (Ipp32f*)srcdst[channelIdx], numFrames);
	}
#else
#ifdef __USE_NE10__
	ne10_result_t result = NE10_OK;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		// NE10_math.h: 124: This operation can be performed inplace
		result = ne10_add_float(srcdst[channelIdx], a[channelIdx], srcdst[channelIdx], numFrames);
		//if (result != NE10_OK)
		//	mThrow("Bacch_math: ne10_add_float error");
	}
#else
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		for (frameIdx = 0; frameIdx < numFrames; frameIdx++) {
			srcdst[channelIdx][frameIdx] += a[channelIdx][frameIdx];
		}
	}
#endif // __USE_NE10__
#endif // __USE_IPP__	
}

void vector_add(BACCH_buffer* a, BACCH_buffer* b, BACCH_buffer* dst, int numChannels, int numFrames) {

	vector_add(a->data, b->data, dst->data, numChannels, numFrames);
}

void vector_add_i(BACCH_buffer* a, BACCH_buffer* srcdst, int numChannels, int numFrames) {

	vector_add_i(a->data, srcdst->data, numChannels, numFrames);
}

void vector_mul(float** a, float** b, float** dst, int numChannels, int numFrames) {
	int channelIdx = 0;

#ifdef __USE_IPP__
	IppStatus status = ippStsNoErr;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		status = ippsMul_32f(a[channelIdx], b[channelIdx], dst[channelIdx], numFrames);
		if (status != ippStsNoErr) {
			mThrow("vector_mul encountered an error");
		}
	}
#else
#ifdef __USE_NE10__
	ne10_result_t result = NE10_OK;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		result = ne10_mul_float(dst[channelIdx], a[channelIdx], b[channelIdx], numFrames);
		//if (result != NE10_OK)
		//	mThrow("ne10_mul_float error");
	}
#else
    int sampleIdx = 0;
    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        for (sampleIdx = 0; sampleIdx < numFrames; sampleIdx++) {
            dst[channelIdx][sampleIdx] = a[channelIdx][sampleIdx] * b[channelIdx][sampleIdx];
        }
    }
#endif
#endif // __USE_IPP__
}

void vector_mul_i(float** a, float** srcdst, int numChannels, int numFrames) {
	int channelIdx = 0;

#ifdef __USE_IPP__
	IppStatus status = ippStsNoErr;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		status = ippsMul_32f_I(a[channelIdx], srcdst[channelIdx], numFrames);
		if (status != ippStsNoErr) {
			mThrow("vector_mul_i encountered an error");
		}
	}
#else
#ifdef __USE_NE10__
	ne10_result_t result = NE10_OK;
	for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		result = ne10_mul_float(srcdst[channelIdx], a[channelIdx], srcdst[channelIdx], numFrames);
		//if (result != NE10_OK) {
		//	mThrow("Bacch_math: ne10_mul_float encountered an error");
		//}
	}
#else
    int sampleIdx = 0;
    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        for (sampleIdx = 0; sampleIdx < numFrames; sampleIdx++) {
            srcdst[channelIdx][sampleIdx] *= a[channelIdx][sampleIdx];
        }
    }
#endif // __USE_NE10__
#endif // __USE_IPP__
}

void bufComplexMultiply(BACCH_complex_buffer* const a, BACCH_complex_buffer* const b, BACCH_complex_buffer* dst, int numChannels, int numFrames) {
    if ((dst->mNumChannels >= numChannels) &&
        ((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numFrames)) {
        for (int i = 0; i < numChannels; i++) {
            vector_mul_c(a->data[i], b->data[i], dst->data[i], numFrames);
        }
        dst->mNumSamplesInBuffer = dst->mWriteIndex = numFrames;
    }
}

void vector_mul_c(const FFTComplex *a, const FFTComplex *b, FFTComplex* dst, int numFrames) {
#ifdef __USE_IPP__
	IppStatus status = ippStsNoErr;
	status = ippsMul_32fc((Ipp32fc*)a, (Ipp32fc*)b, (Ipp32fc*)dst, numFrames);
	if (status != ippStsNoErr) {
		mThrow("vector_mul_c encountered an error");
	}
#else
float	t;
// Store temporarily in case a or b reference the same memory as result
for (int idx = 0; idx < numFrames; idx++) {
	t = a[idx].re * b[idx].im + a[idx].im * b[idx].re;
	dst[idx].re = a[idx].re * b[idx].re - a[idx].im * b[idx].im;
	dst[idx].im = t;
}
#endif // __USE_IPP__
}

void vector_mul_i_c(const FFTComplex *a, FFTComplex *srcdst, int numFrames) {
#ifdef __USE_IPP__
	IppStatus status = ippStsNoErr;
	status = ippsMul_32fc_I((Ipp32fc*)a, (Ipp32fc*)srcdst, numFrames);
	if (status != ippStsNoErr) {
		mThrow("vector_mul_i_c encountered an error");
	}
#else
	float	tr, ti;
	// Store temporarily in case a or b reference the same memory as result
	for (int idx = 0; idx < numFrames; idx++) {
		ti = a[idx].re * srcdst[idx].im + a[idx].im * srcdst[idx].re;
		tr = a[idx].re * srcdst[idx].re - a[idx].im * srcdst[idx].im;
		srcdst[idx].re = tr;
		srcdst[idx].im = ti;
	}
#endif // __USE_IPP__
}

void vector_mul_accum_i_c(const FFTComplex *a, const FFTComplex *b, FFTComplex* srcdst, int numFrames) {
#ifdef __USE_IPP__
	IppStatus status = ippStsNoErr;
	status = ippsAddProduct_32fc((Ipp32fc*)a, (Ipp32fc*)b, (Ipp32fc*)srcdst, numFrames);
	if (status != ippStsNoErr) {
		mThrow("vector_mul_c encountered an error");
	}
#else
	float t;
	for (int idx = 0; idx < numFrames; idx++) {
		// Store temporarily in case a or b reference the same memory as result
		t = a[idx].re * b[idx].im + a[idx].im * b[idx].re;
		srcdst[idx].re = srcdst[idx].re + a[idx].re * b[idx].re - a[idx].im * b[idx].im;
		srcdst[idx].im = srcdst[idx].im + t;
	}
#endif // __USE_IPP__
}


#include "BACCH_complex_buffer.h"
#include "BACCH_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_types.h"
//#include <malloc.h>
#include "BACCH_mem.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif

BACCH_complex_buffer::BACCH_complex_buffer(SInt32 numchannels, SInt32 length) {
    mNumChannels = numchannels;
    mBufferLen = length;

    for (int i = 0; i < MAX_IN_CHANNELS; i++) {
        data[i] = NULL;
    }

    // create a block of storage for numChannels
    chunk = (float*)blMalloc(sizeof(float), mBufferLen*mNumChannels*COMPLEX_WORDS, 128);
    if (NULL != chunk) {
        for (int i = 0; i < mNumChannels; i++) {
            data[i] = (FFTComplex*)&chunk[i*mBufferLen*COMPLEX_WORDS];
        }
    }
	
	mNumSamplesInBuffer = 0;
	mReadIndex = mWriteIndex = 0;
}

BACCH_complex_buffer::~BACCH_complex_buffer() {
  
    blFree(chunk);
}

void BACCH_complex_buffer::reset() {
    mNumSamplesInBuffer = 0;
    mReadIndex = mWriteIndex = 0;
}

void BACCH_complex_buffer::reset(SInt32 numchannels, SInt32 length) {

	if ((length != mBufferLen) || (numchannels != mNumChannels)) {

		// first delete old buffers
        blFree(chunk);

		mBufferLen = length;
		mNumChannels = numchannels;

        // create a block of storage for numChannels
        chunk = (float*)blMalloc(sizeof(float), mBufferLen*mNumChannels*COMPLEX_WORDS, 128);
        if (NULL != chunk) {
            for (int i = 0; i < mNumChannels; i++) {
                data[i] = (FFTComplex*)&chunk[i*mBufferLen*COMPLEX_WORDS];
            }
        }
		
		// set state
		mNumSamplesInBuffer = 0;
		mReadIndex = mWriteIndex = 0;
	}
		
	clear();
}

void BACCH_complex_buffer::clear() {
	vClear(chunk, mNumChannels * mBufferLen * COMPLEX_WORDS);
	mReadIndex = mWriteIndex = 0;
	mNumSamplesInBuffer = 0;
}

void BACCH_complex_buffer::reAlign() {

    if (mNumSamplesInBuffer > 0) {
        if (mReadIndex != 0) {
            for (int i = 0; i < mNumChannels; i++) {
                vCopy((float*)&data[i][mReadIndex], (float*)data[i], mNumSamplesInBuffer * COMPLEX_WORDS);
            }
        }
    }
    mReadIndex = 0;
    mWriteIndex = mNumSamplesInBuffer;
}

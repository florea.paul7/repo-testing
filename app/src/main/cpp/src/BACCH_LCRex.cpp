#include "BACCH_LCRex.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_mem.h"
#include "BACCH_cx.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif


BACCH_LCRex::BACCH_LCRex(int sampleRate)
{
    int i = 0, j = 0;

    int sampleRateIndex = validateSampleRate(sampleRate);
   //if (sampleRateIndex < 0)
    //    mThrow("Bacch_LCRex: Invalid Sample Rate passed to constructor");

    mSampleRate = sampleRate;

    bcx = new BACCH_cx(sampleRate);

	inChunk = (float*)blMalloc(sizeof(float), BACCH_FRAME_SIZE*2, 64);
	outChunk = (float*)blMalloc(sizeof(float), BACCH_FRAME_SIZE*2, 64);
    cChunk = (float*)blMalloc(sizeof(float), BACCH_FRAME_SIZE*2, 64);

	vClear(inChunk, BACCH_FRAME_SIZE);
	vClear(outChunk, BACCH_FRAME_SIZE);
}

void BACCH_LCRex::configure(int samplerate)
{
	int samplerateIndex = validateSampleRate(samplerate);
	//if (samplerateIndex < 0)
	//	mThrow("Bacch_LCRex: Invalid Sample Rate passed to configure");

	mSampleRate = samplerate;
	bcx->configure(samplerate);
}

void BACCH_LCRex::reset()
{
	clear();
}

void BACCH_LCRex::clear()
{
    // do nothing
}

BACCH_LCRex::~BACCH_LCRex()
{
    //S9crossfs_free();
    blFree(inChunk);
    blFree(outChunk);
    blFree(cChunk);
}

void BACCH_LCRex::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, BACCH_buffer* coutbuffer) {
    status_t mStatus = STATUS_OK;
	int outSize[2];
	int cOutSize[2];

    //vCopy(inbuffer->data[0], inChunk, BACCH_FRAME_SIZE);
    //vCopy(inbuffer->data[1], &inChunk[BACCH_FRAME_SIZE], BACCH_FRAME_SIZE);

    bcx->process(inbuffer, outbuffer, coutbuffer);

    //vCopy(outChunk, outbuffer->data[0], BACCH_FRAME_SIZE);
    //vCopy(&outChunk[BACCH_FRAME_SIZE], outbuffer->data[1], BACCH_FRAME_SIZE);

    //vCopy(cChunk, coutbuffer->data[0], BACCH_FRAME_SIZE);

    /******************** SPECIAL CASE BUFFER MGMT ***********************/
    // process chain above does not manipulate the inbuffer or outbuffer buffer metrics
    inbuffer->reset();
    outbuffer->mWriteIndex = outbuffer->mNumSamplesInBuffer = BACCH_FRAME_SIZE;
	coutbuffer->mWriteIndex = coutbuffer->mNumSamplesInBuffer = BACCH_FRAME_SIZE;

    return;
}


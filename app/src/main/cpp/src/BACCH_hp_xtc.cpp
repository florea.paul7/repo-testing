#include "BACCH_hp_xtc.h"
#include "BACCH_HP_XTC_coeffs.h"
#include "BACCH_HP_XTC_constants.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_mem.h"


#ifndef _MSC_VER
#include <android/log.h>
#endif

// in all cases, we lead with a fft size of 2048, this sets the latency at 1024 samples

// 
// eq IRs are [samplerate][volumeStep][channel][ir]
// we will load the TFs on a channel basis, so we will load 
// TFs[0][all blocks] with IR[samplerate][volumestep][0] (left in to left out)
// and IR[samplerate][volumestep][1] (right in to right out)
void BACCH_hp_xtc::FilterLoad() {
    int volumeIdx = 0, irBlockIdx = 0;
	int offset = 0;
    //const float *data[2];

    // load IRs and FFT to generate TFs
    for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
        TFs[BACCH_LEFT][irBlockIdx]->clear();
		TFs[BACCH_RIGHT][irBlockIdx]->clear();

        // load the impulse response data
        vRealToComplex((float*)&hp_xtc_irs[mSampleRateIdx][volumeIdx][LEFT_TO_LEFT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);
        vRealToComplex((float*)&hp_xtc_irs[mSampleRateIdx][volumeIdx][LEFT_TO_RIGHT][offset], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);

		vRealToComplex((float*)&hp_xtc_irs[mSampleRateIdx][volumeIdx][RIGHT_TO_LEFT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);
		vRealToComplex((float*)&hp_xtc_irs[mSampleRateIdx][volumeIdx][RIGHT_TO_RIGHT][offset], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);

		offset += BACCH_FRAME_SIZE;

        fft->Forward(TFs[BACCH_LEFT][irBlockIdx]);
		fft->Forward(TFs[BACCH_RIGHT][irBlockIdx]);
    }
}


void BACCH_hp_xtc::configure(int samplerate, int filterNum)
{
	int samplerateIndex = validateSampleRate(samplerate);

	if ((samplerateIndex != mSampleRateIdx) || (filterNum != mVolumeIdx)) {

		//if (samplerateIndex < 0)
		//	mThrow("Bacch_hp_xtc: Invalid Sample Rate passed to configure");
		mSampleRateIdx = samplerateIndex;

        // will use when we support volume based HP_XTC
		//if ((filterNum < 0) || (filterNum >= HP_XTC_MAX_FILTERNUM))
		//	mThrow("Bacch_hp_xtc: Invalid Filter Number passed to configure");
		mVolumeIdx = filterNum;
		mNumIRBlocks = HP_XTC_IR_NUM_BLOCKS[samplerateIndex];

		FilterLoad();
	}
}

void BACCH_hp_xtc::reset()
{
	clear();
}

void BACCH_hp_xtc::clear()
{
	int i = 0;
	//mOut->clear();
	for (i = 0; i < HP_XTC_IR_NUM_BLOCKS[2]; i++) {
		    mPad[i]->clear();
	}
}

BACCH_hp_xtc::BACCH_hp_xtc(int sampleRate, int filterNum)
{
	int i = 0, j = 0;
	
	// initialize an instance of the fft module
	fft = new BACCH_fft(BACCH_FFT_SIZE);

	int sampleRateIndex = validateSampleRate(sampleRate);
    //if (sampleRateIndex < 0)
    //    mThrow("Bacch_hp_xtc: Invalid Sample Rate passed to constructor");
    mSampleRateIdx = sampleRateIndex;

    // will use when we support volume based HP_XTC
    //if ((filterNum < 0) || (filterNum >= HP_XTC_MAX_FILTERNUM))
    //    mThrow("Bacch_hp_xtc: Invalid Filter Number passed to constructor");
    mVolumeIdx = filterNum;

    mNumIRBlocks = HP_XTC_IR_NUM_BLOCKS[mSampleRateIdx];
    
	// allocate memory
	// transfer function buffers, 2 channels out for 2 potential sources
	// n TFs per source, where n is determined by array in coeffs header file 
	mPad = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*), 32, 32);
	ScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 32, 32);
	mPadIdx = (int*)blMalloc(sizeof(int), 32, 32);
	TFs = (BACCH_complex_buffer***)blMalloc(sizeof(BACCH_complex_buffer**), NUM_OUT_CHANNELS, 32);
	//if ((NULL == TFs) ||(NULL == mPad) || (NULL == ScratchFFT) || (NULL == mPadIdx))
    //    mThrow("Bacch_hp_xtc: constructor failed to allocate memory");

	for (i = 0; i < NUM_OUT_CHANNELS; i++) {
		TFs[i] = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), HP_XTC_IR_NUM_BLOCKS[2], 32);
		for (j = 0; j < HP_XTC_IR_NUM_BLOCKS[2]; j++) {
			TFs[i][j] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		}
	}
	for (j = 0; j < HP_XTC_IR_NUM_BLOCKS[2]; j++) {
		mPad[j] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		ScratchFFT[j] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		mPadIdx[j] = j;
	}
	
	ScratchIn = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);

	// load the filters
	FilterLoad();
}

BACCH_hp_xtc::~BACCH_hp_xtc()
{
	int i = 0, j = 0;
	
	delete fft;
	delete ScratchIn;

	for (j = 0; j < HP_XTC_IR_NUM_BLOCKS[2]; j++) {
		delete TFs[j];
		delete ScratchFFT[j];
		delete mPad[j];
	}
	blFree(TFs);
	blFree(ScratchFFT);
	blFree(mPad);
    blFree(mPadIdx);
}

// receives input, kicks off tail processing, completes head processing, accumulates with previous tail processing and returns
void BACCH_hp_xtc::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer)
{
    int iRet = 0;
    int irBlockIdx = 0, irIdx = 0;

    // acquire lock on data

    // clear scratch buffers for IR head processing
    ScratchIn->clear();

    // write input data into scratch buffer
    cbufRealtoCplx(inbuffer, ScratchIn, NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
   
    // copy pad data to output (last run's overlap/tail)
    bufCopy(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

    // have to clear here, all irBlocks will accumulate into each pad buffer
    mPad[mPadIdx[0]]->clear();

    // swap the pad index to next buffer since current idx=0 is consumed
    for (int padIdx = 0; padIdx < mNumIRBlocks; padIdx++) {
        mPadIdx[padIdx] --;
        if (mPadIdx[padIdx] < 0 )
            mPadIdx[padIdx] = mNumIRBlocks - 1;
    }

    fft->Forward(ScratchIn);

    for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
        // no need to clear for HP_XTC, only one IR per channel, so not accumulating
        ScratchFFT[irBlockIdx]->reset();
        // first vector complex multiply sets the data in ScratchFFT for the current IR Block Index
        //bufComplexMultiply(ScratchIn, TFs[irBlockIdx], ScratchFFT[irBlockIdx], NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
        vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
        vector_mul_c(ScratchIn->data[BACCH_LEFT], TFs[BACCH_LEFT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right

		vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // + right to left
		vector_mul_accum_i_c(ScratchIn->data[BACCH_RIGHT], TFs[BACCH_RIGHT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // + right to right
        
        // inverse fft
        fft->Inverse(ScratchFFT[irBlockIdx]);

        // store results in pad buffers
        cbufCplxtoReal(ScratchFFT[irBlockIdx], mPad[mPadIdx[irBlockIdx]], NUM_OUT_CHANNELS, BACCH_FFT_SIZE, true);
    }

	// add the pad buffer data to the output buffer
    bufAdd_I(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

}



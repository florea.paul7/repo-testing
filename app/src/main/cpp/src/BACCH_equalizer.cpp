#include "BACCH_equalizer.h"
#include "BACCH_sp.h"
#include "BACCH_EQ_coeffs.h"
#include "BACCH_EQ_constants.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_mem.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif

// in all cases, we lead with a fft size of 2048, this sets the latency at 1024 samples

// 
// eq IRs are [samplerate][volumeStep][channel][ir]
// we will load the TFs on a channel basis, so we will load 
// TFs[0][all blocks] with IR[samplerate][volumestep][0] (left in to left out)
// and IR[samplerate][volumestep][1] (right in to right out)
void BACCH_equalizer::FilterLoad() {
    int volumeIdx = 0;

    // load IRs and FFT to generate TFs
	TFs->clear();

    vRealToComplex((float*)eq_irs[mSampleRateIdx][volumeIdx][CHANNEL_TO_LEFT], TFs->data[BACCH_LEFT], mIRLength);
    vRealToComplex((float*)eq_irs[mSampleRateIdx][volumeIdx][CHANNEL_TO_RIGHT], TFs->data[BACCH_RIGHT], mIRLength);

    fft->Forward(TFs);
}


void BACCH_equalizer::configure(int samplerate, int filterNum)
{
	int samplerateIndex = validateSampleRate(samplerate);

	if ((samplerateIndex != mSampleRateIdx) || (filterNum != mVolumeIdx)) {

		//if (samplerateIndex < 0)
		//	mThrow("Bacch_Equalizer: Invalid Sample Rate passed to configure");
		mSampleRateIdx = samplerateIndex;
		mIRLength = EQ_IR_LENGTHS[mSampleRateIdx];
		mFFTLength = mIRLength * 2;

        // will use when we support volume based EQ
		//if ((filterNum < 0) || (filterNum >= EQ_MAX_FILTERNUM))
		//	mThrow("Bacch_Equalizer: Invalid Filter Number passed to configure");
		mVolumeIdx = filterNum;

		FilterLoad();
	}
}

void BACCH_equalizer::reset()
{
	clear();
}

void BACCH_equalizer::clear()
{
	int i = 0;
	mPad->clear();
}

BACCH_equalizer::BACCH_equalizer(int sampleRate, int filterNum)
{
	int i = 0, j = 0;

	int sampleRateIndex = validateSampleRate(sampleRate);
    //if (sampleRateIndex < 0)
       // mThrow("Bacch_equalizer: Invalid Sample Rate passed to constructor");
    mSampleRateIdx = sampleRateIndex;
	mIRLength = EQ_IR_LENGTHS[mSampleRateIdx];
	mFFTLength = mIRLength * 2;
	
	// initialize an instance of the fft module
	fft = new BACCH_fft(mFFTLength);

    // will use when we support volume based EQ
    //if ((filterNum < 0) || (filterNum >= EQ_MAX_FILTERNUM))
    //    mThrow("Bacch_Equalizer: Invalid Filter Number passed to constructor");
    mVolumeIdx = filterNum;

    mNumIRBlocks = EQ_IR_NUM_BLOCKS[mSampleRateIdx];
    
	// allocate memory
	TFs = new BACCH_complex_buffer(NUM_OUT_CHANNELS, mFFTLength);
	mPad = new BACCH_buffer(NUM_OUT_CHANNELS, mFFTLength);
	ScratchFFT = new BACCH_complex_buffer(NUM_OUT_CHANNELS, mFFTLength);
	ScratchIn = new BACCH_complex_buffer(NUM_OUT_CHANNELS, mFFTLength);

	// load the filters
	FilterLoad();
}

BACCH_equalizer::~BACCH_equalizer()
{	
	delete fft;
	delete ScratchIn;

	delete TFs;
	delete ScratchFFT;
	delete mPad;
}

// receives input, kicks off tail processing, completes head processing, accumulates with previous tail processing and returns
void BACCH_equalizer::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, bacch_sp_orientation_t orientation)
{
    int iRet = 0;
    int irBlockIdx = 0, irIdx = 0;
	int numSamples = inbuffer->getNumSamplesInBuffer();

    // acquire lock on data

    // clear scratch buffers for IR head processing
    ScratchIn->clear();

    // write input data into scratch buffer
    cbufRealtoCplx(inbuffer, ScratchIn, NUM_OUT_CHANNELS, numSamples);
   
    // copy pad data to output (last run's overlap/tail)
    bufCopy(mPad, outbuffer, numSamples);
	vClear(&mPad->data[0][mPad->getNumSamplesInBuffer()], mPad->mBufferLen - mPad->getNumSamplesInBuffer());
	vClear(&mPad->data[1][mPad->getNumSamplesInBuffer()], mPad->mBufferLen - mPad->getNumSamplesInBuffer());

    fft->Forward(ScratchIn);

   // no need to clear for EQ, only one IR per channel, so not accumulating
    ScratchFFT->reset();

	bufComplexMultiply(ScratchIn, TFs, ScratchFFT, NUM_OUT_CHANNELS, mFFTLength);

    // inverse fft
    fft->Inverse(ScratchFFT);

    // store results in pad buffers
    cbufCplxtoReal(ScratchFFT, mPad, NUM_OUT_CHANNELS, mFFTLength, true);

	// add the pad buffer data to the output buffer
    bufAdd_I(mPad, outbuffer, numSamples);

}



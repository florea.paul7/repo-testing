#include "BACCH_slide_manager.h"
#include "BACCH_HP_constants.h"
#include "BACCH_GALAXY_TABS_constants.h"
#include "BACCH_EQ_constants.h"
#include "BACCH_constants.h"
#include "BACCH_mem.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"

#include <math.h>

/************************* class definition: slide_info **********************************/

slide_info::slide_info() noexcept {
    initialized = false;
}
slide_info::slide_info(int numIrs, float span, float degrees) {
    halfSpan = span/2.0f;
    degreesPerIR = span / (float)(numIrs-1);
    initialized = true;
	numIRs = numIrs;
    setDegrees(degrees);
}

void slide_info::setInfo(slide_info *info) {
    if (info->initialized) {
        degrees =       info->degrees;
        ir =            info->ir;
        onIR =          info->onIR;
        degreesPerIR =  info->degreesPerIR;
        halfSpan =      info->halfSpan;
        lowIR =         info->lowIR;
        highIR =        info->highIR;
        lowIRGain =     info->lowIRGain;
        highIRGain =    info->highIRGain;
        singleIR =      info->singleIR;
		numIRs =		info->numIRs;
        initialized =   true;
    }
    //else
    //    mThrow("slide_info: cannot setInfo with uninitialized object");
}

void slide_info::setIR(float IR) {

	float tmpF = 0.0f;
	if ((IR >= 0) && (IR <= (numIRs-1))) {
		tmpF = IR - floorf(IR);
		if (FLOAT_IS_ZERO(tmpF)) {
			singleIR = floorf(IR);
			lowIR = singleIR;
			highIR = lowIR + 1; // -1.0f;
			onIR = true;
		} else if (FLOAT_IS_ONE(tmpF)) {
			singleIR = ceilf(IR);
			onIR = true;
			lowIR = singleIR;
			highIR = lowIR + 1; // -1.0f;
		}
		else {
			onIR = false;
			lowIR = (int)floorf(ir);
			highIR = lowIR + 1;
			lowIRGain = 1.0f - tmpF;
			highIRGain = tmpF;
		}
	}
}

void slide_info::setDegrees(float val) {
    //if (!initialized)
       // mThrow("slide_info: cannot set degrees in uninitialized object");

    float newDegrees = val;
	float tmpIR = 0;
    if (newDegrees > halfSpan) newDegrees = halfSpan;
    else if (newDegrees < -halfSpan) newDegrees = -halfSpan;

    degrees = newDegrees;
    // convert to IR terms
    ir = (newDegrees + halfSpan) / degreesPerIR;
	setIR(ir);

}


/************************* class definition: BACCH_slide_manager **************************/

BACCH_slide_manager::BACCH_slide_manager(slide_module_t val, int sampleRateIndex)
{
    if (val >= 0 && val < SLIDE_LAST)
        module = val;
    //lse
       // mThrow("Slide Manager: Invalid module index in initialization");
        
    switch (module) {
        case SLIDE_HP: {
            numIRs = HP_NUM_IRS;
            numPlusMinusIRs = HP_IR_NUM_BLOCKS[sampleRateIndex];
            currentIR = new slide_info(HP_NUM_IRS, HP_SPAN, 0.0f);
            targetIR = new slide_info(HP_NUM_IRS, HP_SPAN, 0.0f);
        }break;
        case SLIDE_SP: {
            numIRs = GALAXY_TABS_NUM_IRS;
            numPlusMinusIRs = GALAXY_TABS_IR_NUM_BLOCKS[sampleRateIndex];
            currentIR = new slide_info(GALAXY_TABS_NUM_IRS, GALAXY_TABS_SPAN, 0.0f);
            targetIR = new slide_info(GALAXY_TABS_NUM_IRS, GALAXY_TABS_SPAN, 0.0f);
        } break;
            // EQ slide mechanism overloaded to support volume changes smoothly
        case SLIDE_EQ: {
            numIRs = EQ_NUM_IRS;
            numPlusMinusIRs = EQ_IR_NUM_BLOCKS[sampleRateIndex];
            currentIR = new slide_info(EQ_NUM_IRS, EQ_SPAN, 0.0f);
            targetIR = new slide_info(EQ_NUM_IRS, EQ_SPAN, 0.0f);
        } break;
        default:
            numIRs = HP_NUM_IRS;
            numPlusMinusIRs = HP_IR_NUM_BLOCKS[sampleRateIndex];
            currentIR = new slide_info(HP_NUM_IRS, HP_SPAN, 0.0f);
            targetIR = new slide_info(HP_NUM_IRS, HP_SPAN, 0.0f);
    }

    dirty = false;
    inProgress = false;
    targetReached = true;
    degreesPerIR[SLIDE_UP] = currentIR->degreesPerIR;
    degreesPerIR[SLIDE_DOWN] = -currentIR->degreesPerIR;

    // isStatic = false will force through setActiveIRs
    isStatic = false;

    staticGain[0] = 1.0f;
    staticGain[1] = 0.0f;

    for (int i = 0; i < numIRs; i++)
        activeIRs[i] = IR_INACTIVE;

    // setup buffers
    for (int i = 0; i < 3; i++) {
        mFade[i] = (float*)blMalloc(sizeof(float), BACCH_FRAME_SIZE * 2, 64);
        slidingGain[i] = (float*)blMalloc(sizeof(float), BACCH_FRAME_SIZE, 64);
        outputIRs[i] = IR_INACTIVE;
    }
    outputIRs[3] = IR_INACTIVE;
	/***************************************************************************
	* modification for slide using constant power curves
	* must be included with 'always rest on IR' modification
	***************************************************************************/
//#define CONST_POWER 
#ifdef CONST_POWER
	float tmpF = 0.0f; 
	for (int i = 0; i < BACCH_FRAME_SIZE; i++) {
		tmpF = ((float)PI * ((float)i / (float)(BACCH_FRAME_SIZE - 1))) / 2.0f;
		mFade[RAMP_UP][i] = sinf(tmpF);
		mFade[RAMP_DOWN][i] = cosf(tmpF);
	}
#else
    float tmpf = 0.0f;
    for (int i = 0; i < BACCH_FRAME_SIZE; i++) {
        tmpf = (float)i / (float)(BACCH_FRAME_SIZE - 1);
        mFade[RAMP_UP][i] = tmpf; 
        mFade[RAMP_DOWN][BACCH_FRAME_SIZE - i - 1] = tmpf;
    }
#endif
    // fade up starts at 0 and moves to 1 within BACCH_FRAME_SIZE, then 1 for BACCH_FRAME_SIZE
    vCopy(mFade[RAMP_UP], mFade[RAMP_UP_DOWN], BACCH_FRAME_SIZE);
    vSet(&mFade[RAMP_UP][BACCH_FRAME_SIZE], 1.0f, BACCH_FRAME_SIZE);

    // fade down starts at 1 and moves to 0 within BACCH_FRAME_SIZE, then 0 for BACCH_FRAME_SIZE
    vCopy(mFade[RAMP_DOWN], &mFade[RAMP_UP_DOWN][BACCH_FRAME_SIZE], BACCH_FRAME_SIZE);
    vClear(&mFade[RAMP_DOWN][BACCH_FRAME_SIZE], BACCH_FRAME_SIZE);
}


BACCH_slide_manager::~BACCH_slide_manager()
{
    for (int i = 0; i < 3; i++) {
        blFree(mFade[i]);
        blFree(slidingGain[i]);
    }
}

void BACCH_slide_manager::reset() {
    currentIR->setDegrees(0.0f);
    targetIR->setDegrees(0.0f);
    targetReached = true;
    slide();
}

void BACCH_slide_manager::setTarget(float targetInDegrees, bool force)
{
	float targetID = targetInDegrees;
	if (targetID > currentIR->halfSpan)
		targetID = currentIR->halfSpan;
	else if (targetID < -currentIR->halfSpan)
		targetID = -currentIR->halfSpan;

	float diff = fabsf(targetID - targetIR->degrees);
	if (FLOAT_IS_ZERO(diff)) // no change
		return;

    slide_info newTarget;
    // initialize the new target object
    newTarget.setInfo(targetIR);
    // update with the requested target
    newTarget.setDegrees(targetID);

	float compareVal = (isSP) ? 5.0f: MIN_FILTER_DELTA;
    if ((fabsf(currentIR->ir - newTarget.ir) >= MIN_FILTER_DELTA) || force) {	

		targetIR->setInfo(&newTarget);
        dirty = true;
        isStatic = false;
        targetReached = false;
	}
	else {
		// exception to MIN_IR_DELTA - the extremes
		float test = fabsf(newTarget.degrees) - newTarget.halfSpan;		
		if (FLOAT_IS_ZERO(test)) {
			targetIR->setInfo(&newTarget);
			dirty = true;
			isStatic = false;
			targetReached = false;
		}
	}
}

bool BACCH_slide_manager::slide()
{
    bool bRet = false;
    // any work to do?
    if (targetReached) {
        if (!isStatic) {
            setStaticMode();
        }
        return true;
    }

    slide_info lastIR;

	float nextStop = 0.0f;
	float nextDegrees = 0.0f;
    
    // grab current IR info
    lastIR.setInfo(currentIR);

    // directionIsDown is movement, will be used as an index following
    directionIsDown = (targetIR->ir > lastIR.ir) ? SLIDE_UP : SLIDE_DOWN;

	// slide to next IR (even if less than degreesPerIr step)
	// if currently on an IR
	// and up, next IR will be currentIR->lowIR  + 1
	// and down, next IR be currentIR->lowIR - 1
	nextStop = currentIR->lowIR;
	if (currentIR->onIR) {
		if (directionIsDown)
			nextStop = currentIR->lowIR - 1;
		else
			nextStop = currentIR->lowIR + 1;
	}
	else 	// if currently straddling....
	{		// and up, next IR = currentIR->lowIR + 1
			// and down, next IR will be currentIR->lowIR
		if (directionIsDown)
			nextStop = currentIR->lowIR;
		else
			nextStop = currentIR->highIR;
	}

	if (nextStop < 0) nextStop = 0;
	else if (nextStop > (numIRs-1)) nextStop = numIRs - 1;
	// calculate degrees for nextStop
	nextDegrees = (nextStop * currentIR->degreesPerIR) - currentIR->halfSpan;

	// check to see if target is reached (or passed)
	if (directionIsDown) {
		if (nextDegrees < targetIR->degrees) {
			nextDegrees = targetIR->degrees;
			targetReached = true;
		}
	}
	else {
		if (nextDegrees > targetIR->degrees) {
			nextDegrees = targetIR->degrees;
			targetReached = true;
		}
	}
	if (FLOAT_IS_ZERO(nextDegrees - targetIR->degrees)) {
		targetReached = true;
	}
		
	//update currentIR with nextDegrees
	currentIR->setDegrees(nextDegrees);


       // generate ramps/curves
    int numContributors = buildRamps(&lastIR, currentIR);

    // set active IRs
    setActiveIRs((directionIsDown) ? currentIR->lowIR : lastIR.lowIR, numContributors);


    return dirty;
}
 
// support functions
// return index in the buffer to start a ramp at the supplied value
int getRampIndex(int buffer, float val) {
    int iRet = 0;
    int index = 0;
    switch (buffer) {
    case RAMP_DOWN: {
        index = (int)((1.0f - val) * (float)BACCH_FRAME_SIZE);
        iRet = index;
    }break;
    case RAMP_UP:
    case RAMP_UP_DOWN: {
        index = (int)(val * (float)BACCH_FRAME_SIZE);
        iRet = index;
    }break;
    }
    return iRet;
}


// takes the last position and the next position and generates the necessary IR gain multiplier arrays to smoothly transition
// returns the number of IRs contributing to output
int BACCH_slide_manager::buildRamps(slide_info *last, slide_info *next) {// float *out, float startVal, float endVal, int shape, bool zerosFirst, int numZerosFirst) {
    int iRet = 2;

    // sliding from last IR to next IR
	// if in decreasing IR index:
	//		next low index IR will be increasing from zero to one, zero to gain, gain to gain, or gain to one
	//		next high index IR will be increasing from zero to one, zero to gain, gain, to gain, or gain to one
    // are two positions straddling an IR, straddle only if both are not single output AND are in between different IRs
    // if not straddling, have to be careful about low and high IR selection, it will be based on direction and could be off by one.
   
	if (directionIsDown) {
		outputIRs[0] = next->lowIR;		// low index IR
		outputIRs[1] = next->lowIR + 1;	// high index IR
		outputIRs[2] = IR_INACTIVE;

		
		// sliding with decreasing IR index
        buildRamp(slidingGain[0], (last->onIR) ? 0.0f : last->lowIRGain, (next->onIR) ? 1.0f : next->lowIRGain, RAMP_UP);
        buildRamp(slidingGain[1], (last->onIR) ? 1.0f : last->highIRGain, (next->onIR) ? 0.0f : next->highIRGain, RAMP_DOWN);
	}
	else { // moving from last->lowIR to lowIR + 1
		outputIRs[0] = last->lowIR;		// low index IR
		outputIRs[1] = last->lowIR + 1;	// high index IR

		outputIRs[2] = IR_INACTIVE;

		// sliding with increasing IR index
        buildRamp(slidingGain[0], (last->onIR) ? 1.0f : last->lowIRGain, (next->onIR) ? 0.0f : next->lowIRGain, RAMP_DOWN);
        buildRamp(slidingGain[1], (last->onIR) ? 0.0f : last->highIRGain, (next->onIR) ? 1.0f : next->highIRGain, RAMP_UP);
	}
 
    return iRet;
}

void BACCH_slide_manager::buildRamp(float *out, float startVal, float endVal, int shape, bool zerosFirst, float zerosTo) {

    int idx1 = 0, idx2 = 0, rampLength = 0, tailLength = 0, numZeros = 0;

    // populate based on requested shape
    switch (shape) {
    case RAMP_DOWN: {
        if (FLOAT_IS_ZERO(startVal - endVal)) {       // ramp from 0 to 1
            vCopy(mFade[RAMP_DOWN], out, BACCH_FRAME_SIZE);
        }
        else {
            if (FLOAT_IS_ZERO(endVal)) {
                if (FLOAT_IS_ONE(startVal)) {	// ramp from 1 to 0
                    vCopy(mFade[RAMP_DOWN], out, BACCH_FRAME_SIZE);
                }
                else {	// ramp from val to 0
                    idx1 = getRampIndex(RAMP_DOWN, startVal);
                    vCopy(&mFade[RAMP_DOWN][idx1], out, BACCH_FRAME_SIZE);
                }
            }
            else { 	// ramp to val, then val
                if (FLOAT_IS_ONE(startVal)) {
                    if (FLOAT_IS_ONE(endVal)) {
                        vCopy(mFade[RAMP_DOWN], out, BACCH_FRAME_SIZE);
                    }
                    else {
                        idx1 = getRampIndex(RAMP_DOWN, endVal);
                        idx2 = BACCH_FRAME_SIZE - idx1;
                        vCopy(mFade[RAMP_DOWN], out, idx1);
#ifdef CONST_POWER
                        vSet(&out[idx1], sinf(PI*endVal/2.0f), idx2);
#else
						vSet(&out[idx1], endVal, idx2);
#endif
                    }
                }
                else { // ramp from startVal to endval, then endval
                    idx1 = getRampIndex(RAMP_DOWN, startVal); // (int)((1.0f - startVal)*(float)BACCH_FRAME_SIZE);
                    idx2 = getRampIndex(RAMP_DOWN, endVal);
                    rampLength = idx2 - idx1;
                    tailLength = BACCH_FRAME_SIZE - rampLength;
                    vCopy(&mFade[RAMP_DOWN][idx1], out, rampLength); 
#ifdef CONST_POWER
                    vSet(&out[rampLength], sinf(PI*endVal/2.0f), tailLength);
#else
					vSet(&out[rampLength], endVal, tailLength);
#endif
                }
            }
        }
    } break;
    case RAMP_UP: {
        if (FLOAT_IS_ONE(endVal)) {
            if (FLOAT_IS_ZERO(startVal)) {	// ramp from 0 to 1
                vCopy(mFade[RAMP_UP], out, BACCH_FRAME_SIZE);
            }
            else {	// ramp from val to 1 and then 1s
                idx1 = getRampIndex(RAMP_UP, startVal); // (int)(startVal * (float)BACCH_FRAME_SIZE);
                vCopy(&mFade[RAMP_UP][idx1], out, BACCH_FRAME_SIZE);
            }
        }
        else { // from startVal to endVal
            idx1 = getRampIndex(RAMP_UP, startVal);
            idx2 = getRampIndex(RAMP_UP, endVal);
            rampLength = idx2 - idx1;
            tailLength = BACCH_FRAME_SIZE - rampLength;
            if (zerosFirst) {
                numZeros = getRampIndex(RAMP_DOWN, zerosTo);
                tailLength -= numZeros;
                vClear(out, numZeros);
            }
            vCopy(&mFade[RAMP_UP][idx1], &out[numZeros], rampLength);

#ifdef CONST_POWER
            vSet(&out[rampLength+numZeros], sinf(PI*endVal/2.0f), tailLength);
#else
			vSet(&out[rampLength + numZeros], endVal, tailLength);
#endif
        }
    } break;
    case RAMP_UP_DOWN: {
        idx1 = getRampIndex(RAMP_UP_DOWN, startVal);
        idx2 = getRampIndex(RAMP_UP_DOWN, endVal);
        idx2 = 2 * BACCH_FRAME_SIZE - idx2 - 1;
        rampLength = idx2 - idx1;
        tailLength = BACCH_FRAME_SIZE - rampLength;
        vCopy(&mFade[RAMP_UP_DOWN][idx1], out, rampLength);
        if (tailLength > 0)
#ifdef CONST_POWER
            vSet(&out[rampLength], sinf(PI*endVal/2.0f), tailLength);
#else
			vSet(&out[rampLength], endVal, tailLength);
#endif
    } break;
    }
}

void BACCH_slide_manager::setActiveIRs(int lowIR, int cnt) {
    int irIterator = 0;
    int irIndex = 0;
    int highIR = lowIR + cnt - 1;

	for (irIterator = 0; irIterator < numIRs; irIterator++) {
		activeIRs[irIterator] = IR_INACTIVE;
	}

    for (irIterator = 0; irIterator < numIRs; irIterator++) {
        // changes in heading will need data in these IRs to avoid clipping
        if ((irIterator >= (lowIR - numPlusMinusIRs)) &&
            (irIterator <= (highIR + numPlusMinusIRs)))
            activeIRs[irIndex++] = irIterator;
    }
}


void BACCH_slide_manager::setStaticMode() {

    int irIterator = 0;

    // clear slide flags
    dirty= false;
    isStatic = true;

#ifdef CONST_POWER
	staticGain[0] = sinf(PI * currentIR->lowIRGain/2.0f);
    staticGain[1] = sinf(PI * currentIR->highIRGain/2.0f);
#else
	staticGain[0] = currentIR->lowIRGain;
	staticGain[1] = currentIR->highIRGain;
#endif

    // clear IR arrays
    for (irIterator = 0; irIterator < 4; irIterator++) {
        outputIRs[irIterator] = IR_INACTIVE;
    }
    for (irIterator = 0; irIterator < numIRs; irIterator++) {
        activeIRs[irIterator] = IR_INACTIVE;
    }

    int activeIndex = 0;
    int outputIndex = 0;
    if (currentIR->onIR) {
        for (irIterator = 0; irIterator < numIRs; irIterator++) {
            // changes in heading will need data in these IRs to avoid clipping
            if ((irIterator >= (currentIR->singleIR - numPlusMinusIRs)) &&
                (irIterator <= (currentIR->singleIR + numPlusMinusIRs)))
                activeIRs[activeIndex++] = irIterator;
        }
        outputIRs[outputIndex] = currentIR->singleIR;
		staticGain[0] = 1.0f;

    }
    else {
        for (irIterator = 0; irIterator < numIRs; irIterator++) {
            // changes in heading will need data in these IRs to avoid clipping
            if ((irIterator >= (currentIR->lowIR - numPlusMinusIRs)) &&
                (irIterator <= (currentIR->highIR + numPlusMinusIRs)))
                activeIRs[activeIndex++] = irIterator;

            if ((irIterator >= currentIR->lowIR) && (irIterator <= currentIR->highIR)) {
                outputIRs[outputIndex++] = irIterator;
            }
        }
    }
    
}

/*******************************getters***********************************/

float BACCH_slide_manager::getCurrentDegrees()
{
    return currentIR->degrees;
}

float BACCH_slide_manager::getCurrentIR()
{
    return currentIR->ir;
}

float BACCH_slide_manager::getTargetIR()
{
    return targetIR->ir;
}

bool BACCH_slide_manager::isDirty()
{
    return dirty;
}

bool BACCH_slide_manager::isInProgress()
{
    return inProgress;
}

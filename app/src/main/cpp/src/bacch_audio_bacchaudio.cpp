//
// Created by Samuel Caldwell on 1/2/2019.
//

#include <jni.h>
#include <stdbool.h>
#include "bacch_audio_bacchaudio.h"
#include "BACCH_Audio_c_wrapper.h"
#include "BACCH_constants.h"
#include <pthread.h>

static baHandle baInstance = NULL;
static float *infbuf = NULL;
static float *outfbuf = NULL;

bool bypassBACCH = false;

static pthread_mutex_t  *configLock = NULL;

#ifdef __cplusplus
extern "C" {
#endif

static inline float clamp_to(float val, float lo, float hi) {
    float tmp = (hi > val) ? val : hi;
    return (lo > tmp) ? lo : tmp;
}

// local functions
void deinterleave( const int16* intrlvd, float* out, int numChannels, int numsamples) {
    float *channels[8]; // = out;
    const int16 *in = intrlvd;
    int i = 0,j=0;
    float ingain = NORM_IN_16;// * 0.707f;

    for (i=0;i<numChannels;i++) {
        channels[i] = &out[i * BACCH_FRAME_SIZE];
        memset((void*)channels[i], 0, sizeof(float) * BACCH_FRAME_SIZE);
    }

    switch (numChannels) {
        case 1: {
            for (i=0;i<numsamples;i++) {
                channels[0][i] = (float) in[i] * ingain;
                channels[1][i] = (float) in[i] * ingain;
            }
        } break;
        case 4: {
            for (i = 0, j = 0; i < numsamples; i++, j += 4) {
                channels[0][i] = (float) in[j] * ingain;
                channels[1][i] = (float) in[j + 1] * ingain;
                channels[2][i] = (float) in[j + 2] * ingain;
                channels[3][i] = (float) in[j + 3] * ingain;
            }
        } break;
        case 6: {
            for (i = 0, j = 0; i < numsamples; i++, j += 6) {
                channels[0][i] = (float) in[j] * ingain;
                channels[1][i] = (float) in[j + 1] * ingain;
                channels[2][i] = (float) in[j + 2] * ingain;
                channels[3][i] = (float) in[j + 3] * ingain;
                channels[4][i] = (float) in[j + 4] * ingain;
                channels[5][i] = (float) in[j + 5] * ingain;
            }
        } break;
        case 8: {
            for (i = 0, j = 0; i < numsamples; i++, j += 8) {
                channels[0][i] = (float) in[j] * ingain;
                channels[1][i] = (float) in[j + 1] * ingain;
                channels[2][i] = (float) in[j + 2] * ingain;
                channels[3][i] = (float) in[j + 3] * ingain;
                channels[4][i] = (float) in[j + 4] * ingain;
                channels[5][i] = (float) in[j + 5] * ingain;
                channels[6][i] = (float) in[j + 6] * ingain;
                channels[7][i] = (float) in[j + 7] * ingain;
            }
        } break;
        case 2:
        default: {
            for (i = 0, j = 0; i < numsamples; i++, j += 2) {
                channels[0][i] = (float) in[j] * ingain;
                channels[1][i] = (float) in[j + 1] * ingain;
            }
        } break;

    }
}

void interleave( float *in, int16* intrlvd, int numsamples) {
    float *left  = in;
    float *right = &in[numsamples];
    int16 *iout = intrlvd;
    int i = 0, j = 0;
    float outgain = NORM_OUT_16;

    for ( i=0,j=0;i<numsamples;i++,j+=2 )
    {
        iout[j] = (int16)(clamp_to(left[i], -1.0f, 1.0f) * outgain);
        iout[j+1] = (int16)(clamp_to(right[i], -1.0f, 1.0f) * outgain);
    }
}

bool lockConfig() {
    if (configLock != NULL) {
        pthread_mutex_lock(configLock);
        return true;
    }
    return false;
}

bool releaseConfig() {
    if (configLock != NULL) {
        pthread_mutex_unlock(configLock);
        return true;
    }
    return false;
}

JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_createEngine
        (JNIEnv *env, jclass obj,
         jint sampleRate,
         jint channelsIn,
         jint orientation) {
    // get external storage path
    JavaVM *jvm;
    env->GetJavaVM(&jvm);

    if(NULL == configLock) {
        configLock = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(configLock, 0);
    }

    lockConfig();

    if (NULL == baInstance) {
        baInstance = get_BA_instance(sampleRate, channelsIn, orientation, NULL);
    }
    if (infbuf == NULL) {
        posix_memalign((void**)&infbuf, 64, sizeof(float)*BACCH_FRAME_SIZE*MAX_IN_CHANNELS);
    }
    if(outfbuf == NULL) {
        posix_memalign((void**)&outfbuf, 64, sizeof(float)*BACCH_FRAME_SIZE*NUM_OUT_CHANNELS);
    }

    releaseConfig();

    return (NULL == baInstance)? false : true;
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_destroyEngine
        (JNIEnv *env, jclass obj) {

    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance){
            delete_BA_instance(baInstance);
            baInstance = NULL;
            if (infbuf != NULL) free((void *) infbuf);
            if (outfbuf != NULL) free((void *) outfbuf);
            releaseConfig();
            if (configLock != NULL) free((void *) configLock);

            infbuf = NULL;
            outfbuf = NULL;
            configLock = NULL;
        }
    }
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configBA
        ( JNIEnv *env, jclass obj,
        jint sampleRate,
        jint channelsIn,
        jint orientation) {
    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance) {
            configure_BA(baInstance, sampleRate, channelsIn, orientation);
        }
        releaseConfig();
    }
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configUBACCH
        ( JNIEnv *env, jclass obj,
        jfloat distance_speaker_to_speaker,
        jfloat distance_listener_to_speaker,
        jboolean isMetricUnits) {
    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance) {
            configure_UBACCH(baInstance, distance_speaker_to_speaker, distance_listener_to_speaker,
                             (bool) (isMetricUnits == JNI_TRUE));
        }
        releaseConfig();
    }
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_reset
        ( JNIEnv *env, jclass obj) {
    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance) {
            reset_BA(baInstance);
        }
        releaseConfig();
    }
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_configVirt
        ( JNIEnv *env, jclass obj,
          jboolean isDolby) {
    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance) {
            configure_VirtMode_BA(baInstance, isDolby);
            releaseConfig();
        }
    }
}

// thread safe
JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableHP
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate) {
    if (NULL != baInstance) {
        setHPEnable_BA(baInstance, (bool)(enabled == JNI_TRUE),
                       (bool)(isUser == JNI_TRUE), (bool)(immediate == JNI_TRUE));
        return true;
    }
    return false;
}

// thread safe
JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableSP
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate) {
    if (NULL != baInstance) {
        setSPEnable_BA(baInstance, (bool)(enabled == JNI_TRUE),
                       (bool)(isUser == JNI_TRUE), (bool)(immediate == JNI_TRUE));
        return true;
    }
    return false;
}

// thread safe
JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableLCex
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate) {
    if (NULL != baInstance) {
        setLCexEnable_BA(baInstance, (bool)(enabled == JNI_TRUE),
                       (bool)(isUser == JNI_TRUE), (bool)(immediate == JNI_TRUE));
        return true;
    }
    return false;
}

// thread safe
JNIEXPORT jboolean JNICALL Java_bacch_audio_BACCHAudio_enableUBACCH
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isUser,
          jboolean immediate) {
    if (NULL != baInstance) {
        setUBACCHEnable_BA(baInstance, (bool)(enabled == JNI_TRUE),
                         (bool)(isUser == JNI_TRUE), (bool)(immediate == JNI_TRUE));
        return true;
    }
    return false;
}

JNIEXPORT void JNICALL Java_bacch_audio_BACCHAudio_enable
        ( JNIEnv *env, jclass obj,
          jboolean enabled,
          jboolean isHP,
          jboolean immediate) {
    if (configLock != NULL && lockConfig()) {
        if (NULL != baInstance) {
            setBACCHEnable_BA(baInstance, (bool)(enabled == JNI_TRUE), (bool)(isHP == JNI_TRUE), (bool)(immediate == JNI_TRUE));
        }
        releaseConfig();
    }
}

        JNIEXPORT jint JNICALL Java_bacch_audio_BACCHAudio_resample
        ( JNIEnv *env, jclass obj,
                jobject in_buf,
                jobject out_buf,
                jint numsamples,
                jint numchannelsin,
                jint srIn,
                jint srOut) {

            jbyte                   *inbyte, *outbyte;
            jint                     iRet = 0;
            int16_t *inbuf;
            int16_t *outbuf;

            if (configLock != NULL && lockConfig()) {
                if (NULL != baInstance) {

                    int numChannels = numchannelsin;
                    int numFrames = numsamples;///numChannels;
                    int sampleRateOut = srOut;

                    inbyte = (jbyte *) env->GetDirectBufferAddress(in_buf);
                    inbuf = (int16_t *) inbyte;
                    outbyte = (jbyte *) env->GetDirectBufferAddress(out_buf);
                    outbuf = (int16_t *) outbyte;

                    iRet = resample(baInstance, inbuf, outbuf, numFrames, numChannels, srIn, &sampleRateOut);
                    // return is total bytes in the resample data
                    iRet = iRet * 2 * numChannels;
                }
                releaseConfig();
            }
            return iRet;
        }

        JNIEXPORT jint JNICALL Java_bacch_audio_BACCHAudio_process
        ( JNIEnv *env, jclass obj,
                jobject in_buf,
                jobject out_buf,
                jint numsamples,
                jint numchannelsin) {

            jbyte                   *inbyte, *outbyte;
            jint                     iRet = 0;

            if (configLock != NULL && lockConfig()) {
                if (NULL != baInstance) {

                    int numChannels = numchannelsin;
                    int numShorts = numsamples / sizeof(int16_t);
                    int numShortsPerBlock = BACCH_FRAME_SIZE * numChannels;
                    float *fBlocks[8];
                    int16 *inbuf;
                    int16 *outbuf;

                    inbyte = (jbyte *) env->GetDirectBufferAddress(in_buf);
                    inbuf = (int16 *) inbyte;
                    outbyte = (jbyte *) env->GetDirectBufferAddress(out_buf);
                    outbuf = (int16 *) outbyte;

                    for (int i = 0; i < numChannels; i++) {
                        fBlocks[i] = &infbuf[i * BACCH_FRAME_SIZE];
                    }

                    while (numShorts >= numShortsPerBlock) {

                        deinterleave(inbuf, infbuf, numChannels, BACCH_FRAME_SIZE);

//                        if (bypassBACCH) {
//                            for (int i=0, j=0;i<BACCH_FRAME_SIZE;i++,j+=2) {
//                                outfbuf[i] = fBlocks[0][i];
//                                outfbuf[i+BACCH_FRAME_SIZE] = fBlocks[1][i];
//                            }
//                            interleave(outfbuf, outbuf, BACCH_FRAME_SIZE);
//                            iRet += BACCH_FRAME_SIZE;
//                        }
//                        else {
                            // return is numFrames processed
                            iRet += process_float_buf(baInstance, infbuf, outfbuf, BACCH_FRAME_SIZE, numChannels);
                            interleave(outfbuf, outbuf, BACCH_FRAME_SIZE);
                        //}

                        inbuf += numShortsPerBlock;
                        outbuf += BACCH_FRAME_SIZE * NUM_OUT_CHANNELS;
                        numShorts -= numShortsPerBlock;
                    }

                    iRet = iRet * 2 * numChannels;    // consumed from the inputbuffer in bytes
                }
                releaseConfig();
            }
            return iRet;
        }

    #ifdef __cplusplus

    };
#endif
#include "BACCH_types.h"
#include "BACCH_constants.h"
#include "BACCH_supporting.h"
#include "BACCH_mem.h"
#include "BACCH_ibuffer.h"


int32_t IBuffer_t::readBuf(int16_t *dest, int32_t numFrames, bool accum) {

    int32_t framesRead = 0;
    if (numFrames <= framesCnt) {
        if (accum) {
            for (int32_t i = 0; i < numFrames*numChannels; i++) {
                dest[i] += s16[readIndx*numChannels + i];
            }
        }
        else {
            for (int32_t i = 0; i < numFrames*numChannels; i++) {
                dest[i] = s16[readIndx*numChannels + i];
            }
        }
        framesCnt -= numFrames;
        readIndx += numFrames;
        framesRead = numFrames;
    }
    // move data to beginning of buffer
    if (framesCnt == 0) {
        readIndx = 0;
        writeIndx = 0;
    }
    else if(readIndx != 0) {
        for (int32_t i = 0; i < framesCnt * numChannels; i++) {
            s16[i] = s16[readIndx * numChannels + i];
        }
        readIndx = 0;
        writeIndx = framesCnt;
    }

    return framesRead;
}

int32_t IBuffer_t::writeBuf(int16_t *src, int32_t numFrames) {

    int32_t framesWritten = 0;

    if (framesCapacity - framesCnt >= numFrames) {
        for (int32_t i = 0; i < numFrames * numChannels; i++) {
            s16[writeIndx * numChannels + i] = src[i];
        }
        writeIndx += numFrames;
        framesCnt += numFrames;
        framesWritten = numFrames;
    }

    return framesWritten;
}

int16_t* IBuffer_t::writePtr() {
    return (&s16[writeIndx * numChannels]);
}

int16_t* IBuffer_t::readPtr() {
    return (&s16[readIndx * numChannels]);
}

status_t IBuffer_t::updateBuffer(int32_t numFrames, buffer_op_t op) {
    status_t mStatus = STATUS_OK;

    if ((op <= BACCH_BUFFER_OP_INVALID) || (op >= BACCH_BUFFER_OP_LAST)) {
        //mThrow("Bacch_ibuffer_t: invalid command op in updateBuffer");
        return STATUS_INVALID_ARG;
    }
    

    if (op == BACCH_BUFFER_OP_READ) {
        if (numFrames <= framesCnt) {
            framesCnt -= numFrames;
            readIndx += numFrames;
        }
        else {
            //mThrow("Bacch ibuffer_t : insufficient data in buffer for read");
            mStatus = STATUS_BUF_EMPTY;
        }
        if (framesCnt == 0) {
            readIndx = 0;
            writeIndx = 0;
        }
    }
    else {
        if (framesCapacity - framesCnt >= numFrames) {
            writeIndx += numFrames;
            framesCnt += numFrames;
        }
        else {
           //mThrow("Bacch ibuffer_t: insufficient space in buffer for write");
            mStatus = STATUS_BUF_FULL;
        }
    }

    // move data to beginning of buffer
    if (readIndx != 0) {
        for (int32_t i = 0; i < framesCnt * numChannels; i++) {
            s16[i] = s16[readIndx * numChannels + i];
        }
        readIndx = 0;
        writeIndx = framesCnt;
    }

    return mStatus;
}


int IBuffer_t::getNumFrames() {
    return framesCnt;
}

void IBuffer_t::clear() {
    readIndx = writeIndx = framesCnt = 0;
    memset(s16, 0x0, framesCapacity * numChannels * sizeof(int16_t));
    /*for (int i = 0; i < framesCapacity*numChannels; i++) {
        s16[i] = 0;
    }*/
}

IBuffer_t::~IBuffer_t() {
    blFree(s16);
}

IBuffer_t::IBuffer_t(int32_t nchannels, int32_t capacityInFrames) {

    s16 = (int16_t*)blMalloc(sizeof(int16_t), capacityInFrames*nchannels, 64);
 
    framesCapacity = capacityInFrames;
    numChannels = nchannels;
    readIndx = 0;
    writeIndx = 0;
    framesCnt = 0;

}

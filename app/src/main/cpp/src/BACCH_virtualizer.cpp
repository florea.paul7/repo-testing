#include "BACCH_virtualizer.h"
#include "../coeffs/BACCH_VIRT_coeffs.h"
#include "../coeffs/BACCH_VIRT_constants.h"
#include "../coeffs/BACCH_VIRT_SHORT_coeffs.h"
#include "../coeffs/BACCH_VIRT_SHORT_constants.h"
#include "BACCH_math.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_supporting.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_mem.h"

#ifndef _MSC_VER
#include <android/log.h>
#endif

// in all cases, we lead with a fft size of 2048, this sets the latency at 1024 samples
// for the case of 8192 total IR length, will have 8 ffts of 2048
// for the case of 16384 total IR length, will have 16 ffts of 2048
// for the case of 32768 total IR length, will have 32 ffts of 2048
// this corresponds to 32 discreet fft instances and a complex buffer length of between 16384 and 65536


// Load the Impulse Responses and generate TFs
// a note on the layout of the virtualizer IR files
// there are 7 input channels supported, and 7 pairs of IRs included in the IR header file
// the layout of the header file is:
// IR 0 = left input to left and right output
// IR 1 = right input to left and right output
// IR 2 = center input to left and right output
// IR 3 = left surround input to left and right output
// IR 4 = right surround input to left and right output
// IR 5 = left rear input to left and right output
// IR 6 = right rear input to left and right output
//
// this matters now why?
// 
// we will load the TFs on an input to each output basis, so we will load 
// TFs[0][all blocks] with IR[samplerate][0][0] (left in to left out) and IR[samplerate][0][1] (left in to right out)
void BACCH_virtualizer::FilterLoad() {
    int irIdx = 0, irBlockIdx = 0;

    if (mIncludeRIR == false) {
        // load short IRs and FFT to generate TFs
        int irLen = VIRT_SHORT_IR_LENGTHS[mSampleRateIndex];
        for (irIdx = 0; irIdx < VIRT_SHORT_NUM_IRS; irIdx++) {
            TFs[irIdx][irBlockIdx]->clear();

            vRealToComplex((float*)virt_short_irs[mSampleRateIndex][irIdx][CHANNEL_TO_LEFT], TFs[irIdx][irBlockIdx]->data[BACCH_LEFT], irLen);
            vRealToComplex((float*)virt_short_irs[mSampleRateIndex][irIdx][CHANNEL_TO_RIGHT], TFs[irIdx][irBlockIdx]->data[BACCH_RIGHT], irLen);

            fft->Forward(TFs[irIdx][irBlockIdx]);
        }
    }
    else {
        // load IRs and FFT to generate TFs
        for (irIdx = 0; irIdx < VIRT_NUM_IRS; irIdx++) {
            for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
                TFs[irIdx][irBlockIdx]->clear();
                vRealToComplex((float*)&virt_irs[mSampleRateIndex][irIdx][CHANNEL_TO_LEFT][irBlockIdx*BACCH_FRAME_SIZE], TFs[irIdx][irBlockIdx]->data[BACCH_LEFT], BACCH_FRAME_SIZE);
                vRealToComplex((float*)&virt_irs[mSampleRateIndex][irIdx][CHANNEL_TO_RIGHT][irBlockIdx*BACCH_FRAME_SIZE], TFs[irIdx][irBlockIdx]->data[BACCH_RIGHT], BACCH_FRAME_SIZE);

                fft->Forward(TFs[irIdx][irBlockIdx]);
            }
        }
    }
}

// setup input channel to IR map
// setup input channel to process map
void BACCH_virtualizer::setChMap() {
    mCh2IRMap[BACCH_IR_LEFT] = BACCH_LEFT;
    mCh2IRMap[BACCH_IR_RIGHT] = BACCH_RIGHT;
    mChMap[0] = BACCH_LEFT;
    mChMap[1] = BACCH_RIGHT;

    switch (mNumChannelsIn) {
    case 4: {
        mCh2IRMap[2] = BACCH_IR_LEFT_REAR;
        mCh2IRMap[3] = BACCH_IR_RIGHT_REAR;
        mChMap[2] = 2;
        mChMap[3] = 3;  // in 4 channel, the 3rd and 4th channels are left and right rear
    } break;
    case 6: {
        mNumCh2Proc = 5;    // only using left, right, center, left and right surround
        mCh2IRMap[2] = BACCH_IR_CENTER;
        mCh2IRMap[3] = BACCH_IR_LEFT_SURROUND;
        mCh2IRMap[4] = BACCH_IR_RIGHT_SURROUND;
        mChMap[2] = BACCH_CENTER;
        mChMap[3] = BACCH_LEFT_SURROUND;  // skip input LFE (channel 3) 
        mChMap[4] = BACCH_RIGHT_SURROUND;
    } break;
    case 8: {
        mNumCh2Proc = 7;  // only using left, right, center, left and right surround, left and right rear
        mCh2IRMap[2] = BACCH_IR_CENTER;
        mCh2IRMap[3] = (mMode == VIRT_MODE_DTS) ? BACCH_IR_LEFT_SURROUND : BACCH_IR_LEFT_REAR;
        mCh2IRMap[4] = (mMode == VIRT_MODE_DTS) ? BACCH_IR_RIGHT_SURROUND : BACCH_IR_RIGHT_REAR;
        mCh2IRMap[5] = (mMode == VIRT_MODE_DOLBY) ? BACCH_IR_LEFT_SURROUND : BACCH_IR_LEFT_REAR;
        mCh2IRMap[6] = (mMode == VIRT_MODE_DOLBY) ? BACCH_IR_RIGHT_SURROUND : BACCH_IR_RIGHT_REAR;
        mChMap[2] = BACCH_CENTER;
        mChMap[3] = BACCH_LEFT_SURROUND;  // skip input LFE (channel 3) 
        mChMap[4] = BACCH_RIGHT_SURROUND;
        mChMap[5] = BACCH_LEFT_REAR;
        mChMap[6] = BACCH_RIGHT_REAR;
    } break;
    }
}

void BACCH_virtualizer::setRIR(bool includeRIR) {
    
    if (mIncludeRIR == includeRIR) return;


    int srIdx = (includeRIR) ? VIRT_IR_NUM_BLOCKS[mSampleRateIndex] : VIRT_SHORT_IR_NUM_BLOCKS[mSampleRateIndex];

    if (srIdx != mNumIRBlocks) {
        for (int i = 0; i < mNumIRBlocks; i++) {
            for (int j = 0; j < VIRT_NUM_IRS; j++) {
                TFs[j][i]->clear();
            }
        }
        mNumIRBlocks = srIdx;
        mIncludeRIR = includeRIR;
    }

    FilterLoad();
}

bool BACCH_virtualizer::setMode(virt_7_1_mode_t mode) {
    if ((mode < VIRT_MODE_FIRST) || (mode >= VIRT_MODE_LAST))
        return false;

    mMode = mode;
    return true;
}

void BACCH_virtualizer::configure(int samplerate, int numChannels, int filterNum, bool includeRIR, virt_7_1_mode_t mode)
{
	int samplerateIndex = validateSampleRate(samplerate);

	if ((samplerateIndex != mSampleRateIndex) ||
		(filterNum != mFilterNum) ||
		(mode != mMode) ||
        (includeRIR != mIncludeRIR)) {

		if (samplerateIndex < 0)
			samplerateIndex = 0;

		mSampleRateIndex = samplerateIndex;

        int srIdx = (includeRIR) ? VIRT_IR_NUM_BLOCKS[mSampleRateIndex] : VIRT_SHORT_IR_NUM_BLOCKS[mSampleRateIndex];

        if (srIdx != mNumIRBlocks) {
            // clear current TFs
            for (int i = 0; i < mNumIRBlocks; i++) {
                for (int j = 0; j < VIRT_NUM_IRS; j++) {
                    TFs[j][i]->clear();
                }
            }
		    mNumIRBlocks = srIdx;
            mIncludeRIR = includeRIR;
        }

        // will get triggered on first call to process
        mNumChannelsIn = numChannels;
        mNumCh2Proc = numChannels;

        // setup channel to IR map
        setChMap();

		if ((filterNum < 0) || (filterNum >= VIRT_MAX_FILTERNUM))
			filterNum = 0;

		mFilterNum = filterNum;

		if ((mode < VIRT_MODE_FIRST) || (mode >= VIRT_MODE_LAST))
		    mode = VIRT_MODE_DTS;

		mMode = mode;

		FilterLoad();
	}
}

void BACCH_virtualizer::reset()
{
	clear();
}

void BACCH_virtualizer::clear()
{
	int i = 0;
	mOut->clear();
	for (i = 0; i < VIRT_IR_NUM_BLOCKS[mSampleRateIndex]; i++) {
		mPad[i]->clear();
	}
}

BACCH_virtualizer::BACCH_virtualizer(int sampleRate, int numChannels, int filternum, bool includeRIR, virt_7_1_mode_t mode)
{
	int i = 0, j = 0;


    int samplerateIndex = validateSampleRate(sampleRate);

    if (samplerateIndex < 0)
        samplerateIndex = 0;

    mSampleRate = sampleRate;
    mSampleRateIndex = samplerateIndex;

    // initialize an instance of the fft module
    fft = new BACCH_fft();

    if ((filternum < 0) || (filternum >= VIRT_MAX_FILTERNUM))
        filternum = 0;

    mFilterNum = filternum;

    if ((mode < VIRT_MODE_FIRST) || (mode >= VIRT_MODE_LAST))
        mode = VIRT_MODE_DTS;

    mMode = mode;

    mIncludeRIR = includeRIR;
    mNumIRBlocks = (includeRIR) ? VIRT_IR_NUM_BLOCKS[mSampleRateIndex] : VIRT_SHORT_IR_NUM_BLOCKS[mSampleRateIndex];

    // will get triggered on first call to process
    mNumChannelsIn = numChannels;
    mNumCh2Proc = numChannels;

    // setup channel to IR map
    setChMap();
    
	// allocate memory
	// transfer function buffers, 2 channels out for 7 potential sources
	// n TFs per source, where n is determined by array in coeffs header file 
	TFs = (BACCH_complex_buffer***)blMalloc(sizeof(BACCH_complex_buffer**), VIRT_NUM_IRS, 32);
	//if (NULL == TFs) mThrow("Virtualizer failed to allocate memory");

    // we will allocate enough ir block buffers for the maximum necessary and not delete them
	for (i = 0; i < VIRT_NUM_IRS; i++) {
		TFs[i] = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 32, 32);
		//if (NULL == TFs[i]) mThrow("Virtualizer failed to allocate memory");

		for (j = 0; j < VIRT_IR_NUM_BLOCKS[2]; j++) {
			TFs[i][j] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		}
	}	

	ScratchIn = new BACCH_complex_buffer(mNumChannelsIn, BACCH_FFT_SIZE);
	mPad = (BACCH_buffer**)blMalloc(sizeof(BACCH_buffer*), 32, 32);
	ScratchFFT = (BACCH_complex_buffer**)blMalloc(sizeof(BACCH_complex_buffer*), 32, 32);
	mPadIdx = (int*)blMalloc(sizeof(int), 32, 32);

	//if ((NULL == mPad) || (NULL == ScratchFFT) || (NULL == mPadIdx)) mThrow("Virtualizer failed to allocate memory");

	for (i = 0; i < VIRT_IR_NUM_BLOCKS[2]; i++) {
		mPad[i] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		ScratchFFT[i] = new BACCH_complex_buffer(NUM_OUT_CHANNELS, BACCH_FFT_SIZE);
		mPadIdx[i] = i;
	}   

	// load the filters
	FilterLoad();
}

BACCH_virtualizer::~BACCH_virtualizer()
{
	int i = 0, j = 0;
	
	delete fft;
	delete ScratchIn;

	for (i = 0; i < VIRT_NUM_IRS; i++) {
		for (j = 0; j < VIRT_IR_NUM_BLOCKS[2]; j++) {
			delete TFs[i][j];
		}
		blFree(TFs[i]);
	}
	blFree(TFs);

	for (j = 0; j < VIRT_IR_NUM_BLOCKS[2]; j++) {
		delete ScratchFFT[j];
		delete mPad[j];
	}
	blFree(ScratchFFT);
	blFree(mPad);
    blFree(mPadIdx);
}
#if 0
void BACCH_virtualizer::convolve_head(const BACCH_complex_buffer *in, BACCH_complex_buffer ** const *tf, BACCH_buffer *out) {

	int channelIdx;

    ScratchFFT[IR_HEAD]->clear();
	// first 2 passes = left to left channel and left to right channel
	// sets the data in the scratch fft output buffers
	vector_mul_c(in->data[BACCH_LEFT], tf[BACCH_IR_LEFT][IR_HEAD]->data[BACCH_LEFT], ScratchFFT[IR_HEAD]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
	vector_mul_c(in->data[BACCH_LEFT], tf[BACCH_IR_LEFT][IR_HEAD]->data[BACCH_RIGHT], ScratchFFT[IR_HEAD]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right

	// then, accumulate convolution of each channel to left and right, starting with right
	for (channelIdx = 1; channelIdx < mNumChannelsIn; channelIdx++) {
		vector_mul_accum_i_c(in->data[channelIdx], TFs[channelIdx][IR_HEAD]->data[BACCH_LEFT], ScratchFFT[IR_HEAD]->data[BACCH_LEFT], BACCH_FFT_SIZE); // channel to left
		vector_mul_accum_i_c(in->data[channelIdx], TFs[channelIdx][IR_HEAD]->data[BACCH_RIGHT], ScratchFFT[IR_HEAD]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // channel to right
	}

	// inverse fft
	fft->Inverse(ScratchFFT[IR_HEAD]);

    ScratchFFT[IR_HEAD]->updateBuffer(BACCH_FFT_SIZE, BACCH_BUFFER_OP_WRITE);

	// copy results to the output buffer
	ScratchFFT[0]->read_real_block(out->data, VIRT_NUM_CHANNELS_OUT, BACCH_FFT_SIZE);
}

void BACCH_virtualizer::convolve_tail(const BACCH_complex_buffer *in, BACCH_complex_buffer ** const *tf, BACCH_buffer **out) {

	int channel = 0, fftIdx = 0;
	// the 'in' data is in complex format and has already been FFT'd

	// acquire lock on data if multithreaded

	// first pass convolutions set the data in the each fft block's scratch buffers
	for (fftIdx = 1; fftIdx < mNumIRBlocks; fftIdx++) {
		vector_mul_c(in->data[BACCH_LEFT], tf[BACCH_LEFT][fftIdx]->data[BACCH_LEFT], ScratchFFT[fftIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
		vector_mul_c(in->data[BACCH_LEFT], tf[BACCH_LEFT][fftIdx]->data[BACCH_RIGHT], ScratchFFT[fftIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right
	}

	// subsequent convolution passes accumulate
	for (channel = BACCH_RIGHT; channel < mNumChannelsIn; channel++) {
		for (fftIdx = 1; fftIdx < mNumIRBlocks; fftIdx++) {
			vector_mul_accum_i_c(in->data[channel], tf[channel][fftIdx]->data[BACCH_LEFT], ScratchFFT[fftIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // channel to left
			vector_mul_accum_i_c(in->data[channel], tf[channel][fftIdx]->data[BACCH_RIGHT], ScratchFFT[fftIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // channel to right
		}
	}

	// reverse fft and copy results to *out
	for (fftIdx = 1; fftIdx < mNumIRBlocks; fftIdx++) {
		fft->Inverse(ScratchFFT[fftIdx]);
        ScratchFFT[fftIdx]->updateBuffer(BACCH_FFT_SIZE, BACCH_BUFFER_OP_WRITE);
		ScratchFFT[fftIdx]->read_real_block(out[mPadIdx[fftIdx]]->data, 2, BACCH_FFT_SIZE); 
        //out[mPadIdx[fftIdx]]->setNumSampleFramesInBuffer(BACCH_FFT_SIZE);
	}
	// realease lock on data if multithreaded
}
#endif


// receives input, kicks off tail processing, completes head processing, accumulates with previous tail processing and returns
void BACCH_virtualizer::process(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer)
{
    int iRet = 0;
    int irBlockIdx = 0, irIdx = 0;


    //float *data[2];
#if 0
    /*for (int i = 0; i < length; i++) {
        outbuffer[0][i] = inbuffer[0][i];
        outbuffer[1][i] = inbuffer[1][i];
    }*/
    vCopy(inbuffer->data[0], outbuffer->data[0], BACCH_FRAME_SIZE); // VALIDATED!!
    vCopy(inbuffer->data[1], outbuffer->data[1], BACCH_FRAME_SIZE);
    return;
#else

    // clear scratch buffers for IR head processing
    ScratchIn->clear();

    // write input data into scratch buffer
    cbufRealtoCplx(inbuffer, ScratchIn, mNumChannelsIn, BACCH_FRAME_SIZE);

    bufCopy(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

    // zero out this buffer, subsequent operations are accumulates
    mPad[mPadIdx[0]]->clear();

    // swap the pad index to next buffer since current idx=0 is consumed
    for (int padIdx = 0; padIdx < mNumIRBlocks; padIdx++) {
        mPadIdx[padIdx] --;
        if (mPadIdx[padIdx] < 0)
            mPadIdx[padIdx] = mNumIRBlocks-1;
    }

    // fft the input (skip LFE is present)
    for (irIdx = 0; irIdx < mNumCh2Proc; irIdx++) {
        fft->Forward(ScratchIn->data[mChMap[irIdx]]); 
    }

    for (irBlockIdx = 0; irBlockIdx < mNumIRBlocks; irBlockIdx++) {
        ScratchFFT[irBlockIdx]->reset();
        // first vector complex multiply sets the data in ScratchFFT for the current IR Block Index
        vector_mul_c(ScratchIn->data[mChMap[BACCH_LEFT]], TFs[BACCH_IR_LEFT][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // left to left
        vector_mul_c(ScratchIn->data[mChMap[BACCH_LEFT]], TFs[BACCH_IR_LEFT][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // left to right

        for (int channel = BACCH_RIGHT; channel < mNumCh2Proc; channel++) {
            vector_mul_accum_i_c(ScratchIn->data[mChMap[channel]], TFs[mCh2IRMap[channel]][irBlockIdx]->data[BACCH_LEFT], ScratchFFT[irBlockIdx]->data[BACCH_LEFT], BACCH_FFT_SIZE); // + channel to left
            vector_mul_accum_i_c(ScratchIn->data[mChMap[channel]], TFs[mCh2IRMap[channel]][irBlockIdx]->data[BACCH_RIGHT], ScratchFFT[irBlockIdx]->data[BACCH_RIGHT], BACCH_FFT_SIZE); // + channel to right		
        }
                                                                                                                                                                        // inverse fft
        fft->Inverse(ScratchFFT[irBlockIdx]);

        // store results in pad buffers
        cbufCplxtoReal(ScratchFFT[irBlockIdx], mPad[mPadIdx[irBlockIdx]], NUM_OUT_CHANNELS, BACCH_FFT_SIZE, true);
    }


    // add current output to last run's tail
    bufAdd_I(mPad[mPadIdx[0]], outbuffer, BACCH_FRAME_SIZE);

#endif
}



#include "BACCH_supporting.h"
#include <stdarg.h>

#ifdef __USE_IPP__
#include "ipps.h"
#include "ippcore.h"
#include "ippvm.h"
#endif // __USE_IPP

//#include <cmath>

#ifdef _MSC_VER
#include <concrt.h>         // critical section header
#else
#include <android/log.h>
#endif //_MSC_VER

#include <stdlib.h>

enum var_types_t {
    VAR_STRING = 0,
    VAR_INT,
    VAR_FLOAT,
    VAR_DOUBLE,

};

typedef struct linked_var {
    size_t      pos;
    char        var;
    char*       cval;
    int         ival;
    float       fval;
    double      dval;
    var_types_t var_t;
    linked_var* next;
}linked_var_t;



int validateSampleRate(int samplerate) {
	int samplerateIndex = 0;
	switch (samplerate) {
		case FS_44_1k:
		samplerateIndex = 0;
		break;
		case FS_48k:
		samplerateIndex = 1;
		break;
		case FS_96k:
		samplerateIndex = 2;
		break;
		case FS_192k:
		samplerateIndex = 3;
		break;
		default:
		samplerateIndex = -1;
	};
	return samplerateIndex;
}

#ifdef MULTI_THREAD

crit_section::crit_section() {
	InitializeCriticalSection(&csMutex);
}

crit_section::~crit_section() {
	DeleteCriticalSection(&csMutex);
}

void crit_section::Enter(void) {
#ifdef _MSC_VER
	EnterCriticalSection(&csMutex);
#else

#endif // _MSC_VER
}

void crit_section::Leave(void) {
#ifdef _MSC_VER
	LeaveCriticalSection(&csMutex);
#else

#endif // _MSC_VER
}
#endif // MULTI_THREAD



//

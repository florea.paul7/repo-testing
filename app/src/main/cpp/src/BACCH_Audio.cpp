#include <stdbool.h>
#include "BACCH_Audio.h"
#include "BACCH_library.h"
#include "BACCH_hp.h"
#include "BACCH_sp.h"
#include "BACCH_LCRex.h"
#include "BACCH_equalizer.h"
#include "BACCH_types.h"
#include "BACCH_supporting.h"
#include "BACCH_constants.h"
#include "BACCH_HP_constants.h"
#include "BACCH_GALAXY_TABS_constants.h"
#include "BACCH_EQ_constants.h"
#include "BACCH_slide_manager.h"
#include "BACCH_mem.h"
#include "BACCH_buffer.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_virtualizer.h"

#ifdef __USE_NE10__
#include "NE10.h"
#endif

//#include <iostream>
#include <cmath> //fabsf

#ifndef _MSC_VER
#include <android/log.h>
#define LOG_TAG "BACCH_AUDIO_LIBRARY"
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#endif //_MSC_VER

#ifdef _FDEBUG
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <cerrno>
#include <BACCH_virtualizer.h>

#ifndef O_BINARY
#define O_BINARY 0
#endif
#endif


#ifdef __USE_IPP__
#include "ipp.h"
#endif // __USE_IPP__


BACCH_Audio::BACCH_Audio(int sampleRate, int channelsIn, bacch_sp_orientation_t orientation, const char* sp_id)
{
    int filterNum = 0, i = 0;

#ifdef __USE_NE10__
    ne10_init_dsp(true);
#endif

    // set up sample rate
    int tmpSampleRateIdx = validateSampleRate(sampleRate);
    if (tmpSampleRateIdx < 0) {
        mSampleRateIndex = 0;
        mSampleRate = 44100;
    } else {
        mSampleRate = sampleRate;
        mSampleRateIndex = tmpSampleRateIdx;
    }

    mNumChannelsIn = channelsIn;

    if ((orientation > BACCH_SP_ORIENTATION_INVALID) && (orientation <= BACCH_SP_ORIENTATION_LAST))
        mOrientation = orientation;
    else
        mOrientation = BACCH_SP_ORIENTATION_LANDSCAPE;

#ifdef __USE_IPP__
	ippInit();
#endif // __USE_IPP__

#ifdef _FDEBUG
	dfile = fopen("/sdcard/Music/debug.raw", "w"); //.open("debug_slides.raw", std::ios::out | std::ios::binary | std::ios::trunc);
	if (dfile == NULL) {
	    int *error = __errno();
	    char *strerr = strerror(*error);
	    LOGI("Error returned by fopen: %d : %s", *error, strerr);
	}
#endif
    
	// setup internal configuration
    int numSamplesToTransition = (int)(STATE_TRANSITION_TIME_SEC * (float)mSampleRate);
    mNumBlocksToTransition = (int)ceilf((float)numSamplesToTransition / (float)BACCH_FRAME_SIZE);
    numSamplesToTransition = BACCH_FRAME_SIZE * mNumBlocksToTransition;
	bhpBlocksInTransition = 0;
	bspBlocksInTransition = 0;
    blcBlocksInTransition = 0;
    beqBlocksInTransition = 0;
    bhpOutputGoodCnt = 0;
    bspOutputGoodCnt = 0;
    blcOutputGoodCnt = 0;
    beqOutputGoodCnt = 0;

    eqSlide = new BACCH_slide_manager(SLIDE_EQ, mSampleRateIndex);

    bhpState = STATE_BYPASS;
	bspState = STATE_BYPASS;
    beqState = STATE_BYPASS;
    blcState = STATE_BYPASS;
    lastHPState = STATE_BYPASS;
    lastSPState = STATE_BYPASS;

    mEQFilterNum = 0;

    mCGain = -3.0f;
    mCWidth = 0.0f;

	// initialize convolvers, control arrays, inputs/outputs for BACCHp
	for (filterNum = 0; filterNum < HP_NUM_IRS; filterNum++) {
		bhp = new BACCH_hp(mSampleRate);
	}

	// intialize SP convolvers
	for (filterNum = 0; filterNum < GALAXY_TABS_NUM_IRS; filterNum++) {
		// @SAM - change to remove SP & EQ orientation change
		bsp = new BACCH_sp(mSampleRate, BACCH_SP_ORIENTATION_LANDSCAPE);
	}

    // intialize EQ convolvers
    //for (filterNum = 0; filterNum < EQ_NUM_IRS; filterNum++) {
//    filterNum = 0;
//    beq[filterNum] = new BACCH_equalizer(mSampleRate, filterNum);
//    beq_parallel_ins[filterNum] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
//    beq_parallel_outs[filterNum] = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
   // }

	bxtc = new BACCH_hp_xtc(mSampleRate, 0);

    bcex = new BACCH_LCRex(mSampleRate);

    bcomp = new BACCH_compressor(mSampleRate);

    bv = NULL;
    if ( mNumChannelsIn >= VIRT_MIN_CHANNELS_IN) {
        bv = new BACCH_virtualizer(mSampleRate, mNumChannelsIn, 0, false, VIRT_MODE_DTS);
    }

	// initialize input/output buffers for modules
    init_buffer_ops();
	inFBuf = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
	outFBuf = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
    mCoutBuffer =  new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
	tmpFBuf = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
	processBuf = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);
	virtBuf = new BACCH_buffer(NUM_OUT_CHANNELS, BACCH_FRAME_SIZE);


	// initialize the fade in/out gain values
	mTransitionFade = new BACCH_buffer(NUM_OUT_CHANNELS, numSamplesToTransition+128);
#ifdef __USE_IPP__
	float* tmpBuf = (float*)blMalloc(sizeof(float), numSamplesToTransition, 64);
    ippsVectorSlope_32f(tmpBuf, numSamplesToTransition, 0.0f, (1.0f / ((float)numSamplesToTransition-1.0f)));
    ippsMulC_32f_I((float)PI / 2.0f, tmpBuf, numSamplesToTransition);

	IppStatus status = ippsSin_32f_A24(tmpBuf, mTransitionFade->data[BACCH_TRANSITION_ON], numSamplesToTransition);
	status = ippsCos_32f_A24(tmpBuf, mTransitionFade->data[BACCH_TRANSITION_OFF], numSamplesToTransition);
	blFree(tmpBuf);
#else
    float tmpF;
	for (i = 0; i < numSamplesToTransition; i++) {
		tmpF = ((float)PI * ((float)i / (float)(numSamplesToTransition - 1))) / 2.0f;
        mTransitionFade->data[BACCH_TRANSITION_ON][i] = sinf(tmpF);
        mTransitionFade->data[BACCH_TRANSITION_OFF][i] = cosf(tmpF);
	}
#endif
    
	scratchFade = new BACCH_buffer(3, BACCH_FRAME_SIZE);

	// debug
	blockNum = 0;
}

BACCH_Audio::~BACCH_Audio() {

	delete bhp;
	delete bsp;

    for (int filterNum = 0; filterNum < EQ_NUM_IRS; filterNum++) {
        //delete beq[filterNum];
        delete beq_parallel_ins[filterNum];
        delete beq_parallel_outs[filterNum];
    }

    delete bcex;

#ifdef _FDEBUG
	fclose(dfile);
#endif

	delete inFBuf;
	delete outFBuf;
	delete mCoutBuffer;
    delete tmpFBuf;
    delete processBuf;
    delete scratchFade;

    delete eqSlide;

}

void BACCH_Audio::test_deinterleave(const int16_t *intrlvd_in, float** out, float gain, int numChannels, int numSamples) {
    vIntrlvdToBlock_16s_32f(intrlvd_in, out, gain, numChannels, numSamples);
}

void BACCH_Audio::test_interleave( float** data, int16_t* out, float postGain, int numChannelsOut, int numSampleFrames) {
    vBlockToInterleaved_32f_16s((float**)data, out, postGain, numChannelsOut, numSampleFrames, false);
}

void BACCH_Audio::configure(int sampleRate, int channelsIn, bacch_sp_orientation_t orientation)
{
    int filterNum;
    bool isHPChanged = false;
    bool isSPChanged = false;
    bool isEQChanged = false;
    bool isCexChanged = false;
    bool isCompChanged = false;
    bool isVirtChanged = false;

    if ((mSampleRate == sampleRate) && (orientation == mOrientation) && (mNumChannelsIn == channelsIn))
        return;     // nothing to change here

    if (mSampleRate != sampleRate) {
        // set up sample rate
        int tmpSampleRateIdx = validateSampleRate(sampleRate);
        if (tmpSampleRateIdx < 0)
            return;

        mSampleRate = sampleRate;
        mSampleRateIndex = tmpSampleRateIdx;
        isHPChanged = isSPChanged = isEQChanged = isCexChanged = isCompChanged = isVirtChanged = true;
    }

    if (mOrientation != orientation) {
        if ((orientation > BACCH_SP_ORIENTATION_INVALID) &&
            (orientation <= BACCH_SP_ORIENTATION_LANDSCAPE_REVERSE)) { //BACCH_SP_ORIENTATION_LAST)) {
            mOrientation = orientation;
            // @SAM - change to remove SP & EQ orientation change
            //isSPChanged = true;
        }
    }

    if (mNumChannelsIn != channelsIn) {
        if ((channelsIn >= 2) && (channelsIn <= VIRT_MAX_CHANNELS_IN)) {
            mNumChannelsIn = channelsIn;
            isVirtChanged = true;
        }
    }

    if (isHPChanged) {
         bhp->configure(mSampleRateIndex);
    }

    if (isSPChanged) {
        bsp->configure(mSampleRateIndex, mOrientation);
    }

    if (isCexChanged) {
        bcex->configure(mSampleRate);
    }

    if (isCompChanged) {
        bcomp->configure(mSampleRate);
    }

    if (isVirtChanged) {
        if (mNumChannelsIn >= VIRT_MIN_CHANNELS_IN) {
            bool isRIRs = (bhpState >= STATE_TRANSITION_TO_NORMAL) ? false : true;
            if (bv == NULL) {
                bv = new BACCH_virtualizer(mSampleRateIndex, mNumChannelsIn, 0, isRIRs, VIRT_MODE_DTS);
            } else {
                bv->configure(mSampleRateIndex, mNumChannelsIn, 0, isRIRs, VIRT_MODE_DTS);
            }
        }
    }

    if (bv != NULL) {
        if (bhpState >= STATE_TRANSITION_TO_NORMAL) {
            bv->setRIR(false);
        }
        else if (bspState >= STATE_TRANSITION_TO_NORMAL) {
            bv->setRIR(true);
        }
    }
}

void BACCH_Audio::configure_LCRex(int sampleRate, float width, float gain) {
    if (sampleRate != mSampleRate) {
        mCWidth = width;
        mCGain = gain;
        configure(sampleRate, mNumChannelsIn, mOrientation);
    }
    else if ((gain != mCGain) || (width != mCWidth)) {
        mCWidth = width;
        mCGain = gain;
        if (bcex != NULL) {
            bcex->configure(sampleRate);
        }
    }
}

void BACCH_Audio::configure_VIRT_Mode(virt_7_1_mode_t mode) {

    if (bv != NULL) {
        bv->setMode(mode);
    }
}

void BACCH_Audio::updateOrientation(bacch_sp_orientation_t val)
{ 
    if ((val > BACCH_SP_ORIENTATION_INVALID) && (val <= BACCH_SP_ORIENTATION_LAST)) {
		//ALOGI("Bacch_audio changing orientation to %d", val);
        mOrientation = val;

		// @SAM - change to remove SP & EQ orientation change
        //for (int i = 0; i < GALAXY_TABS_NUM_IRS; i++) {
        //    bsp[i]->updateOrientation(mOrientation);
        //}
    }
}

bacch_sp_orientation_t BACCH_Audio::getOrientation() {
    return mOrientation;
}

void BACCH_Audio::reset()
{
	bhpBlocksInTransition = 0;
    bspBlocksInTransition = 0;
    blcBlocksInTransition = 0;

    eqSlide->reset();

    bhpState = STATE_BYPASS;
	bspState = STATE_BYPASS;
    blcState = STATE_BYPASS;
    beqState = STATE_BYPASS;

	clear();
}

void BACCH_Audio::clear()
{
	int filterNum = 0;

	for (filterNum = 0; filterNum < HP_NUM_IRS; filterNum++) {
		bhp->reset();
	}
	
	for (filterNum = 0; filterNum < GALAXY_TABS_NUM_IRS; filterNum++) {
		bsp->reset();
	}

//    for (filterNum = 0; filterNum < EQ_NUM_IRS; filterNum++) {
//        beq[filterNum]->clear();
//    }

    bcex->clear();

}

// populates values in array, follows order in effect_bacch_audio.h
int BACCH_Audio::getState() {
    int state = 0;

    if (bhpState > STATE_TRANSITION_TO_BYPASS) state |= (BACCH_FEATURE_ON << BACCH_HP_STATUS_SHIFT);

    if (bspState > STATE_TRANSITION_TO_BYPASS) state |= (BACCH_FEATURE_ON << BACCH_SP_STATUS_SHIFT);

    if (blcState > STATE_TRANSITION_TO_BYPASS) state |= (BACCH_FEATURE_ON << BACCH_LCEX_STATUS_SHIFT);
    return state;
}

void BACCH_Audio::setEnabled(bool enable, bool isHP, bool immediate){

    if (enable) {
        if (isHP)
            setHPEnable(enable, true, false);
        else
            setSPEnable(enable, true, false);
    }
    else {
        lastSPState = bspState;
        lastHPState = bhpState;
        setHPEnable(enable, true, false);
        setSPEnable(enable, true, false);
    }
}

void BACCH_Audio::setHPEnable(bool enabled, bool isUser, bool immediate)
{
    immediate = true;
    if (enabled) {
        if (bv != NULL) {
            bv->setRIR(false);
        }
        if (immediate) {
            bhpState = STATE_NORMAL;
            // activate the sp output even though it won't be primed
            bhpOutputGoodCnt = HP_IR_NUM_BLOCKS[mSampleRateIndex];
        }
        else {
            switch (bhpState) {
            case STATE_TRANSITION_TO_BYPASS:
                bhpState = STATE_TRANSITION_TO_NORMAL;
                // reverse the count, going the other way now
                bhpBlocksInTransition = mNumBlocksToTransition - bhpBlocksInTransition;
                break;
            case STATE_BYPASS:
                bhpState = STATE_TRANSITION_TO_NORMAL;
                bhpBlocksInTransition = 0;
                break;
            case STATE_TRANSITION_TO_NORMAL:
            case STATE_NORMAL:
                break;
            }
        }
    }
    else {
        if (immediate)
            bhpState = STATE_BYPASS;
        else {
            switch (bhpState) {
            case STATE_TRANSITION_TO_NORMAL:
                bhpState = STATE_TRANSITION_TO_BYPASS;
                bhpBlocksInTransition = mNumBlocksToTransition - bhpBlocksInTransition;
                break;
            case STATE_NORMAL:
                bhpState = STATE_TRANSITION_TO_BYPASS;
                bhpBlocksInTransition = 0;
                break;
            case STATE_TRANSITION_TO_BYPASS:
            case STATE_BYPASS:
                break;
            }
        }
    }
    if (isUser) {   // save state of other modules
		if (enabled)
        setSPEnable(false, false, immediate);
    }
}

void BACCH_Audio::setSPEnable(bool enabled, bool isUser, bool immediate)
{
    immediate = true;
    if (enabled) {
        if (bv != NULL) {
            bv->setRIR(true);
        }

        if (immediate) {
            bspState = STATE_NORMAL;
            // activate the sp output even though it won't be primed
            bspOutputGoodCnt = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIndex];
        }
        else {
            switch (bspState) {
            case STATE_TRANSITION_TO_BYPASS:
                bspState = STATE_TRANSITION_TO_NORMAL;
                // reverse the count, going the other way now
                bspBlocksInTransition = mNumBlocksToTransition - bspBlocksInTransition;
                // no need to change output good cnt, already primed
                bspOutputGoodCnt = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIndex];
                break;
            case STATE_BYPASS:
                bspState = STATE_TRANSITION_TO_NORMAL;
                bspBlocksInTransition = 0;
                bspOutputGoodCnt = 0;
                break;
            case STATE_TRANSITION_TO_NORMAL:
            case STATE_NORMAL:
                break;
            }
        }
    }
    else {
        if (immediate)
            bspState = STATE_BYPASS;
        else {
            switch (bspState) {
            case STATE_TRANSITION_TO_NORMAL:
                bspState = STATE_TRANSITION_TO_BYPASS;
                bspBlocksInTransition = mNumBlocksToTransition - bspBlocksInTransition;
                break;
            case STATE_NORMAL:
                bspState = STATE_TRANSITION_TO_BYPASS;
                bspBlocksInTransition = 0;
                break;
            case STATE_TRANSITION_TO_BYPASS:
            case STATE_BYPASS:
                break;
            }
        }
    }

    if (isUser) {
		if (enabled)
			setHPEnable(false, false, immediate);
    }
}

void BACCH_Audio::setLCexEnable(bool enabled, bool immediate) {
    immediate = true;
    if (enabled) {
        if (immediate) {
            blcState = STATE_NORMAL;
            // activate the sp output even though it won't be primed
            blcOutputGoodCnt = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIndex];
        }
        else {
            switch (blcState) {
                case STATE_TRANSITION_TO_BYPASS:
                    blcState = STATE_TRANSITION_TO_NORMAL;
                    // reverse the count, going the other way now
                    blcBlocksInTransition = mNumBlocksToTransition - blcBlocksInTransition;
                    // no need to change output good cnt, already primed
                    blcOutputGoodCnt = GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIndex];
                    break;
                case STATE_BYPASS:
                    blcState = STATE_TRANSITION_TO_NORMAL;
                    blcBlocksInTransition = 0;
                    blcOutputGoodCnt = 0;
                    break;
                case STATE_TRANSITION_TO_NORMAL:
                case STATE_NORMAL:
                    break;
            }
        }
    }
    else {
        if (immediate)
            blcState = STATE_BYPASS;
        else {
            switch (blcState) {
                case STATE_TRANSITION_TO_NORMAL:
                    blcState = STATE_TRANSITION_TO_BYPASS;
                    blcBlocksInTransition = mNumBlocksToTransition - blcBlocksInTransition;
                    break;
                case STATE_NORMAL:
                    blcState = STATE_TRANSITION_TO_BYPASS;
                    blcBlocksInTransition = 0;
                    break;
                case STATE_TRANSITION_TO_BYPASS:
                case STATE_BYPASS:
                    break;
            }
        }
    }
}

bool BACCH_Audio::getLCexEnable() {
    return (blcState > STATE_TRANSITION_TO_BYPASS) ? true : false;
}

void BACCH_Audio::setEQEnable(bool enabled, bool immediate)
{
//    if (enabled) {
//        if (immediate) {
//            beqState = STATE_NORMAL;
//            //eqSlide->slide();
//            // activate the sp output even though it won't be primed
//            beqOutputGoodCnt = EQ_IR_NUM_BLOCKS[mSampleRateIndex];
//        }
//        else {
//            switch (beqState) {
//            case STATE_TRANSITION_TO_BYPASS:
//                beqState = STATE_TRANSITION_TO_NORMAL;
//                // reverse the count, going the other way now
//                beqBlocksInTransition = mNumBlocksToTransition - beqBlocksInTransition;
//                // no need to change output good cnt, already primed
//                beqOutputGoodCnt = EQ_IR_NUM_BLOCKS[mSampleRateIndex];
//                break;
//            case STATE_BYPASS:
//                beqState = STATE_TRANSITION_TO_NORMAL;
//                beqBlocksInTransition = 0;
//                beqOutputGoodCnt = 0;
//                break;
//            case STATE_TRANSITION_TO_NORMAL:
//            case STATE_NORMAL:
//                break;
//            }
//        }
//    }
//    else {
//        if (immediate)
//            beqState = STATE_BYPASS;
//        else {
//            switch (beqState) {
//            case STATE_TRANSITION_TO_NORMAL:
//                beqState = STATE_TRANSITION_TO_BYPASS;
//                beqBlocksInTransition = mNumBlocksToTransition - beqBlocksInTransition;
//                break;
//            case STATE_NORMAL:
//                beqState = STATE_TRANSITION_TO_BYPASS;
//                beqBlocksInTransition = 0;
//                break;
//            case STATE_TRANSITION_TO_BYPASS:
//            case STATE_BYPASS:
//                break;
//            }
//        }
//    }
}

bool BACCH_Audio::getEQEnable() {
    return false; //(beqState>STATE_TRANSITION_TO_BYPASS)?true:false;
}

int BACCH_Audio::processHP(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer) {
    int iRet = 0;
    int filterNum = 0;
    int filtIndex = 0;
	float bypassGain = pow(10.0f, -5.5f / 10.0f);

    bhp->process(inbuffer, outbuffer);
    iRet = BACCH_FRAME_SIZE;

    // account for state transition
    if (bhpState < STATE_NORMAL) {
        // is BACCH_HP primed yet? don't use output until it is
        if (bhpOutputGoodCnt < HP_IR_NUM_BLOCKS[mSampleRateIndex]) {
            bhpOutputGoodCnt++;
            vMulC(inbuffer->data[BACCH_LEFT], bypassGain, outbuffer->data[BACCH_LEFT], BACCH_FRAME_SIZE);
            vMulC(inbuffer->data[BACCH_RIGHT], bypassGain, outbuffer->data[BACCH_RIGHT], BACCH_FRAME_SIZE);
        }
        else {
            setBHPStateTransition();
            bhpBlocksInTransition++;
            // LEFT
            vMul_I(hpStateTransition[BACCH_PROCESSED], outbuffer->data[BACCH_LEFT], BACCH_FRAME_SIZE);
            vMul_I(hpStateTransition[BACCH_BYPASS], inbuffer->data[BACCH_LEFT], BACCH_FRAME_SIZE);
            vMulC_I(inbuffer->data[BACCH_LEFT], bypassGain, BACCH_FRAME_SIZE);
            vAdd_I(inbuffer->data[BACCH_LEFT], outbuffer->data[BACCH_LEFT], BACCH_FRAME_SIZE);
            // RIGHT
            vMul_I(hpStateTransition[BACCH_PROCESSED], outbuffer->data[BACCH_RIGHT], BACCH_FRAME_SIZE);
            vMul_I(hpStateTransition[BACCH_BYPASS], inbuffer->data[BACCH_RIGHT], BACCH_FRAME_SIZE);
            vMulC_I(inbuffer->data[BACCH_RIGHT], bypassGain, BACCH_FRAME_SIZE);
            vAdd_I(inbuffer->data[BACCH_RIGHT], outbuffer->data[BACCH_RIGHT], BACCH_FRAME_SIZE);
        }
    }

    /********************* SPECIAL CASE ********************/
    outbuffer->mNumSamplesInBuffer = outbuffer->mWriteIndex = BACCH_FRAME_SIZE;
    inbuffer->mNumSamplesInBuffer -= BACCH_FRAME_SIZE;
    inbuffer->mReadIndex += BACCH_FRAME_SIZE;
    inbuffer->reAlign();

    return iRet;
}

int BACCH_Audio::processSP(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer) {
    int iRet = BACCH_FRAME_SIZE;
    int filterNum = 0;
    int filtIndex = 0;
	float bypassGain = pow(10.0f, -5.5f / 10.0f);
	int numSamples = BACCH_FRAME_SIZE;

    if (getLCexEnable()) {
        mCoutBuffer->clear();
        processBuf->clear();
        iRet = processCex(inbuffer, processBuf, mCoutBuffer);
        bsp->process(processBuf, outbuffer);
        vAdd_I(mCoutBuffer->data[0], outbuffer->data[0], BACCH_FRAME_SIZE);
        vAdd_I(mCoutBuffer->data[0], outbuffer->data[1], BACCH_FRAME_SIZE);
    } else {
        bsp->process(inbuffer, outbuffer);
    }

    // account for state transition
    if (bspState < STATE_NORMAL) {
        // is BACCH_SP primed yet? don't use output until it is
        if (bspOutputGoodCnt < GALAXY_TABS_IR_NUM_BLOCKS[mSampleRateIndex]) {
            bspOutputGoodCnt++;
            // @SAM - change to remove SP & EQ orientation change
            vMulC(inbuffer->data[BACCH_LEFT], bypassGain, outbuffer->data[BACCH_LEFT], numSamples);
            vMulC(inbuffer->data[BACCH_RIGHT], bypassGain, outbuffer->data[BACCH_RIGHT], numSamples);
        } else {
            setBSPStateTransition();
            bspBlocksInTransition++;
            // LEFT
            vMul_I(spStateTransition[BACCH_PROCESSED], outbuffer->data[BACCH_LEFT], numSamples);
            vMulC_I(inbuffer->data[BACCH_LEFT], bypassGain, numSamples);
            vMul_I(spStateTransition[BACCH_BYPASS], inbuffer->data[BACCH_LEFT], numSamples);
            vAdd_I(inbuffer->data[BACCH_LEFT], outbuffer->data[BACCH_LEFT], numSamples);
            // RIGHT
            vMul_I(spStateTransition[BACCH_PROCESSED], outbuffer->data[BACCH_RIGHT], numSamples);
            vMulC_I(inbuffer->data[BACCH_RIGHT], bypassGain, numSamples);
            vMul_I(spStateTransition[BACCH_BYPASS], inbuffer->data[BACCH_RIGHT], numSamples);
            vAdd_I(inbuffer->data[BACCH_RIGHT], outbuffer->data[BACCH_RIGHT], numSamples);
        }
    }

    /********************* SPECIAL CASE ********************/
    outbuffer->mNumSamplesInBuffer = outbuffer->mWriteIndex = BACCH_FRAME_SIZE;
    inbuffer->mNumSamplesInBuffer -= BACCH_FRAME_SIZE;
    inbuffer->mReadIndex += BACCH_FRAME_SIZE;
    inbuffer->reAlign();

    return iRet;
}

int BACCH_Audio::processXTC(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer) {

	bxtc->process(inbuffer, outbuffer);

	return BACCH_FRAME_SIZE;
}

int BACCH_Audio::processEQ(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer) {
	int numSamples = inbuffer->getNumSamplesInBuffer();

	beq[0]->process(inbuffer, outbuffer, BACCH_SP_ORIENTATION_LANDSCAPE);

    return numSamples;
}

int BACCH_Audio::processCex(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, BACCH_buffer* coutbuffer) {

    bcex->process(inbuffer, outbuffer, coutbuffer);

    return BACCH_FRAME_SIZE;
}

int BACCH_Audio::processComp(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer, bool isHP ) {

    bcomp->process(inbuffer, outbuffer, isHP);

    return BACCH_FRAME_SIZE;
}

int BACCH_Audio::processVirt(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer) {

    bv->process(inbuffer, outbuffer);

    outbuffer->mNumSamplesInBuffer = outbuffer->mWriteIndex = BACCH_FRAME_SIZE;

    return BACCH_FRAME_SIZE;
}

int BACCH_Audio::process_array(float *in, float *out, int numsamples, int numchannels) {
    int iRet = 0;

    BACCH_buffer *inBB = new BACCH_buffer(numchannels, BACCH_FRAME_SIZE);
    BACCH_buffer *outBB = new BACCH_buffer(2, BACCH_FRAME_SIZE);

    for (int i = 0; i < numchannels; i++) {
		inBB->data[i] = (float *) &in[i * BACCH_FRAME_SIZE];
	}

    outBB->data[0] = (float *) out;
    outBB->data[1] = (float *) &out[BACCH_FRAME_SIZE];

	inBB->mNumSamplesInBuffer = inBB->mWriteIndex = inBB->mBufferLen = BACCH_FRAME_SIZE;
	outBB->mNumSamplesInBuffer = outBB->mReadIndex = outBB->mWriteIndex = 0;
	outBB->mBufferLen = BACCH_FRAME_SIZE;

	//bufCopy(inBB, outBB, numsamples);
	iRet = process_block(inBB, outBB);

	delete inBB;
	delete outBB;

    return iRet;
}

// this function only processes BACCH_FRAME_SIZE sample frames
// it uses the raw arrays of the buffers, it does not update the buffer info
int BACCH_Audio::process_block(BACCH_buffer* inbuffer, BACCH_buffer* outbuffer)
{
	int iRet = 0;

    BACCH_buffer *p1;
    BACCH_buffer *p2;

    BACCH_buffer *tmpBuf;

    inFBuf->reset();
    outFBuf->reset();

    p1 = inFBuf;
    p2 = outFBuf;

    if ( mNumChannelsIn > 2 ) {
        bv->process(inbuffer, virtBuf);
        virtBuf->mWriteIndex = virtBuf->mNumSamplesInBuffer = BACCH_FRAME_SIZE;
        tmpBuf = virtBuf;
    } else {
        tmpBuf = inbuffer;
    }

    // HP -> Compressor Chain
    if (bhpState > STATE_BYPASS) {

        if (getLCexEnable()) {
            mCoutBuffer->clear();
            iRet = processCex(tmpBuf, p1, mCoutBuffer); // checked
            iRet = processXTC(p1, p2); // checked
            vAdd_I(mCoutBuffer->data[0], p2->data[0], BACCH_FRAME_SIZE);
            vAdd_I(mCoutBuffer->data[0], p2->data[1], BACCH_FRAME_SIZE);
            p1->reset();
            iRet = processHP(p2, p1);   // checked
            iRet = processComp(p1, outbuffer, true);    // fails on isHP = true (rerouted in BACCH_compressor temporarily)
        }
        else {
            iRet = processHP(tmpBuf, p1);
            iRet = processComp(p1, outbuffer, true);
        }
    } 
    else if (bspState > STATE_BYPASS) {     // checked
        iRet = processSP(tmpBuf, p1);
        iRet = processComp(p1, outbuffer, false);

        if (getEQEnable()) {                // checked
            iRet = processEQ(p1, outbuffer);
        }
#ifdef _FDEBUG
        if (dfile != NULL) {
            int numchannels = 4;
            BACCH_buffer *debugBuf = new BACCH_buffer(numchannels, BACCH_FRAME_SIZE);
            vCopy(inbuffer->data[0], debugBuf->data[0],BACCH_FRAME_SIZE );
            vCopy(inbuffer->data[1], debugBuf->data[1],BACCH_FRAME_SIZE );
            vCopy(outbuffer->data[0], debugBuf->data[2],BACCH_FRAME_SIZE );
            vCopy(outbuffer->data[1], debugBuf->data[3],BACCH_FRAME_SIZE );
            //bufCopy(inbuffer, debugBuf, BACCH_FRAME_SIZE);
            int16_t *i16buf = (int16_t *) blMalloc(sizeof(int16_t), numchannels * BACCH_FRAME_SIZE, 64);
            vBlockToInterleaved_32f_16s(debugBuf->data, i16buf, 1.0f, numchannels, BACCH_FRAME_SIZE, false);
            fwrite((char *) i16buf, 1, (long long) (numchannels * BACCH_FRAME_SIZE), dfile);
            blFree(i16buf);
            delete debugBuf;
        }
#endif
    }
    else {
        bufCopy(tmpBuf, outbuffer, BACCH_FRAME_SIZE);
        iRet = BACCH_FRAME_SIZE;
    }
     
    return iRet;
}

void BACCH_Audio::setStateTransition(state_t state, int blocksInTransition, float** fadeArray) {
	int i = 0;
	int fadeIdx = blocksInTransition * BACCH_FRAME_SIZE;

    if (state == STATE_TRANSITION_TO_NORMAL) {
	    fadeArray[BACCH_PROCESSED] = &mTransitionFade->data[BACCH_TRANSITION_ON][fadeIdx];
	    fadeArray[BACCH_BYPASS] = &mTransitionFade->data[BACCH_TRANSITION_OFF][fadeIdx];
    }
    else {
        fadeArray[BACCH_PROCESSED] = &mTransitionFade->data[BACCH_TRANSITION_OFF][fadeIdx];
        fadeArray[BACCH_BYPASS] = &mTransitionFade->data[BACCH_TRANSITION_ON][fadeIdx];
    }
}

void BACCH_Audio::setBHPStateTransition() {

	if (bhpBlocksInTransition >= mNumBlocksToTransition) {	// transition complete, set new state and return
        bhpBlocksInTransition = 0;
		if (bhpState == STATE_TRANSITION_TO_BYPASS)
			bhpState = STATE_BYPASS;
		else
			bhpState = STATE_NORMAL;
	}
    else {
        setStateTransition(bhpState, bhpBlocksInTransition, hpStateTransition);
    }
}

void BACCH_Audio::setBSPStateTransition() {

	if (bspBlocksInTransition >= mNumBlocksToTransition) {	// transition complete, set new state and return
        bspBlocksInTransition = 0;
		if (bspState == STATE_TRANSITION_TO_BYPASS)
			bspState = STATE_BYPASS;
		else
			bspState = STATE_NORMAL;
	}
    else {
        setStateTransition(bspState, bspBlocksInTransition, spStateTransition);
    }
} 
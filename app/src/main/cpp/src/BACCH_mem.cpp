#include "BACCH_mem.h"
#include <malloc.h>
#include <stdlib.h>

#ifdef __USE_IPP__
#include "ipps.h"
#include "ippcore.h"
#include "ippvm.h"
#endif // __USE_IPP

void* blMalloc(SInt32 elementSize, SInt32 length, SInt32 alignment) {
#ifdef __USE_IPP__
    if (elementSize == sizeof(int16_t))
        return ippsMalloc_16s(length);
    else if (elementSize == sizeof(float))
        return ippsMalloc_32f(length);
    else if (elementSize == sizeof(FFTComplex))
        return ippsMalloc_32fc(length);
    else
        return ippsMalloc_8u(elementSize * length);
#else
    // implement an aligned memory allocate function
#ifdef _MSC_VER
    return _aligned_malloc(elementSize * length, alignment);
#else
    void *ptr;
    posix_memalign(&ptr, alignment, elementSize * length ); //alignment, elementSize * length);
    return ptr;
#endif
#endif // __USE_IPP
}

void blFree(void* ptr) {
#ifdef __USE_IPP__
    ippsFree(ptr);
#else
#ifdef _MSC_VER
    _aligned_free(ptr);
#else
    free(ptr);
#endif
#endif // __USE_IPP
}

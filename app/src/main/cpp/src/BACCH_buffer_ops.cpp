#include "BACCH_types.h"
#include "BACCH_buffer_ops.h"
#include "BACCH_buffer.h"
#include "BACCH_complex_buffer.h"
#include "BACCH_ibuffer.h"
#include "BACCH_mem.h"
#include "BACCH_supporting.h"
#include <malloc.h>
#include <stdlib.h> // div_t
#ifndef _MSC_VER
#include <android/log.h>
#endif

#ifdef __USE_IPP__
#include "ipps.h"
#include "ippcore.h"
#include "ippvm.h"
#endif // __USE_IPP

static int16_t* sScratch = NULL;
#ifdef __USE_IPP__
static float* fScratch = NULL;
static const float zeros[MAX_IN_CHANNELS*BACCH_FFT_SIZE*COMPLEX_WORDS] = { 0 };
#endif

template <typename T>
static inline T clamp(T val, T lo, T hi) {
    T tmp = (hi > val) ? val : hi;
    return (lo > tmp) ? lo : tmp; 
}

void init_buffer_ops() {
    sScratch = (int16_t*)blMalloc(sizeof(int16_t), MAX_IN_CHANNELS*BACCH_FRAME_SIZE, 64);
    //if (sScratch == NULL)
        //mThrow("Failed to allocate memory for buffer_ops");
#ifdef __USE_IPP__
    fScratch = (float*)blMalloc(sizeof(float), MAX_IN_CHANNELS*BACCH_FRAME_SIZE, 64);
    // the zeros buffer is used in vClear and vRealToComplex
    //zeros = (float*)blMalloc(sizeof(float), MAX_IN_CHANNELS*BACCH_FFT_SIZE*COMPLEX_WORDS, 64);
    if (fScratch == NULL ) //|| zeros == NULL)
        mThrow("Failed to allocate memory for buffer_ops");
    //else
        //vSet(zeros, 0.0f, MAX_IN_CHANNELS*BACCH_FFT_SIZE*COMPLEX_WORDS);
#endif
}

void delete_buffer_ops() {
#ifdef __USE_IPP__
    if (NULL != fScratch) blFree(fScratch);
    //if (NULL != zeros) blFree(zeros);
#endif
    if (NULL != sScratch) blFree(sScratch);
}


void bufReset(BACCH_buffer * dst)
{
    dst->mNumSamplesInBuffer = 0;
    dst->mReadIndex = 0;
    dst->mWriteIndex = 0;
}

void bufClear(BACCH_buffer * dst)
{
    vClear(dst->chunk, dst->mBufferLen*dst->mNumChannels);

    dst->mNumSamplesInBuffer = 0;
    dst->mReadIndex = 0;
    dst->mWriteIndex = 0;
}

void bufCopy(BACCH_buffer * src, BACCH_buffer * dst, int numSamples)
{
    if (( src->mNumChannels == dst->mNumChannels ) &&                   // same number of channels
        (src->mNumSamplesInBuffer >= numSamples) &&                     // enough data in src
        ((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples)) { // enough space in dst
        for (int i = 0; i < src->mNumChannels; i++) {                   // copy the data
            vCopy(&src->data[i][src->mReadIndex], &dst->data[i][dst->mWriteIndex], numSamples);
        }
        // update src buffer
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();
        // update the dst buffer
        dst->mWriteIndex += numSamples;
        dst->mNumSamplesInBuffer += numSamples;
    }
}

void bufBlockToBuf(float** src, BACCH_buffer* dst, int numChannels, int numSamples)
{
    if (((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples) && // enough space in dst 
        ( dst->mNumChannels >= numChannels)) {
        for (int i = 0; i < numChannels; i++) {                    // copy the data
            vCopy(src[i], &dst->data[i][dst->mWriteIndex], numSamples);
        }
        // update the dst buffer
        dst->mWriteIndex += numSamples;
        dst->mNumSamplesInBuffer += numSamples;
    }
}

void bufBufToBlock(BACCH_buffer* src,float** dst, int numChannels, int numSamples)
{
    if ((src->mNumSamplesInBuffer >= numSamples) && // enough space in dst 
        (src->mNumChannels >= numChannels)) {
        for (int i = 0; i < numChannels; i++) {                    // copy the data
            vCopy(&src->data[i][src->mReadIndex], dst[i], numSamples);
        }
        // update the dst buffer
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();
    }
}

void bufDuplicate(BACCH_buffer * src, BACCH_buffer * dst, int numSamples)
{
    if ((src->mNumChannels == dst->mNumChannels) &&                 // same number of channels
        (src->mNumSamplesInBuffer >= numSamples) &&                 // enough data in src
        (dst->mBufferLen >= numSamples)) {                          // enough space in dst
        for (int i = 0; i < src->mNumChannels; i++) {               // copy the data
            vCopy(&src->data[i][src->mReadIndex], dst->data[i], numSamples);
        }
        // update the dst buffer
        dst->mReadIndex = 0;
        dst->mWriteIndex = numSamples;
        dst->mNumSamplesInBuffer = numSamples;
    }
}

// 
void bufAdd_I(BACCH_buffer * src, BACCH_buffer * srcdst, int numSamples)
{
    if ((src->mNumChannels == srcdst->mNumChannels) &&              // same number of channels
        (src->mNumSamplesInBuffer >= numSamples)) {                 // enough data in src
        if (srcdst->mNumSamplesInBuffer < numSamples) {             // just copy the data
            for (int i = 0; i < src->mNumChannels; i++) {
                vCopy(&src->data[i][src->mReadIndex], srcdst->data[i], numSamples);
            }
            srcdst->mReadIndex = 0;
            srcdst->mWriteIndex = srcdst->mNumSamplesInBuffer = numSamples;
        }
        else {
            for (int i = 0; i < src->mNumChannels; i++) {           // add the data
                vAdd_I(&src->data[i][src->mReadIndex], &srcdst->data[i][srcdst->mReadIndex], numSamples);
            }
        }
        // update the src buffer
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();
    }
}

void bufMul_I(BACCH_buffer * src, BACCH_buffer * srcdst, int numSamples)
{
    if ((src->mNumChannels == srcdst->mNumChannels) &&              // same number of channels
        (src->mNumSamplesInBuffer >= numSamples) &&                 // enough data in src
        (srcdst->mNumSamplesInBuffer >= numSamples)) {                       // enough space in dst
        for (int i = 0; i < src->mNumChannels; i++) {               // copy the data
            vMul_I(&src->data[i][src->mReadIndex], &srcdst->data[i][srcdst->mReadIndex], numSamples);
        }
        // update the src buffer
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();
    }
}


void vClear(float* dst, int length) {
#ifdef __USE_IPP__
	IppStatus status = 0;
	//if (length > MAX_IN_CHANNELS*BACCH_FFT_SIZE)
		status = ippsSet_32f(0.0f, dst, length);
	//else
		status = ippsCopy_32f(zeros, dst, length);
    //ippsZero_32f(dst, length);

    if (status != ippStsNoErr)
        printf("Error zeroing data %d", status);
#else
    for (int i = 0; i < length; i++) {
        dst[i] = 0.0f;
    }
#endif // __USE_IPP__
}

int vSet(float* dst, float val, int length) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsSet_32f(val, dst, length);
    if (status != ippStsNoErr)
        printf("vSet: Error setting data of length %d, Status: %d\n", length, status);
    else
        iRet = length;
#else
    for (int i = 0; i < length; i++) {
        dst[i] = val;
    }
#endif // __USE_IPP__
    return iRet;
}

int vCopy(float* src, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsCopy_32f(src, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vCopy: Error copying data of length: %d, Status: %d\n", numSamples, status);
    else
        iRet = numSamples;
#else
    int i = 0;
    /*float* pDst = dst;
    float* pSrc = src;
    int cnt = numSamples / 2;*/

    for (i = 0; i < numSamples; i++) {
        dst[i] = src[i];
    }
    iRet = numSamples;
    /*while (cnt--) {
        pDst[0] = pSrc[0];
        pDst[1] = pSrc[1];
        pDst += 2;
        pSrc += 2;
    }*/
#endif // __USE_IPP__
    return iRet;
}

int vAdd(float* src1, float* src2, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsAdd_32f(src1, src2, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vAdd: Error adding data %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for(i=0;i<numSamples;i++)  {
        dst[i] = src1[i] + src2[i];
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vAdd_I(float* src1, float* srcdst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsAdd_32f_I(src1, srcdst, numSamples);
    if (status != ippStsNoErr)
        printf("vAdd: Error adding data %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        srcdst[i] += src1[i];
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vAddProductC(float* src1, float c, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsAddProductC_32f(src1, c, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        dst[i] += (src1[i] * c);
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vAddProduct(float* src1, float *src2, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsAddProduct_32f(src1, src2, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        dst[i] += (src1[i] * src2[i]);
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vMul(float *src1, float *src2, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsMul_32f(src1, src2, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        dst[i] = (src1[i] * src2[i]);
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vMul_I(float *src1, float *srcdst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsMul_32f_I(src1, srcdst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        srcdst[i] *= src1[i];
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vMulC(float *src1, float c, float* dst, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsMulC_32f(src1, c, dst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        dst[i] = (src1[i] * c);
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vMulC_I(float *srcdst, float c, int numSamples) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsMulC_32f_I(c, srcdst, numSamples);
    if (status != ippStsNoErr)
        printf("vAddProduct: Error in ippsAddProduct %d", status);
    else
        iRet = numSamples;
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        srcdst[i] *= c;
    }
    iRet = numSamples;
#endif // __USE_IPP__

    return iRet;
}

int vAdd(float* src, float* srcdst, int numSamples) {
    int iRet = numSamples;
#ifdef __USE_IPP__
    IppStatus status = ippsAdd_32f_I(src, srcdst, numSamples);
    if (status != ippStsNoErr) {
        printf("Error adding data %d", status);
        iRet = 0;
    }
#else
    int i = 0;
    for (i = 0; i < numSamples; i++) {
        srcdst[i] += src[i];
    }
#endif // __USE_IPP__
    return iRet;
}

int vComplexToReal(FFTComplex* in, float* out, int numSamples, bool accum) {
    int iRet = 0;
#ifdef __USE_IPP__
    IppStatus status = ippsCplxToReal_32fc((Ipp32fc*)in, out, fScratch, numSamples);
    if (status != ippStsNoErr)
        printf("blReadReal: Error Status: %d", status);
    else
        iRet = numSamples;
#else
    if (accum) {
        for (int j = 0; j < numSamples; j++) {
            out[j] += in[j].re;
        }
    }
    else {
        for (int j = 0; j < numSamples; j++) {
            out[j] = in[j].re;
        }
    }
    iRet = numSamples;
#endif // __USE_IPP__
    return iRet;
}

int vRealToComplex(float* in, FFTComplex* out, int numSamples) {
    int iRet = 0;

#ifdef __USE_IPP__
    IppStatus status = ippsRealToCplx_32f(in, zeros, (Ipp32fc*)out, numSamples);
    if (status != ippStsNoErr)
        printf("blReadReal: Error Status: %d", status);
    else
        iRet = numSamples;
#else
    int j = 0;
    for (j = 0; j < numSamples; j++) {
        out[j].re = in[j];
        out[j].im = 0;
    }
    iRet = numSamples;
#endif // __USE_IPP__
    return iRet;
}

// assumes aligned and integer multiple of 8 bytes aka 4 shorts
int interleave(int16_t * const *in, int16_t *out, int numChannels, int numSamples, bool accum = false) {
    int iRet = 0;
    int i = 0, j = 0;

    if (numChannels == 2) {
        div_t dstLength_2 = div(numSamples, 2);
        ab2x2_t *dst64 = (ab2x2_t*)out;
        int i = 0, j = 0;
        if (accum) {
            for (i = 0; i < dstLength_2.quot; i++) {
                //ab2x2_t cursor;
                dst64[i].narrow.a1 += in[0][j];
                dst64[i].narrow.b1 += in[1][j++];
                dst64[i].narrow.a2 += in[0][j];
                dst64[i].narrow.b2 += in[1][j++];
            }
        }
        else {
            for (i = 0; i < dstLength_2.quot; i++) {
                //ab2x2_t cursor;
                dst64[i].narrow.a1 = in[0][j];
                dst64[i].narrow.b1 = in[1][j++];
                dst64[i].narrow.a2 = in[0][j];
                dst64[i].narrow.b2 = in[1][j++];
            }
        }
        iRet = dstLength_2.quot * 2;
    }
    else {
        int						sampleIdx, channelIdx;
        int16					*iout = out;
        int16_t * const			*sIn = in;

        if (accum) {
            for (sampleIdx = 0; sampleIdx < numSamples; sampleIdx++) {
                for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
                    *iout++ += sIn[channelIdx][sampleIdx];
                }
            }
        }
        else {
            // need to rework this, inefficient as is
            for (sampleIdx = 0; sampleIdx < numSamples; sampleIdx++) {
                for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
                    *iout++ = sIn[channelIdx][sampleIdx];
                }
            }
        }
        iRet = numSamples;
    }
    return iRet;
}

// on Intel, want to put/get in 8 byte increments... 
int deinterleave(const int16_t *in, int16_t **out, int numChannels, int numSamples) {
    int iRet = 0;
    int16 const		        *iIn = in;
    int16 ** __restrict  	iOut = out;
    int			            i = 0, j = 0;
    const int16_t           *pSrc;
    int16_t                 *pDst[MAX_IN_CHANNELS];

    switch (numChannels) {
    case 2: {
        div_t srcLength_2 = div(numSamples, 2);
        ab2x2_t *src64 = (ab2x2_t*)in;
        for (i = 0; i < srcLength_2.quot; i++) {
            out[0][j] = src64[i].narrow.a1;
            out[1][j++] = src64[i].narrow.b1;
            out[0][j] = src64[i].narrow.a2;
            out[1][j++] = src64[i].narrow.b2;
            iRet += 2;
        }
    }break;
    case 3: {
        //abc8x16_t *src = (abc8x16_t*)iIn;

        div_t srcLen_3 = div(numSamples, 3);
        for (i = 0; i < numSamples; i++) {
            iOut[0][i] = *iIn++;
            iOut[1][i] = iIn[j++];
            iOut[2][i] = iIn[j++];
        }
    } break;
    case 4: {
        ab1x4_t sample;
        int64 *in64 = (int64*)in;
        for (j = 0; j < numSamples; j++)
        {
            sample.wide = in64[j];
            iOut[j][0] = sample.narrow.a1;
            iOut[j][1] = sample.narrow.b1;
            iOut[j][2] = sample.narrow.c1;
            iOut[j][3] = sample.narrow.d1;
        }

    } break;
        //	case 5: {

        //	} break;
    case 6: {
        ab2x6_t *in192 = (ab2x6_t*)in;
        i = 0;
        for (j = 0; j < numSamples / 2; j++)
        {
            iOut[0][i] = in192[j].narrow.a1;
            iOut[1][i] = in192[j].narrow.b1;
            iOut[2][i] = in192[j].narrow.c1;
            iOut[3][i] = in192[j].narrow.d1;
            iOut[4][i] = in192[j].narrow.e1;
            iOut[5][i++] = in192[j].narrow.f1;
            iOut[0][i] = in192[j].narrow.a2;
            iOut[1][i] = in192[j].narrow.b2;
            iOut[2][i] = in192[j].narrow.c2;
            iOut[3][i] = in192[j].narrow.d2;
            iOut[4][i] = in192[j].narrow.e2;
            iOut[5][i++] = in192[j].narrow.f2;
        }
    } break;
        //	case 7: {

        //	} break;
    case 8: {
        ab1x8_t *in128 = (ab1x8_t*)in;
        for (j = 0; j < numSamples; j++)
        {
            iOut[0][j] = in128[j].narrow.a1;
            iOut[1][j] = in128[j].narrow.b1;
            iOut[2][j] = in128[j].narrow.c1;
            iOut[3][j] = in128[j].narrow.d1;
            iOut[4][j] = in128[j].narrow.e1;
            iOut[5][j] = in128[j].narrow.f1;
            iOut[6][j] = in128[j].narrow.g1;
            iOut[7][j] = in128[j].narrow.h1;
        }

    } break;
    default: {
        //gonna assume an even number of channels. i.e. 4, 6 or 8
        pSrc = in;
        for (i = 0; i < numChannels; i++) {
            pDst[i] = out[i];
        }
        for (i = 0; i < numChannels; i++) {
            for (j = 0; j < numSamples; j++) {
                pDst[i][j] = pSrc[j * numChannels + i];
            }
        }
    }
    }
    return iRet;
}


int bufDeinterleave(const int16_t *intrlvd_in, BACCH_buffer* out, float gain, int numChannels, int numSamples) {
    int iReturn = 0;
    float *ptr[MAX_IN_CHANNELS];
    if (((out->mBufferLen - out->mNumSamplesInBuffer) >= numSamples) &&
        (out->mNumChannels >= numChannels)) {

        for (int i = 0; i < numChannels; i++)
            ptr[i] = &out->data[i][out->mWriteIndex];

        vIntrlvdToBlock_16s_32f(intrlvd_in, (float**)ptr, gain, numChannels, numSamples);

        out->mWriteIndex += numSamples;
        out->mNumSamplesInBuffer += numSamples;
    }

    return iReturn;
}

int cbufCopy(BACCH_complex_buffer * src, BACCH_complex_buffer * dst, int numChannels, int numSamples)
{
    int i = 0, iRet = 0;
    if (( src->mNumChannels >= numChannels ) &&                             // src have enough channels?
        ( dst->mNumChannels >= numChannels ) &&                             // dst have enough channels?          
        ( src->mNumSamplesInBuffer >= numSamples ) &&                       // enough data in src?
        ((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples )) {    // enough space in dst?
        for (i = 0; i < numChannels; i++) {
            vCopy((float*)&src->data[i][src->mReadIndex], (float*)&dst->data[i][dst->mWriteIndex], numSamples * COMPLEX_WORDS);
        }
        // update source
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();

        // update destination
        dst->mWriteIndex += numSamples;
        dst->mNumSamplesInBuffer += numSamples;

        iRet = numSamples;
    }
    return iRet;
}

int cbufIntrlvdToCplx(float * intrlvd_src, BACCH_complex_buffer * dst, int numchannels, int numSamples)
{
    int i, j, iRet = 0;
    //float* pDst;
    //float* pSrc = intrlvd_src;
    if (((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples) &&
        (dst->mNumChannels >= numchannels)) {
        for (i = 0; i < numchannels; i++) {
            //pDst = (float*)&dst->data[i][dst->mReadIndex];
            for (j = 0; j < numSamples; j++) {
                dst->data[i][dst->mWriteIndex + j].re = intrlvd_src[i + (j*numchannels)]; 
                // *pDst++ = *pSrc++; // src[i + j * numchannels];
                dst->data[i][dst->mWriteIndex +j].im = 0;
            }
        }
        iRet = numSamples;
        dst->mWriteIndex += numSamples;
        dst->mNumSamplesInBuffer += numSamples;
    }
    return iRet;
}

int cbufCplxToIntrlvd(BACCH_complex_buffer * src, float * intrlvd_dst, int numchannels, int numSamples)
{
    int i, j, iRet = 0;
    if ((src->mNumSamplesInBuffer >= numSamples) && 
        (numchannels <= src->mNumChannels)) {
        for (i = 0; i < numchannels; i++) {
            for (j = 0; j < numSamples; j++) {
                intrlvd_dst[i + (j * numchannels)] = src->data[i][j + src->mReadIndex].re;
            }
        }
        iRet = numSamples;
       src->mReadIndex += numSamples;
       src->mNumSamplesInBuffer -= numSamples;
       src->reAlign();
    }
    return iRet;
}

int cbufRealtoCplx(BACCH_buffer * src, BACCH_complex_buffer * dst, int numchannels, int numSamples)
{
    int i = 0, iRet = 0;

    if (( src->mNumSamplesInBuffer >= numSamples) &&
        ((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples ) &&
         (src->mNumChannels >= numchannels) &&
         (dst->mNumChannels >= numchannels )) {
        for (i = 0; i < numchannels; i++) {
            iRet = vRealToComplex(&src->data[i][src->mReadIndex], &dst->data[i][dst->mWriteIndex], numSamples);
        }
        iRet = numSamples;
        // cleanup source buffer
        src->mReadIndex += numSamples;
        src->mNumSamplesInBuffer -= numSamples;
        src->reAlign();
        
        // dst buffer
        dst->mWriteIndex += numSamples;
        dst->mNumSamplesInBuffer += numSamples;
    }
    return iRet;
}

// this call will leave src empty, will set dst to numsamples
int cbufCplxtoReal(BACCH_complex_buffer * src, BACCH_buffer * dst, int numchannels, int numSamples, bool accum)
{
    int i, iRet = 0;
    if ((src->mNumSamplesInBuffer >= numSamples) &&
        (src->mNumChannels >= numchannels) &&
        (dst->mNumChannels >= numchannels)) {
        if (accum) {
            for (i = 0; i < numchannels; i++) {
                vComplexToReal(src->data[i], dst->data[i], numSamples, accum);
            }
        }
        else { // not accumulating, make sure there is room
            if ((dst->mBufferLen - dst->mNumSamplesInBuffer) >= numSamples) {
                for (i = 0; i < numchannels; i++) {
                    vComplexToReal(src->data[i], dst->data[i], numSamples, accum);
                }
            }
        }
        iRet = numSamples;
        // update dst buffer
        dst->mReadIndex = 0;
        dst->mWriteIndex = numSamples;
        dst->mNumSamplesInBuffer = numSamples;
        // update src buffer
        src->mReadIndex = src->mWriteIndex = src->mNumSamplesInBuffer = 0;
    }
    return iRet;
}

int vIntrlvdToBlock_16s_32f(const int16_t *intrlvd_in, float** out, float gain, int numChannels, int numSamples) {
    int						iRet = 0;
    int16 const		        *in = intrlvd_in;
    float ** __restrict  	fout = out;
    int			            channelIdx = 0, sampleIdx = 0;
	float			        mGain = gain/NORM_16; // (float)NORM_IN_16 * gain;
    int16_t*				blockIn[MAX_IN_CHANNELS]; // = (int16_t**)malloc(sizeof(int16_t*)*numChannels);
    int16_t*                sPtr;

    if (numSamples > BACCH_FRAME_SIZE)
        sPtr = (int16_t*)blMalloc(sizeof(int16_t), numSamples * numChannels, 64);
    else
        sPtr = sScratch;

    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        blockIn[channelIdx] = (int16_t*)&sPtr[channelIdx * numSamples]; // blMalloc(sizeof(int16_t), numSamples, 64); // SBuf[channelIdx];
    }
    iRet = deinterleave(in, blockIn, numChannels, numSamples);

#ifdef __USE_IPP__
    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        if (ippStsNoErr != ippsConvert_16s32f_Sfs(blockIn[channelIdx], out[channelIdx], numSamples, 0)) {
            printf("vBlockToInterleaved_32f_16s: Convert Error\n");
            iRet = 0;
            break;
        }
        if (ippStsNoErr != ippsMulC_32f_I(mGain, out[channelIdx], numSamples)) {
            printf("vBlockToInterleaved_32f_16s: MulC Error\n");
            iRet = 0;
            break;
        }
    }

#else
    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        for (sampleIdx = 0; sampleIdx < numSamples; sampleIdx+=2)
        {
            fout[channelIdx][sampleIdx] = mGain * ((float)blockIn[channelIdx][sampleIdx]);
			fout[channelIdx][sampleIdx+1] = mGain * ((float)blockIn[channelIdx][sampleIdx+1]);
        }
    }
#endif // __USE_IPP__

    if (numSamples > BACCH_FRAME_SIZE)
        blFree(sPtr);

    return iRet;
}

int bufInterleave(BACCH_buffer* in, IBuffer_t* intrlvd_out, float gain, int numChannels, int numSamples, bool accum) {
    int iRet = 0;
    status_t mStatus = STATUS_OK;
    float*  pSrc[MAX_IN_CHANNELS];
    int16_t* pDst = intrlvd_out->writePtr();

    if ((in->mNumChannels >= numChannels) &&
        (in->mNumSamplesInBuffer >= numSamples) &&
        (intrlvd_out->numChannels >= numChannels) &&
        ((intrlvd_out->framesCapacity - intrlvd_out->framesCnt) >= numSamples)) {

        for (int i = 0; i < numChannels; i++)
            pSrc[i] = &in->data[i][in->mReadIndex];

        iRet = vBlockToInterleaved_32f_16s(pSrc, pDst, gain, numChannels, numSamples, accum);

        in->mReadIndex += numSamples;
        in->mNumSamplesInBuffer -= numSamples;
        in->reAlign();

        intrlvd_out->updateBuffer(numSamples, BACCH_BUFFER_OP_WRITE);
    }

    return iRet;
}

int vBlockToInterleaved_32f_16s(float ** in, int16_t *intrlvd_out, float gain, int numChannels, int numSamples, bool accum) {
    int         iRet = 0;
    int			channelIdx;
    int16_t*    sPtr;
	int16_t     *tmpSBuf[41];/// MAX_IN_CHANNELS]; // = (int16_t**)malloc(sizeof(int16_t*)*numChannels);

    if (numSamples > BACCH_FRAME_SIZE)
        sPtr = (int16_t*)blMalloc(sizeof(int16_t), numSamples * numChannels, 64);
    else
        sPtr = sScratch;

    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        tmpSBuf[channelIdx] = &sPtr[channelIdx*numSamples];
    }


#ifdef __USE_IPP__    
    IppStatus	status = ippStsNoErr;
    float       *fPtr;
    float       **tmpFBuf = (float**)malloc(sizeof(float*)*numChannels);

    if (numSamples > BACCH_FRAME_SIZE)
        fPtr = (float*)blMalloc(sizeof(float), numSamples * numChannels, 64);
    else
        fPtr = fScratch;

    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
        tmpFBuf[channelIdx] = &fPtr[channelIdx*numSamples];

        if (ippStsNoErr != ippsMulC_32f(in[channelIdx], gain*NORM_OUT_16, tmpFBuf[channelIdx], numSamples)) {
            mThrow("vBlockToInterleaved_32f_16s: MulC Error");
            break;
        }
        // conversion will saturate if input exceeds range
        if (ippStsNoErr != ippsConvert_32f16s_Sfs(tmpFBuf[channelIdx], tmpSBuf[channelIdx], numSamples, ippRndHintAccurate, 0)) {
            mThrow("vBlockToInterleaved_32f_16s: Threshold Error");
            break;
        }
    }

    if (numSamples > BACCH_FRAME_SIZE)
        blFree(fPtr);

    free(tmpFBuf);
    iRet = numSamples;

#else
    int						sampleIdx;
    int16					*iout = intrlvd_out;
    float * const			*fIn = in;
	float					mgain = gain * NORM_OUT_16;

    // multiply each element by gain, clamp, and then multiply by NORM_OUT_16
    for (channelIdx = 0; channelIdx < numChannels; channelIdx++) {
		vMulC_I(fIn[channelIdx], mgain, numSamples);
        for (sampleIdx = 0; sampleIdx < numSamples; sampleIdx++) {
            tmpSBuf[channelIdx][sampleIdx] = (int16_t)clamp(fIn[channelIdx][sampleIdx], -NORM_OUT_16, NORM_OUT_16);
        }
    }
    iRet = numSamples;

#endif // __USE_IPP__

    // then interleave
    if (iRet == numSamples) {		// sanity check
        iRet = interleave(tmpSBuf, intrlvd_out, numChannels, numSamples, accum);
        //if (iRet != numSamples)
            //mThrow("vBlockToInterleaved_32f_16s: Interleave Error");
    }

    if (numSamples > BACCH_FRAME_SIZE)
        blFree(sPtr);


    return iRet;
}
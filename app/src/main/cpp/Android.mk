LOCAL_PATH:= $(call my-dir)

# BACCH Filter library
include $(CLEAR_VARS)

LOCAL_MODULE:= bacchaudio

LOCAL_LDLIBS += \
	-llog \
	-landroid

LOCAL_STATIC_LIBRARIES := ne10_static
LOCAL_STATIC_LIBRARIES += mat_static

LOCAL_CFLAGS	+= -O1
#LOCAL_CFLAGS	+= -fvisibility=hidden
LOCAL_CFLAGS    += -D__ARM_NEON
LOCAL_CFLAGS	+= -D__USE_NE10__

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
#    LOCAL_CFLAGS  += -DIS64
#    LOCAL_CFLAGS  += -DTABS4
endif

# static lib include files
LOCAL_C_INCLUDES += $(LOCAL_PATH)/matlab
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Ne10/inc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Ne10/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/Ne10/modules/dsp

# module includes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/inc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/coeffs

# module sources
libbacch_srcs_cpp 	+= src/bacch_audio_bacchaudio.cpp
#libbacch_srcs_cpp 	+= src/BACCH_SP_C_wrapper.cpp
#libbacch_srcs_cpp 	+= src/BACCH_Audio_SP.cpp
libbacch_srcs_cpp 	+= src/BACCH_Audio_C_wrapper.cpp
libbacch_srcs_cpp 	+= src/BACCH_Audio.cpp
libbacch_srcs_cpp 	+= src/BACCH_buffer.cpp
libbacch_srcs_cpp 	+= src/BACCH_buffer_ops.cpp
libbacch_srcs_cpp 	+= src/BACCH_complex_buffer.cpp
libbacch_srcs_cpp 	+= src/BACCH_compressor.cpp
libbacch_srcs_cpp 	+= src/BACCH_cx.cpp
libbacch_srcs_cpp 	+= src/BACCH_equalizer.cpp
libbacch_srcs_cpp 	+= src/BACCH_hp_xtc.cpp
libbacch_srcs_cpp 	+= src/BACCH_hp.cpp
libbacch_srcs_cpp 	+= src/BACCH_ibuffer.cpp
libbacch_srcs_cpp 	+= src/BACCH_LCRex.cpp
libbacch_srcs_cpp 	+= src/BACCH_math.cpp
libbacch_srcs_cpp 	+= src/BACCH_mem.cpp
libbacch_srcs_cpp 	+= src/BACCH_slide_manager.cpp
libbacch_srcs_cpp 	+= src/BACCH_sp.cpp
libbacch_srcs_cpp 	+= src/BACCH_supporting.cpp
libbacch_srcs_cpp   += src/BACCH_virtualizer.cpp

LOCAL_SRC_FILES := $(libbacch_srcs_cpp)

include $(BUILD_SHARED_LIBRARY)

this_path := $(LOCAL_PATH)

include $(this_path)/Ne10/Android.mk
include $(this_path)/matlab/Android.mk
